

# eo-dataflow-manager

Checks online services for changed in data files, and downloads them when needed.

# Installation

## Conda

```bash
# Create conda env
conda env create -f assets/conda/run-env.yaml

# Activate created env
conda activate eo-dataflow-manager

# Install poetry
pip install poetry

# install / upgrade eo-dataflow-manager
poetry install --all-extras
```

# Development

## Tooling

```bash
# Install developer tools
pip install -r requirements-dev.txt
```

### Linter

Using command-line:
```bash
# Run linter against current codebase
pylint -j8 --errors-only --output-format=colorized lib/
# The --errors-only flag only shows the errors

# Detect more issues:
pylint -j8 lib/
```

There are also plugins for many editors and IDEs.

### Code formatting

The code has been automatically formatted to follow PEP8 standard.
```bash
# Format every python source file in lib/ folder
autopep8 --in-place --recursive -j8 lib/
```

There are also plugins for many editors and IDEs.

### Unit testing

```bash
pytest lib/
```

### Docker

```bash
docker run -v /data:/data --network=host -it eo-dataflow_manager bash
```
