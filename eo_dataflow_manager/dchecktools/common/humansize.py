"""
    docstring
"""
__revision__ = '0.2'


def getHumanSize(size):
    """
Returns human readable size string from byte-size (returned by os.getsize() for example)
    >>> from eo_dataflow_manager.dchecktools.common.humansize import getHumanSize
    >>> getHumanSize(500)
    '500.00'
    >>> getHumanSize(1024)
    '1.00Ko'
    >>> getHumanSize(1024*1024)
    '1.00Mo'
    >>> getHumanSize(1024*1024*1024)
    '1.00Go'
    >>> getHumanSize(49821)
    '48.65Ko'
    >>> getHumanSize(5409759)
    '5.16Mo'

    """
    abbrevs = [
        (1 << 50, 'Po'),
        (1 << 40, 'To'),
        (1 << 30, 'Go'),
        (1 << 20, 'Mo'),
        (1 << 10, 'Ko'),
        (1, '')
    ]
    for factor, suffix in abbrevs:
        if size >= factor:
            break
    return '%.2f' % (float(size) / factor) + suffix
