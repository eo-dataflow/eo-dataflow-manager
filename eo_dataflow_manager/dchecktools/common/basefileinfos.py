

import logging
from datetime import datetime

from sqlalchemy import Boolean, Column, DateTime, Integer, String, and_, column, desc, func, or_
from sqlalchemy.ext.declarative import declarative_base

from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError

# from sqlalchemy.orm import *
# from sqlalchemy.exc import ProgrammingError, IntegrityError, DBAPIError
# from sqlalchemy.interfaces import PoolListener

# import os
# import sys
# from threading import get_ident

log = logging.getLogger('SqliteBaseFilesInfos')
log.setLevel(logging.INFO)


# Highly recommended to activate thel flush buffer
# otherwise, memory consumption could be huge !
FLUSH_FILE_BUFFER_ACTIVATED = True
FLUSH_FILE_BUFFER = 1000


FILE_URL_SEPARATOR = '  -|-  '

Base = declarative_base()


class Constant(Base):
    # SQLite table: constants_infos
    __tablename__ = 'constants_infos'

    id = Column(Integer, primary_key=True)
    id_execution = Column(Integer)
    title = Column(String)
    data = Column(String)

    def __repr__(self):
        return 'Constant(id=%s, id_execution=%s, title=%s, data=%s)' % (
            self.id,
            self.id_execution,
            self.title,
            self.data,
        )


class ExecInfo(Base):
    # SQLite table: exec_infos
    __tablename__ = 'exec_infos'

    id = Column(Integer, primary_key=True)
    id_execution = Column(Integer)
    date = Column(DateTime)
    title = Column(String)
    data = Column(String)

    def __repr__(self):
        return 'ExecInfo(id=%s, id_execution=%s, date=%s, title=%s, data=%s)' % (
            self.id,
            self.id_execution,
            self.date,
            self.title,
            self.data,
        )


class Execution(Base):
    # SQLite table: executions
    __tablename__ = 'executions'

    id = Column(Integer, primary_key=True)
    date_start = Column(DateTime)
    date_stop = Column(DateTime)
    valid_execution = Column(Boolean)

    def __repr__(self):
        return 'Execution(id=%s, date_start=%s, date_stop=%s, valid_execution=%s)' % (
            self.id,
            self.date_start,
            self.date_stop,
            self.valid_execution,
        )


class File(Base):
    # SQLite table: files
    __tablename__ = 'files'

    id_execution = Column(Integer, primary_key=True)
    filename = Column(String, primary_key=True)
    isDirectory = Column(Boolean)
    isSymLink = Column(Boolean)
    size = Column(Integer)
    mtime = Column(DateTime)
    sensingtime = Column(DateTime)

    @staticmethod
    def nameToColumn(columnName):
        # TODO: find a better way
        return {
            'id_execution': File.id_execution,
            'filename': File.filename,
            'isDirectory': File.isDirectory,
            'isSymLink': File.isSymLink,
            'size': File.size,
            'mtime': File.mtime,
            'sensingtime': File.sensingtime,
        }[columnName]

    def toDict(self):
        return vars(self)

    def __repr__(self):
        return 'File(id_execution=%s, ' \
               'filename=%s, ' \
               'isDirectory=%s, ' \
               'isSymLink=%s, ' \
               'size=%s, ' \
               'mtime=%s, ' \
               'sensingtime=%s)' % (
            self.id_execution,
            self.filename,
            self.isDirectory,
            self.isSymLink,
            self.size,
            self.mtime,
            self.sensingtime,
        )


class ConfigEntry(Base):
    __tablename__ = 'config'

    key = Column(String, primary_key=True)
    value = Column(String)

    def toDict(self):
        return vars(self)


class SqliteBaseFilesInfos(object):

    def __init__(self):
        self.infosKeyList = []
        self.__sql_engine = None
        self.__db_url = None
        self.__sql_session = None

        self.config = None
        self.database_path = None
        self.check_infos = None

        self.current_execution = None
        self.__id_execution = None

        # db tables
        self.tableList = None

        self.added_files_nbr = 0

        self.connection = None

    def setConfig(self, config):
        self.config = config

        if config.check_infos is not None:
            self.check_infos = config.check_infos
        if config.database_path is not None:  # TODO: rename database_path to db_url
            self.__db_url = config.database_path
        else:
            raise DC_ConfigError('Configuration error: missing database path')

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

    def createEngine(self):
        assert self.__sql_engine is None

        # Connect to SQL database
        from sqlalchemy import create_engine
        print('Open SQL db: %s', 'sqlite:///' + self.__db_url)
        self.__sql_engine = create_engine('sqlite:///' + self.__db_url, echo=False)

        # Create all tables
        Base.metadata.create_all(self.__sql_engine)

        # Create session for transactions
        from sqlalchemy.orm import sessionmaker
        self.__sql_session = sessionmaker(bind=self.__sql_engine)()
        self.__sql_session.execute('PRAGMA foreign_keys=ON')

        vacuum = self.__sql_session.execute('select * from pragma_auto_vacuum').fetchall()
        if vacuum[0][0] == 0:
            self.__sql_session.execute('PRAGMA auto_vacuum=1')
            self.__sql_session.execute('vacuum')

        version = self.__sql_session \
            .query(ConfigEntry) \
            .filter(ConfigEntry.key == 'version') \
            .one_or_none()
        if version is None:
            version = ConfigEntry(key='version', value=1)
            self.__sql_session.add(version)
            self.__sql_session.commit()

        print('SQL DB Version: ', version.value)

    def initExecution(self):
        assert self.config is not None
        assert self.config._start_time is not None

        e = Execution(
            date_start=self.config._start_time,
            date_stop=None,
            valid_execution=None,
        )

        # Add the object to SQL
        self.__sql_session.add(e)
        # Commit transaction (will update e)
        self.__sql_session.commit()

        self.current_execution = e
        self.__id_execution = e.id

    def setCurrentExecutionStopDate(self):
        self.current_execution.date_stop = datetime.now()
        self.__sql_session.commit()

    def setValidExecution(self, status):
        self.current_execution.valid_execution = status
        self.__sql_session.commit()

    def _getFile(self, filename):
        return self.__sql_session \
            .query(File) \
            .filter(File.filename == filename) \
            .one()

    def addFile(self, file):
        """ Add a single file. If adding many files, prefer using addFileList """
        assert file.id_execution is None
        assert file.mtime is None or isinstance(file.mtime, datetime), 'is a %s, instead of a datetime.datetime' % type(file.mtime)
        assert file.sensingtime is None or isinstance(file.sensingtime, datetime), 'is a %s, instead of a datetime.datetime' % type(file.sensingtime)

        f = File(
            id_execution=self.__id_execution,
            filename=file.filename,
            isDirectory=file.isDirectory,
            isSymLink=file.isSymLink,
            size=file.size,
            mtime=file.mtime,
            sensingtime=file.sensingtime,
        )
        self.__sql_session.add(f)
        self.__sql_session.commit()
        self.added_files_nbr += 1

    def addFileList(self, fileList):
        assert isinstance(fileList, list)

        for f in fileList:
            if f.id_execution is None:
                f.id_execution = self.__id_execution

        self.__sql_session.add_all(fileList)
        self.__sql_session.commit()
        self.added_files_nbr += len(fileList)
        print('PERF: inserted %s files' % len(fileList))

    def addConstant(self, title, data):
        c = Constant(
            id_execution=self.__id_execution,
            title=title,
            data=data,
        )
        self.__sql_session.add(c)
        self.__sql_session.commit()

    def addExecInfo(self, title, data):
        assert self.__id_execution is not None

        ei = ExecInfo(
            id_execution=self.__id_execution,
            date=datetime.now(),
            title=title,
            data=data,
        )
        self.__sql_session.add(ei)
        self.__sql_session.commit()

    def req_IsLastExecutionId(self, executionId):
        return self.__sql_session \
            .query(Execution) \
            .filter(Execution.id == executionId) \
            .one() == self.__id_execution

    def req_PurgeExecution(self, executionId, keep_last=True):
        if keep_last is True and self.req_IsLastExecutionId(executionId):
            executionId -= 1

        self.__sql_session \
            .query(Execution) \
            .filter(Execution.id == executionId) \
            .delete()
        self.__sql_session \
            .query(File.id_execution) \
            .filter(File.id_execution == executionId) \
            .delete()
        self.__sql_session.commit()

    def req_PurgeOlderThan(self, executionId, keep_last=True):
        if keep_last:
            execs = self.req_PreviousExecutionOKId(executionId)
            if len(execs) > 0:
                executionId =  execs[0].id
            else:
                executionId -= 1

        self.__sql_session \
            .query(Execution) \
            .filter(Execution.id < executionId) \
            .delete()
        self.__sql_session \
            .query(File.id_execution) \
            .filter(File.id_execution < executionId) \
            .delete()

        self.__sql_session.commit()

    def req_PreviousExecutionOKId(self, refid, limit=1):
        return self.__sql_session \
            .query(Execution) \
            .filter(and_(Execution.valid_execution == 1, Execution.id < refid)) \
            .order_by(desc(Execution.id)) \
            .limit(limit) \
            .all()

    def req_PurgeOlderThanStartDate(self, startDate, keep_last=True):
        assert isinstance(startDate, datetime)

        execs = self.req_NextExecutionsIds(startDate)
        if len(execs) > 0:
            self.req_PurgeOlderThan(execs[0].id, keep_last)

    def req_LastTerminatedExecutions(self, limit=10):
        return self.__sql_session \
            .query(Execution) \
            .filter(and_(Execution.valid_execution == 1, Execution.date_stop is not None)) \
            .order_by(desc(Execution.date_start)) \
            .limit(limit) \
            .all()

    def req_NextExecutionsIds(self, fromdate, limit=10):
        assert isinstance(fromdate, datetime)
        return self.__sql_session \
            .query(Execution) \
            .filter(Execution.date_start >= fromdate) \
            .order_by(Execution.date_start) \
            .limit(limit) \
            .all()

    def req_NextTerminatedExecutionsIds(self, fromdate, limit=10):
        assert isinstance(fromdate, datetime)
        return self.__sql_session \
            .query(Execution) \
            .filter(and_(Execution.valid_execution == 1, Execution.date_stop >= fromdate)) \
            .order_by(Execution.date_start) \
            .limit(limit) \
            .all()

    def req_PreviousExecutionsIds(self, date, limit=10):
        assert isinstance(date, datetime)
        return self.__sql_session \
            .query(Execution) \
            .filter(and_(Execution.valid_execution == 1, Execution.date_start <= date)) \
            .order_by(desc(Execution.date_start)) \
            .limit(limit) \
            .all()

    def req_PreviousTerminatedExecutionsIds(self, date, only_valid_exec=True, limit=10):
        assert isinstance(date, datetime)
        return self.__sql_session \
            .query(Execution) \
            .filter(and_(not only_valid_exec or Execution.valid_execution == 1, Execution.date_stop is not None)) \
            .order_by(desc(Execution.date_start)) \
            .limit(limit) \
            .all()

    def req_IsConstantIdentical(self, exec_id_from, exec_id_to):
        return self.__sql_session \
            .query(func.count()) \
            .filter(and_(
                or_(Constant.id_execution == exec_id_from, Constant.id_execution == exec_id_to),
                Constant.title == 'config'
            )) \
            .group_by(Constant.title, Constant.data) \
            .one() == 2

    def req_ListFiles(self, executionId=None):
        if executionId is None:
            executionId = self.req_LastTerminatedExecutions()[0].id

        return self.__sql_session \
            .query(File.filename) \
            .filter(File.id_execution == executionId) \
            .all()

    def req_ListFilesBetweenDates(self, start, stop, execution_id=None):
        if execution_id is None:
            execution = self.req_LastTerminatedExecutions()
            if execution:
                execution_id = execution[0].id
            else:
                # first execution
                return []

        return self.__sql_session \
            .query(File) \
            .filter(File.id_execution == execution_id, File.sensingtime.between(start, stop)) \
            .all()


    def req_ListFilesInfo(self, executionId=None):
        if executionId is None:
            executionId = self.req_LastTerminatedExecutions()[0].id

        return self.__sql_session \
            .query(File) \
            .filter(File.id_execution == executionId) \
            .all()

    def req_ListFilesBetweenExecutions(self, exec_id_from, exec_id_to, distinct=True):

        return self.__sql_session \
            .query(distinct(File.filename)) \
            .filter(and_(File.id_execution >= exec_id_from, File.id_execution <= exec_id_to)) \
            .all()

    def req_UnModifiedFiles(
            self,
            executionId,
            previousExecId,
            checkInfosList=[],
            ignoreFiles=False,
            ignoreDirectories=False,
            ignoreSymLinks=False):
        return self.req_ModifiedFiles(
            executionId,
            previousExecId,
            checkInfosList,
            ignoreFiles,
            ignoreDirectories,
            ignoreSymLinks,
            checkUnmodified=True)

    def req_ModifiedFiles(
            self,
            executionId,
            previousExecId,
            checkInfosList=[],
            ignoreFiles=False,
            ignoreDirectories=False,
            ignoreSymLinks=False,
            checkUnmodified=False):

        fileFilter = SqliteBaseFilesInfos.__buildFileFilter(ignoreFiles, ignoreDirectories, ignoreSymLinks)
        if checkUnmodified is True:
            return self.__sql_session \
                .query(File.filename, *[func.max(File.nameToColumn(c)).label(c) for c in checkInfosList]) \
                .filter(and_(
                    or_(File.id_execution == previousExecId, File.id_execution == executionId),
                    fileFilter
                )) \
                .group_by(File.filename, *[File.nameToColumn(c) for c in checkInfosList]) \
                .having(func.count() == 2) \
                .all()
        else:
            query1 = self.__sql_session \
                .query(File.filename, *[func.max(File.nameToColumn(c)).label('inner_' + c) for c in checkInfosList]) \
                .filter(and_(
                    or_(File.id_execution == previousExecId, File.id_execution == executionId),
                    fileFilter,
                )) \
                .group_by(File.filename, *[File.nameToColumn(c) for c in checkInfosList]) \
                .having(func.count() == 1) \
                .subquery()

            return self.__sql_session \
                .query(query1.c.filename, *[func.max(column('inner_' + c)).label(c) for c in checkInfosList]) \
                .group_by('filename') \
                .having(func.count() == 2) \
                .all()

    def req_CreatedFiles(
            self,
            executionId,
            previousExecId,
            checkInfosList=[],
            ignoreFiles=False,
            ignoreDirectories=False,
            ignoreSymLinks=False):

        fileFilter = SqliteBaseFilesInfos.__buildFileFilter(ignoreFiles, ignoreDirectories, ignoreSymLinks)
        return self.__sql_session \
            .query(File.filename, *[func.max(File.nameToColumn(c)).label(c) for c in checkInfosList]) \
            .filter(and_(
                fileFilter,
                or_(File.id_execution == previousExecId, File.id_execution == executionId)
            )) \
            .group_by(File.filename) \
            .having(and_(func.count() == 1, func.min(File.id_execution) == executionId)) \
            .all()

    def req_RemovedFiles(
            self,
            executionId,
            previousExecId,
            checkInfosList=[],
            ignoreFiles=False,
            ignoreDirectories=False,
            ignoreSymLinks=False):

        fileFilter = SqliteBaseFilesInfos.__buildFileFilter(ignoreFiles, ignoreDirectories, ignoreSymLinks)
        return self.__sql_session \
            .query(File.filename, *[func.max(File.nameToColumn(c)).label(c) for c in checkInfosList]) \
            .filter(and_(
                fileFilter,
                or_(File.id_execution == previousExecId, File.id_execution == executionId)
            )) \
            .group_by(File.filename) \
            .having(and_(func.count() == 1, func.max(File.id_execution) == previousExecId)) \
            .all()

    def req_GetErrors(self, executionId):
        return self.__sql_session \
            .query(ExecInfo.date, ExecInfo.title, ExecInfo.data) \
            .filter(ExecInfo.id_execution == executionId) \
            .all()

    @staticmethod
    def __buildFileFilter(
            ignoreFiles,
            ignoreDirectories,
            ignoreSymLinks):

        ret = True
        if ignoreFiles is True:
            ret = and_(ret, File.isDirectory == 1)
        if ignoreDirectories is True:
            ret = and_(ret, File.isDirectory == 0)
        if ignoreSymLinks is True:
            ret = and_(ret, File.isSymLink == 0)
        return ret
