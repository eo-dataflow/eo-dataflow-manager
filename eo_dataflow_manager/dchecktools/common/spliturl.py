#!/usr/bin/env python
"""
    docsttring
"""
__revision__ = '0.2'

import logging
import sys
import urllib.error
import urllib.request
from urllib.parse import urlsplit

from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError

logging.basicConfig()
log = logging.getLogger('spliturl')
log.setLevel(logging.DEBUG)


def spliturl(url, defaultpath='/'):
    """
    Returns url splitted, to get scheme, domain, path, port, username, password
    >>> from eo_dataflow_manager.dchecktools.common.spliturl import spliturl
    >>> spliturl('http://www.monsite.com')
    ('http', 'www.monsite.com', '/', None, None, None)
    >>> spliturl('http://username:password@www.monsite.com:80/folder1/subfolder/file.html')
    ('http', 'www.monsite.com', '/folder1/subfolder/file.html', 80, 'username', 'password')
    >>> spliturl('ftp://www.monsite.com:80/folder/file.txt')
    ('ftp', 'www.monsite.com', '/folder/file.txt', 80, None, None)
    >>> spliturl('ftp://anonymous:password@www.monsite.com:80/folder/file.txt')
    ('ftp', 'www.monsite.com', '/folder/file.txt', 80, 'anonymous', 'password')
    >>> spliturl('file:///home/public/folder/file.txt')
    ('file', '', '/home/public/folder/file.txt', None, None, None)
    >>> spliturl('/tmp/folder1/folder2/file')
    ('', '', '/tmp/folder1/folder2/file', None, None, None)
    >>> spliturl('http://foo:pass@www.protected.com:8080/rep1')
    ('http', 'www.protected.com', '/rep1', 8080, 'foo', 'pass')
    # 30/03/2018 PMT#34 ajout
    >>> spliturl('https_opensearch://foo:pass@www.protected.com:8080/rep1')
    ('https', 'www.protected.com', '/rep1', 8080, 'foo', 'pass')
    >>> spliturl('ftps_xx://foo:pass@jsimpsonftps.pps.eosdis.nasa.gov/rep1')
    ('ftps_xx', jsimpsonftps.pps.eosdis.nasa.gov', '/rep1', null, 'foo', 'pass')

    """
    scheme = None
    domain = None
    username = None
    password = None
    port = None
    temp_scheme = None
    temp_url = None

    # 30/03/2018 PMT#34 ajout du protocole opensearch en https
    if url[:6] == 'https_':
        temp_scheme, temp_url = url.split(':', 1)
        url = 'https:' + temp_url

    scheme, username_password_domain, path, query, \
        fragment = urlsplit(url)

    # 30/03/2018 PMT#34 ajout du protocole opensearch en https
    if temp_scheme is not None:
        scheme = temp_scheme
    log.debug(
        'spliturl %s: scheme:%s username_password_domain:%s path:%s query:%s fragment:%s' %
        (url, scheme, username_password_domain, path, query, fragment))

    username_password, domain = urllib.parse.splituser(
        username_password_domain)
    if username_password:
        username, password = urllib.parse.splitpasswd(username_password)
    domain, port = urllib.parse.splitport(domain)
    if port is not None:
        port = int(port)

    # if at this point, path == '', defaulting to /
    if path == '':
        path = '/'


    if scheme != 'https':
        # on enleve le dernier '/'
        while len(
                path) > 1 and path[-1] == '/':  # > 1 because we don't want to remove / directory
            path = path[:-1]

    return (scheme, domain, path, port, username, password)


def getProtocolName(scheme, path, domain, exitonfail=True):
    if scheme == 'ftp':
        return 'ftp'
    elif scheme == 'sftp':
        return 'sftp'
    elif scheme == 'http':
        return 'http'
    elif scheme == 'https':
        if (domain == 'oceandata.sci.gsfc.nasa.gov'):
            #return "https_filetable"
            return 'https_directorylist'
        else:
            return 'https_directorylist'
    elif scheme == 'file' \
            or (scheme == '' and path.startswith('/')) \
            or (scheme == '' and path.startswith('./')):
        return 'localpath'
    elif scheme == 'lslr':
        return 'lslr_file'
    elif scheme in ['https_opensearch', 'https_listing_file', 'webdav',
                    'https_po_daac', 'https_eop_os', 'https_xx_daac']:
        return scheme
    elif scheme[:4] == 'ftps':
        return scheme
    else:
        if exitonfail:
            error = DC_ConfigError(
                'unknown protocol %s for url : %s' %
                (scheme, path))
            log.error(error)
            sys.exit(1)
        return None


def setConfigFromUrl(config, url):
    scheme, domain, path, port, username, password = spliturl(url)
    config.path = path
    config.protocol = getProtocolName(scheme, config.path, domain)
    config.server_address = domain
    config.server_port = port

    # Check for username and password, to take the config file ones if needed
    # in case we don't want to have it on the command line
    if username is not None and username != '':
        config.auth_username = username
    if password is not None and password != '':
        config.auth_password = password
