
class DC_Error(Exception):
    def __init__(self, args, db=None):
        if isinstance(args, list) or isinstance(args, type):
            self.args = args
        else:
            self.args = [args]

        if db:
            classname = self.__class__.__name__
            db.addExecInfo(classname, str(args))


class DC_ConfigError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_ConfigError : ' + str(self.args)


class DC_ConnectionError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_ConnectionError : ' + str(self.args)


class DC_IOError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_IOError : ' + str(self.args)


class DC_EOFError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_EOFError : ' + str(self.args)


class DC_DbError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_DbError : ' + str(self.args)


class DC_FtpError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_FtpError : ' + str(self.args)


class DC_UrlError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_UrlError : ' + str(self.args)


class DC_HttpError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_HttpError: ' + str(self.args)


class DC_LocalpathError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_LocalpathError : ' + str(self.args)


class DC_ExtractingError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_ExtractingError : ' + str(self.args)


class DC_TimeoutError(DC_Error):
    def __init__(self, args, db=None):
        DC_Error.__init__(self, args, db)

    def __str__(self):
        return 'DC_TimeoutError : ' + str(self.args)
