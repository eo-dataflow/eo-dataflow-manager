import os

DEFAULT_CONFIG_WORKING_DIR = '/tmp/dchecktools'
DEFAULT_CONFIG_WORKING_DIR = None
if os.getenv('HOME'):
    DEFAULT_CONFIG_WORKING_DIR = os.path.join(
        os.getenv('HOME'), '.dchecktools.d')
if os.getenv('DCT_WORKSPACE'):
    DEFAULT_CONFIG_WORKING_DIR = os.environ['DCT_WORKSPACE']


class WorkingdirError(Exception):
    def __init__(self, args, db=None):
        self.args = args


def getDatabasePathFromUrl(
        url,
        prefix='',
        working_dir=None,
        relativeurl='var/lib/auto',
        filename_ext='.db',
        create_path=False):
    if url is None or url.isspace() is True or url == '':
        raise WorkingdirError('url is not valid : \'%s\'' % (url))
    if not working_dir:
        working_dir = DEFAULT_CONFIG_WORKING_DIR
    while len(url) > 1 and url[-1] == '/':
        url = url[:-1]
    db_path = os.path.join(working_dir, relativeurl)
    database_path = os.path.join(
        db_path, prefix + url.replace('/', '_')) + filename_ext
    if create_path and not os.path.exists(db_path):
        os.makedirs(db_path)
    return database_path
