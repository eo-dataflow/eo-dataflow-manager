#!/usr/bin/env python

"""Start DCheck (and end) here - read arguments, set global settings, etc."""
import logging
import sys
import traceback
from datetime import datetime, timedelta
from optparse import OptionParser
from xml.etree.ElementTree import ParseError
from xml.parsers.expat import ExpatError

from eo_dataflow_manager.dchecktools.common import workingdir
from eo_dataflow_manager.dchecktools.common.basefileinfos import SqliteBaseFilesInfos
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_DbError, DC_Error
from eo_dataflow_manager.dchecktools.common.spliturl import setConfigFromUrl
from eo_dataflow_manager.dchecktools.filters.StringFilters import FileFilter
from eo_dataflow_manager.dchecktools.protocols import (
    api_eodms,
    file_extractor,
    ftp,
    ftps,
    https_directorylist,
    https_eop_os,
    https_listing_file,
    https_opensearch,
    https_s3_cmems,
    https_xx_daac,
    localpath,
    lslr_file,
    sftp,
    webdav,
)
from eo_dataflow_manager.scheduler.com.ext.XMLReader import XMLReader
from eo_dataflow_manager.scheduler.sc.ConfigurationFileUtil import ConfigurationFileUtil
from eo_dataflow_manager.scheduler.sc.Download import DownloaderException
from eo_dataflow_manager.scheduler.sc.DownloaderDatabase import LOGGER_NAME

logging.basicConfig()
log = logging.getLogger('dcheck')
log.setLevel(logging.INFO)

VERSION = '7.0.0'

DEFAULT_FTP_LISTING_TYPE = 'unix'
DEFAULT_PURGE_OLDER_THAN = 0


class Config:

    def __init__(self):
        self._start_time = None
        self.configfile = None

        # default workspace
        self.default_workspace = workingdir.DEFAULT_CONFIG_WORKING_DIR

        # runtime parameters
        self.test_mode = False
        self.debug = False

        # database configuration
        self.database_path = None
        self.check_infos = None
        self.purge_older_than = DEFAULT_PURGE_OLDER_THAN
        self.purge_remove_last = False

        # Protocol parameters
        self.protocol = None
        self.protocol_option = None
        self.protocol_timeout = None
        self.path = None
        self.server_address = None
        self.server_port = None
        self.auth_username = None
        self.auth_password = None
        self.ftp_listing_type = None
        self.follow_symlink = False

        # filters
        self.forceFiles = []
        self.forceFilesRegexp = []
        self.ignoreFiles = []
        self.ignoreFilesRegexp = []
        self.ignoreFilesOlderThan = None
        self.default_file_accept = True

        self.forceDirectories = []
        self.forceDirectoriesRegexp = []
        self.ignoreDirectories = []
        self.ignoreDirectoriesRegexp = []
        self.ignoreDirectoriesOlderThan = None
        self.ignoreDirectoriesChangedSince = None
        self.default_directory_accept = True

        self.logLevel = logging.INFO

        self.smart_crawler_delta = None
        self.smart_crawler_directories_pattern = None
        self.smart_crawler_mindate = None
        self.smart_crawler_maxdate = None

        self.data_reader = None
        self.regexp_date = None
        self.date_format = None
        self.storage_path = None
        self.dir_prefix = None
        self.date_index = None

        self.resultFormat = None
        self.selectGeo = None
        self.selectProduct = None

    def get_node_string(self, nodename):
        string = ''
        is_node_type_valid = (isinstance(self.__dict__[nodename], list))
        if is_node_type_valid and len(self.__dict__[nodename]) == 0:
            return string
        elif self.__dict__[nodename] is not None:
            return '<%s>%s</%s>' % (nodename,
                                    self.__dict__[nodename], nodename)

        return string

    def const_config_string(self):
        # Attention, la constConfig ne doit pas varier dans le temps,
        # sauf si l'utilisateur change la config
        # les informations variables, comme _start_time sont a proscrire
        cfg_str = '<config>'
        cfg_str += self.get_node_string('configfile')
        cfg_str += self.get_node_string('test_mode')
        cfg_str += self.get_node_string('default_workspace')
        cfg_str += '<database>'
        cfg_str += self.get_node_string('database_path')
        cfg_str += self.get_node_string('check_infos')
        cfg_str += self.get_node_string('purge_older_than')
        cfg_str += self.get_node_string('purge_remove_last')
        cfg_str += '</database>'
        cfg_str += '<protocol>'
        cfg_str += self.get_node_string('protocol')
        cfg_str += self.get_node_string('protocol_option')
        cfg_str += self.get_node_string('protocol_timeout')
        cfg_str += self.get_node_string('path')
        cfg_str += self.get_node_string('server_address')
        cfg_str += self.get_node_string('server_port')
        cfg_str += self.get_node_string('auth_username')
        cfg_str += self.get_node_string('auth_password')
        cfg_str += self.get_node_string('follow_symlink')
        cfg_str += self.get_node_string('ftp_listing_type')
        cfg_str += '</protocol>'
        cfg_str += '<filters>'
        cfg_str += self.get_node_string('forceFiles')
        cfg_str += self.get_node_string('forceFilesRegexp')
        cfg_str += self.get_node_string('ignoreFiles')
        cfg_str += self.get_node_string('ignoreFilesRegexp')
        cfg_str += self.get_node_string('default_file_accept')
        cfg_str += self.get_node_string('forceDirectories')
        cfg_str += self.get_node_string('forceDirectoriesRegexp')
        cfg_str += self.get_node_string('ignoreDirectories')
        cfg_str += self.get_node_string('ignoreDirectoriesRegexp')
        cfg_str += self.get_node_string('default_directory_accept')
        cfg_str += self.get_node_string('ignoreFilesOlderThan')
        cfg_str += self.get_node_string('ignoreDirectoriesOlderThan')
        cfg_str += self.get_node_string('ignoreDirectoriesChangedSince')
        cfg_str += '</filters>'

        cfg_str += '<smart-crawler>'
        cfg_str += self.get_node_string('smart_crawler_delta')
        cfg_str += self.get_node_string('smart_crawler_directories_pattern')
        cfg_str += self.get_node_string('smart_crawler_maxdate')
        cfg_str += self.get_node_string('smart_crawler_mindate')
        cfg_str += '</smart-crawler>'

        cfg_str += '<date-reader>'
        cfg_str += self.get_node_string('data_reader')
        cfg_str += self.get_node_string('regexp_date')
        cfg_str += self.get_node_string('date_format')
        cfg_str += self.get_node_string('storage_path')
        cfg_str += self.get_node_string('dir_prefix')
        cfg_str += self.get_node_string('date_index')
        cfg_str += '</date-reader>'

        cfg_str += '<open-search>'  # 2018/04/04 PMT ID#34
        cfg_str += self.get_node_string('resultFormat')
        cfg_str += self.get_node_string('selectGeo')
        cfg_str += self.get_node_string('selectProduct')
        cfg_str += '</open-search>'

        cfg_str += '</config>'
        return cfg_str

    def read_configuration(self, xml_file, is_smartcrawler):

        xr = XMLReader(LOGGER_NAME)

        log.debug('  --> read of the download configuration : %s', xml_file)

        try:
            xmltree = xr.open(xml_file)
        except ExpatError:
            raise IOError('invalid download_config file %s ' % xml_file)
        except IOError as e:
            log.warning("Download_config file doesn't exist %s ", xml_file)
            raise DownloaderException(e)
        except ParseError:
            log.warning('Download_config file parse error %s ', xml_file)
            raise IOError('invalid download_config file %s ' % xml_file)
        except Exception as e:
            log.warning('Read download_config file error %s (%s)', xml_file, e)
            raise IOError('invalid download_config file %s ' % xml_file)

        configuration = ConfigurationFileUtil(LOGGER_NAME, xr, xmltree)
        configuration.Read()

        self.default_file_accept = configuration.source.files_regexp is None
        self.purge_older_than = configuration.settings.database_purgeScansOlderThan
        self.purge_remove_last = not configuration.settings.database_keepLastScan

        # Protocol parameters
        protocol = configuration.source.protocol
        if protocol == 'Onlynotify':
            protocol = 'Localpath'
        self.protocol = protocol.lower()

        self.protocol_option = dict(configuration.source.protocol_option)
        self.protocol_timeout = configuration.settings.protocolTimeout
        self.path = configuration.source.rootPath
        self.server_address = configuration.source.server
        self.server_port = configuration.source.port
        self.auth_username = configuration.source.login
        self.auth_password = configuration.source.password

        now_datetime = datetime.now()
        today_datetime = datetime.strptime(now_datetime.strftime('%Y-%m-%d'), '%Y-%m-%d')
        # File filters
        self.ignoreFilesRegexp = [configuration.source.files_ignoreRegexp]
        self.forceFilesRegexp = [configuration.source.files_regexp]
        if configuration.source.files_ignoreOlderThan is not None:
            start_datetime = today_datetime - \
                             timedelta(days=configuration.source.files_ignoreOlderThan)
            self.ignoreFilesOlderThan = start_datetime

        # Directory filters
        self.default_directory_accept = configuration.source.directories_regexp is None
        self.forceDirectoriesRegexp = [configuration.source.directories_regexp]

        if configuration.source.directories_ignoreRegexp is not None:
            self.ignoreDirectoriesRegexp = [configuration.source.directories_ignoreRegexp]
        if configuration.source.directories_ignoreOlderThan is not None:
            start_datetime = today_datetime - \
                             timedelta(days=configuration.source.directories_ignoreOlderThan)
            self.ignoreDirectoriesOlderThan = start_datetime
        if configuration.source.directories_ignoreNewerThan is not None:
            stop_datetime = today_datetime - \
                            timedelta(minutes=configuration.source.directories_ignoreNewerThan)
            self.ignoreDirectoriesChangedSince = stop_datetime

        # smart crawl
        if configuration.source.date_pattern:
            self.smart_crawler_delta = configuration.source.date_backlogInDays
            self.smart_crawler_directories_pattern = configuration.source.date_pattern

            date_format = '%Y%m%d'
            is_smart_date = False

            if configuration.source.date_maxDate is not None:
                try:
                    self.smart_crawler_maxdate = datetime.strptime(
                        configuration.source.date_maxDate, date_format)
                    is_smart_date = True
                except BaseException:
                    log.error('SMART_CRAWLER_MAXDATE: Date format must be yyyymmdd')

            if configuration.source.date_minDate is not None:
                try:
                    self.smart_crawler_mindate = datetime.strptime(
                        configuration.source.date_minDate, date_format)
                    is_smart_date = is_smart_date and True
                except BaseException:
                    log.error('SMART_CRAWLER_MINDATE: Date format must be yyyymmdd')
                    is_smart_date = False
        else:
            is_smart_date = False

        if is_smart_date and (self.smart_crawler_maxdate < self.smart_crawler_mindate):
            log.error(
                'SMART_CRAWLER_MAXDATE = %s <  SMART_CRAWLER_MINDATE =%s' %
                (self.smart_crawler_maxdate, self.smart_crawler_mindate))

        # sensing time

        self.data_reader = configuration.source.plugin
        self.regexp_date = configuration.source.dateRegexp
        self.date_format = configuration.source.dateFormat

        self.storage_path = configuration.destination.subpath

        self.resultFormat = configuration.source.opensearch_requestFormat
        self.selectGeo = configuration.source.opensearch_area
        self.selectProduct = configuration.source.opensearch_dataset

        self.geotemporal_list = configuration.source.geotemporal_list

        self.database_path = configuration.settings.database_path

        """
        # default workspace
        self.default_workspace = configuration.DEFAULT_WORKSPACE
        self.check_infos = configuration.CHECK_INFOS
        self.dir_prefix = configuration.DIR_PREFIX
        self.date_index = configuration.DATE_INDEX
        self.forceDirectories = configuration.FORCE_DIRECTORIES
        self.ignoreDirectories = configuration.IGNORE_DIRECTORIES
        self.forceFiles = module.FORCE_FILES
        self.ignoreFiles = module.IGNORE_FILES
        self.follow_symlink = configuration.FOLLOW_SYMLINK
        self.ftp_listing_type = configuration.FTP_LISTING_TYPE
        """


class DCheck(object):

    def getProtocolObject(self, protocolname, path, option=None, timeout=None):
        obj = None
        if protocolname in ('localpath', 'localmove', 'localpointer') \
                or protocolname == 'file' \
                or (protocolname == '' and path.startswith('/')) \
                or (protocolname == '' and path.startswith('./')):
            obj = localpath.Protocol_localpath()
        elif protocolname == 'ftp':
            kwargs = {}
            if 'FollowLinks' in option:
                kwargs['followLinks'] = True \
                    if option['FollowLinks'].capitalize() == 'True' else False
            if 'SkipPermissions' in option:
                kwargs['skipPermissions'] = True \
                    if option['SkipPermissions'].capitalize() == 'True' else False
            obj = ftp.Protocol_ftp(**kwargs)
        elif protocolname == 'lslr':
            obj = lslr_file.Protocol_lslr_file()
        elif protocolname == 'https_opensearch':
            obj = https_opensearch.Protocol_https_opensearch()
        elif protocolname in ['https_po_daac', 'https_xx_daac']:
            obj = https_xx_daac.Protocol_https_xx_daac()
        elif protocolname == 'https_eop_os':
            obj = https_eop_os.Protocol_https_eop_os()
        elif protocolname == 'https_listing_file':
            data_server_address = ''
            if 'DataServerAddress' in option:
                data_server_address = option['DataServerAddress']
            obj = https_listing_file.Protocol_https_listing_file(data_server_address)
        elif protocolname == 'https_s3_cmems':
            obj = https_s3_cmems.Protocol_https_s3_cmems()
        elif protocolname == 'sftp':
            obj = sftp.Protocol_sftp()
        elif protocolname == 'webdav':
            obj = webdav.Protocol_webdav()
        elif protocolname == 'ftps':
            obj = ftps.Protocol_ftps(option)
        # elif protocolname == "https_directorylist":
        elif protocolname in ('https', 'http'):
            class_file_extractor = https_directorylist.FileExtractor
            redirector = None
            if 'Redirector' in option:
                redirector = option['Redirector']
            if 'FileExtractor' in option:
                fileExtractor = 'FileExtractor_' + option['FileExtractor']
                if hasattr(file_extractor, fileExtractor):
                    module = file_extractor
                    log.debug(f"FileExtractor : {option['FileExtractor']} found in regular module")
                else:
                    # module to take in the plugins directory,
                    #   directory add to sys.path in loadFromModule().
                    module = __import__('https_FileExtractor')
                    log.warning('The HTTP protocol uses the FileExtractor module of the plugin directory')
                class_file_extractor = getattr(module, fileExtractor)
            obj = https_directorylist.Protocol_https_directorylist(
                file_extractor=class_file_extractor,
                redirector=redirector)
        elif protocolname == 'eodms':
            obj = api_eodms.Protocol_api_eodms(option)

        if timeout is not None:
            obj.setTimeout(timeout)

        log.debug(f'protocol : {protocolname} => set timeout : {obj.getDefaultTimeout()}')

        return obj

    def get_options(self, args_list=None):
        # Get a parser
        usage = 'usage: %prog [options] (-c configfile | url)'
        parser = OptionParser(usage=usage, version='%prog ' + VERSION)

        # Set need argument

        parser.add_option('-c', '--config',
                          action='store', type='string',
                          dest='configfile', metavar='FILE [REQUIRED]',
                          help='path of a XML config file')

        parser.add_option('-d', '--database',
                          action='store', type='string',
                          dest='database', metavar='FILE',
                          help='path of a sqlite database ' +
                               '(will be created if not exists)')
        parser.add_option(
            '',
            '--ftp-listing-type',
            action='store',
            type='string',
            dest='ftp_listing_type',
            metavar='FILE',
            help='type of listing [default=%s]' % DEFAULT_FTP_LISTING_TYPE)
        parser.add_option('', '--follow-symlink',
                          action='store_true',
                          dest='follow_symlink', metavar='',
                          help='follow symlink for directories')
        parser.add_option(
            '',
            '--purge-older-than',
            action='store',
            type='int',
            dest='purge_older_than',
            metavar='DAYS',
            help='purges all executions before n days, keeping last execution in all cases')
        parser.add_option(
            '',
            '--purge-remove-last',
            action='store_true',
            dest='purge_remove_last',
            metavar='',
            help='force the purge of the last execution if needed',
            default=False)
        parser.add_option('-t', '--test',
                          action='store_true',
                          dest='test_mode', metavar='',
                          help="don't add file to database, just display" +
                               ' what would be done without the -t')
        parser.add_option('', '--no-summary', action='store_true',
                          dest='no_summary', metavar='',
                          help='print summary informations after execution',
                          default=False)
        parser.add_option('', '--debug', action='store_true',
                          dest='debug_mode', metavar='',
                          help='run in debug mode (more verbose)',
                          default=False)

        parser.add_option('', '--smart-crawler', action='store_true',
                          dest='smart_crawler', metavar='',
                          help='smart crawler actived', default=False)

        #         parser.add_option("", "--min-date", action="store",type="string",
        #                           dest="min_date", metavar="",
        #                           help="local path")
        #
        #         parser.add_option("", "--max-date", action="store",type="string",
        #                           dest="max_date", metavar="",
        #                           help="local path")

        if args_list is None:
            (options, args) = parser.parse_args()
        else:
            (options, args) = parser.parse_args(args_list)

        # Check if required argument are here:
        if not options.configfile and len(args) == 0:
            msg = 'Missing required argument !!!'
            log.error(msg)
            parser.print_help()
            sys.exit(1)

        return options, args

    def run(self, options, args, config=None):
        if config is None:
            config = Config()
        config._start_time = datetime.now()
        config.test_mode = options.test_mode
        data_reader = None
        url = None


        if options.configfile is not None:
            # check for WORKSPACE in config path
            modulepath = options.configfile
            if modulepath.startswith('WORKSPACE'):
                modulepath = modulepath.replace(
                    'WORKSPACE', config.default_workspace, 1)

            modulepath = options.configfile
            if modulepath[-3:] == '.xml':
                modulepath = modulepath[:-3]
                log.debug('Using config file : %s' % modulepath)

            try:
                config.read_configuration(modulepath, options.smart_crawler)

            except Exception as e:
                log.exception(e)
                sys.exit(1)


        elif len(args) > 0:
            url = args[0]
            if config.protocol is None:
                setConfigFromUrl(config, url)

        if options.debug_mode:
            config.debug = True
            config.logLevel = logging.DEBUG

        if options.ftp_listing_type:
            config.ftp_listing_type = options.ftp_listing_type
        else:
            if config.ftp_listing_type is None:
                config.ftp_listing_type = DEFAULT_FTP_LISTING_TYPE

        if options.database:
            config.database_path = options.database
        else:  # ???
            config.database_path = workingdir.getDatabasePathFromUrl(
                url, prefix=config.protocol + '_', create_path=True)

        if options.follow_symlink:
            config.follow_symlink = options.follow_symlink

        # set valid path for workspace
        if config.database_path and config.database_path.startswith(
                'WORKSPACE'):
            config.database_path = config.database_path.replace(
                'WORKSPACE', config.default_workspace, 1)

        # add column sensingtime if not present
        # alterTable(config.database_path, 'files', 'sensingtime', 'TIMESTAMP')

        log.setLevel(config.logLevel)

        log.debug('Config : ' + config.const_config_string())
        proto = self.getProtocolObject(config.protocol,
                                       config.path,
                                       config.protocol_option,
                                       config.protocol_timeout)
        if proto is None:
            raise DC_ConfigError('Unknown protocol : %s' % config.protocol)

        if data_reader is not None:
            proto.setDataReader(data_reader)
        proto.setConfig(config)

        # Objets initialization
        bfi = SqliteBaseFilesInfos()
        try:
            bfi.setConfig(config)
        except DC_ConfigError as msg:
            log.error(msg)
            raise Exception(msg)
            # sys.exit(1)
        try:
            bfi.createEngine()
        except DC_DbError as msg:
            log.error(msg)
            raise Exception(msg)
            # sys.exit(1)
        bfi.initExecution()

        bfi.addConstant('config', config.const_config_string())

        proto.setFileInfoDatabase(bfi)

        dir_filter = FileFilter(
            forceRegexp=list(config.forceDirectories) + list(config.forceDirectoriesRegexp),
            ignoreRegexp=list(config.ignoreDirectories) + list(config.ignoreDirectoriesRegexp),
            ignoreOlderThan=config.ignoreDirectoriesOlderThan,
            ignoreNewerThan=config.ignoreDirectoriesChangedSince,
            interestingByDefault=config.default_directory_accept,
        )
        proto.setDirectoryFilter(dir_filter)

        file_filter = FileFilter(
            forceRegexp=list(config.forceFiles) + list(config.forceFilesRegexp),
            ignoreRegexp=list(config.ignoreFiles) + list(config.ignoreFilesRegexp),
            ignoreOlderThan=config.ignoreFilesOlderThan,
            ignoreNewerThan=None,
            interestingByDefault=config.default_file_accept,
        )
        proto.setFileFilter(file_filter)

        if options.purge_older_than is not None:
            config.purge_older_than = options.purge_older_than

        if options.purge_remove_last is not None:
            config.purge_remove_last = options.purge_remove_last

        if config.purge_older_than is not None:
            keep_last = not config.purge_remove_last
            purge_date = config._start_time - datetime.fromtimestamp(config.purge_older_than * 86400)
            purge_date_str = datetime.fromtimestamp(purge_date.total_seconds())
            log.debug(
                'purging database : all executions before : %s, keep_last=%s' %
                (purge_date_str, keep_last))
            bfi.req_PurgeOlderThanStartDate(
                purge_date_str, keep_last=keep_last)

        log.debug('Starting...')
        try:
            proto.run()
            summary_string = '%s interesting files have been saved into database in %.2fs (walked on %s files)' \
                             % (bfi.added_files_nbr, (datetime.now() - config._start_time).total_seconds(),
                                proto.nbr_walked_files)
            if options.no_summary:
                log.debug(summary_string)
            else:
                log.info(summary_string)
        except Exception as e:
            exec_id = bfi.current_execution.id
            log.error(
                'Error during execution (error=%s). Invalidation of execution_id=%s in database...' %
                (e, exec_id))
            bfi.setValidExecution(status=False)
            # si ce n'est pas une erreur creee par l'application, on affiche le
            # traceback
            if not isinstance(e, DC_Error):
                traceback.print_exc()
            raise e
        log.debug('End !')


def main(options=None, config=None):
    args = None
    dc = DCheck()
    if options is None:
        options, args = dc.get_options()
    try:
        dc.run(options, args, config)
    except DC_ConfigError as msg:
        logging.exception(msg)
        sys.exit(1)

    sys.exit(0)


if __name__ == '__main__':
    main()
