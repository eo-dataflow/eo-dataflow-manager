
import logging
import os
import re

from eo_dataflow_manager.dchecktools.common.humansize import getHumanSize
from eo_dataflow_manager.dchecktools.reports.AbstractReport import AbstractReport

log = logging.getLogger('Report_DataArchive')
log.setLevel(logging.INFO)


class Report_DataArchive(AbstractReport):

    def __init__(self):
        AbstractReport.__init__(self)

        # parametres de generation de rapport
        self.title_main = 'Report Archive Changes'
        self.display_main_title = True
        self.title_execution_start_time = 'Report execution start at :'
        self.display_execution_start_time = True
        self.title_report_period = 'Report execution period : '
        self.display_execution_period_infos = True
        self.title_unmodified_files = 'Un-modified files in archive :'
        self.display_unmodified_files_title = True
        self.display_unmodified_files = False
        self.title_modified_files = 'Modified files in archive :'
        self.display_modified_files_title = True
        self.display_modified_files = True
        self.title_removed_files = 'Removed files in archive :'
        self.display_removed_files_title = True
        self.display_removed_files = True
        self.title_created_files = 'Created files in archive :'
        self.display_created_files_title = True
        self.display_created_files = True

        self.prefix_title = '########## '

    def __title(self, title):
        self.reportString(self.prefix_title + title)

    def setDirectoryFilter(self, directoryFilter):
        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        self.fileFilter = fileFilter

    def doReport(self):
        if self.report_infos != []:
            log.warning(
                'doReport : report_infos are not used by Report_DataArchive report type')

        if 'size' not in self.check_infos:
            log.warning(
                "doReport : you must use the 'size' check_info to use this report type")
            return

        # Write headers
        if self.display_main_title:
            self.__title(self.title_main)

        if self.display_execution_start_time:
            date = self.config._start_time.strftime('%Y-%m-%d %H:%M:%S')
            string = self.title_execution_start_time + ' ' + date
            self.__title(string)

            string = self.title_report_period + \
                ' from %s to %s' % (self.date_from, self.date_to)
            self.__title(string)

        # fetch interesting executions
        fromExec, toExec = self.fetchExecutionsFromDatesLimits()

        if fromExec is None or toExec is None:
            log.warning(
                'No previous or more recent execution... no report! (previousExec=%s, toExec=%s)' %
                (fromExec is not None, toExec is not None))
            return

        if self.display_execution_period_infos:
            self.__title(
                'Report type=DataArchive : execId from=%s[date=%s] to=%s[date=%s]' %
                (fromExec.id, fromExec.date_start, toExec.id, toExec.date_start))

        if fromExec.id == toExec.id:
            log.warning('toExec.id == fromExec.id... no report !')
            return

        # Reporting informations
        if self.display_unmodified_files:
            if self.display_unmodified_files_title:
                self.__title(self.title_unmodified_files)
            self.doSynthese(
                self.db.req_UnModifiedFiles(
                    toExec.id,
                    fromExec.id,
                    self.check_infos))

        if self.display_modified_files:
            if self.display_modified_files_title:
                self.__title(self.title_modified_files)
            self.doSynthese(
                self.db.req_ModifiedFiles(
                    toExec.id,
                    fromExec.id,
                    self.check_infos))

        if self.display_created_files:
            if self.display_created_files_title:
                self.__title(self.title_created_files)
            self.doSynthese(
                self.db.req_CreatedFiles(
                    toExec.id,
                    fromExec.id,
                    self.check_infos))

        if self.display_removed_files:
            if self.display_removed_files_title:
                self.__title(self.title_removed_files)
            self.doSynthese(
                self.db.req_RemovedFiles(
                    toExec.id,
                    fromExec.id,
                    self.check_infos))

    def doSynthese(self, requestResult):

        dir_info = {}
        others_string = 'other directories'
        dir_info[others_string] = 0
        group_regexp = re.compile(r'(.*)\/\d\d\d\d/\d\d\d\/.*\..*')
        for result in requestResult:
            filepath = result.filepath
            size = result.size

            basedir, filename = os.path.split(filepath)

            if not self.isFileReportable(filename):
                continue

            if not self.isDirectoryReportable(basedir):
                continue

            match = group_regexp.match(filepath)
            if match:
                basedir = match.group(1)
                if basedir in dir_info:
                    dir_info[basedir] = dir_info[basedir] + size
                else:
                    dir_info[basedir] = size
            else:
                dir_info[others_string] += size

        for key in list(dir_info.keys()):
            if key != others_string:
                self.reportString('%s : %s' %
                                  (key, getHumanSize(dir_info[key])))

        if dir_info[others_string] != 0:
            self.reportString(
                '%s : %s' %
                (others_string, getHumanSize(
                    dir_info[others_string])))
