
import logging
import os
import re
import sys
from datetime import datetime

log = logging.getLogger('AbstractReport')
log.setLevel(logging.INFO)


class AbstractReport(object):

    def __init__(self):
        self.db = None
        self.config = None
        self.check_infos = []
        self.report_infos = []

        self.directoryFilter = None
        self.fileFilter = None

        # date limits
        self.date_from = None
        self.date_to = None

        self.__output = None

        # report string content, set it to != None tu use it instead of
        # os.write
        self.reportStringContent = None

    def __getTimestampDate(self):
        path = self.config.timestamp_filepath
        if path and os.path.exists(path):
            log.debug('Using timestamp file : %s' % (path))
            with open(path, 'r') as fp:
                lines = fp.readlines()
                for line in lines:
                    m = re.match('^lastCorrectExecutionDate=(.*)', line)
                    if m:
                        date = m.groups()[0]
                        return datetime.fromisoformat(date)
        return None

    def __setTimestampDate(self):
        path = self.config.timestamp_filepath
        date = self.config._start_time
        if path and date:
            line = 'lastCorrectExecutionDate='
            line += date.strftime('%Y-%m-%d %H:%M:%S') + '\n'
            with  open(path, 'w+') as fp:
                log.debug('Setting timestamp %s in file : %s)' % (line, path))
                fp.writelines(line)


    def setValidDatesLimits(self):
        if self.date_from is None or self.date_to is None:
            assert(self.db is not None)

        if self.date_from is None:
            last_execution_list = self.db.req_LastTerminatedExecutions()
            if len(last_execution_list) > 1:
                self.date_from = last_execution_list[1].date_start

        if self.date_to is None:
            self.date_to = self.config._start_time

    def fetchExecutionsFromDatesLimits(self):
        assert self.db is not None

        date_from = self.date_from
        date_to = self.date_to

        assert date_from is None or isinstance(date_from, datetime), \
            '%s instead of datetime.datetime' % type(date_from)
        assert date_to is None or isinstance(date_to, datetime), \
            '%s instead of datetime.datetime' % type(date_from)
        fromExec = None
        toExec = None

        log.debug(
            'fetchExecutionIdFromDatesLimits : date_from = %s, date_to = %s' %
            (date_from, date_to))
        if date_to:
            execution = self.db.req_PreviousTerminatedExecutionsIds(date_to)
            if len(execution):
                toExec = execution[0]

        if date_from:
            execution = self.db.req_NextTerminatedExecutionsIds(date_from)
            if len(execution):
                fromExec = execution[0]
                # Dans le cas ou le timestamp precedent serait trop rapproche du nouveau timestamp,
                # on prend le rapport precedent le timestamp precedent
                if toExec and fromExec.id == toExec.id:
                    execution = self.db.req_PreviousTerminatedExecutionsIds(
                        date_from)
                    if len(execution):
                        fromExec = execution[0]

        return fromExec, toExec

    def isExecutionPeriodValid(self, fromExec, toExec):
        if fromExec is None or toExec is None:
            log.warning(
                'isExecutionPeriodValid : No previous or more recent valid execution... no report! (previousExec=%s, toExec=%s)' %
                (fromExec is not None, toExec is not None))
            return False

        if fromExec.id >= toExec.id:
            log.warning(
                'isExecutionPeriodValid : fromExec.id >= toExec.id (%s >= %s)... no report !' %
                (fromExec.id, toExec.id))
            return False

        return True

    def isConfigIdentical(self, fromExec, toExec):
        return self.db.req_IsConstantIdentical(
            fromExec, toExec, title='config')

    def setConfig(self, config):
        self.config = config
        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

    def run(self):
        self.__output = None
        try:
            tmp_output_path = None
            if self.config.output_filepath is not None:
                tmp_output_path = self.config.output_filepath + '.tmp'
                self.__output = open(tmp_output_path, 'w')
            self.date_from = self.__getTimestampDate()
            self.date_to = None
            self.setValidDatesLimits()
            self.doReport()
        except IOError as msg:
            log.error(msg)
            sys.exit(1)
        finally:
            if self.__output:
                self.__output.close()
                os.rename(tmp_output_path, self.config.output_filepath)

        self.__setTimestampDate()

    def doReport(self):
        raise NotImplementedError

    def setFileInfoDatabase(self, database):
        self.db = database

    def setDirectoryFilter(self, directoryFilter):
        # raise error to force each report to say if it uses this filter or not
        raise NotImplementedError

    def setFileFilter(self, fileFilter):
        # raise error to force each report to say if it uses this filter or not
        raise NotImplementedError

    def reportString(self, string, suffix='\n', prefix=''):
        string = prefix + string + suffix
        if self.__output:
            self.__output.write(string)
        else:
            if self.reportStringContent:
                self.reportStringContent += string
            else:
                sys.stdout.write(string)

    def isDirectoryReportable(self, directory):
        interestingDirectory = self.directoryFilter.isInteresting(directory)
        if not interestingDirectory:
            log.debug('directory %s is not interesting' % directory)
        return interestingDirectory

    def isFileReportable(self, filename):
        interestingFilename = self.fileFilter.isInteresting(filename)
        if not interestingFilename:
            log.debug('filename %s is not interesting' % filename)
        return interestingFilename
