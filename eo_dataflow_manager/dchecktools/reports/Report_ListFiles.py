
import logging
import os

from eo_dataflow_manager.dchecktools.reports.AbstractReport import AbstractReport

log = logging.getLogger('Report_ListFiles')
log.setLevel(logging.INFO)


class Report_ListFiles(AbstractReport):
    """ Class used to generated standard reports. Configurable as needed,
    flagging each file on 1 line to say if it was
    added (A), modified (M), removed (R), identical (I).
    Errors are include in the report, preceded by a '!'
    """

    def __init__(self):
        AbstractReport.__init__(self)

        # parametres de generation de rapport
        self.display_titles = True
        self.title_main = 'Report ListFiles'
        self.display_main_title = True
        self.display_execution_period_infos = True
        self.title_unmodified_files = 'Un-modified files :'
        self.display_unmodified_files = False
        self.display_unmodified_files_title = True
        self.title_modified_files = 'Modified files :'
        self.display_modified_files = True
        self.display_modified_files_title = True
        self.title_removed_files = 'Removed files :'
        self.display_removed_files = True
        self.display_removed_files_title = True
        self.title_created_files = 'Created files :'
        self.display_created_files = True
        self.display_created_files_title = True
        self.display_file_prefix = False

        self.ignoreUnModifiedFiles = False
        self.ignoreUnModifiedDirectories = False
        self.ignoreUnModifiedSymLinks = False
        self.ignoreModifiedFiles = False
        self.ignoreModifiedDirectories = False
        self.ignoreModifiedSymLinks = False
        self.ignoreCreatedFiles = False
        self.ignoreCreatedDirectories = False
        self.ignoreCreatedSymLinks = False
        self.ignoreRemovedFiles = False
        self.ignoreRemovedDirectories = False
        self.ignoreRemovedSymLinks = False

        self.prefix_title = '########## '

    def __title(self, title):
        if self.display_titles:
            self.reportString(self.prefix_title + title)

    def setDirectoryFilter(self, directoryFilter):
        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        self.fileFilter = fileFilter

    def doReport(self):
        # Write headers
        if self.display_main_title:
            self.__title(self.title_main)

        # fetch interesting executions
        fromExec, toExec = self.fetchExecutionsFromDatesLimits()

        # Reporting exec informations here, so that we don't care about the
        # valid executions
        lastToExecLst = self.db.req_PreviousTerminatedExecutionsIds(
            self.date_to, only_valid_exec=False)
        if len(lastToExecLst):
            lastToExec = lastToExecLst[0]
            for exec_info in self.db.req_GetErrors(lastToExec.id):
                exec_info_str = '[%s] %s : %s' % (
                    exec_info.date, exec_info.title, exec_info.data)
                self.reportString(exec_info_str, prefix='!   ')

        if not self.isExecutionPeriodValid(fromExec, toExec):
            return

        if self.config.ignore_config_diff is False and not self.isConfigIdentical(
                fromExec.id, toExec.id):
            log.warning(
                'dcheck was not running with same configuration. No report ! (use --ignore-config-differences to force) (execfrom=%s, execto=%s)' %
                (fromExec.id, toExec.id))
            return

        if self.display_execution_period_infos:
            self.__title(
                'Report type=DataArchive : execId from=%s[date=%s] to=%s[date=%s]' %
                (fromExec.id, fromExec.date_start, toExec.id, toExec.date_start))

        # Reporting informations
        if self.display_execution_period_infos:
            self.__title(
                'Report type=ListFiles : from %s to %s' %
                (self.date_from, self.date_to))

        if self.display_unmodified_files:
            if self.display_unmodified_files_title:
                self.__title(self.title_unmodified_files)
            files = self.db.req_UnModifiedFiles(
                toExec.id,
                fromExec.id,
                self.check_infos,
                ignoreFiles=self.ignoreUnModifiedFiles,
                ignoreDirectories=self.ignoreUnModifiedDirectories,
                ignoreSymLinks=self.ignoreUnModifiedSymLinks
            )
            self.reportFile(
                files,
                self.report_infos,
                'I   ')

        if self.display_modified_files:
            if self.display_modified_files_title:
                self.__title(self.title_modified_files)
            files = self.db.req_ModifiedFiles(
                toExec.id,
                fromExec.id,
                self.check_infos,
                ignoreFiles=self.ignoreModifiedFiles,
                ignoreDirectories=self.ignoreModifiedDirectories,
                ignoreSymLinks=self.ignoreModifiedSymLinks
            )
            self.reportFile(
                files,
                self.report_infos,
                'M   ')

        if self.display_created_files:
            if self.display_created_files_title:
                self.__title(self.title_created_files)
            files = self.db.req_CreatedFiles(
                toExec.id,
                fromExec.id,
                self.check_infos,
                ignoreFiles=self.ignoreCreatedFiles,
                ignoreDirectories=self.ignoreCreatedDirectories,
                ignoreSymLinks=self.ignoreCreatedSymLinks
            )
            self.reportFile(
                files,
                self.report_infos,
                'A   ')
        if self.display_removed_files:
            if self.display_removed_files_title:
                self.__title(self.title_removed_files)
            files = self.db.req_RemovedFiles(
                toExec.id,
                fromExec.id,
                self.check_infos,
                ignoreFiles=self.ignoreRemovedFiles,
                ignoreDirectories=self.ignoreRemovedDirectories,
                ignoreSymLinks=self.ignoreRemovedSymLinks
            )
            self.reportFile(
                files,
                self.report_infos,
                'R   ')

    def reportFile(self, dbFileList, reportInfos=[], prefix=''):
        for file in dbFileList:
            filepath = file.filename
            basedir, filename = os.path.split(filepath)

            if not self.isFileReportable(filename):
                continue

            if not self.isDirectoryReportable(basedir):
                continue

            infosString = ''
            for info in reportInfos:
                infosString += ', %s=%s' % (info, file._asdict()[info])

            if not self.display_file_prefix:
                prefix = ''

            log.debug(
                'Write line : %s+%s prefix=%s' %
                (filepath, infosString, prefix))
            self.reportString(filepath + infosString, prefix=prefix)
