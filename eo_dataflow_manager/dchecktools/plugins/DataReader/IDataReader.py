#
# -*- coding: UTF-8 -*-
#


class IDataReader(object):
    """
    Interface class for L{IDataReader}
    """

    def getRelativePath(self, filepath):
        """Returns the local relative filepath we will use as directory for storage.
        """
        raise RuntimeError(
            "getRelativePath is an abstract method in IDataReader")

    def getStorageName(self, filepath):
        """Returns the local filename we will use for storage.
        """
        raise RuntimeError(
            "getStorageName is an abstract method in IDataReader")

    def getDate(self, filepath):
        """Returns the date of the data file content. Used for advanced filtering,
        to know if we really have to download the file for example.
        """
        raise RuntimeError("getDate is an abstract method in IDataReader")
