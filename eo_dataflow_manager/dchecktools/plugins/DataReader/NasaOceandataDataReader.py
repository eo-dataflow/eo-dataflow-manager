#
# -*- coding: UTF-8 -*-
#

import datetime
import logging
import os

from eo_dataflow_manager.dchecktools.plugins.DataReader.IDataReader import IDataReader


class NasaOceandataDataReader(IDataReader):

    def __init__(self, loggerName):
        self._log = logging.getLogger(loggerName)

    # emplacement local
    def getRelativePath(self, filepath):
        filename = self.getStorageName(filepath)
        dt = self.getDate(filepath)

        if filename[0] == 'A':
            platform = 'AQUA'
            sensor = 'MODIS'
        else:
            platform = 'SNPP'
            sensor = 'VIIRS'

        paths = [
            platform,
            sensor,
            'L3_BIN',
            '4_KM',
            '1_DAY',
            dt.strftime('%Y'),
            dt.strftime('%j'),
            filename]
        return os.path.join(*paths)

    # nom du fichier local
    def getStorageName(self, filepath):
        filedir, filename = os.path.split(filepath)
        return filename

    # date du fichier lue dans le nom
    def getDate(self, filepath):
        filedir, filename = os.path.split(filepath)
        # filename = [A|V][YYYY][DOY]L3b_DAY_[SNPP]?[RRS|KD490|IOP|CHL].nc
        filedate = filename[1:8]
        dt = None
        try:
            dt = datetime.datetime.strptime(filedate, '%Y%j')
        except Exception:
            self._log.debug(
                "NasaOceandataDataReader::getDate : Unable to get date for file '%s'" %
                (filename))

        return dt


if __name__ == '__main__':

    filepath_list = [
        'https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/A2017021.L3b_DAY_RRS.nc',
        'https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/A2017021.L3b_DAY_KD490.nc',
        'https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/A2017021.L3b_DAY_IOP.nc',
        'https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/A2017021.L3b_DAY_CHL.nc',
        'https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/V2017021.L3b_DAY_SNPPCHL.nc']
    logging.basicConfig()

    reader = NasaOceandataDataReader('root')
    for filepath in filepath_list:
        print('filepath = ', filepath)
        print('getDate() => ', reader.getDate(filepath))
        print('getStorageName() => ', reader.getStorageName(filepath))
        print('getRelativePath() => ', reader.getRelativePath(filepath))
