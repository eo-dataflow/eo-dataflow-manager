#
# -*- coding: UTF-8 -*-
#
#!/usr/bin/env python

# fcad: adding DirNameBasedDataReader class

import datetime
import logging
import os

from eo_dataflow_manager.dchecktools.plugins.DataReader.IDataReader import IDataReader


class DirNameBasedDataReader(IDataReader):

    def __init__(
            self,
            loggerName,
            dirPrefix,
            dateFormat,
            storageRegexp,
            dateIndex=0):
        self._log = logging.getLogger(loggerName)
        self._log.debug(
            "DirNameBasedDataReader: dir prefix ' %s dateFormat ' %s storageRegexp %s'" %
            (dirPrefix, dateFormat, storageRegexp))

        self.dir_prefix = dirPrefix
        self.date_index = dateIndex
        self.date_format = dateFormat
        self.storage_regexp = storageRegexp

    def getRelativePath(self, filepath):
        filename = self.getStorageName(filepath)
        dt = self.getDate(filepath)

        if self.storage_regexp is not None:
            filedir = dt.strftime(self.storage_regexp)
            return os.path.join(
                os.path.join(
                    filedir,
                    self.getLastDir(filepath)),
                filename)
        else:
            filepath = filepath.lstrip('/')
            return filepath

    def getStorageName(self, filepath):
        self.getDate(filepath)

        filedir, filename = os.path.split(filepath)

        return filename

    def getDate(self, filepath):

        dt = None
        lastdir = self.getLastDir(filepath)
        if lastdir.startswith(self.dir_prefix):
            lastdir = lastdir.replace(self.dir_prefix, '')
            tokens = lastdir.split('_')
            if len(tokens) > self.date_index:
                dateStr = tokens[self.date_index]
                dt = datetime.datetime.strptime(dateStr, self.date_format)

        return dt

    def getLastDir(self, filepath):

        #        filedir, filename = os.path.split(filepath)
        filedirs = filepath.split('/')

        return filedirs[len(filedirs) - 2]


if __name__ == '__main__':

    filepath = '//PDGS_SL_2_WCT____NR/S3A_SL_2_WCT____20180305T220034_20180305T220334_20180305T235858_0179_028_300_2880_MAR_O_NR_002.SEN3/D2_SST_io.nc'
    logging.basicConfig()
    prefix_dir = 'S3A_SL_2_WCT____'
    date_format = '%Y%m%dT%H%M%S'
    storage_regexp = '%Y/%j'

    reader = DirNameBasedDataReader(
        'root', prefix_dir, date_format, storage_regexp)
    print('filepath = ', filepath)
    print('getDate() => ', reader.getDate(filepath))
    print('getStorageName() => ', reader.getStorageName(filepath))
    print('getRelativePath() => ', reader.getRelativePath(filepath))
