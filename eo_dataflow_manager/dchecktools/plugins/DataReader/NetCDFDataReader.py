#
# -*- coding: UTF-8 -*-
#
#!/usr/bin/env python
#
# Hist : 2018/11/19 Removing explicit path names
#


import datetime
import logging
import os

import netCDF4

from eo_dataflow_manager.scheduler.sc.IDataReader import IDataReader


class NetCDFDataReader(IDataReader):

    def __init__(self, loggerName, dateName, dateFormat, storagePath):
        self._log = logging.getLogger(loggerName)
        self._log.debug(
            "NetCDFDataReader: date name '%s dateFormat %s storagePath %s'" %
            (dateName, dateFormat, storagePath))

        if not dateName or not dateFormat:
            self.date_name = ''
            self.date_format = ''
        else:
            self.date_name = dateName
            self.date_format = dateFormat

        self.storage_path = storagePath

    def getRelativePath(self, filepath):
        filename = self.getStorageName(filepath)
        dt = self.getDate(filepath)
        if self.storage_path is not None and dt is not None:
            filedir = dt.strftime(self.storage_path)
            return os.path.join(filedir, filename)
        else:
            filepath = filepath.lstrip('/')
            return filepath

    def getStorageName(self, filepath):

        filename = os.path.split(filepath)[1]
        return filename

    def getDate(self, filepath):

        dt = None
        if os.path.isfile(filepath):  # if file is a local file
            try:
                nc_file = netCDF4.Dataset(filepath, 'r')  # pylint: disable=no-member
                # to access the content of the attribute
                date_name = nc_file.getncattr(self.date_name)
                dt = datetime.datetime.strptime(date_name, self.date_format)
                nc_file.close()

            except Exception as e:
                self._log.exception(
                    "NetCDFDataReader::getDate : Unable to get date for file '%s' (date name=%s) " %
                    (filepath, self.date_name))
                raise e

        return dt


if __name__ == '__main__':

    filepath = '//...../lsa/cs2igdr/c004/c2p0347c004.nc'
    logging.basicConfig()
    date_name = 'first_meas_time'
    date_format = '%Y-%m-%d %H:%M:%S.%f'
    storage_path = '%Y/%j'

    reader = NetCDFDataReader('root', date_name, date_format, storage_path)
    print('getStorageName() => ', reader.getStorageName(filepath))
    print('getDate() => ', reader.getDate(filepath))
    print('getRelativePath() => ', reader.getRelativePath(filepath))
