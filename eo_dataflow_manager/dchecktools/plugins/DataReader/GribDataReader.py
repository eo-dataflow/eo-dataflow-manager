#
# -*- coding: UTF-8 -*-
#


import logging
import os
from datetime import datetime, timedelta

# from eccodes import *
from eccodes import codes_get, codes_get_string, codes_grib_new_from_file, codes_release

from eo_dataflow_manager.scheduler.sc.IDataReader import IDataReader


class GribDataReader(IDataReader):

    Table4 = {'m': 60,
              'h': 3600,
              'D': 3600 * 24,
              '3h': 3600 * 3,
              '6h': 3600 * 6,
              '12h': 3600 * 12,
              '15m': 60 * 15,
              '30m': 60 * 30,
              's': 1
              }

    def __init__(self, loggerName, dummy1, dummy2, storagePath):
        self._log = logging.getLogger(loggerName)
        self._log.debug("GribDataReader: storagePath %s'" %
                        (storagePath))
        self.storage_path = storagePath

    def getRelativePath(self, filepath):
        filename = self.getStorageName(filepath)
        dt = self.getDate(filepath)
        if self.storage_path is not None and dt is not None:
            filedir = dt.strftime(self.storage_path)
            return os.path.join(filedir, filename)
        else:
            filepath = filepath.lstrip('/')
            return filepath

    def getStorageName(self, filepath):

        filename = os.path.split(filepath)[1]
        return filename

    def getDate(self, filepath):
        grip_type = None
        first_grib = None
        data_date = None
        data_time = None
        step_units = None
        data_datetime = 0
        forecastTime = None

        dt = None

        if os.path.isfile(filepath):  # if file is a local file
            try:
                with open(filepath, 'rb') as f:
                    first_grib = codes_grib_new_from_file(f)
                    grip_type = codes_get(first_grib, 'editionNumber')
                    data_date = codes_get_string(first_grib, 'dataDate')
                    data_time = codes_get_string(first_grib, 'dataTime')
                    step_units = codes_get_string(first_grib, 'stepUnits')
                    data_datetime = datetime.strptime(data_date + data_time, '%Y%m%d%H%M')
                    if grip_type == 1:
                        forecastTime = codes_get(first_grib, 'startStep')
                    else:
                        forecastTime = codes_get(first_grib, 'forecastTime')
                    forecastTimeSeconds = forecastTime * self.Table4[step_units]
                    dt = data_datetime + timedelta(seconds=forecastTimeSeconds)

                    codes_release(first_grib)

            except Exception as e:
                self._log.exception(
                    "GribDataReader::getDate : Unable to get date for file '%s' "
                    '(type=%s, date=%s time=%s startStep/forecastTime=%s stepUnits=%s) ' %
                    (filepath, str(grip_type), data_date,
                     data_time, str(forecastTime), str(step_units)))
                raise e

        return dt


if __name__ == '__main__':

    filepath = '/export/home1/pmaissiat/downloader_daemon/unittests/dataset/data/Test_grib/data1' #"C:\\Cersat\\Daemon\\unittests\\dataset\\data\\Test_grib\\data1"
    logging.basicConfig()
    date_name = 'first_meas_time'
    date_format = '%Y-%m-%d %H:%M:%S.%f'
    storage_path = '%Y/%j'
    print('GRIB1')
    reader = GribDataReader('root', date_name, date_format, storage_path)
    print('getStorageName() => ', reader.getStorageName(filepath))
    print('getDate() => ', reader.getDate(filepath))
    print('getRelativePath() => ', reader.getRelativePath(filepath))
    print('\nGRIB2')
    filepath = '/export/home1/pmaissiat/downloader_daemon/unittests/dataset/data/Test_grib/data2'
    reader = GribDataReader('root', date_name, date_format, storage_path)
    print('getStorageName() => ', reader.getStorageName(filepath))
    print('getDate() => ', reader.getDate(filepath))
    print('getRelativePath() => ', reader.getRelativePath(filepath))
