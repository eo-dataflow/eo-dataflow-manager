#
# -*- coding: UTF-8 -*-
#

import datetime
import logging
import os

from eo_dataflow_manager.dchecktools.plugins.DataReader.IDataReader import IDataReader


class NameBasedDataReader(IDataReader):

    def __init__(self, loggerName):
        self._log = logging.getLogger(loggerName)

    def getRelativePath(self, filepath):
        filename = self.getStorageName(filepath)
        dt = self.getDate(filepath)
        filedir = dt.strftime('%Y/%j')

        return os.path.join(filedir, filename)

    def getStorageName(self, filepath):
        filedir, filename = os.path.split(filepath)
        return filename

    def getDate(self, filepath):
        filedir, filename = os.path.split(filepath)
        filedate = filename[:8]
        dt = None
        try:
            dt = datetime.datetime.strptime(filedate, '%Y%m%d')
        except Exception:
            self._log.debug(
                "NameBasedDataReader::getDate : Unable to get date for file '%s'" %
                (filename))

        return dt
