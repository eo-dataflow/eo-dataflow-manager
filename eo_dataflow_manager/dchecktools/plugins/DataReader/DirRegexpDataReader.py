#
# -*- coding: UTF-8 -*-
#
#!/usr/bin/env python

# fcad: adding DirNameBasedDataReader class

import datetime
import logging
import os
import re

from eo_dataflow_manager.dchecktools.plugins.DataReader.IDataReader import IDataReader


class DirRegexpDataReader(IDataReader):

    def __init__(
            self,
            loggerName,
            date_regexp_pattern,
            dateFormat,
            storage_regexp,
            dateIndex=0):
        self._log = logging.getLogger(loggerName)
#        self._log.debug("DirNameBasedDataReader: dir prefix ' %s dateFormat ' %s storageRegexp %s'"%(dirPrefix, dateFormat, storageRegexp))

        self.date_index = dateIndex

        if not date_regexp_pattern or not dateFormat:
            self.date_regexp_pattern = '()'
            self.date_format = ''
        else:
            self.date_regexp_pattern = date_regexp_pattern
            self.date_format = dateFormat

        self.storage_regexp = storage_regexp

    def getRelativePath(self, filepath):
        filename = self.getStorageName(filepath)
        dt = self.getDate(filepath)

        if self.storage_regexp is not None:
            filedir = dt.strftime(self.storage_regexp)
            return os.path.join(
                os.path.join(
                    filedir,
                    self.getLastDir(filepath)),
                filename)
        else:
            filepath = filepath.lstrip('/')
            return filepath

    def getStorageName(self, filepath):

        filedir, filename = os.path.split(filepath)

        return filename

    def getDate(self, filepath):

        self.getLastDir(filepath)

        filename = filepath
        datePattern = re.compile(self.date_regexp_pattern)
        dt = None
        try:
            m = datePattern.match(filename)
            filedate = m.groups()[self.date_index]
            dt = datetime.datetime.strptime(filedate, self.date_format)
            self._log.debug(
                "DirRegexpDataReader::getDate for file '%s' : %s" %
                (filename, dt))
        except Exception as e:
            self._log.exception(
                "DirRegexpDataReader::getDate : Unable to get date for file '%s' (regexp=%s) (date_format=%s)" %
                (filename, self.date_regexp_pattern, self.date_format))
            raise e

        return dt

    def getLastDir(self, filepath):

        filedir, filename = os.path.split(filepath)
        filedirs = filepath.split('/')

        return filedirs[len(filedirs) - 2]


if __name__ == '__main__':

    filepath = '//PDGS_SL_2_WCT____NR/S3A_SL_2_WCT____20181009T131325_20181009T131625_20181009T145459_0179_036_323_4680_MAR_O_NR_003.SEN3/D2_SST_io.nc'
    logging.basicConfig()
    date_format = '%Y%m%dT%H%M%S'
    storage_regexp = '%Y/%j'
    date_regexp_pattern = '.*S3A_SL_2_WCT____([0-9]{8}T[0-9]{6}).*'

    reader = DirRegexpDataReader(
        'root',
        date_regexp_pattern,
        date_format,
        storage_regexp)
    print('filepath = ', filepath)
    print('getLastDir() => ', reader.getLastDir(filepath))
    print('getDate() => ', reader.getDate(filepath))
    print('getStorageName() => ', reader.getStorageName(filepath))
    print('getRelativePath() => ', reader.getRelativePath(filepath))
