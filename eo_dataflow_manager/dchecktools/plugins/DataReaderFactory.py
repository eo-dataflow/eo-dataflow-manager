#
# -*- coding: UTF-8 -*-
#
"""Factory module, to load plugins
"""

__docformat__ = 'epytext'

import logging

logging.basicConfig()
log = logging.getLogger('dcheck')
log.setLevel(logging.INFO)


know_classes = {}


def my_import(name):
    mod = __import__(name)
    components = name.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
        log.debug(mod)
    return mod


def DataReaderFactory(
        classname,
        loggerName=None,
        regexpDate=None,
        dateFormat=None,
        storagePath=None,
        dirPrefix=None,
        dateIndex=0):
    """
    ProcessRunnerFactory instanciate a ProcessRunner class
    in function ot the class name.
    If module or class is unreachable log the error and raise
    a RuntimeError exception
    """

    # If the class is unknow, try to load it
    if classname not in know_classes:

        # Try to import the modules "classname"
        try:
            tmpModule = my_import(
                "eo_dataflow_manager.dchecktools.plugins.DataReader." + classname)
            #tmpModule = my_import("plugins.DataReader." + classname)
            #tmpModule = my_import("DataReader." + classname)
        except ImportError:
            # Try without appending the "scheduler.sc.ProcessRunner" strings
            try:
                tmpModule = my_import(classname)
            except ImportError:
                # add log information ??
                raise Exception("Unknown plugin : %s" % (classname))

        if classname.split('.')[-1] not in dir(tmpModule):
            # add log information
            raise RuntimeError(
                "Class %s unreachable in the %s module" %
                (classname, classname))

        # Store the class in the know_class dictionnary
        know_classes[classname] = tmpModule.__dict__[classname.split('.')[-1]]

    # Build a instance of the class know_class[classname]
    if dirPrefix is not None and dateFormat is not None:
        return know_classes[classname](
            loggerName,
            dirPrefix,
            dateFormat,
            storagePath,
            dateIndex)
    elif regexpDate is not None:
        return know_classes[classname](
            loggerName, regexpDate, dateFormat, storagePath)

    else:
        return know_classes[classname](
            loggerName, regexpDate, dateFormat, storagePath)
