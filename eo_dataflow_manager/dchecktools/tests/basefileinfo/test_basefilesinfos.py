import os
import unittest
from datetime import datetime

from eo_dataflow_manager.dchecktools.common.basefileinfos import SqliteBaseFilesInfos
from eo_dataflow_manager.dchecktools.dreport import Config


class testBaseFilesInfos(unittest.TestCase):

    def setUp(self):
        self.config = Config()
        self.config._start_time = datetime.now()
        self.config.database_path = os.path.join(os.path.dirname(__file__), 'test_database1.db')
        self.bfi = SqliteBaseFilesInfos()
        self.bfi.setConfig(self.config)
        self.id_from = 1
        self.id_to = 2

    def requestFiles(
            self,
            check_infos,
            ignoreFiles=False,
            ignoreDirectories=False,
            ignoreSymLinks=False):
        files_unmodified = self.bfi.req_UnModifiedFiles(
            self.id_to,
            self.id_from,
            check_infos,
            ignoreFiles=ignoreFiles,
            ignoreDirectories=ignoreDirectories,
            ignoreSymLinks=ignoreSymLinks)
        files_modified = self.bfi.req_ModifiedFiles(
            self.id_to,
            self.id_from,
            check_infos,
            ignoreFiles=ignoreFiles,
            ignoreDirectories=ignoreDirectories,
            ignoreSymLinks=ignoreSymLinks)
        files_removed = self.bfi.req_RemovedFiles(
            self.id_to,
            self.id_from,
            check_infos,
            ignoreFiles=ignoreFiles,
            ignoreDirectories=ignoreDirectories,
            ignoreSymLinks=ignoreSymLinks)
        files_created = self.bfi.req_CreatedFiles(
            self.id_to,
            self.id_from,
            check_infos,
            ignoreFiles=ignoreFiles,
            ignoreDirectories=ignoreDirectories,
            ignoreSymLinks=ignoreSymLinks)
        return (files_unmodified, files_modified, files_removed, files_created)

    def requestNbrFiles(
            self,
            check_infos,
            ignoreFiles=False,
            ignoreDirectories=False,
            ignoreSymLinks=False):
        unmodified, modified, removed, created = self.requestFiles(
            check_infos, ignoreFiles, ignoreDirectories, ignoreSymLinks)
        return (len(unmodified), len(modified), len(removed), len(created))

    def assertNbrFiles(
            self,
            requestResults,
            unmodified,
            modified,
            removed,
            created):
        res_unmodified, res_modified, res_removed, res_created = requestResults
        self.assertEqual(
            res_unmodified,
            unmodified,
            'Invalid number of unmodified files : %d (expecting : %d)' %
            (res_unmodified,
             unmodified))
        self.assertEqual(
            res_modified,
            modified,
            'Invalid number of modified files : %d (expecting : %d)' %
            (res_modified,
             modified))
        self.assertEqual(
            res_removed, removed, 'Invalid number of removed files : %d (expecting : %d)' %
            (res_removed, removed))
        self.assertEqual(
            res_created, created, 'Invalid number of created files : %d (expecting : %d)' %
            (res_created, created))

    def test_Results(self):
        self.bfi.createEngine()

        # check avec infos size et mtime
        check_infos = ['size', 'mtime']
        results = self.requestNbrFiles(check_infos)
        self.assertNbrFiles(results, unmodified=11, modified=8, removed=2, created=3)

        # en ne prenant en compte que la size
        check_infos = ['size']
        results = self.requestNbrFiles(check_infos)
        self.assertNbrFiles(results, unmodified=17, modified=2, removed=2, created=3)

        # en ignorant les fichiers
        check_infos = ['size', 'mtime']
        results = self.requestNbrFiles(check_infos, ignoreFiles=True)
        self.assertNbrFiles(results, unmodified=2, modified=5, removed=0, created=1)

        # en ignorant les dossiers
        results = self.requestNbrFiles(check_infos, ignoreDirectories=True)
        self.assertNbrFiles(results, unmodified=9, modified=3, removed=2, created=2)

        # en ignorant les liens
        results = self.requestNbrFiles(check_infos, ignoreSymLinks=True)
        self.assertNbrFiles(results, unmodified=10, modified=7, removed=2, created=2)

        # en ignorant fichiers et dossiers
        results = self.requestNbrFiles(
            check_infos, ignoreFiles=True, ignoreDirectories=True)
        self.assertNbrFiles(results, unmodified=0, modified=0, removed=0, created=0)

        # en ignorant fichiers et symlinks
        results = self.requestNbrFiles(
            check_infos, ignoreFiles=True, ignoreSymLinks=True)
        self.assertNbrFiles(results, unmodified=2, modified=4, removed=0, created=0)

        # en ignorant dossiers et symlinks
        results = self.requestNbrFiles(
            check_infos,
            ignoreDirectories=True,
            ignoreSymLinks=True)
        self.assertNbrFiles(results, unmodified=8, modified=3, removed=2, created=2)
