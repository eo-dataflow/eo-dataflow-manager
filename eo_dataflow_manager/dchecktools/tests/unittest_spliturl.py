import unittest

from eo_dataflow_manager.dchecktools.common import spliturl


class testSplitUrl(unittest.TestCase):

    def setUp(self):
        pass

    def test_LocalpathUrlStandard(self):
        url_lst = ('/', '/tmp', '/tmp/toto')
        url_results = (('', '', '/', None, None, None),
                       ('', '', '/tmp', None, None, None),
                       ('', '', '/tmp/toto', None, None, None))
        for i in range(len(url_lst)):
            self.assertEqual(spliturl.spliturl(url_lst[i]), url_results[i])
