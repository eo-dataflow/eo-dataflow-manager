import unittest

from eo_dataflow_manager.dchecktools.filters import StringFilters


class testStringFilters(unittest.TestCase):

    def setUp(self):
        self.resetFilter()

    def resetFilter(self):
        self.filter = StringFilters.FileFilter()

    def test_Files(self):
        self.resetFilter()

        files_listed = [
            'forcedfile.txt',
            'ignoredfile.txt',
            'standardfile.txt',
            'yolo.md5',
            'otherfile.xml']

        # without configuration
        for file in files_listed:
            self.assertEqual(self.filter.isInteresting(file), True)

        # ignoring file, interestingByDefault = True
        self.filter.setIgnoreRegex(['ignoredfile.txt', 'otherignored.txt', r'.*\.md5$'])
        for file in files_listed:
            interesting = self.filter.isInteresting(file)
            if file not in ['ignoredfile.txt', 'otherignored.txt', 'yolo.md5']:
                self.assertEqual(
                    interesting,
                    True,
                    'file %s should be interesting' %
                    file)
            else:
                self.assertEqual(
                    interesting,
                    False,
                    'file %s should not be interesting' %
                    file)

        # forcing file, interestingByDefault = True
        self.filter.setForceInterestingRegex(['forcedfile.txt', 'ignoredfile.txt', 'yolo.md5'])
        self.filter.setIgnoreRegex([])
        for file in files_listed:
            interesting = self.filter.isInteresting(file)
            self.assertEqual(
                interesting,
                True,
                'file %s should be interesting' %
                file)

        # set interestingByDefault = False
        self.filter.setDefault(False)
        for file in files_listed:
            interesting = self.filter.isInteresting(file)
            if file in ['forcedfile.txt', 'ignoredfile.txt', 'yolo.md5']:
                self.assertEqual(
                    interesting,
                    True,
                    'file %s should be interesting' %
                    file)
            else:
                self.assertEqual(
                    interesting,
                    False,
                    'file %s should not be interesting' %
                    file)

        # forcing file regexp, with interestingByDefault == False
        self.filter.setForceInterestingRegex([r'.*\.txt'])
        for file in files_listed:
            interesting = self.filter.isInteresting(file)
            if file.endswith('.txt'):
                self.assertEqual(
                    interesting,
                    True,
                    'file %s should be interesting' %
                    file)
            else:
                self.assertEqual(
                    interesting,
                    False,
                    'file %s should not be interesting' %
                    file)

        self.filter.setForceInterestingRegex(['forcedfile.txt', 'ignoredfile.txt'])
        self.filter.setDefault(True)

        self.assertEqual(self.filter.isInteresting('standardfile.ext'), True)
        self.assertEqual(self.filter.isInteresting('forcedfile.txt'), True)
        self.assertEqual(self.filter.isInteresting('ignoredfile.txt'), True)

        self.filter.setForceInterestingRegex(['forcedfile.txt'])
        self.filter.setDefault(False)
        self.assertEqual(self.filter.isInteresting('standardfile.ext'), False)
        self.assertEqual(self.filter.isInteresting('forcedfile.txt'), True)
        self.assertEqual(self.filter.isInteresting('ignoredfile.txt'), False)

        self.resetFilter()
        self.filter.setForceInterestingRegex(r'ascat_([0-9]{8})_.*_coa_.*\.nc.gz')
        self.filter.setIgnoreRegex([r'.*\.md5$'])
        self.filter.setDefault(False)

        self.assertTrue(self.filter.isInteresting('ascat_20190906_025700_metopb_36149_eps_o_250_3201_ovw.l2.nc.gz') is False)
        self.assertTrue(self.filter.isInteresting('ascat_20190906_025700_metopb_36149_eps_o_250_3201_ovw.l2.nc.gz.md5') is False)

        self.assertTrue(self.filter.isInteresting('ascat_20190906_025700_metopb_36149_eps_o_coa_3201_ovw.l2.nc.gz') is True)
        self.assertTrue(self.filter.isInteresting('ascat_20190906_025700_metopb_36149_eps_o_coa_3201_ovw.l2.nc.gz.md5') is False)
