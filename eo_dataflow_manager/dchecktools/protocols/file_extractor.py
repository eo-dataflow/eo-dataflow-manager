
import html.parser
import os
import re
from urllib.parse import urljoin, urlparse, urlunparse

# import pandas


class FileExtractor_UniBremen(html.parser.HTMLParser):

    DATE_FORMAT = '%Y-%m-%d %H:%M'
    FACTOR =  {'o': 1,
               'K': 1 << 10,  # 1024
               'M': 1 << 20,
               'G': 1 << 30,
               'T': 1 << 40,
               'P': 1 << 50}

    @staticmethod
    def get_download_url(dcheck_url):
        return dcheck_url

    @staticmethod
    def get_new_dcheck_url(old_dcheck_url, path):
        return old_dcheck_url + '/' + path

    @staticmethod
    def get_recurcive():
        return False

    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.links = {}
        self._to_ignore = False
        self._is_directory = False
        self._is_file = False
        self._current_name = ''
        self.__mtime_pattern = re.compile(' *([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}) *')
        self.__size_pattern = re.compile(' *([0-9a-zA-Z.-]*) *')
        self.__td_count = 0
        self.__mtime_str = ''

    def handle_data(self, data):
        size_str = None
        #print(f"handle_data : {self._current_name} :{data} (count = {self.__td_count})")
        if not self._to_ignore:
            if len(data) > 0 and self._current_name != '':
                if self.__td_count == 3:
                    mg = self.__mtime_pattern.match(data)
                    if mg is not None:
                        self.__mtime_str = mg.groups()[0]
                elif self.__td_count == 4:
                    if self._is_file:
                        isdirectory = 0
                        mg = self.__size_pattern.match(data)
                        if mg is not None:
                            size_str = mg.groups()[0]
                        self.links[self._current_name] = \
                            (isdirectory,self.__mtime_str, size_str)
                        #print("handle_data =====> FILE name : %s, mtime : %s, size : %s" % (self._current_name, self.__mtime_str, size_str))
                    else:
                        isdirectory = 1
                        size_str = '0'
                        self.links[self._current_name] = (isdirectory, self.__mtime_str, size_str)
                        #print("handle_data =====> DIR  name : %s, mtime : %s"%(self._current_name, self.__mtime_str))


    def handle_starttag(self, tag, attrs):
        #print(f"handle_starttag =====> {tag} = {attrs} (count = {self.__td_count})")
        if tag == 'img' and self.__td_count == 1:
            if len(attrs) > 0:
                for attr in attrs:
                    if attr[0] == 'alt':
                        if attr[1] == '[DIR]':
                            self._to_ignore = False
                            self._is_directory = True
                            self._is_file = False
                        elif not attr[1].startswith('['):
                            self._to_ignore = True
                            self._is_directory = False
                            self._is_file = False
                        elif attr[1] == '[PARENTDIR]':
                            self._to_ignore = True
                            self._is_directory = True
                            self._is_file = False
                        else:
                            self._is_directory = False
                            self._is_file = True
                            self._to_ignore = False
        elif tag == 'a' and self.__td_count == 2:
            if not self._to_ignore:
                if len(attrs) > 0:
                    for attr in attrs:
                        if attr[0] == 'href':
                            if attr[1][0:1] == '/':
                                continue
                            self._current_name = attr[1]
                            self._current_name = self._current_name.rstrip('/')
                            if self._is_directory:
                                self.links[self._current_name] = list()
                            if self._is_file:
                                self.links[self._current_name] = list()
        elif tag == 'tr':
            self.__td_count = 0
            self.__mtime_str = None
        elif tag == 'td':
            self.__td_count += 1
        #print(f"handle_starttag =====> ({self._is_directory}, {self._is_file}, {self._to_ignore})")

    def handle_endtag(self, tag):
        if tag == 'tr':
            if self._is_directory:
                self._is_directory = False
        elif tag == 'td':
            if self.__td_count == 4:
                self.__td_count = 0

    def get_files(self):
        return self.links

    def set_mtime_pattern(self, pattern):
        self.__mtime_pattern = re.compile(pattern)

    def set_size_pattern(self, pattern):
        self.__size_pattern = re.compile(pattern)


class FileExtractor_Jasmin(FileExtractor_UniBremen):

    @staticmethod
    def get_new_dcheck_url(old_dcheck_url, path):
        return old_dcheck_url + '/' + path + '/'


class FileExtractor_Nomads(FileExtractor_UniBremen):

    DATE_FORMAT = '%d-%b-%Y %H:%M'

    def __init__(self):
        super().__init__()
        self.set_mtime_pattern(' *([0-9]{2}-[a-zA-Z]{3}-[0-9]{4} [0-9]{2}:[0-9]{2}) *')


class FileExtractor_UniHamburg(html.parser.HTMLParser): #thredds

    DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
    FACTOR = {'bytes': 1,
              'Kbytes': 10E3,  # 1024
              'Mbytes': 10E6,
              'Gbytes': 10E9,
              'Tbytes': 10E12,
              'Pbytes': 10E15}

    @staticmethod
    def get_download_url(dcheck_url):
        url = os.path.split(dcheck_url)[0].replace('catalog', 'fileServer')
        return url

    @staticmethod
    def get_new_dcheck_url(old_dcheck_url, path):
        url = os.path.split(old_dcheck_url)
        return url[0] + '/' + path.rstrip('/') + '/' +  url[1]

    @staticmethod
    def get_recurcive():
        return False

    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.links = {}
        self._to_ignore = False
        self._is_file = False
        self._current_name = ''
        self.__mtime_pattern = re.compile(' *([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z) *')
        self.__size_pattern = re.compile(' *([0-9.]* [a-zA-z]*) *')
        self.__td_count = 0
        self.__size_str = None
        self.__is_value = False

    def handle_data(self, data):
        mtime_str = ''
        #print(f"handle_data : {self._current_name} :{data} (count = {self.__td_count})")
        if not self._to_ignore and self.__is_value and len(data) > 0:
            if self._current_name == '' and  self.__td_count == 1:
                self._current_name = data
            if self._current_name != '':
                if self.__td_count == 2:
                    if self._is_file:
                        mg = self.__size_pattern.match(data)
                        if mg is not None:
                            self.__size_str = mg.groups()[0]
                    else:
                        self.__size_str = '0'
                elif self.__td_count == 3:
                    mg = self.__mtime_pattern.match(data)
                    if mg is not None:
                        mtime_str = mg.groups()[0]
                    if self._is_file:
                        isdirectory = 0
                        self.links[self._current_name] = \
                            (isdirectory,mtime_str, self.__size_str)
                        #print("handle_data =====> FILE name : %s, mtime : %s, size : %s" % (
                        #    self._current_name, mtime_str, self.__size_str))
                    else:
                        # skip current directory entry
                        if self._current_name[-1:] == '/':
                            isdirectory = 1
                            self.links[self._current_name] = (isdirectory, mtime_str, self.__size_str)
                            #print("handle_data =====> DIR  name : %s, mtime : %s"%(self._current_name, mtime_str))


    def handle_starttag(self, tag, attrs):
        #print(f"handle_starttag =====> {tag} = {attrs} (count = {self.__td_count})")
        if tag == 'img' and self.__td_count == 1:
            if len(attrs) > 0:
                for attr in attrs:
                    if attr[0] == 'alt':
                        if attr[1] == '[DIR]' or attr[1] == 'Folder':
                            self._to_ignore = False
                            self._is_file = False
                        elif attr[1] == '[PARENTDIR]' or not attr[1].startswith('['):
                            self._to_ignore = True
                            self._is_file = False
                        else:
                            self._is_file = True
                            self._to_ignore = False
        elif tag == 'tr':
            self.__td_count = 0
            self._to_ignore = False
            self.__mtime_str = None
            self._is_file = True
            self._current_name = ''
        elif tag == 'td':
            self.__td_count += 1
        elif tag == 'tt':
            self.__is_value = True
        #print(f"handle_starttag =====> ({self._is_file}, {self._to_ignore})")

    def handle_endtag(self, tag):
        if tag == 'tr':
            self._to_ignore = True
        elif tag == 'tt':
            self.__is_value = False

    def get_files(self):
        return self.links

    def set_mtime_pattern(self, pattern):
        self.__mtime_pattern = re.compile(pattern)

    def set_size_pattern(self, pattern):
        self.__size_pattern = re.compile(pattern)


class FileExtractor_RdaUcar_Thredds(html.parser.HTMLParser):

    DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
    FACTOR =  {'o': 1,
               'K': 1 << 10,  # 1024
               'M': 1 << 20,
               'G': 1 << 30,
               'T': 1 << 40,
               'P': 1 << 50,
               'bytes': 1,
               'Kbytes': 10E3,  # 1024
               'Mbytes': 10E6,
               'Gbytes': 10E9,
               'Tbytes': 10E12,
              ' Pbytes': 10E15}

    @staticmethod
    def get_download_url(dcheck_url):
        url = urlparse(dcheck_url)
        # replace "/thredds/catalog/files/g/" by "/data/" and suppress "/catalog.html" at the end
        return urlunparse(url._replace(path='/data/' + '/'.join(url.path.split('/')[5:-1])))

    @staticmethod
    def get_new_dcheck_url(old_dcheck_url, path):
        return urljoin(old_dcheck_url, path)

    @staticmethod
    def get_recurcive():
        return False

    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.links = {}
        self._to_ignore = False
        self._is_directory = False
        self._is_file = False
        self._current_name = ''
        self.__mtime_pattern = re.compile(' *([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z) *')
        self.__size_pattern = re.compile(' *([0-9-.]* [a-zA-Z]*) *')
        self.__td_count = 0
        self.__mtime_str = ''
        self.__size_str = '0'

    def handle_data(self, data):
        pass
        #print(f"handle_data : {self._current_name} :{data} (count = {self.__td_count})")
        if not self._to_ignore:
            if len(data.strip()) > 0 and self._current_name != '':
                if self.__td_count == 2:
                    if self._is_file:
                        mg = self.__size_pattern.match(data)
                        if mg is not None:
                            self.__size_str = mg.groups()[0]
                elif self.__td_count == 3:
                    mg = self.__mtime_pattern.match(data)
                    if mg is not None:
                        self.__mtime_str = mg.groups()[0]
                    if self._is_file:
                        isdirectory = 0
                        #print("handle_data =====> FILE name : %s, mtime : %s, size : %s" % (self._current_name, self.__mtime_str, size_str))
                    else:
                        isdirectory = 1
                        #print("handle_data =====> DIR  name : %s, mtime : %s"%(self._current_name, self.__mtime_str))
                    self.links[self._current_name] = (isdirectory, self.__mtime_str, self.__size_str)

    def handle_starttag(self, tag, attrs):
        #print(f"handle_starttag =====> {tag} = {attrs} (count = {self.__td_count})")
        if tag == 'img' and self.__td_count == 1:
            if len(attrs) > 0:
                for attr in attrs:
                    if attr[0] == 'alt':
                        if attr[0] == 'alt':
                            if attr[1] == '[DIR]' or attr[1] == 'Folder':
                                self._to_ignore = False
                                self._is_file = False
                                self._is_directory = True
                            elif attr[1] == '[PARENTDIR]' or not attr[1].startswith('['):
                                self._to_ignore = True
                                self._is_file = False
                                self._is_directory = True
                            else:
                                self._is_directory = False
                                self._is_file = True
                                self._to_ignore = False
                        else:
                            self._is_directory = False
                            self._is_file = True
                            self._to_ignore = False
        elif tag == 'a' and self.__td_count == 1:
            if not self._to_ignore:
                if len(attrs) > 0:
                    for attr in attrs:
                        if attr[0] == 'href':
                            if attr[1][0:1] == '/':
                                continue
                            self._current_name = attr[1]
                            self._current_name = self._current_name.rstrip('/')
                            if self._is_directory:
                                self.links[self._current_name] = tuple()
                            if self._is_file:
                                self._current_name = os.path.split(self._current_name)[1]
                                self.links[self._current_name] = tuple()
        elif tag == 'tr':
            self.__td_count = 0
            self.__mtime_str = None
            self.__size_str = '0'
            self._is_directory = False
            self._is_file = True
            self._to_ignore = False
        elif tag == 'td':
            self.__td_count += 1

        #print(f"handle_starttag =====> ({self._is_directory}, {self._is_file}, {self._to_ignore})")

    def handle_endtag(self, tag):
        if tag == 'tr':
            if self._is_directory:
                self._is_directory = False
        elif tag == 'td':
            if self.__td_count == 3:
                self.__td_count = 0

    def get_files(self):
        return self.links

    def set_mtime_pattern(self, pattern):
        self.__mtime_pattern = re.compile(pattern)

    def set_size_pattern(self, pattern):
        self.__size_pattern = re.compile(pattern)

class FileExtractor_RdaUcar_FileList(html.parser.HTMLParser):

    DATE_FORMAT = '%Y-%m-%d'
    FACTOR = {'o': 1}

    @staticmethod
    def get_download_url(dcheck_url):
        url = urlparse(dcheck_url)
        return urlunparse(url._replace(path='', query=''))

    @staticmethod
    def get_new_dcheck_url(old_dcheck_url, path):
        return urljoin(old_dcheck_url, path)

    @staticmethod
    def get_recurcive():
        return True

    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.links = {}
        self._to_ignore = True
        self._is_directory = False
        self._is_file = False
        self._current_name = ''
        self.__mtime_pattern = re.compile(' *([0-9]{4}-[0-9]{2}-[0-9]{2}) *')
        self.__size_pattern = re.compile(' *([0-9-.]*) *')
        self.__td_count = 0
        self.__mtime_str = ''
        self.__size_str = '0'
        self.__next = False

    def handle_data(self, data):
        pass
        #print(f"handle_data : {self._current_name} :{data} (count = {self.__td_count})")
        if not self._to_ignore:
            if len(data.strip()) > 0 and self._current_name != '':
                if self.__td_count == 3:
                    if self._is_file:
                        mg = self.__size_pattern.match(data.strip())
                        if mg is not None:
                            self.__size_str = mg.groups()[0]
                elif self.__td_count == 5:
                    mg = self.__mtime_pattern.match(data.strip())
                    if mg is not None:
                        self.__mtime_str = mg.groups()[0]
                    if self._is_file:
                        isdirectory = 0
                        #print("handle_data =====> FILE name : %s, mtime : %s, size : %s" % (self._current_name, self.__mtime_str, size_str))
                    else:
                        isdirectory = 1
                        #print("handle_data =====> DIR  name : %s, mtime : %s"%(self._current_name, self.__mtime_str))
                    self.links[self._current_name] = (isdirectory, self.__mtime_str, self.__size_str)
        if self.__next and self.__td_count == 0 and len(data.strip()) > 0:
            if data.strip() == 'Next':
                self.links[self._current_name] = (True, '', '0')
                self.__next = False

    def handle_starttag(self, tag, attrs):
        #print(f"handle_starttag =====> {tag} = {attrs} (count = {self.__td_count})")
        if tag == 'a' and self.__td_count == 2 and self._current_name == '':
            if len(attrs) > 0:
                for attr in attrs:
                    if attr[0] == 'href':
                        self._current_name = attr[1].lstrip('/')
                        self._is_directory = False
                        self._is_file = True
                        self._to_ignore = False
        elif tag == 'a' and not self.__next and self.__td_count == 0:
            if len(attrs) > 3:
                if attrs[3][1] == 'Next':
                    next_page = attrs[2][1]
                    match = re.search(r'(\?page=[0-9]*)', next_page)
                    if match:
                        self._current_name = match[0]
                        self.__next = True
        elif tag == 'tr':
            self.__td_count = 0
            self.__mtime_str = None
            self.__size_str = '0'
            self._is_directory = False
            self._is_file = True
            self._to_ignore = False
            self._current_name = ''
        elif tag == 'td':
            self.__td_count += 1

        #print(f"handle_starttag =====> ({self._is_directory}, {self._is_file}, {self._to_ignore})")

    def handle_endtag(self, tag):
        if tag == 'tr':
            if self._is_directory:
                self._is_directory = False
        elif tag == 'td':
            if self.__td_count == 5:
                self.__td_count = 0

    def get_files(self):
        return self.links

    def set_mtime_pattern(self, pattern):
        self.__mtime_pattern = re.compile(pattern)

    def set_size_pattern(self, pattern):
        self.__size_pattern = re.compile(pattern)


class FileExtractor_HwrfTrak(html.parser.HTMLParser):

    DATE_FORMAT = '%d-%b-%Y %H:%M'
    FACTOR =  {'o': 1,
               'K': 1 << 10,  # 1024
               'M': 1 << 20,
               'G': 1 << 30,
               'T': 1 << 40,
               'P': 1 << 50}

    @staticmethod
    def get_download_url(dcheck_url):
        return dcheck_url

    @staticmethod
    def get_new_dcheck_url(old_dcheck_url, path):
        return old_dcheck_url + '/' + path

    @staticmethod
    def get_recurcive():
        return False

    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.links = {}
        self._to_ignore = True
        self._is_directory = False
        self._is_file = False
        self._current_name = ''
        self.__mtime_size_pattern = re.compile(' *([0-9]{2}-[A-Z][a-z]*-[0-9]{4} [0-9]{2}:[0-9]{2}) *([0-9.-]+[A-Z]*)')

    def handle_data(self, data):
        size_str = None
        mtime_str = None
        if not self._to_ignore:
            if len(data) > 0 and self._current_name != '':
                mg = self.__mtime_size_pattern.match(data)
                if mg is not None and len(mg.groups()) == 2:
                    mtime_str = mg.groups()[0]
                    size_str = mg.groups()[1]
                    if size_str == '-':
                        size_str = '0'
                    self.links[self._current_name] = \
                        (self._is_directory, mtime_str, size_str)
            self._to_ignore = True
            self._current_name = ''

    def handle_starttag(self, tag, attrs):
        if tag == 'a' and self._to_ignore:
            if len(attrs) > 0:
                for attr in attrs:
                    if attr[0] == 'href':
                        if attr[1][0:1] == '/':
                            continue
                        self._current_name = attr[1]
                        if self._current_name[-1] == '/':
                            self._is_directory = True
                            self._is_file = False
                        else:
                            self._is_directory = False
                            self._is_file = True
                        self._current_name = self._current_name.rstrip('/')

    def handle_endtag(self, tag):
        if tag == 'a':
            if self._current_name:
                self._to_ignore = False

    def get_files(self):
        return self.links
