

import logging
import os
import socket
import time
import urllib.error
import urllib.parse
import urllib.request

from eo_dataflow_manager.dchecktools.common.basefileinfos import File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_ConnectionError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

DEFAULT_SERVER_PORT = 80
DEFAULT_USERNAME = ''
DEFAULT_PASSWORD = ''

log = logging.getLogger('http_ostia')
log.setLevel(logging.INFO)


LOOK_FOR_LAST_N_DAYS = 15


class Protocol_http_ostia(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(60)

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server_address = None

        self.check_infos = []
        self._checkable_infos = []

        self.__current_path = None
        self.__current_dbfile = None

        self.__tmpdirBlocs = {}
        self.__session = None

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username
            raise DC_ConfigError(
                'Configuration error : username is not used for http_ostia protocol')

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password
            raise DC_ConfigError(
                'Configuration error : password is not used for http_ostia protocol')

        # check for required config
        if config.path is not None:
            self.path = config.path
        else:
            raise DC_ConfigError('Configuration error : missing path')

        if config.server_address is not None:
            self.server = config.server_address
        else:
            raise DC_ConfigError(
                'Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

    def setDirectoryFilter(self, directoryFilter):
        # TODO : avertir que l'on ne gere pas le directoryfilter... verifier s'il est egal a celui par defaut ?
        #        if directoryFilter:
        # raise DC_ConfigError, "Configuration error : directory filter is not
        # used for http_ostia protocol"

        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        #        if fileFilter:
        # raise DC_ConfigError, "Configuration error : file filter is not used
        # for http_ostia protocol"

        self.fileFilter = fileFilter

    def getFileInfoList(self):

        self.path = 'data/OSTIA'
        self.server_address = 'ghrsst-pp.metoffice.com'

        baseurl = 'http://%s/%s/' % (self.server_address, self.path)

        # build url's files list
        url_list = []
        for i in range(0, LOOK_FOR_LAST_N_DAYS):
            date = time.localtime(time.time() - i * 86400)
            file_url = time.strftime(
                '%Y/%m/%Y%m%d-UKMO-L4UHfnd-GLOB-v01.nc.bz2', date)
            # CPR : evol nouveau format ghrsst
            #file_url = time.strftime("%Y/%j/%Y%m%d%H%M%S-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB-v02.0-fv02.0.nc", date)
            url_list += [file_url]

        # for each url, verify the file availability
        files_to_insert = []
        for file_url in url_list:
            self.nbr_walked_files += 1

            url = os.path.join(baseurl, file_url)
            #req = urllib2.Request(url)
            try:
                urllib.request.urlopen(url)
            except urllib.error.HTTPError as e:
                if e.code == 404:
                    pass
                else:
                    raise
            except urllib.error.URLError as msg:
                error = DC_ConnectionError(
                    'http_ostia url error : %s (url=%s)' %
                    (msg.reason, url), db=self.fileInfoDatabase)
                log.error(error)

            else:
                files_to_insert.append(File(
                    id_execution=None,
                    filename=url,
                    isDirectory=False,
                    isSymLink=False,
                    size=None,
                    mtime=None,
                    sensingtime=None,
                ))

        if files_to_insert:
            self.fileInfoDatabase.addFileList(files_to_insert)
        self.updateValidExecutionStatus(True)
