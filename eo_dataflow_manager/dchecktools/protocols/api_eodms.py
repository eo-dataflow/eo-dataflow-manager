import json
import logging
import os
import socket
import time
from datetime import datetime
from pathlib import Path

import dateparser
import simplejson as json
from eodms_rapi import EODMSRAPI
from lxml import etree

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR, File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_UrlError
from eo_dataflow_manager.dchecktools.plugins import DataReaderFactory
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

log = logging.getLogger('api_eodms')
log.setLevel(logging.DEBUG)

DEFAULT_USERNAME = ''
DEFAULT_PASSWORD = ''
DEFAULT_DATE_FORMAT = '%Y%m%d_%H%M%S'
DEFAULT_SORT_KEY = 'start_date'

class Protocol_api_eodms(AbstractProtocol):

    def __init__(self, options):
        AbstractProtocol.__init__(self)

        socket.setdefaulttimeout(self.getDefaultTimeout())

        # default config
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server_address = None
        self.provider = None
        self.short_name = None
        self.start_date = None
        self.end_date = None
        self.updated_since = None
        self.date_format = DEFAULT_DATE_FORMAT
        self.sort_key = DEFAULT_SORT_KEY
        self.bounding_box = None
        self.geometry = None
        self.geotemporal_list = None
        self.geo_temporal = None

        self.collection = None
        self.filters = None
        self.features = None
        self.dates = None
        self.result_fields = None
        self.max_results = 1000
        self.first_result = None
        self.hit_count = False


        self.check_infos = ['size', 'mtime', 'sensingtime']
        self._checkable_infos = ['size', 'mtime']


    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # check for required config
        if config.path is None:
            raise DC_ConfigError('Configuration error : missing path')
        if config.server_address is None:
            raise DC_ConfigError(
                'Configuration error : missing server')
        if 'collection' not in config.protocol_option:
            raise DC_ConfigError(
                'Configuration error : missing collection option protocol')

        # overwrite default config
        self.path = config.path
        self.server_address = config.server_address

        self.collection = config.protocol_option['collection']
        if 'filters' in config.protocol_option:
            self.filters = config.protocol_option['filters']

        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

        if config.data_reader:
            self.dataReader = DataReaderFactory.DataReaderFactory(
                config.data_reader,
                log.name,
                config.regexp_date,
                config.date_format,
                config.storage_path,
                config.dir_prefix,
                0)
            log.debug('data_reader : %s' % config.data_reader)

        # add option protocol values if needed
        if 'StartDate' in config.protocol_option and config.protocol_option['StartDate']:
            self.start_date = dateparser.parse(config.protocol_option['StartDate'])
        if 'EndDate' in config.protocol_option and config.protocol_option['EndDate']:
            self.end_date = dateparser.parse(config.protocol_option['EndDate'])
        if 'BoundingBox' in config.protocol_option and config.protocol_option['BoundingBox']:
            self.bounding_box = config.protocol_option['BoundingBox']
        if 'Polygon' in config.protocol_option and config.protocol_option['Polygon']:
            self.geometry = config.protocol_option['Polygon']

        if config.geotemporal_list is not None and config.geotemporal_list != '':
            self.geotemporal_list = self.verify_geotemporal(config.geotemporal_list)
            if self.geotemporal_list is None:
                raise DC_ConfigError('Configuration error : geo temporal list is not valid')

    def verify_geotemporal(self, source: str):
        try:
            if source.strip().startswith('[') and source.strip().endswith(']'):
                geo_temporal = json.loads(source)
            else:
                file_list = Path(source)
                if not file_list.is_file:
                    log.warning(f'{source} is not a file.')
                    return None
                with open(file_list, 'r') as fd:
                    geo_temporal = json.load(fd)
            result_liste = []
            for elt in geo_temporal:
                geometry = elt.get('geometry', None)
                bbox = elt.get('bounding_box', None)
                start = elt.get('start_date', None)
                stop = elt.get('end_date', None)

                if (not geometry and not bbox):
                    error = f'Geo-temporal configuration error : geometry or bounding_box must be defined (elt :{elt})'
                    log.warning(error)
                    return None
                if not start or not stop:
                    error = f'Geo-temporal configuration error : start_date and end_date must be defined (elt :{elt})'
                    log.warning(error)
                    return None

                # if geometry and geometry.strip().startswith('POLYGON'):

                result_liste.append(
                    (
                        str(geometry),
                        None if geometry else str(bbox),
                        dateparser.parse(start),
                        dateparser.parse(stop),
                    )
                )

            return result_liste
        except json.JSONDecodeError as error:
            log.warning('Geo-temporal configuration error : {}'.format(error))
            return None

    def get_geotemporals(self):
        if self.geotemporal_list is not None:
            return self.geotemporal_list
        return [(self.geometry, self.bounding_box, self.start_date, self.end_date)]

    def setDirectoryFilter(self, directoryFilter):
        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        self.fileFilter = fileFilter

    def get_files_from_download_results(self, download_results):
        files  = []

        for result in download_results:
            downloadPaths = result['downloadPaths']
            for downloadPath in downloadPaths:
                file_name = downloadPath['local_destination']
                size = os.stat(file_name).st_size
                files.append((file_name, size, ''))

        return files

    def _get_file_info(self, order):
        dests = order['destinations']
        manifest_key = list(order['manifest'].keys()).pop()
        size = int(order['manifest'][manifest_key])

        mtime = datetime.utcnow()


        for d in dests:
            # Get the string value of the destination
            str_val = d['stringValue']
            str_val = str_val.replace('</br>', '')

            str_val = str_val.replace('&', '?')

            # Parse the HTML text of the destination string
            root = etree.fromstring(str_val)
            url = root.text
            # url = root.tostring(root)
            url = url.split('?')[0]

            os.path.basename(url)

            name = order['name']
            sensing_time, insert = self.getSensingTime(name)

            order['ready'] = True

        return name, url , size, mtime, sensing_time


    def getFileInfoList(self):
        try:
            rapi = EODMSRAPI(self.username, self.password)
            files_already_downloaded = {}
            selected_files = []
            geotemporals = self.get_geotemporals()
            for geometry, bounding_box, start_date, end_date in geotemporals:
                # find files already downloaded to avoid ordering again and merging with previous files
                already_downloaded = self.fileInfoDatabase.req_ListFilesBetweenDates(start_date, end_date)
                files_already_downloaded.update({file.filename.split(FILE_URL_SEPARATOR)[0]: file
                                                 for file in already_downloaded})
                # Selection of products following geotemporal filter and other filtering
                temp_dates = {}
                if start_date:
                    temp_dates['start'] = datetime.strftime(start_date, self.date_format)
                if end_date:
                    temp_dates['end'] = datetime.strftime(end_date, self.date_format)
                if temp_dates == {}:
                    dates = None
                else:
                    dates = [temp_dates]
                if geometry:
                    features = ('intersects', geometry)
                else:
                    features = None
                log.debug('collection: {}, filters: {}, features: {}, dates: {}'
                          .format(self.collection, self.filters, features, dates))
                rapi.search(self.collection, filters=self.filters,
                                 features=features, dates=dates,
                                 result_fields=self.result_fields, max_results=self.max_results,
                                 first_result=self.first_result, hit_count=self.hit_count)

            # Result of selection
            rapi.set_field_convention('upper')
            selected_files += rapi.get_results('full', show_progress=False)
            rapi.clear_results()

            # Construction of the purged list of products to be ordered
            products_already_downloaded = []
            files_already_ordered = []
            products_to_order = []
            for record in selected_files:
                filename = record['TITLE']
                if filename in files_already_ordered:
                    continue
                if filename in files_already_downloaded:
                    if files_already_downloaded[filename] not in products_already_downloaded:
                        products_already_downloaded.append(files_already_downloaded[filename])
                    continue
                if self.fileFilter.isInteresting(filename):
                    products_to_order.append(record)
                    files_already_ordered.append(filename)

            files_to_insert = []
            orders_ready_for_download = []
            if len(products_to_order) != 0:
                # order products
                order_result = rapi.order(products_to_order)
                final_order_result = rapi.remove_duplicate_orders(order_result['items'])
                files_in_error = 0
                while len(final_order_result) > (len(files_to_insert) + files_in_error):
                    time.sleep(60)
                    files_in_error = 0
                    order_details = rapi.get_orders(final_order_result)
                    for order in order_details:
                        if order['status'] == 'AVAILABLE_FOR_DOWNLOAD' \
                                and order['recordId'] not in orders_ready_for_download:
                            orders_ready_for_download.append(order['recordId'])
                            name, url , size, mtime, sensing_time = self._get_file_info(order)
                            files_to_insert.append(File(id_execution=None,
                                                    filename=name + FILE_URL_SEPARATOR + url,
                                                    isDirectory=False,
                                                    isSymLink=False,
                                                    size=size,
                                                    mtime=mtime,
                                                    sensingtime=sensing_time
                                                    ))
                        if order['status'] in rapi.failed_status:
                            files_in_error += 1

            if len(products_already_downloaded) != 0:
                files_to_insert += [File(id_execution=None,
                                         filename=file.filename,
                                         isDirectory=file.isDirectory,
                                         isSymLink=file.isSymLink,
                                         size=file.size,
                                         mtime=file.mtime,
                                         sensingtime=file.sensingtime
                                         )
                                    for file in products_already_downloaded]

            if files_to_insert:
                self.fileInfoDatabase.addFileList(files_to_insert)

        except DC_UrlError as msg:
            log.debug(
                'https_iteration error : %s. Stopping iteration.' %
                (str(msg)))
            raise msg

        self.updateValidExecutionStatus(True)
