
import json
import logging
import socket
import time
import urllib.error
import urllib.parse
import urllib.request
from datetime import datetime
from pathlib import Path

import dateparser

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR, File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_Error, DC_ExtractingError, DC_UrlError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol
from eo_dataflow_manager.ifr_lib_modules.ifr_exception import GenericException

DEFAULT_SERVER_PORT = 443
DEFAULT_USERNAME = ''
DEFAULT_PASSWORD = ''
DEFAULT_PAGE_SIZE = 2000
DEFAULT_DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
DEFAULT_SORT_KEY = 'start_date'


log = logging.getLogger('https_xx_daac')
log.setLevel(logging.DEBUG)


class Protocol_https_xx_daac(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(self.getDefaultTimeout())

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server_address = None
        self.provider = None
        self.short_name = None
        self.page_size = DEFAULT_PAGE_SIZE
        self.start_date = None
        self.end_date = None
        self.updated_since = None
        self.date_format = DEFAULT_DATE_FORMAT
        self.sort_key = DEFAULT_SORT_KEY
        self.bounding_box = None
        self.geometry = None
        self.geotemporal_list = None
        self.geo_temporal = None

        self.check_infos = ['size', 'mtime', 'sensingtime']
        self._checkable_infos = ['size', 'mtime']

        self.__current_path = None

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # check for required config
        if config.path is None:
            raise DC_ConfigError('Configuration error : missing path')
        if config.server_address is None:
            raise DC_ConfigError(
                'Configuration error : missing server')
        if 'Provider' not in config.protocol_option:
            raise DC_ConfigError(
                'Configuration error : missing Provider option protocol')
        if 'ShortName' not in config.protocol_option:
            raise DC_ConfigError(
                'Configuration error : missing ShortName option protocol')

        # overwrite default config
        self.path = config.path
        self.server_address = config.server_address
        self.short_name = config.protocol_option['ShortName']
        self.provider = config.protocol_option['Provider']

        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

        # add option protocol values if needed
        if 'DateFormat' in config.protocol_option:
            self.date_format = config.protocol_option['DateFormat']
        if 'PageSize' in config.protocol_option:
            self.page_size = config.protocol_option['PageSize']
        if 'SortKey' in config.protocol_option:
            self.sort_key = config.protocol_option['SortKey']

        if 'StartDate' in config.protocol_option and config.protocol_option['StartDate']:
            self.start_date = datetime.strftime(dateparser.parse(config.protocol_option['StartDate']),
                                                self.date_format)
        if 'EndDate' in config.protocol_option and config.protocol_option['EndDate']:
            self.end_date = datetime.strftime(dateparser.parse(config.protocol_option['EndDate']),
                                              self.date_format)
        if 'UpdatedSince' in config.protocol_option and config.protocol_option['UpdatedSince']:
            self.updated_since = datetime.strftime(dateparser.parse(config.protocol_option['UpdatedSince']),
                                                   self.date_format)
        if 'BoundingBox' in config.protocol_option and config.protocol_option['BoundingBox']:
            self.bounding_box = config.protocol_option['BoundingBox']
        if 'Polygon' in config.protocol_option and config.protocol_option['Polygon']:
            self.geometry = config.protocol_option['Polygon']
        if config.geotemporal_list is not None and config.geotemporal_list != '':
            self.geotemporal_list = self.verify_geotemporal(config.geotemporal_list)
            if self.geotemporal_list is None:
                raise DC_ConfigError('Configuration error : geo temporal list is not valid')

    def setDirectoryFilter(self, directory_filter):
        self.directoryFilter = directory_filter

    def setFileFilter(self, file_filter):
        self.fileFilter = file_filter

    def getURL(self, geometry: str, bounding_box: str, start_date: str, end_date: str) -> str:
        url = self.server_address.rstrip('/') + '/' + self.path.lstrip('/')
        if not url.startswith('http'):
            url = 'https://' + url

        url = url + '?page_size=' + str(
            self.page_size) + '&echo_compatible=true'
        # see: https://cmr.earthdata.nasa.gov/search/site/docs/search/api.html#query-parameters

        if self.sort_key:
            url = url + '&sort_key=' + self.sort_key
        if self.updated_since:
            url = url + '&updated_since=' + self.updated_since
        if self.start_date or self.end_date:
            url = url + '&temporal[]=' + self.__get_temporal_range(start_date, end_date)
        if self.provider != '':
            url = url + '&provider=' + self.provider
        if self.short_name != '':
            url = url + '&short_name=' + self.short_name
        if bounding_box and bounding_box != 'None':
            url = url + '&bounding_box=' + bounding_box
        if geometry and geometry != 'None':
            url = url + '&polygon=' + geometry

        return url

    def getFileInfoList(self):
        try:
            files = []
            geotemporals = self.get_geotemporals()
            for geometry, bounding_box, start_date, end_date in geotemporals:
                url = self.getURL(geometry, bounding_box, start_date, end_date)
                files += self.https_get_files_from_po_daac_json(
                    url=url,
                    timeout=self.getDefaultTimeout()
                )

            files = list(set(files))
            files_to_insert = []
            try:
                for filename in files:
                    filepath = filename[0].split(FILE_URL_SEPARATOR)[0]
                    try:
                        mtime = time.mktime(
                        time.strptime(
                            filename[2].split('.')[0], '%Y-%m-%dT%H:%M:%S'))
                    except ValueError:
                        try:
                            mtime = time.mktime(time.strptime(
                                filename[2],
                                '%Y %m %dT%H:%M:%S'))
                        except:
                            raise

                    interestingFile = self.fileFilter.isInteresting(
                        filepath,
                        mtime)
                    log.debug(
                        'file %s is interesting ? %s' %
                        (filepath, interestingFile))

                    # add sensingtime if necessairy
                    # sensingtime, insert = self.getSensingTime(filepath)

                    if interestingFile:
                        self.nbr_walked_files += 1
                        files_to_insert.append(File(
                            # id_execution=None,
                            filename=filename[0],
                            isDirectory=False,
                            isSymLink=False,
                            size=filename[1],
                            mtime=datetime.fromtimestamp(mtime),
                            # sensingtime=sensingtime,
                        ))
            except StopIteration as msg:
                log.debug(
                    "getFileInfoList iteration stopped '%s'" %
                    (str(msg)))
                pass  # fin normale de l'iteration

            if files_to_insert:
                self.fileInfoDatabase.addFileList(files_to_insert)
        except DC_UrlError as msg:
            log.debug(
                'https_iteration error : %s. Stopping iteration.' %
                (str(msg)))
            raise msg

        self.updateValidExecutionStatus(True)

    def extract_files_from_json(self, data):

        files = []

        for item in data:
            file_name = None
            date_time = None
            size = 0
            try:
                for relatedUrl in item['umm']['RelatedUrls']:
                    if relatedUrl['Type'].upper() == 'GET DATA' and file_name is None:
                        HTTPS_url = relatedUrl['URL']
                        file_name = relatedUrl['URL'].split('/')[-1]
                for providerDate in item['umm']['ProviderDates']:
                    if providerDate['Type'].lower() == 'update':
                        date_time = providerDate['Date']
                    elif providerDate['Type'].lower() == 'insert':
                        date_time = providerDate['Date']
                    elif providerDate['Type'].lower() == 'create':
                        date_time = providerDate['Date']
                    else:
                        raise GenericException('Error Date')
                for infos in item['umm']['DataGranule']['ArchiveAndDistributionInformation']:
                    if infos['Name'] == file_name:
                        size = infos['SizeInBytes']

                files.append((
                    file_name + FILE_URL_SEPARATOR + HTTPS_url, size, date_time
                ))

            except NameError as error:
                msg = 'File information is not complete: %s (%s)' % (item['meta']['native-id'], error)
                raise DC_ExtractingError(msg)
            except UnboundLocalError as error:
                msg = 'File information is not complete: %s (%s)' % (item['meta']['native-id'], error)
                raise DC_ExtractingError(msg)
            except Exception as error:
                msg = 'ERROR : %s' % error
                raise DC_Error(msg)

        return files

    def https_get_files_from_po_daac_json(self, url='', timeout=120):
        log.debug('https_xx_daac : url=%s' % url)
        CMR_Search_After = ' '
        final_items = []
        try:
            opener = urllib.request.build_opener()

            while CMR_Search_After:
                opener.addheaders = [('CMR-Search-After', CMR_Search_After)]

                with opener.open(url, timeout=60) as response:
                    CMR_Search_After = response.getheader('CMR-Search-After')

                    data = response.read().decode('utf-8')

                data_json = json.loads(data)
                final_items.extend(data_json['items'])

        except urllib.error.URLError as msg:
            error = DC_UrlError('URL/HTTP error : %s' % (msg))
            log.debug(error)
            raise error

        return self.extract_files_from_json(final_items)

    def __get_temporal_range(self, start_date, end_date):

        if start_date is not None and end_date is not None:
            return start_date + ',' + end_date
        if start_date is not None and end_date is None:
            return start_date + ','
        if start_date is None and end_date is not None:
            return ',' + end_date
        return ','

    def verify_geotemporal(self, source: str):
        try:
            if source.strip().startswith('[') and source.strip().endswith(']'):
                geo_temporal = json.loads(source)
            else:
                file_list = Path(source)
                if not file_list.is_file:
                    log.warning(f'{source} is not a file.')
                    return None
                with open(file_list, 'r') as fd:
                    geo_temporal = json.load(fd)
            result_liste = []
            for elt in geo_temporal:
                geometry = elt.get('geometry', None)
                bbox = elt.get('bounding_box', None)
                start = elt.get('start_date', None)
                stop = elt.get('end_date', None)

                if (not geometry and not bbox):
                    error = f'Geo-temporal configuration error : geometry or bounding_box must be defined (elt :{elt})'
                    log.warning(error)
                    return None
                if not start or not stop:
                    error = f'Geo-temporal configuration error : start_date and end_date must be defined (elt :{elt})'
                    log.warning(error)
                    return None

                if geometry and geometry.strip().startswith('POLYGON'):
                    geometry = geometry[len('POLYGON'):].strip('()').replace(' ', ',')
                result_liste.append(
                    (
                        str(geometry),
                        None if geometry else str(bbox),
                        datetime.strftime(dateparser.parse(start), self.date_format),
                        datetime.strftime(dateparser.parse(stop), self.date_format),
                    )
                )

            return result_liste
        except json.JSONDecodeError as error:
            log.warning('Geo-temporal configuration error : {}'.format(error))
            return None

    def get_geotemporals(self):
        if self.geotemporal_list is not None:
            return self.geotemporal_list
        return [(self.geometry, self.bounding_box, self.start_date, self.end_date)]
