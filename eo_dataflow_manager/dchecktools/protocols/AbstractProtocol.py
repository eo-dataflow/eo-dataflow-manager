
import logging
from datetime import datetime, timedelta

from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError

log = logging.getLogger('AbstractProtocol')
log.setLevel(logging.INFO)

DEFAULT_TIMEOUT = 120


class AbstractProtocol(object):
    """
    Abstract class used to define a Protocol object, whose role is to walk
    on a tree, and to populate the database with interesting files. Such an
    object has to take care about a fileFiter, a directoryFilter, a config,
    informations to check (and default checks), following symlinks and so on...
    """

    def __init__(self):
        # database to fill
        self.fileInfoDatabase = None

        # user configuration
        self.config = None

        # filters to take care of interesting files or directory only
        self.directoryFilter = None
        self.fileFilter = None

        # informations we have to check during the walk
        self.check_infos = []
        # a given protocol can access to specific infos... listing them
        # ensure that we will not have a look at unavailable informations
        # protected attribute because content is protocol-dependant, not
        # user-choices-dependant
        self._checkable_infos = []

        # protocol dependant option
        self.follow_symlink = None

        # internal flag
        self._tmp_valid_execution_state = None

        # used for summary text informations only
        self.nbr_walked_files = 0
        self.directories_smart_crawler = []

        self.path = None
        self.dataReader = None
        self.smart_crawler = False

        self.timeout = DEFAULT_TIMEOUT

        # unused any more...
        #self.last_directory_level = None
        #self.timer_level_dict = {}

    def setDataReader(self, dataReader):
        self.dataReader = dataReader
        self.check_infos.append('sensingtime')

    def setPath(self, path):
        self.path = path

    def setConfig(self, config):
        """ apply a configuration to protocol : set the informations to check,
        and the logger logLevel, and keep a ref to the config for other var"""
        self.config = config
        if self.config.check_infos is not None:
            self.check_infos = self.config.check_infos
        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

        # check for required config
        if config.path is not None:
            self.path = config.path
        else:
            raise DC_ConfigError('Configuration error : missing path')

        log.debug('self.path %s' % self.path)
        log.debug('self.config.smart_crawler_delta %s' %
                  self.config.smart_crawler_delta)
        log.debug('self.config.smart_crawler_directories_pattern %s' %
                  self.config.smart_crawler_directories_pattern)
        if self.config.smart_crawler_directories_pattern is not None and (
                self.config.smart_crawler_delta is not None or self.config.smart_crawler_mindate is not None):
            self.setDirectoriesSmartCrawller()

    def setDirectoriesSmartCrawller(self):
        if self.config.smart_crawler_maxdate is None:
            maxdate = datetime.now()
        else:
            maxdate = self.config.smart_crawler_maxdate

        if self.config.smart_crawler_mindate is not None:
            maxdelta = (maxdate - self.config.smart_crawler_mindate).days
        else:
            maxdelta = self.config.smart_crawler_delta

        for delta in range(0, int(maxdelta)):
            newdate = maxdate - timedelta(days=delta)
            pathDate = self.path + '/' + \
                newdate.strftime(self.config.smart_crawler_directories_pattern)
            try:
                self.directories_smart_crawler.index(pathDate)
            except ValueError:
                self.directories_smart_crawler.append(pathDate)

    def run(self):
        """ entry point of a protocol. Manage the execution state in database,
        run the getFileInfoList abstract method.
        """
        self.fileInfoDatabase.setValidExecution(False)
        self.smart_crawler = (len(self.directories_smart_crawler) > 0)
        if self.smart_crawler:
            for dir_sc in self.directories_smart_crawler:
                self.setPath(dir_sc)
                self.getFileInfoList()
        else:
            self.getFileInfoList()
        self.fileInfoDatabase.setCurrentExecutionStopDate()
        self.fileInfoDatabase.setValidExecution(self.getValidExecutionStatus())

    def getSensingTime(self, filename):
        date = None
        insert = True
        if 'sensingtime' in self.check_infos:
            try:
                date = self.dataReader.getDate(filename)
                if (date is not None and self.config.smart_crawler_maxdate is not None) and date > self.config.smart_crawler_maxdate:
                    insert = False

                if (date is not None and self.config.smart_crawler_mindate is not None) and date < self.config.smart_crawler_mindate:
                    insert = False
            except AttributeError:
                # TODO que faire en cas d'erreur
                pass
        return date, insert

#    def directoryChanged(self, path):
#        pass
#        display_walk_time_level = 1
#        base_level = len(self.config.path.split('/'))-1
#        print
#        if display_walk_time_level != 0:
#            #print path, len(path.split('/')), "max = ", display_walk_time_level
#            depth = len(path.split('/'))-base_level
#            if depth <= display_walk_time_level:
#                print path, 'depth=',depth, 'max_depth', display_walk_time_level                #print path.split('/')[base_level-1:display_walk_time_level+1]
#                dir_level = '/'.join(["%s"%(dir) for dir in path.split('/')[base_level:display_walk_time_level]])
#                if self.last_directory_level != dir_level:
#                    print "On change de level : ", dir_level
#                    if self.last_directory_level != None:
#                        elapsed = time.time()-self.timer_level_dict[self.last_directory_level]
#                        del self.timer_level_dict[self.last_directory_level]
#                        print "Elapsed time for dir %s : %.3fs"%(self.last_directory_level, elapsed)
#                    self.timer_level_dict[dir_level] = time.time()
#                    self.last_directory_level = dir_level

    def getFileInfoList(self):
        """
        Main protocol abstract method, to implement into each protocol.
        Manage the tree walk, add the informations (errors, files...) to db.
        """
        raise NotImplementedError

    def setFileInfoDatabase(self, database):
        """ set fileInfoDatabase. Method is used here to permit actions
        in subclasses if needed """
        self.fileInfoDatabase = database

    def setDirectoryFilter(self, directoryFilter):
        # force subclass to say if it uses this filter or not, by raising error
        # by default
        raise NotImplementedError

    def setFileFilter(self, fileFilter):
        # force subclass to say if it uses this filter or not, by raising error
        # by default
        raise NotImplementedError

    def getCheckInfos(self):
        """ returns the informations to check. Method is used here to permit actions
        in subclasses if needed """
        return self.check_infos

    def setTimeout(self, timeout):
        """ set method for  timeout, see method above for more info """
        self.timeout = timeout

    def setCheckInfos(self, check_infos):
        """ set the informations to check, verifying if each wanted info
        is compliant with the protocol checkable infos
        """
        for info in check_infos:
            if info not in self._checkable_infos:
                raise RuntimeError(
                    'Protocol %s does not support info : %s' %
                    (self.__class__.__name__, info))
        self.check_infos = check_infos

    def updateValidExecutionStatus(self, status, force=False):
        """ method to set the current execution state (valid or not).
        An execution is valid when we can consider its results as valid.
        For example, a ftp protocol which cannot connect to host has a valid
        execution (connect error managed), but results are not valid. Useful
        for the reports to know which executions contains good data.
        """
        if force:
            self._tmp_valid_execution_state = status
        else:
            if self._tmp_valid_execution_state or self._tmp_valid_execution_state is None:
                self._tmp_valid_execution_state = status

    def getValidExecutionStatus(self):
        """ get method for execution status, see method above for more info """
        if self._tmp_valid_execution_state is None:
            return False
        else:
            return self._tmp_valid_execution_state

    def getDefaultTimeout(self):
        """ get method for default timeout, see method above for more info """
        return self.timeout
