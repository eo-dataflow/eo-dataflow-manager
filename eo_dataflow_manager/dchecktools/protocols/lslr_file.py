

import logging
import os
import re

from eo_dataflow_manager.dchecktools.common.basefileinfos import File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

DEFAULT_USERNAME = 'anonymous'
DEFAULT_PASSWORD = 'anon@anon.org'

log = logging.getLogger('lslr_file')
log.setLevel(logging.INFO)


FILE_FORMAT_REGEXP = '([rwxdblcpsStT-]{10}) +([0-9]+) +(.*?) +(.*?) +([0-9]*?) +([a-zA-Z]{3} +[0-9-]* [0-9:]*) (.*)\n?'
# rights             links   user   group    size     date (ex: Sep 17
# 11:04)          name
FILE_FORMAT_REGEXP2 = '([rwxdlcpsStT-]{10}) +([0-9]+) +(.*?) +(.*?) +([0-9]*?) +([0-9-]* [0-9:]*) (.*)\n?'
# rights             links   user   group    size     date (ex: 2007-09-25
# 15:14)          name
DIR_FORMAT_REGEXP = '(.*):\n'
LNK_FORMAT_REGEXP = '(.*) -> (.*)'
lnk_regexp = re.compile(LNK_FORMAT_REGEXP)


DEFAULT_SERVER_PORT = 21
DEFAULT_USERNAME = 'anonymous'
DEFAULT_PASSWORD = 'anon@anon.org'


def getLnkSrcDst(lnk_str):
    m_lnk_str = lnk_regexp.match(lnk_str)
    if m_lnk_str is not None:
        name_src, name_dst = m_lnk_str.groups()
    else:
        print('Devrait pas arriver !!')
    return name_src, name_dst


class Protocol_lslr_file(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server_address = None
        self.listing_type = 'unix'

        self.check_infos = ['size', 'mtime']
        self._checkable_infos = [
            'rights',
            'nb_links',
            'user',
            'group',
            'size',
            'mtime']  # rights vs mode ?

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        # check for required config
        if config.path is not None:
            self.path = config.path
        else:
            raise DC_ConfigError('Configuration error : missing path')

        if config.server_address is not None:
            self.server = config.server_address
        else:
            raise DC_ConfigError(
                'Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

    def setDirectoryFilter(self, directoryFilter):
        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        self.fileFilter = fileFilter

    def getFileInfoList(self):

        log.debug(
            'Start lslr_file parsing (directoryFilter = %s) :' %
            (self.directoryFilter))

        f = None
        current_dir = ''
        file_regexp = re.compile(FILE_FORMAT_REGEXP)
        file_regexp2 = re.compile(FILE_FORMAT_REGEXP2)
        dir_regexp = re.compile(DIR_FORMAT_REGEXP)

        files = []
        with open(self.path, 'r') as f:
            for line in f:
                line_recognized = False
                if line.startswith('total '):
                    continue
                if line.isspace():
                    continue

                m = file_regexp.match(line)
                if m is not None:
                    self.nbr_walked_files += 1
                    line_recognized = True
                    rights, links, user, group, size, datetime, name = m.groups()
                    isSymLink = False
                    if rights[0] == 'l':
                        isSymLink = True
                        name, dst = getLnkSrcDst(name)
                    path = os.path.join(current_dir, name)
                    # print "FILE_FMT_1 : rights=%s, links=%s, user=%s, groups=%s, size=%s, date=%s, name=%s, path=%s"%(rights,
                    #       links, user, group, size, datetime, name, path)
                    files.append(File(
                        id_execution=None,
                        filename=path,
                        isDirectory=False,
                        isSymLink=isSymLink,
                        size=size,
                        mtime=datetime.fromtimestamp(0),  # TODO !
                        sensingtime=None,
                    ))
                    continue

                m = file_regexp2.match(line)
                if m is not None:
                    self.nbr_walked_files += 1
                    line_recognized = True
                    rights, links, user, group, size, datetime, name = m.groups()
                    isSymLink = False
                    if rights[0] == 'l':
                        isSymLink = True
                        name, dst = getLnkSrcDst(name)
                    path = os.path.join(current_dir, name)
                    # print "FILE_FMT 2 : rights=%s, links=%s, user=%s, groups=%s, size=%s, date=%s, name=%s, path=%s"%(rights,
                    #       links, user, group, size, datetime, name, path)
                    files.append(File(
                        id_execution=None,
                        filename=path,
                        isDirectory=False,
                        isSymLink=isSymLink,
                        size=size,
                        mtime=datetime.fromtimestamp(0),  # TODO !
                        sensingtime=None,
                    ))
                    continue

                m = dir_regexp.match(line)
                if m is not None:
                    line_recognized = True
                    dirpath = m.groups()
                    dirpath = dirpath[0]
                    print('DIR_FMT : dir=%s' % (dirpath))
                    size = 0  # TODO
                    #self.__addFile(dirpath, size, datetime_value, isDirectory=True, isSymLink=False)
                    current_dir = dirpath
                    continue

                if not line_recognized:
                    print('NO MATCH  : %s' % (line))
                    continue

        self.updateValidExecutionStatus(True)
