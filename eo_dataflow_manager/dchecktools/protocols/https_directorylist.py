import html.parser
import http.cookiejar
import logging
import os
import re
import sys
import time
import urllib.error
import urllib.parse
import urllib.request
from datetime import datetime
from socket import timeout

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR, File
from eo_dataflow_manager.dchecktools.common.errors import (
    DC_ConfigError,
    DC_ConnectionError,
    DC_Error,
    DC_LocalpathError,
    DC_TimeoutError,
    DC_UrlError,
)
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

log = logging.getLogger('https_directorylist')
log.setLevel(logging.DEBUG)


def getNumericSize(size_str, factor_model=None):

    if factor_model is None:
        factor = {'o': 1,
                  'Ko': 1 << 10,  # 1024
                  'Mo': 1 << 20,
                  'Go': 1 << 30,
                  'To': 1 << 40,
                  'Po': 1 << 50}

    else:
        factor = factor_model

    size_pattern = re.compile('([0-9.]*) *([a-zA-Z]*)')
    mg = size_pattern.match(size_str)
    if mg is None or len(mg.groups()) < 2:
        raise Exception("Invalid pattern for size_st: '%s'" % size_str)
    value = float(mg.groups()[0])
    unit = mg.groups()[1]
    # on accepte le unit == '', qui correspond a l'octet
    if unit == '':
        unit = 'o'
    if value is None or value == '' or unit is None:
        raise Exception("invalid value or unit for size_str: '%s'" % size_str)
    if unit in list(factor.keys()):
        factor_value = factor[unit]
    else:
        raise Exception("invalid unit for size_str: '%s'" % size_str)
    value_final = round(value * factor_value)
    return value_final


class FileExtractor(html.parser.HTMLParser):

    DATE_FORMAT = '%d-%b-%Y %H:%M'
    FACTOR = {'o': 1,
              'K': 1 << 10,  # 1024
              'M': 1 << 20,
              'G': 1 << 30,
              'T': 1 << 40,
              'P': 1 << 50}

    @staticmethod
    def get_download_url(dcheck_url):
        return dcheck_url

    @staticmethod
    def get_new_dcheck_url(old_dcheck_url, path):
        return old_dcheck_url + '/' + path

    @staticmethod
    def get_recurcive():
        return False

    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.links = {}
        self._to_ignore = False
        self._is_directory = False
        self._is_file = False
        self._current_name = ''
        self.__fileinfo_pattern = re.compile(
            ' *([0-9]{2}-[a-zA-Z]{3}-[0-9]{4} [0-9]{2}:[0-9]{2}) *([0-9a-zA-Z.-]*) *.*')

    def handle_data(self, data):
        if not self._to_ignore:
            if len(data) > 0 and self._current_name != '':
                mg = self.__fileinfo_pattern.match(data)
                if mg is not None:
                    mtime_str = mg.groups()[0]
                    if self._is_file:
                        isdirectory = 0
                        size_str = mg.groups()[1]
                        self.links[self._current_name] = \
                            (isdirectory, mtime_str, size_str)
                    else:
                        # print "DIR  name : %s, mtime : %s"%(self._current_name, mg.groups()[0])
                        isdirectory = 1
                        size_str = '0'
                        self.links[self._current_name] = (isdirectory, mtime_str, size_str)

    def handle_starttag(self, tag, attrs):
        if tag == 'img':
            if len(attrs) > 0:
                for attr in attrs:
                    if attr[0] == 'alt':
                        if attr[1] == '[DIR]':
                            self._to_ignore = False
                            self._is_directory = True
                            self._is_file = False
                        elif not attr[1].startswith('['):
                            self._to_ignore = True
                            self._is_directory = False
                            self._is_file = False
                        else:
                            self._is_directory = False
                            self._is_file = True
                            self._to_ignore = False
        elif tag == 'a':
            if not self._to_ignore:
                if len(attrs) > 0:
                    for attr in attrs:
                        if attr[0] == 'href':
                            self._current_name = attr[1]
                            self._current_name = self._current_name.rstrip('/')
                            if self._is_directory:
                                self.links[self._current_name] = list()
                            if self._is_file:
                                self.links[self._current_name] = list()

    def handle_endtag(self, tag):
        if tag == 'img':
            if self._is_directory:
                self._is_directory = False

    def get_files(self):
        return self.links

    def set_fileinfo_pattern(self, pattern):
        self.__fileinfo_pattern = re.compile(pattern)


class Protocol_https_directorylist(AbstractProtocol):

    def __init__(self, file_extractor=FileExtractor, redirector=None):
        AbstractProtocol.__init__(self)

        self.path = None
        self.check_infos = ['size', 'mtime']
        self._checkable_infos = ['size', 'mtime']

        self._protocol_str = 'https://'
        self._parser = None

        self._baseurl = None
        self.url = None

        self._find_interesting_directory_max_depth = 0

        self.username = None
        self.password = None

        self.__fileExtractor = file_extractor
        self.date_format = self.__fileExtractor.DATE_FORMAT
        self.__redirector = redirector
        self.port = None
        self.server = None
        self.__data_server_adress = None

        self.retry = False

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        # check for required config
        if config.path is not None:
            self.path = config.path
            self.path = self.path.lstrip('/')
        else:
            raise DC_ConfigError('Configuration error : missing path')

        if config.server_address is not None:
            self.server = config.server_address
        else:
            raise DC_ConfigError(
                'Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)
        log.setLevel(logging.DEBUG)

    def setDirectoryFilter(self, directory_filter):
        self.directoryFilter = directory_filter

    def setFileFilter(self, file_filter):
        self.fileFilter = file_filter

    def getFileInfoList(self):
        # gestion de l'authentification http
        if self.username is not None and self.password is not None:
            password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
            password_mgr.add_password(
                realm=None, uri=self.server, user=self.username, passwd=self.password)

            if self.__redirector is not None:
                password_mgr.add_password(
                    realm=None, uri=self.__redirector, user=self.username, passwd=self.password)
                handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
                cookie_jar = http.cookiejar.CookieJar()
                handler_cookie = urllib.request.HTTPCookieProcessor(cookie_jar)
                opener = urllib.request.build_opener(handler, handler_cookie)
            else:
                handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
                opener = urllib.request.build_opener(handler)
            urllib.request.install_opener(opener)

        self._baseurl = self._protocol_str + self.server
        self.url = self._baseurl + '/' + self.path
        self.url = self.url.replace('//', '/').replace(':/', '://')
        if self.__url_exists(self.url):
            current_depth = 0  # des qu'on fait le url_walk, on incremente.
            self.__url_walk(self.url, current_depth)
        else:
            error = DC_LocalpathError('path does not exists : %s' % self.url)
            log.error(error)
            sys.exit(1)
        self.updateValidExecutionStatus(True)

    def __url_exists(self, url):
        self.config.debug = True
        if self.config.debug:
            log.debug('check if url exists : %s' % url)
        try:
            urllib.request.urlopen(url, timeout=self.getDefaultTimeout())
        except urllib.error.HTTPError as e:
            if e.code == 404:
                return False
            else:
                raise
        except urllib.error.URLError as msg:
            if isinstance(msg.reason, timeout):
                error = DC_TimeoutError(
                    'https_directorylist url error : %s (url=%s)' %
                    (msg.reason, url), db=self.fileInfoDatabase)
            else:
                error = DC_ConnectionError(
                    'https_directorylist url error : %s (url=%s)' %
                    (msg.reason, url), db=self.fileInfoDatabase)
            log.error(error)
            return False
        return True

    def url_get_filelist(self, url):
        self._parser = self.__fileExtractor()
        try:
            data = urllib.request \
                .urlopen(url, timeout=self.getDefaultTimeout()) \
                .read() \
                .decode('utf-8')
            self._parser.feed(data)
        except urllib.error.HTTPError as e:
            error = DC_ConnectionError(
                'https_directorylist http error : %s (url=%s)' %
                (e.reason, url), db=self.fileInfoDatabase)
            # retry once
            if not self.retry and e.code == 500:
                warning_error = f'{error} ==> Retry once...'
                log.warning(warning_error)
                self.retry = True
                files = self.url_get_filelist(url)
                self.retry = False
                return files
            log.error(error)
            self.retry = False
            raise
        except urllib.error.URLError as error:
            if isinstance(error.reason, timeout):
                error = DC_TimeoutError(
                    'https_directorylist url error : %s (url=%s)' %
                    (error.reason, url), db=self.fileInfoDatabase)
            else:
                error = DC_UrlError(
                    'https_directorylist url error : %s (url=%s)' %
                    (error.reason, url), db=self.fileInfoDatabase)
            log.error(error)
            raise
        self._parser.close()
        files = self._parser.get_files()
        return files

    def __url_walk(self, url, current_depth):
        if self.config.debug:
            log.debug('walking in directory %s (depth = %s)' %
                      (url, current_depth))
        # Filter directory
        current_directory_interesting = True
        current_directory = os.path.split(url.rstrip('/'))
        if self.directoryFilter is not None:
            if not self.directoryFilter.isInterestingDirectory(dirname=current_directory[1],
                                                               dirpath=current_directory):
                if self.config.debug:
                    log.debug('directory %s is not interesting' % url)
                current_directory_interesting = False

        recurse = True
        # Si le dossier est pas interessant, et qu'on a depasse le max_depth
        # pour trouver les dossiers interessant, on arrete la.
        if current_depth >= self._find_interesting_directory_max_depth:
            recurse = self.__fileExtractor.get_recurcive()
            if not recurse:
                log.debug(
                    "get listing for directory '%s', but don't recurse in subfolders if they are not interesting" % url)

        download_url = self.__fileExtractor.get_download_url(url)
        fnames = self.url_get_filelist(url)

        files_to_insert = []
        for fname in list(fnames.keys()):
            if fname == '' or len(fnames[fname]) == 0:
                continue
            # print(f"=====> {fname} : {fnames[fname]}")
            self.nbr_walked_files += 1
            url = url.rstrip('/')
            if fname.startswith('http:') or fname.startswith('https:'):
                current_path = fname
            else:
                current_path = download_url.rstrip('/') + '/' + fname
            islink = False
            isdirectory = fnames[fname][0]
            mtime_str = fnames[fname][1]
            if mtime_str != '' and mtime_str is not None:
                mtime_value = time.mktime(time.strptime(mtime_str, self.date_format))
            else:
                mtime_value = 0
            size_str = fnames[fname][2]

            # on ne s'occupe pas des fichiers si le dossier n'est pas interessant
            # mais on s'occupe qd meme des autres dossiers pour voir s'il n'y a pas
            # qqchose d'interessant dedans

            if not current_directory_interesting and not isdirectory:
                continue

            # si c'est un dossier, et que le recurse est a False, on verifie
            # s'il est interessant avant de le lister
            insert = True
            size_value = 0
            sensingtime = None
            if isdirectory:
                current_path_slashed = current_path.rstrip('/') + '/'
                walk = True
                if not recurse:
                    if self.directoryFilter is not None:
                        dirname = os.path.split(current_path.rstrip('/'))[1]
                        if not self.directoryFilter.isInterestingDirectory(
                                dirname=dirname, dirpath=url):
                            if self.config.debug:
                                log.debug(
                                    "subdirectory %s is not interesting, don't go in" % current_path_slashed)
                            walk = False
                if walk:
                    # print(f"=====> {url} + {fname}=> {self.__fileExtractor.get_new_dcheck_url(url, fname)}")
                    self.__url_walk(self.__fileExtractor.get_new_dcheck_url(url, fname), current_depth + 1)
            else:
                # Filter filename
                if self.fileFilter is not None:
                    interesting_file = self.fileFilter.isInteresting(
                        current_path, mtime_value)
                    if self.config.debug:
                        log.debug('file %s is interesting ? %s' %
                                  (current_path, interesting_file))
                    if not interesting_file:
                        continue

                # get sensingtime and size
                sensingtime, insert = self.getSensingTime(current_path)

                try:
                    size_value = getNumericSize(size_str, self.__fileExtractor.FACTOR)
                except Exception as e:
                    error = DC_Error(
                        'Cannot get numeric size for size_str: %s (file %s) [msg=%s]' %
                        (size_str, fname, str(e)))
                    log.error(error)
                    # continue

            # pas la peine d'enregistrer le nom du dossier s'il n'est pas
            # interessant...
            if isdirectory and not current_directory_interesting:
                continue

            # on ne stocke pas le current path, mais le chemin relatif du fichier !
            # TODO : a blinder.. par defaut, je met le current_path, mais ce n'est pas
            # sense arriver (raise dans le else ?)
            relative_path = current_path
            if current_path.startswith(self._baseurl):
                relative_path = '/' + relative_path[len(self._baseurl):]
            path, filename = os.path.split(relative_path)

            if insert:
                files_to_insert.append(File(
                    id_execution=None,
                    filename=relative_path + FILE_URL_SEPARATOR + current_path,
                    isDirectory=isdirectory,
                    isSymLink=islink,
                    size=size_value,
                    mtime=datetime.fromtimestamp(mtime_value),
                    sensingtime=sensingtime,
                ))

            if len(files_to_insert) > 10000:
                self.fileInfoDatabase.addFileList(files_to_insert)
                files_to_insert = []

        if files_to_insert:
            self.fileInfoDatabase.addFileList(files_to_insert)
