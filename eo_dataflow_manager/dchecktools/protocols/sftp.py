

import logging
import os
import socket
from datetime import datetime
from stat import S_IMODE, S_ISDIR, S_ISLNK, S_IXOTH

import pysftp

from eo_dataflow_manager.dchecktools.common.basefileinfos import File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_ConnectionError, DC_FtpError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

DEFAULT_SERVER_PORT = 22
DEFAULT_USERNAME = 'anonymous'
DEFAULT_PASSWORD = 'anon@anon.org'

log = logging.getLogger('sftp')
log.setLevel(logging.INFO)

FORCE_CHROOT = True

"""
sftpwalk -- Walk a hierarchy of files using FTP (Adapted from os.walk()).
"""
# TODO : utiliser le flag onerror, ou le virer
# un return dans cette methode genere en fait un 'raise StopIteration'
# exception


def sftpwalk(
        isSmartCrawler,
        directoryFilter,
        sftp,
        top,
        topdown=True,
        onerror=None,
        db=None,
        listing_type='unix'):
    """
    Generator that yields tuples of (root, dirs, nondirs).
    """
    # Make the FTP object's current directory to the top dir.
    log.debug('ftpwalk : dir=%s' % (top))
    try:

        if top is not None and top != '':
            if FORCE_CHROOT:
                if top == '/':
                    top = sftp.pwd()
                    if not top:
                        top = '/'
            try:
                sftp.cwd(top)
            except IOError as msg:
                if not isSmartCrawler:
                    error = DC_FtpError(
                        'error_perm : ' + str(msg) + ' [dir=%s]' % (top), db)
                    log.error(error)
                    if onerror is not None:
                        onerror(msg)
                    raise error

                return
        try:
            dirs, nondirs = _ftp_listdir(sftp, db, listing_type)
        except os.error as err:
            error = DC_FtpError('sftp listdir error : %s' % (err))
            log.debug(error)
            if onerror is not None:
                onerror(err)
            # 19/06/2018 PMT : raise error instead of return
            # return
            raise error
            # raise StopIteration, error

        if topdown:
            yield top, dirs, nondirs
        for entry in dirs:
            dname = entry[0]
            mtime = entry[2]  # fcad: add modification time
            path = os.path.join(top, dname)

            # Filter directory
            if directoryFilter is not None:
                interestingDirectory = directoryFilter.isInterestingDirectory(
                    dirname=dname, mtime=mtime, dirpath=os.path.join(top, dname))
                log.debug('directory %s is interesting ? %s' %
                          (path, interestingDirectory))
                if not interestingDirectory:
                    continue

                # 19/06/2018 PMT : skip directories without permissions
                if not (S_IMODE(entry[3]) & S_IXOTH):
                    log.debug(
                        'directory %s does not have the right permissions [%s]' %
                        (path, entry[3]))
                    continue

            if entry[-1] is False:  # not a link
                for x in sftpwalk(
                        isSmartCrawler,
                        directoryFilter,
                        sftp,
                        path,
                        topdown,
                        onerror,
                        db,
                        listing_type=listing_type):
                    #log.debug("yield x : %s"%(str(x)))
                    yield x
            else:  # TODO : gerer le follow-link
                pass

        if not topdown:
            yield top, dirs, nondirs

    except IOError as msg:
        error = DC_FtpError('ftp error : %s' % (msg))
        log.debug(error)
        if onerror is not None:
            onerror(msg)
        raise error


def _ftp_listdir(sftp, db=None, listing_type='unix'):
    """
    List the contents of the FTP opbject's cwd and return two tuples of

       (filename, size, mtime, mode, link)

    one for subdirectories, and one for non-directories (normal files and other
    stuff).  If the path is a symbolic link, 'link' is set to the target of the
    link (note that both files and directories can be symbolic links).

    Note: we only parse Linux/UNIX style listings; this could easily be
    extended.
    """
    # log.debug('_ftp_listdir')
    list_files = []
    list_attrs = []
    try:
        list_files = sftp.listdir()
        list_attrs = sftp.listdir_attr()
    except IOError as msg:
        error = DC_FtpError('error_perm : ' + str(msg), db)
        log.error(error)
        return

    dirs, nondirs = [], []

    """
    filenames list of filenames
    attributes list of attribute st_size, st_uid, st_gid, st_mode, st_atime, st_mtime
    """

    for index in range(len(list_files)):
        # for line in listing:

        # Get the filename.
        filename = list_files[index]
        #filename = words[-1].lstrip()
        if filename in ('.', '..'):
            continue

        # Get the link target, if the file is a symlink.
        i = filename.find(' -> ')
        if i >= 0:
            # words[0] had better start with 'l'...
            filename = filename[:i]

        # Get the file size.
        size = list_attrs[index].st_size

        # Get the date.
        mtime = list_attrs[index].st_mtime
        #mtime = time.mktime(dt.timetuple())

        # Get the type and mode.
        mode = list_attrs[index].st_mode

        islink = False
        if S_ISLNK(mode):
            islink = True

        entry = (filename, size, mtime, mode, islink)
        if S_ISDIR(mode):
            dirs.append(entry)
        else:
            nondirs.append(entry)

    return dirs, nondirs


class Protocol_sftp(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(self.getDefaultTimeout())

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server_address = None
        self.listing_type = 'unix'

        self.check_infos = ['size', 'mtime', 'sensingtime']
        #self.check_infos = [ 'size', 'mtime']
        self._checkable_infos = ['size', 'mtime', 'mode']

        self.__current_path = None
        self.__current_dbfile = None

        self.__tmpdirBlocs = {}
        self.__session = None

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        if config.ftp_listing_type != '' and config.ftp_listing_type is not None:
            self.listing_type = config.ftp_listing_type

        if config.server_address is not None:
            self.server = config.server_address
        else:
            raise DC_ConfigError(
                'Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

    def setDirectoryFilter(self, directoryFilter):
        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        self.fileFilter = fileFilter

    def getFileInfoList(self):
        if self.__session is None:
            try:
                cnopts = pysftp.CnOpts()
                cnopts.hostkeys = None
                cinfo = {
                    'host': self.server,
                    'username': self.username,
                    'password': self.password,
                    'port': self.port,
                    'cnopts': cnopts}
                self.__session = pysftp.Connection(**cinfo)
                self.__session.timeout = self.getDefaultTimeout()
            except IOError as exc:
                try:
                    error = DC_ConnectionError(
                        'sftp connection error : %s [errno=%s]' %
                        (exc.strerror, exc.errno), db=self.fileInfoDatabase)
                except ValueError:  # exc n'est pas toujours un tuple, auquel cas le unpack ne passe pas.
                    error = DC_ConnectionError('sftp connection error : %s' % (
                        str(exc)), db=self.fileInfoDatabase)
                log.debug(error)
                raise error
            except Exception as exc:
                error = DC_ConnectionError(
                    'sftp connection error : %s' %
                    (exc), db=self.fileInfoDatabase)
                log.debug(error)
                raise error

        basedir = self.path

        log.debug("Start sftpwalk (directoryFilter = %s), basedir : '%s'" %
                  (self.directoryFilter, basedir))
        a = sftpwalk(
            self.smart_crawler,
            self.directoryFilter,
            self.__session,
            basedir,
            self.directoryFilter,
            db=self.fileInfoDatabase,
            listing_type=self.listing_type)

        files_to_insert = []
        try:
            while a.__next__:
                # log.debug("a.next")
                basedir, dirs, files = next(a)

                log.debug('LIST for directory : %s' % (basedir))
                log.debug('dirs = %s' % dirs)
                log.debug('files = %s' % (files))

                for dirname in dirs:
                    dirpath = dirname[0]
                    mtime = dirname[2]
                    interestingDir = self.directoryFilter.isInterestingDirectory(
                        dirname=dirpath, mtime=mtime, dirpath=os.path.join(basedir, dirpath))
                    if not interestingDir:
                        continue

                    files_to_insert.append(File(
                        id_execution=None,
                        filename=os.path.join(basedir, dirpath),
                        isDirectory=True,
                        isSymLink=dirname[4],
                        size=dirname[1],
                        mtime=datetime.fromtimestamp(mtime),
                        sensingtime=None,
                    ))

                for filename in files:
                    self.nbr_walked_files += 1

                    insert = True
                    filepath = filename[0]
                    mtime = filename[2]
                    interestingFile = self.fileFilter.isInteresting(
                        filepath, mtime)  # fcad: add mtime condition
                    log.debug('file %s is interesting ? %s' %
                              (filepath, interestingFile))
                    if not interestingFile:
                        continue

                    filePath = os.path.join(basedir, filename[0])
                    sensingtime, insert = self.getSensingTime(filePath)

                    if insert:
                        files_to_insert.append(File(
                            id_execution=None,
                            filename=filePath,
                            isDirectory=False,
                            isSymLink=filename[4],
                            size=filename[1],
                            mtime=datetime.fromtimestamp(mtime),
                            sensingtime=sensingtime,
                        ))
        except StopIteration:
            #log.debug("ftpwalk iteration stopped '%s'"%(str(msg)))
            pass  # fin normale de l'iteration
        except DC_FtpError as msg:
            log.debug('sftpwalk error : %s. Stopping iteration.' % (str(msg)))
            raise msg

        if files_to_insert:
            self.fileInfoDatabase.addFileList(files_to_insert)
        self.updateValidExecutionStatus(True)
