

import logging
import os
import time
import xml.etree.cElementTree as xml
from datetime import datetime

import requests
from requests.exceptions import RequestException

from eo_dataflow_manager.dchecktools.common.basefileinfos import File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_ConnectionError, DC_UrlError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

DEFAULT_PROTOCOL = 'http'
DEFAULT_PROTOCOL_SECURE = 'https'
DEFAULT_SERVER_PORT = 80
DEFAULT_SERVER_PORT_SECURE = 443
DEFAULT_USERNAME = 'anonymous'
DEFAULT_PASSWORD = 'anonymous'

log = logging.getLogger('webdav')
log.setLevel(logging.INFO)

FORCE_CHROOT = True

"""
ftpwalk -- Walk a hierarchy of files using FTP (Adapted from os.walk()).
"""
# un return dans cette methode genere en fait un 'raise StopIteration' exception


def webdavwalk(isSmartCrawler, directoryFilter, webdav, baseurl, root,
               topdown=True, db=None, timeout=None):
    """
    Generator that yields tuples of (root, dirs, nondirs).
    """
    # Make the FTP object's current directory to the top dir.
    #log.info('webdavwalk : dir=%s' % (root))
    try:

        try:
            dirs, nondirs = _webdav_listdir(webdav, baseurl, root, timeout, db)
        except os.error as err:
            error = DC_UrlError('webdav listdir error : %s' % (err))
            log.debug(error)
            raise error
            # raise StopIteration, error

        if topdown:
            yield root, dirs, nondirs

        for entry in dirs:
            dname = entry[0]
            mtime = entry[2]  # fcad: add modification time
            path = dname

            # Filter directory
            if directoryFilter is not None:
                currentDirectory = os.path.split(dname.rstrip('/'))
                interestingDirectory = directoryFilter.isInterestingDirectory(
                    dirname=currentDirectory[1], mtime=mtime, dirpath=currentDirectory)
                log.debug('directory %s is interesting ? %s' % (path, interestingDirectory))
                if not interestingDirectory:
                    continue

            for x in webdavwalk(isSmartCrawler, directoryFilter, webdav, baseurl, path, topdown, db, timeout):
                #log.debug("yield x : %s"%(str(x)))
                yield x

        if not topdown:
            yield root, dirs, nondirs

    except requests.exceptions.RequestException as msg:
        error = DC_UrlError('webdav error : %s' % (msg))
        log.debug(error)
        raise error


_calmonths = dict((x, i + 1) for i, x in
                  enumerate(('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                             'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')))


def _webdav_parse(listdir, db=None):
    """
    """
    dirs, nondirs = [], []
    filename= None
    entry = None

    log.debug('_webdav_parse')

    try:
        for element in listdir:
            filename = element.find('.//{DAV:}' + 'href').text
            elementsize = element.find('.//{DAV:}' + 'getcontentlength')
            if elementsize is not None:
                size = int(elementsize.text)
            else:
                size = 0
            dt = datetime.strptime(element.find('.//{DAV:}' + 'getlastmodified').text, '%a, %d %b %Y %X %Z')
            mtime = time.mktime(dt.timetuple())

            islink = False

            entry = (filename, size, mtime, None, islink)

            if filename[-1:] == '/':
                dirs.append(entry)
            else:
                nondirs.append(entry)

    except Exception as msg:
        error = DC_UrlError('parse error : ' + str(msg) + ' filename:' + filename + ';entry:' + str(entry), db)
        log.error(error)
        return error

    return dirs[1:], nondirs


def _webdav_listdir(webdav, baseurl, path, timeout, db=None):
    """
    List the contents of the webDAV object's PROPFIND request and return two tuples of

       (filename, size, mtime, mode, link)

    one for subdirectories, and one for non-directories (normal files and other
    stuff).  If the path is a symbolic link, 'link' is set to the target of the
    link (note that both files and directories can be symbolic links).

    Note: if ConnectionError exception, retry the request twice before returning an error
    """
    last_error = ''

    log.debug(f'_webdav_listdir => baseurl : {baseurl}, path : {path}, timeout : {timeout}')
    dirs, nondirs = [], []
    url = baseurl + path
    headers = {'Depth': '1'}
    nbretry = 0
    request_OK = False
    error = ''
    last_errorcode = None

    try:
        while not request_OK and nbretry < 3:
            try:

                response = webdav.request('PROPFIND', url, allow_redirects=False, headers=headers, timeout=timeout)
                if response.status_code != 207:  # MULTI_STATUS
                    msg = 'Error request return status code : ' + str(response.status_code)
                    last_error = str(msg)
                    last_errorcode = response.status_code
                    nbretry = nbretry + 1
                    continue

                tree = xml.fromstring(response.content)

            except ConnectionError as msg:
                last_error = str(msg)
                last_errorcode = -1
                nbretry = nbretry + 1
                continue

            dirs, nondirs = _webdav_parse(tree.findall('{DAV:}response'), db=None)
            request_OK = True

        if not request_OK:
            error = DC_ConnectionError('listdir ConnectionError url:' + url + ' : ' + last_error)
            log.error(error)

    except RequestException as msg:
        error = DC_UrlError('listdir RequestException url:' + url + ' : ' + str(msg))
        log.error(error)
    except Exception as msg:
        error = DC_UrlError('listdir error : ' + str(msg), db)
        log.error(error)

    if not request_OK and last_errorcode != 404:
        print(f'Raise error code {last_errorcode}')
        raise error

    return dirs, nondirs


class Protocol_webdav(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD
        self.protocol = DEFAULT_PROTOCOL

        # required config
        self.path = None
        self.server_address = None

        self.check_infos = ['size', 'mtime', 'sensingtime']
        #self.check_infos = [ 'size', 'mtime']
        self._checkable_infos = ['size', 'mtime', 'mode']

        self.__current_path = None
        self.__current_dbfile = None

        self.__tmpdirBlocs = {}
        self.__session = None

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        if config.ftp_listing_type != '' and config.ftp_listing_type is not None:
            self.listing_type = config.ftp_listing_type

        if config.server_port is not None:
            self.port = config.server_port

        if self.port == DEFAULT_SERVER_PORT_SECURE:
            self.protocol = DEFAULT_PROTOCOL_SECURE

        if config.server_address is not None:
            self.server = config.server_address
        else:
            raise DC_ConfigError('Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

    def setDirectoryFilter(self, directoryFilter):
        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        self.fileFilter = fileFilter

    def getFileInfoList(self):
        files = []
        if self.__session is None:
            self.__session = requests.session()

        self.__session.verify = True
        self.__session.stream = True
        self.__session.auth = (self.username, self.password)

        self.baseurl = '{0}://{1}:{2}'.format(self.protocol, self.server, self.port)

        if self.path[-1:] != '/':
            basedir = self.path + '/'
        else:
            basedir = self.path

        log.debug("Start webdavwalk (directoryFilter = %s), basedir : '%s'" % (self.directoryFilter, basedir))
        a = webdavwalk(self.smart_crawler,
                       self.directoryFilter,
                       self.__session,
                       self.baseurl,
                       basedir,
                       db=self.fileInfoDatabase,
                       timeout=self.getDefaultTimeout())

        files_to_insert = []
        try:
            while True:
                # log.debug("a.next")
                basedir, dirs, files = next(a)

                log.debug('LIST for directory : %s' % (basedir))
                log.debug('dirs = %s' % dirs)
                log.debug('files = %s' % (files))

                for dirname in dirs:
                    dirpath = dirname[0]
                    mtime = dirname[2]
                    currentDirectory = os.path.split(dirpath.rstrip('/'))
                    interestingDirectory = self.directoryFilter.isInterestingDirectory(
                        dirname=currentDirectory[1], mtime=mtime, dirpath=currentDirectory)
                    if not interestingDirectory:
                        continue

                    files_to_insert.append(File(
                        id_execution=None,
                        filename=os.path.join(basedir, dirpath),
                        isDirectory=True,
                        isSymLink=dirname[4],
                        size=dirname[1],
                        mtime=datetime.fromtimestamp(mtime),
                        sensingtime=None,
                    ))
                for file in files:
                    self.nbr_walked_files += 1

                    insert = True
                    filename = file[0]
                    mtime = file[2]
                    interestingFile = self.fileFilter.isInteresting(filename, mtime)  # fcad: add mtime condition
                    log.debug('file %s is interesting ? %s' % (filename, interestingFile))
                    if not interestingFile:
                        continue

                    fullpath = os.path.join(basedir, filename)
                    sensingdate, insert = self.getSensingTime(fullpath)

                    if insert:
                        files_to_insert.append(File(
                            id_execution=None,
                            filename=fullpath,
                            isDirectory=False,
                            isSymLink=file[4],
                            size=file[1],
                            mtime=datetime.fromtimestamp(mtime),
                            sensingtime=sensingdate,
                        ))

        except StopIteration:
            #log.debug("ftpwalk iteration stopped '%s'"%(str(msg)))
            pass  # fin normale de l'iteration
        except DC_UrlError as msg:
            log.debug('webdavwalk error : %s. Stopping iteration.' % (str(msg)))
            raise msg

        if files:
            self.fileInfoDatabase.addFileList(files_to_insert)
        self.updateValidExecutionStatus(True)
