

import logging
import socket
import ssl
import time
import urllib.error
import urllib.parse
import urllib.request
from datetime import datetime
from xml.etree import ElementTree

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR, File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_UrlError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

DEFAULT_SERVER_PORT = 443
DEFAULT_USERNAME = ''
DEFAULT_PASSWORD = ''
DEFAULT_ORDERBY = '&orderby=beginposition%20desc'
DEFAULT_SELECTGEO = ''

log = logging.getLogger('https_opensearch')
log.setLevel(logging.INFO)


LOOK_FOR_LAST_N_DAYS = 0


def https_iteration(opener, url='', count=0, onerror=None, db=None):

    # number of rows in the selection. This number is updated after each
    # iteration
    max_row = 1
    # number of rows to read per iteration
    nb_row = 100

    files = []
    #
    log.debug('https_iteration : url=%s' % (url))
    try:
        # loop
        while max_row > count:
            iteration = '&start=' + str(count) + '&rows=' + str(nb_row)
            url_iteration = url + iteration

            with opener.open(url_iteration, timeout=60) as response:
                data = response.read().decode('utf-8')
                RepXml = ElementTree.fromstring(data)

            ns = {'feed': 'http://www.w3.org/2005/Atom',
                  'ops': 'http://a9.com/-/spec/opensearch/1.1/'}
            # update the max number of files to extract
            totalResults = RepXml.find('ops:totalResults', ns)
            if totalResults is None:
                max_row = 0  # pas de totalResults = dernière page
            else:
                max_row = int(totalResults.text)
            log.debug(
                'iteration : %s => max lignes retournées : %d' %
                (iteration, max_row))

            # for each entry, collect informations
            for entry in RepXml.findall('feed:entry', ns):
                filesize = None
                filedate = None
                datadate = None
                # file name
                filename = entry.find('feed:title', ns).text

                # link to download file
                filelink = None
                for link in entry.findall('feed:link', ns):
                    if link.get('rel') is None:
                        filelink = link.get('href')

                # ingestion date
                for date in entry.findall('feed:date', ns):
                    if date.get('name') == 'ingestiondate':
                        filedate = time.strftime(
                            '%Y-%m-%d %H:%M:%S', time.strptime(date.text[:19], '%Y-%m-%dT%H:%M:%S'))

                # start date of the data
                for date in entry.findall('feed:date', ns):
                    if date.get('name') == 'beginposition':
                        datadate = time.strftime(
                            '%Y-%m-%d %H:%M:%S', time.strptime(date.text[:19], '%Y-%m-%dT%H:%M:%S'))

                # size of the file and convert in BYTE
                for datasize in entry.findall('feed:str', ns):
                    if datasize.get('name') == 'size':
                        size = datasize.text
                        if size[-2:] == 'GB':
                            filesize = int(
                                float(size[0:len(size) - 3]) * 1024 * 1024 * 1024)
                        elif size[-2:] == 'MB':
                            filesize = int(
                                float(size[0:len(size) - 3]) * 1024 * 1024)
                        elif size[-2:] == 'KB':
                            filesize = int(float(size[0:len(size) - 3]) * 1024)
                        else:
                            filesize = -1

                entry = (
                    filename +
                    FILE_URL_SEPARATOR +
                    filelink,
                    filesize,
                    filedate,
                    datadate)

                files.append(entry)


#                log.debug('find : %s (begin : %s)'%(filename, datadate))

            count += nb_row

    except urllib.error.URLError as msg:
        error = DC_UrlError('URL/HTTP error : %s' % (msg))
        log.debug(error)
        if onerror is not None:
            onerror(msg)
        raise error

    return(files)


class Protocol_https_opensearch(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(self.getDefaultTimeout())

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = None
        self.password = None

        # required config
        self.path = None
        self.server_address = None

        self.check_infos = ['size', 'mtime', 'sensingtime']
        self._checkable_infos = ['size', 'mtime']

        self.__current_path = None
        self.__current_dbfile = None

        self.__tmpdirBlocs = {}
        self.__session = None

        self.selectproduct = None
        self.selectdate = None
        self.selectgeo = None
        self.resultformat = DEFAULT_ORDERBY

        self.ignoreFilesOlderThan = None
        self.check_certificate = True

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username
        else:
            raise DC_ConfigError('Configuration error : missing username')

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password
        else:
            raise DC_ConfigError('Configuration error : missing password')

        # check for required config
        if config.path is not None:
            self.path = config.path
        else:
            raise DC_ConfigError('Configuration error : missing path')

        if config.server_address is not None:
            self.server = config.server_address
        else:
            raise DC_ConfigError(
                'Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

        if config.selectProduct is not None:
            self.selectproduct = config.selectProduct
        else:
            raise DC_ConfigError(
                'Configuration error : missing select product')

        if config.ignoreFilesOlderThan is not None:
            self.ignoreFilesOlderThan = config.ignoreFilesOlderThan

        if config.resultFormat is not None:
            self.resultformat = config.resultFormat

        if config.selectGeo is not None:
            self.selectgeo = config.selectGeo

        if config.protocol_option and 'CheckCertificate' in config.protocol_option:
            if config.protocol_option['CheckCertificate'].lower() == 'false':
                self.check_certificate = False

    def setDirectoryFilter(self, directoryFilter):

        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):

        self.fileFilter = fileFilter

    def getFileInfoList(self):

        log.debug('serveur:%s, user:%s, pass:%s.' %
                  (self.server, self.username, self.password))

        try:
            password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
            password_mgr.add_password(
                None, self.server, self.username, self.password)
            handler = urllib.request.HTTPBasicAuthHandler(password_mgr)

            context = None
            if not self.check_certificate:
                context = ssl._create_unverified_context()
            sslHandler = urllib.request.HTTPSHandler(context=context)

            opener = urllib.request.build_opener(handler, sslHandler)

            url = 'https://' + self.server + '/' + self.path + \
                '/search?q=' + urllib.parse.quote(self.selectproduct, '/().;:')

            if self.ignoreFilesOlderThan is not None:
                url += '%20AND%20ingestiondate:[' + self.ignoreFilesOlderThan.strftime(
                    '%Y-%m-%dT%H:%M:%S.000Z') + '%20TO%20NOW]'

            if self.selectgeo is not None:
                url += '%20AND%20footprint:"Intersects(' + urllib.parse.quote(
                    self.selectgeo, '/().;:') + ')"'

            url += '&format=xml' + self.resultformat

            log.debug('url:%s.' % (url))

            files = https_iteration(
                opener=opener,
                url=url,
                count=0,
                onerror=None,
                db=None)
            # ignore duplicate files
            list_files = []
            for filename in files:
                self.nbr_walked_files += 1

                if filename not in list_files:
                    list_files.append(filename)
                else:
                    log.debug(
                        "getFileInfoList duplicate file skip '%s'" %
                        (filename[0].split(FILE_URL_SEPARATOR)[0]))

            files_to_insert = []
            try:
                for filename in list_files:
                    filepath = filename[0].split(FILE_URL_SEPARATOR)[0]
                    mtime = time.mktime(
                        time.strptime(
                            filename[2],
                            '%Y-%m-%d %H:%M:%S'))
                    sensingtime = time.mktime(
                        time.strptime(
                            filename[3],
                            '%Y-%m-%d %H:%M:%S'))
                    interestingFile = self.fileFilter.isInteresting(
                        filepath, mtime)
                    log.debug(
                        'file %s is interesting ? %s' %
                        (filepath, interestingFile))

                    if interestingFile:
                        files_to_insert.append(File(
                            id_execution=None,
                            filename=filename[0],
                            isDirectory=False,
                            isSymLink=False,
                            size=filename[1],
                            mtime=datetime.fromtimestamp(mtime),
                            sensingtime=datetime.fromtimestamp(sensingtime),
                        ))
            except StopIteration as msg:
                log.debug(
                    "getFileInfoList iteration stopped '%s'" %
                    (str(msg)))
                pass  # fin normale de l'iteration

            if files_to_insert:
                self.fileInfoDatabase.addFileList(files_to_insert)
        except DC_UrlError as msg:
            log.debug(
                'https_iteration error : %s. Stopping iteration.' %
                (str(msg)))
            raise msg

        self.updateValidExecutionStatus(True)
