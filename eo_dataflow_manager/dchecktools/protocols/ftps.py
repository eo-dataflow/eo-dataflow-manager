
import ftplib
import logging
import os
import re
import socket
import ssl
import sys
import time
from datetime import datetime

from eo_dataflow_manager.dchecktools.common.basefileinfos import File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_ConnectionError, DC_FtpError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

DEFAULT_SERVER_PORT = 21
DEFAULT_USERNAME = 'anonymous'
DEFAULT_PASSWORD = 'anon@anon.org'

log = logging.getLogger('ftps')
log.setLevel(logging.INFO)

FORCE_CHROOT = True

"""
ftpwalk -- Walk a hierarchy of files using FTPS (Adapted from os.walk()).
"""
# TODO : utiliser le flag onerror, ou le virer
# un return dans cette methode genere en fait un 'raise StopIteration'
# exception


class ImplicitFTP_TLS(ftplib.FTP_TLS):
    """
    FTP_TLS subclass that automatically wraps sockets in SSL to support implicit FTPS.
    Prefer explicit TLS whenever possible.
    """

    def __init__(self, *args, **kwargs):
        """Initialise self."""
        super().__init__(*args, **kwargs)
        self._sock = None

    @property
    def sock(self):
        """Return the socket."""
        return self._sock

    @sock.setter
    def sock(self, value):
        """When modifying the socket, ensure that it is SSL wrapped."""
        if value is not None and not isinstance(value, ssl.SSLSocket):
            value = self.context.wrap_socket(value)
        self._sock = value

    def ntransfercmd(self, cmd, rest=None):
        """Override the ntransfercmd method"""
        conn, size = ftplib.FTP.ntransfercmd(self, cmd, rest)
        conn = self.sock.context.wrap_socket(
            conn, server_hostname=self.host, session=self.sock.session
        )
        return conn, size


def ftpswalk(
        isSmartCrawler,
        directoryFilter,
        ftp,
        top,
        topdown=True,
        onerror=None,
        db=None,
        listing_type='unix',
        skip_permissions=False
):
    """
    Generator that yields tuples of (root, dirs, nondirs).
    """
    # Make the FTP object's current directory to the top dir.
    log.debug('ftpswalk : dir=%s' % (top))
    try:

        if top is not None and top != '':
            if FORCE_CHROOT:
                if top == '/':
                    top = ftp.pwd()
                    if not top:
                        top = '/'
            try:
                ftp.cwd(top)
            except ftplib.error_perm as msg:
                if not isSmartCrawler:
                    error = DC_FtpError(
                        'error_perm : ' +
                        str(msg) +
                        ' [dir=%s]' %
                        (top),
                        db)
                    log.error(error)
                    if onerror is not None:
                        onerror(msg)
                    raise error

                return
        try:
            dirs, nondirs = _ftp_listdir(ftp, db, listing_type)
        except os.error as err:
            error = DC_FtpError('ftp listdir error : %s' % (err))
            log.debug(error)
            if onerror is not None:
                onerror(err)
            # 19/06/2018 PMT : raise error instead of return
            # return
            raise error
            # raise StopIteration, error

        if topdown:
            yield top, dirs, nondirs
        for entry in dirs:
            dname = entry[0]
            mtime = entry[2]  # fcad: add modification time
            path = os.path.join(top, dname)

            # Filter directory
            if directoryFilter is not None:
                interestingDirectory = directoryFilter.isInterestingDirectory(
                    dirname=dname, mtime=mtime, dirpath=os.path.join(top, dname))
                log.debug(
                    'directory %s is interesting ? %s' %
                    (path, interestingDirectory))
                if not interestingDirectory:
                    continue

                # 19/06/2018 PMT : skip directories without permissions
                if entry[3][9] != 'x' and not skip_permissions:
                    log.debug(
                        'directory %s does not have the right permissions [%s]' %
                        (path, entry[3]))
                    continue

            if entry[-1] is False:  # not a link
                for x in ftpswalk(
                        isSmartCrawler,
                        directoryFilter,
                        ftp,
                        path,
                        topdown,
                        onerror,
                        db,
                        listing_type=listing_type,
                        skip_permissions=skip_permissions
                ):
                    #log.debug("yield x : %s"%(str(x)))
                    yield x
            else:  # TODO : gerer le follow-link
                pass

        if not topdown:
            yield top, dirs, nondirs

    except ftplib.all_errors as msg:
        error = DC_FtpError('ftp error : %s' % (msg))
        log.debug(error)
        if onerror is not None:
            onerror(msg)
        raise error


_calmonths = dict((x, i + 1) for i, x in
                  enumerate(('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                             'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')))


def _ftp_parse_unix(listing):
    """
    drwxrwxrwx    4 ftp      ftp          4096 Jan 02 01:47 FOLDER

    """
    dirs, nondirs = [], []
    for line in listing:
        if line.startswith('total'):
            continue
        words = line.split(None, 8)
        if len(words) < 6:
            print('Warning: Error reading short line (unix) : ',
                  line, file=sys.stderr)
            continue

        # Get the filename.
        filename = words[-1].lstrip()
        if filename in ('.', '..'):
            continue

        # Get the link target, if the file is a symlink.
        extra = None
        i = filename.find(' -> ')
        if i >= 0:
            # words[0] had better start with 'l'...
            extra = filename[i + 4:]
            filename = filename[:i]

        try:
            # Get the file size.
            size = int(words[4])
        except ValueError:
            log.error(
                'ftp parse [unix] failure. Try to use --ftp-listing-type option. (line=%s)' %
                (line))
            raise

        # Get the date.
        year = datetime.today().year  # par defaut, l'annee courante
        month = _calmonths[words[5]]
        day = int(words[6])

        mo = re.match(r'(\d+):(\d+)', words[7])
        if mo:
            hour, min = list(map(int, mo.groups()))
            # correction of the sliding year
            if (month > datetime.today().month)\
                or (month == datetime.today().month and day > datetime.today().day)\
                or (month == datetime.today().month and day == datetime.today().day and hour > datetime.today().hour)\
                or (month == datetime.today().month and day == datetime.today().day\
                    and hour == datetime.today().hour and min > datetime.today().minute):
                year = year - 1
        else:
            mo = re.match(r'(\d\d\d\d)', words[7])
            if mo:
                year = int(mo.group(1))
                hour, min = 0, 0
            else:
                raise ValueError(
                    "Could not parse time/year in line: '%s'" %
                    line)
        dt = datetime(year, month, day, hour, min)
        mtime = time.mktime(dt.timetuple())

        # Get the type and mode.
        mode = words[0]

        islink = False
        if mode[0] == 'l':
            islink = True

        entry = (filename, size, mtime, mode, islink)
        if mode[0] == 'd':
            dirs.append(entry)
        else:
            nondirs.append(entry)

    return dirs, nondirs


def _ftp_parse_unix2(listing):
    """
    drwxrwsrwx   5 10158       4096 Feb 21  2003 quicklook
    drwxrwxr-x   6 10158        512 Jun  8  2005 COLOC
    """
    dirs, nondirs = [], []
    for line in listing:
        if line == '226 Transfer complete.':
            continue
        if line.startswith('total'):
            continue
        words = line.split(None, 7)
        if len(words) < 6:
            print('Warning: Error reading short line (unix2) : ',
                  line, file=sys.stderr)
            continue

        # Get the filename.
        filename = words[-1].lstrip()
        if filename in ('.', '..'):
            continue

        # Get the link target, if the file is a symlink.
        extra = None
        i = filename.find(' -> ')
        if i >= 0:
            # words[0] had better start with 'l'...
            extra = filename[i + 4:]
            filename = filename[:i]

        try:
            # Get the file size.
            size = int(words[3])
        except ValueError:
            log.error(
                'ftp parse [unix2] failure. Try to use --ftp-listing-type option. (line=%s)' %
                (line))
            raise

        # Get the date.
        month = _calmonths[words[4]]
        day = int(words[5])
        year = datetime.today().year  # par defaut, l'annee courante

        mo = re.match(r'(\d+):(\d+)', words[6])
        if mo:
            hour, min = list(map(int, mo.groups()))
            # correction of the sliding year
            if (month > datetime.today().month) \
                    or (month == datetime.today().month and day > datetime.today().day) \
                    or (month == datetime.today().month and day == datetime.today().day and hour > datetime.today().hour) \
                    or (month == datetime.today().month and day == datetime.today().day \
                        and hour == datetime.today().hour and min > datetime.today().minute):
                year = year - 1
        else:
            mo = re.match(r'(\d\d\d\d)', words[6])
            if mo:
                year = int(mo.group(1))
                hour, min = 0, 0
            else:
                raise ValueError(
                    "Could not parse time/year in line: '%s'" %
                    line)

        dt = datetime(year, month, day, hour, min)
        mtime = time.mktime(dt.timetuple())

        # Get the type and mode.
        mode = words[0]

        islink = False
        if mode[0] == 'l':
            islink = True

        entry = (filename, size, mtime, mode, islink)
        if mode[0] == 'd':
            dirs.append(entry)
        else:
            nondirs.append(entry)

    return dirs, nondirs


def _ftp_listdir(ftp, db=None, listing_type='unix'):
    """
    List the contents of the FTP opbject's cwd and return two tuples of

       (filename, size, mtime, mode, link)

    one for subdirectories, and one for non-directories (normal files and other
    stuff).  If the path is a symbolic link, 'link' is set to the target of the
    link (note that both files and directories can be symbolic links).

    Note: we only parse Linux/UNIX style listings; this could easily be
    extended.
    """
    # log.debug('_ftp_listdir')
    listing = []
    try:
        ftp.retrlines('LIST', listing.append)
    except ftplib.error_perm as msg:
        error = DC_FtpError('error_perm : ' + str(msg), db)
        log.error(error)
        return

    dirs, nondirs = [], []
    if listing_type == 'unix':
        dirs, nondirs = _ftp_parse_unix(listing)
    elif listing_type == 'unix2':
        dirs, nondirs = _ftp_parse_unix2(listing)
    else:
        error = DC_FtpError(
            "error_listing_type : unknown listing type '%s'" %
            (listing_type))
        log.error(error)

    return dirs, nondirs


class Protocol_ftps(AbstractProtocol):

    def __init__(self, options=None):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(self.getDefaultTimeout())

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        if options is not None:
            if 'SkipPermissions' in options and options['SkipPermissions'].capitalize() == 'True':
                self.skip_permission = True
            else:
                self.skip_permission = False

            if 'ssl_protocol' in options:
                self.ssl_protocol = options['ssl_protocol']
            else:
                self.ssl_protocol = None

        # required config
        self.path = None
        self.server_address = None
        self.listing_type = 'unix'

        self.check_infos = ['size', 'mtime', 'sensingtime']
        #self.check_infos = [ 'size', 'mtime']
        self._checkable_infos = ['size', 'mtime', 'mode']

        self.__current_path = None
        self.__current_dbfile = None

        self.__tmpdirBlocs = {}
        self.__session = None

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            if isinstance(config.server_port, str):
                self.port = int(config.server_port)
            else:
                self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        if config.ftp_listing_type != '' and config.ftp_listing_type is not None:
            self.listing_type = config.ftp_listing_type

        if config.server_address is not None:
            self.server = config.server_address
        else:
            raise DC_ConfigError(
                'Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

    def setDirectoryFilter(self, directoryFilter):
        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        self.fileFilter = fileFilter

    def getFileInfoList(self):

        if self.__session is None:
            try:
                try:
                    self.__session = ImplicitFTP_TLS(timeout=self.getDefaultTimeout())
                    self.__session.connect(self.server, self.port)
                except ssl.SSLError:  # ssl.SSLZeroReturnError:  -> error only for python 3.8.18 and above
                    self.__session = ftplib.FTP_TLS(timeout=self.getDefaultTimeout())
                    self.__session.connect(self.server, self.port)
                self.__session.login(self.username, self.password)
                self.__session.prot_p()
            except ftplib.all_errors as msg:
                if isinstance(msg, tuple):
                    errno, string = msg
                    error = DC_ConnectionError(
                        'ftp connection error : %s [errno=%s]' %
                        (string, errno), db=self.fileInfoDatabase)
                else:  # msg n'est pas toujours un tuple, auquel cas le unpack ne passe pas.
                    error = DC_ConnectionError(
                        'ftp connection error : %s' %
                        (str(msg)), db=self.fileInfoDatabase)
                log.debug(error)
                raise error
            except Exception as msg:
                error = DC_ConnectionError(
                    'ftp connection error : %s' %
                    msg, db=self.fileInfoDatabase)
                log.debug(error)
                raise error
            ftp_syst = None
            try:
                ftp_syst = self.__session.sendcmd('SYST')
            except ftplib.all_errors as msg:
                # pas la peine de logger ca...
                pass
                # error = DC_FtpError("Cannot retrieve 'SYST' informations : %s"%(msg), db=self.fileInfoDatabase)
                # log.error(error)
            if ftp_syst:
                self.fileInfoDatabase.addConstant('ftp_syst', ftp_syst)

            ftp_welcome = None
            try:
                ftp_welcome = self.__session.getwelcome()
            except ftplib.all_errors as msg:
                # pas la peine de logger ca...
                pass
                # error = DC_FtpError("Cannot retrieve 'WELCOME' informations : %s"%(msg), db=self.fileInfoDatabase)
                # log.error(error)
            if ftp_welcome:
                self.fileInfoDatabase.addConstant('ftp_welcome', ftp_welcome)

        basedir = self.path

        log.debug(
            "Start ftpwalk (directoryFilter = %s), basedir : '%s'" %
            (self.directoryFilter, basedir))
        a = ftpswalk(
            self.smart_crawler,
            self.directoryFilter,
            self.__session,
            basedir,
            self.directoryFilter,
            db=self.fileInfoDatabase,
            listing_type=self.listing_type,
            skip_permissions=self.skip_permission
        )

        files_to_insert = []
        try:
            while a.__next__:
                # log.debug("a.next")
                basedir, dirs, files = next(a)

                log.debug('LIST for directory : %s' % (basedir))
                log.debug('dirs = %s' % dirs)
                log.debug('files = %s' % (files))

                for dirname in dirs:
                    dirpath = dirname[0]
                    mtime = dirname[2]
                    interestingDir = self.directoryFilter.isInterestingDirectory(
                        dirname=dirpath, mtime=mtime, dirpath=os.path.join(basedir, dirpath))
                    if not interestingDir:
                        continue

                    files_to_insert.append(File(
                        id_execution=None,
                        filename=os.path.join(basedir, dirpath),
                        isDirectory=True,
                        isSymLink=dirname[4],
                        size=dirname[1],
                        mtime=datetime.fromtimestamp(mtime),
                        sensingtime=None,
                    ))
                    if len(files_to_insert) > 10000:
                        self.fileInfoDatabase.addFileList(files_to_insert)
                        files_to_insert = []

                for filename in files:
                    self.nbr_walked_files = self.nbr_walked_files + 1

                    insert = True
                    filepath = filename[0]
                    mtime = filename[2]
                    interestingFile = self.fileFilter.isInteresting(filepath, mtime)
                    log.debug(
                        'file %s is interesting ? %s' %
                        (filepath, interestingFile))
                    if not interestingFile:
                        continue

                    filePath = os.path.join(basedir, filename[0])
                    sensingtime, insert = self.getSensingTime(filePath)

                    if insert:
                        files_to_insert.append(File(
                            id_execution=None,
                            filename=filePath,
                            isDirectory=False,
                            isSymLink=filename[4],
                            size=filename[1],
                            mtime=datetime.fromtimestamp(mtime),
                            sensingtime=sensingtime,
                        ))
        except StopIteration:
            #log.debug("ftpwalk iteration stopped '%s'"%(str(msg)))
            pass  # fin normale de l'iteration
        except DC_FtpError as msg:
            log.debug('ftpwalk error : %s. Stopping iteration.' % (str(msg)))
            raise msg

        if files_to_insert:
            self.fileInfoDatabase.addFileList(files_to_insert)

        self.updateValidExecutionStatus(True)
