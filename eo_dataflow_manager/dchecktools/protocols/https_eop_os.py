
import json
import logging
import socket
import time
from datetime import datetime
from pathlib import Path
from typing import Iterable, List, NamedTuple, Tuple

import dateparser
import requests
import simplejson as json
from requests.auth import AuthBase, HTTPBasicAuth

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR, File
from eo_dataflow_manager.dchecktools.common.errors import (
    DC_ConfigError,
    DC_Error,
    DC_ExtractingError,
    DC_HttpError,
    DC_UrlError,
)
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol


class Credentials(NamedTuple):
    consumer_key: str
    consumer_secret: str


DEFAULT_SERVER_PORT = 443
DEFAULT_USERNAME = ''
DEFAULT_PASSWORD = ''
DEFAULT_PAGE_SIZE = 100
PAGE_SIZE_MIN = 0
PAGE_SIZE_MAX = 1000
DEFAULT_DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'


log = logging.getLogger('https_eop_os')
log.setLevel(logging.DEBUG)


class Protocol_https_eop_os(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(self.getDefaultTimeout())

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server_address = None
        self.parent_identifier = None
        self.platform_short_name = None
        self.product_type = None
        self.timeliness = None
        self.geometry = None
        self.bounding_box = None
        self.start_date = None
        self.end_date = None
        self.updated_since = None
        self.geometry = None
        self.geotemporal_list = None
        self.geo_temporal = None
        self.sort_key = None
        self.date_format = DEFAULT_DATE_FORMAT
        self.page_size = DEFAULT_PAGE_SIZE

        self.check_infos = ['size', 'mtime', 'sensingtime']
        self._checkable_infos = ['size', 'mtime']

        self.__current_path = None

    def setConfig(self, config):

        AbstractProtocol.setConfig(self, config)

        # check for required config
        if config.path is None:
            raise DC_ConfigError('Configuration error : missing path')
        if config.server_address is None:
            raise DC_ConfigError(
                'Configuration error : missing server')

        if 'ParentIdentifier' in config.protocol_option and \
                config.protocol_option['ParentIdentifier']:
            self.parent_identifier = config.protocol_option['ParentIdentifier']
        else:
            raise DC_ConfigError(
                'Configuration error : missing parent identifier')

        # overwrite default config
        self.path = config.path
        self.server_address = config.server_address

        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

        # add option protocol values if needed
        if 'PlatformShortName' in config.protocol_option:
            self.platform_short_name = config.protocol_option['PlatformShortName']

        if 'ProductType' in config.protocol_option:
            self.product_type = config.protocol_option['ProductType']

        if 'Timeliness' in config.protocol_option:
            self.timeliness = config.protocol_option['Timeliness']

        if 'Geometry' in config.protocol_option:
            self.geometry = config.protocol_option['Geometry']

        if 'BoundingBox' in config.protocol_option:
            self.bounding_box = config.protocol_option['BoundingBox']

        if 'StartDate' in config.protocol_option and config.protocol_option['StartDate']:
            self.start_date = datetime.strftime(dateparser.parse(config.protocol_option['StartDate']),
                                                self.date_format)

        if 'EndDate' in config.protocol_option and config.protocol_option['EndDate']:
            self.end_date = datetime.strftime(dateparser.parse(config.protocol_option['EndDate']),
                                              self.date_format)

        if 'UpdatedSince' in config.protocol_option and config.protocol_option['UpdatedSince']:
            self.updated_since = datetime.strftime(dateparser.parse(config.protocol_option['UpdatedSince']),
                                                   self.date_format)

        if config.geotemporal_list is not None and config.geotemporal_list != '':
            self.geotemporal_list = self.verify_geotemporal(config.geotemporal_list)
            if self.geotemporal_list is None:
                raise DC_ConfigError('Configuration error : geo temporal list is not valid')

        if 'SortKey' in config.protocol_option:
            self.sort_key = config.protocol_option['SortKey']

        if 'PageSize' in config.protocol_option:
            self.page_size = int(config.protocol_option['PageSize'])
            # minInclusive = "0" maxExclusive = "1001"
            if self.page_size < PAGE_SIZE_MIN or self.page_size > PAGE_SIZE_MAX:
                raise DC_ConfigError(
                    f'Configuration error : page_size value must be between : {PAGE_SIZE_MIN} and {PAGE_SIZE_MAX}'
                )

    def setDirectoryFilter(self, directory_filter):
        self.directoryFilter = directory_filter

    def setFileFilter(self, file_filter):
        self.fileFilter = file_filter

    def getURL(self, geometry: str ,bounding_box: str, start_date: str, end_date: str) -> str:
        url = self.server_address.rstrip('/') + '/' + self.path.lstrip('/')
        if not url.startswith('http'):
            url = 'https://' + url

        url = url + '?c=' + str(self.page_size)
        url = url + '&pi=' + str(self.parent_identifier)

        if self.platform_short_name:
            url = url + '&sat=' + self.platform_short_name
        if self.product_type:
            url = url + '&type=' + self.product_type
        if self.timeliness:
            url = url + '&timeliness=' + self.timeliness
        if geometry and geometry != 'None':
            url = url + '&geo=' + geometry
        if bounding_box and bounding_box != 'None':
            url = url + '&bbox=' + bounding_box
        if start_date:
            url = url + '&dtstart=' + start_date
        if end_date:
            url = url + '&dtend=' + end_date
        if self.updated_since:
            url = url + '&publication=' + self.updated_since
        if self.sort_key:
            url = url + '&sort=' + self.sort_key

        url = url + '&format=json'
        return url

    def getFileInfoList(self):
        try:
            files = []
            geotemporals = self.get_geotemporals()
            for geometry, bounding_box, start_date, end_date in geotemporals:
                url = self.getURL(geometry, bounding_box, start_date, end_date)
                files += self.https_get_files_from_eop_os_json(url=url)

            files = list(set(files))
            files_to_insert = []
            try:
                for filename in files:
                    filepath = filename[0].split(FILE_URL_SEPARATOR)[0]
                    try:
                        # Format Eumetsat : 2022-05-11T09:47:01Z ou '2022-05-12T13:38:08.088Z'
                        mtime = time.mktime(
                            time.strptime(filename[2].split('.')[0], '%Y-%m-%dT%H:%M:%S')
                        )
                    except ValueError:
                        try:
                            mtime = time.mktime(time.strptime(
                                filename[2],
                                '%Y-%m-%dT%H:%M:%SZ'))
                        except Exception:
                            raise

                    interesting_file = self.fileFilter.isInteresting(
                        filepath,
                        mtime)
                    log.debug(
                        'file %s is interesting ? %s' %
                        (filepath, interesting_file))

                    # add sensingtime if necessairy
                    sensingtime, insert = self.getSensingTime(filepath)

                    if interesting_file:
                        self.nbr_walked_files += 1
                        files_to_insert.append(File(
                            # id_execution=None,
                            filename=filename[0],
                            isDirectory=False,
                            isSymLink=False,
                            size=filename[1],
                            mtime=datetime.fromtimestamp(mtime),
                            sensingtime=sensingtime,
                        ))
            except StopIteration as msg:
                log.debug(
                    "getFileInfoList iteration stopped '%s'" %
                    (str(msg)))
                pass  # fin normale de l'iteration

            if files_to_insert:
                self.fileInfoDatabase.addFileList(files_to_insert)
        except DC_UrlError as msg:
            log.debug(
                'https_iteration error : %s. Stopping iteration.' %
                (str(msg)))
            raise msg

        self.updateValidExecutionStatus(True)

    @staticmethod
    def extract_files_from_json(data) -> List[Tuple]:
        """
        Extract file information from the JSON data
        Args:
            data: the JSON data

        Returns:
            A list of tuples containing file information for each file.
        """

        files = []
        file_name = ''
        for item in data:
            try:
                file_name = item['properties']['identifier']
                https_url = item['properties']['links']['data'][0]['href']
                #  "updated": "2022-04-28T14:01:35.756Z",
                date_time = item['properties']['updated']
                size = item['properties']['productInformation']['size']

                files.append((
                    file_name + FILE_URL_SEPARATOR + https_url, size, date_time
                ))

            except NameError as error:
                msg = 'File information is not complete: %s (%s)' % (file_name, error)
                raise DC_ExtractingError(msg)

        return files

    def get_session(self):
        credentials = (self.username, self.password)
        apis_endpoint = 'https://' + self.server_address
        return AccessToken(credentials=credentials, apis_endpoint=apis_endpoint, timeout=self.timeout)

    def https_get_files_from_eop_os_json(self, url=''):
        log.debug('https_eop_os : url=%s' % url)
        index = 0
        next_link = True
        final_items = []
        try:
            access_token = self.get_session()

            # For each page
            while next_link:
                url_idx = url + '&si=' + str(index)

                with requests.get(url_idx, auth=access_token.auth,
                                  stream=True, timeout=self.timeout) as response:
                    response.raise_for_status()
                    response.raw.decode_content = True
                    data_json = json.loads(response.content)
                    final_items.extend(data_json['features'])
                    index = index + self.page_size

                # Check if there is a next page to read
                if 'next' not in data_json['properties']['links']:
                    next_link = False

        except requests.exceptions.HTTPError as msg:
            error = DC_HttpError('HTTP error : %s' % msg)
            log.debug(error)
            raise error

        except requests.exceptions.RequestException as msg:
            error = DC_UrlError('URL error : %s' % msg)
            log.debug(error)
            raise error

        except Exception as msg:
            error = DC_Error('JSON error : %s' % msg)
            log.debug(error)
            raise error

        return self.extract_files_from_json(final_items)

    def verify_geotemporal(self, source: str):
        try:
            if source.strip().startswith('[') and source.strip().endswith(']'):
                geo_temporal = json.loads(source)
            else:
                file_list = Path(source)
                if not file_list.is_file:
                    log.warning(f'{source} is not a file.')
                    return None
                with open(file_list, 'r') as fd:
                    geo_temporal = json.load(fd)
            result_liste = []
            for elt in geo_temporal:
                geometry = elt.get('geometry', None)
                bbox = elt.get('bounding_box', None)
                start = elt.get('start_date', None)
                stop = elt.get('end_date', None)

                if (not geometry and not bbox):
                    error = f'Geo-temporal configuration error : geometry or bounding_box must be defined (elt :{elt})'
                    log.warning(error)
                    return None
                if not start or not stop:
                    error = f'Geo-temporal configuration error : start_date and end_date must be defined (elt :{elt})'
                    log.warning(error)
                    return None

                result_liste.append(
                    (
                        str(geometry),
                        None if geometry else str(bbox),
                        datetime.strftime(dateparser.parse(start), self.date_format),
                        datetime.strftime(dateparser.parse(stop), self.date_format),
                    )
                )

            return result_liste
        except json.JSONDecodeError as error:
            log.warning('Geo-temporal configuration error : {}'.format(error))
            return None

    def get_geotemporals(self):
        if self.geotemporal_list is not None:
            return self.geotemporal_list
        return [(self.geometry, self.bounding_box, self.start_date, self.end_date)]


class HTTPBearerAuth(AuthBase):
    """Attaches HTTP Bearer Authentication to the given Request object.

    Attributes:
        token: Bearer token

    Arguments:
        token: Bearer token
    """

    def __init__(self, token: str) -> None:
        self.token = token

    def __call__(self, request: requests.PreparedRequest) -> requests.PreparedRequest:
        request.headers['authorization'] = f'Bearer {self.token}'
        return request


class AccessToken:
    """Eumetsat API access token.

    Used to handle requesting of API tokens and there refreshment after expiration.

    Attributes:
        request_margin: seconds before expiration to start re-requesting
        expires_in: seconds before token expiration
        access_token: the value of the access token

    Arguments:
        cache (default: true): if true, use data from previous request until expiration
    """

    request_margin: int = 15  # seconds
    _expiration: int = 0
    _access_token: str = ''

    credentials: Credentials
    token_url: str
    cache: bool

    def __init__(
        self, credentials: Iterable[str], apis_endpoint: str, timeout: int, cache: bool = True
    ) -> None:
        self.credentials = Credentials(*credentials)
        self.token_url = apis_endpoint + '/token'
        self.cache = cache
        self.timeout = timeout

    def __str__(self) -> str:
        return self._access_token

    @property
    def expiration(self) -> datetime:
        """Expiration of the current token string"""
        return datetime.fromtimestamp(self._expiration)

    @property
    def access_token(self) -> str:
        """Token string"""
        expires_in = self._expiration - time.time()
        if not self.cache or expires_in < self.request_margin:
            self._update_token_data()
        return self._access_token

    @property
    def auth(self) -> AuthBase:
        return HTTPBearerAuth(self.access_token)

    def _update_token_data(self) -> None:
        auth = HTTPBasicAuth(*self.credentials)
        now = time.time()
        response = requests.post(
            self.token_url, auth=auth, data={'grant_type': 'client_credentials'}, timeout=self.timeout
        )
        response.raise_for_status()
        token_data = response.json()
        self._expiration = now + token_data['expires_in']
        self._access_token = token_data['access_token']
