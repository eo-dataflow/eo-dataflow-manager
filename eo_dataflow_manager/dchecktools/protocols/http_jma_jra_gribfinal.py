

import logging
import socket
import urllib.error
import urllib.parse
import urllib.request

from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_ConnectionError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

DEFAULT_SERVER_PORT = 80
DEFAULT_USERNAME = ''
DEFAULT_PASSWORD = ''

log = logging.getLogger('http_jma_jra_gribfinal')
log.setLevel(logging.INFO)


LOOK_FOR_LAST_N_DAYS = 2


class Protocol_http_jma_jra_gribfinal(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(60)

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server = None

        self.check_infos = ['size', 'mtime']
        self._checkable_infos = ['size', 'mtime']

        self.__current_path = None
        self.__current_dbfile = None

        self.__tmpdirBlocs = {}
        self.__session = None

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        # check for required config
        if config.path is not None:
            self.path = config.path
        else:
            raise DC_ConfigError('Configuration error : missing path')

        if config.server_address is not None:
            self.server = config.server_address
        else:
            raise DC_ConfigError(
                'Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

    def setDirectoryFilter(self, directoryFilter):
        # TODO : avertir que l'on ne gere pas le directoryfilter... verifier s'il est egal a celui par defaut ?
        #        if directoryFilter:
        # raise DC_ConfigError, "Configuration error : directory filter is not
        # used for http_jma_jra_gribfinal protocol"

        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        #        if fileFilter:
        # raise DC_ConfigError, "Configuration error : file filter is not used
        # for http_jma_jra_gribfinal protocol"

        self.fileFilter = fileFilter

    def getFileInfoList(self):

        baseurl = 'http://%s/%s/' % (self.server, self.path)

        print('SERVER : %s PATH : %s' % (self.server, self.path))
        print('USERNAME : %s PASSWORD : %s' % (self.username, self.password))
        # pour la gestion de l'authentification http
        self.__password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
        self.__password_mgr.add_password(
            None, self.server + '/' + self.path, self.username, self.password)
        self.__auth_handler = urllib.request.HTTPBasicAuthHandler(
            self.__password_mgr)
        self.__http_opener = urllib.request.build_opener(self.__auth_handler)
        urllib.request.install_opener(self.__http_opener)

        # after this, all calls to urllib2.urlopen use the http_opener
        urllib.request.install_opener(self.__http_opener)

        # build url's files list
        #url_list= []
        # for i in range(0,LOOK_FOR_LAST_N_DAYS):
        #    date = time.localtime(time.time()-i*86400)
        #    for hours in [ '0000', '0600', '1200', '1800' ]:
        #        predicted_filename = '%Y%m%d'+".%s.pmw.rainsum.global.24.jpg"%(hours)
        #        file_url = time.strftime(predicted_filename, date)
        #        url_list += [ file_url ]

        # for each url, verify the file availability
        # for file_url in url_list:
        #    url = os.path.join(baseurl, file_url)
        #    #req = urllib2.Request(url)
        content = None
        try:
            content = urllib.request.urlopen(baseurl)
        except urllib.error.HTTPError as e:
            if e.code == 404:
                pass
            else:
                raise
        except urllib.error.URLError as msg:
            error = DC_ConnectionError(
                'http_jma_jra_gribfinal url error : %s (url=%s)' %
                (msg.reason, baseurl), db=self.fileInfoDatabase)
            log.error(error)
        print(content.readlines())

        #    else:
        #        current_dbfile = File()
        #        current_dbfile.filename = url
        #        current_dbfile.isDirectory = False
        #        current_dbfile.isSymLink = False

        #        if 'size' in self.check_infos:
        #            current_dbfile.size = 0
        #        if 'mtime' in self.check_infos:
        #            #current_dbfile.mtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(mtime_value))
        #            current_dbfile.mtime = "2008-01-01 00:00:00"
        #        self.fileInfoDatabase.addFile(current_dbfile)

        self.updateValidExecutionStatus(True)
