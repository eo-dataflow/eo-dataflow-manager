
import json
import logging
import re
import socket
from datetime import datetime
from itertools import chain
from typing import Optional, Tuple

import boto3
import botocore
from requests import PreparedRequest

CLIENT_NAME = 'copernicus-marine-client'
MARINE_DATA_LAKE_CDN_LISTING_ENDPOINT = 'https://marine.copernicus.eu'
MARINE_DATA_LAKE_NON_LISTABLE_BUCKETS = {
    'mdl-native': {
        'endpoint_url': MARINE_DATA_LAKE_CDN_LISTING_ENDPOINT,
        'bucket': 'mdl-native-list',
    },
    'mdl-native-dta': {
        'endpoint_url': MARINE_DATA_LAKE_CDN_LISTING_ENDPOINT,
        'bucket': 'mdl-native-list-dta',
    },
}

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR, File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_ExtractingError, DC_UrlError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

DEFAULT_SERVER_PORT = 443
DEFAULT_USERNAME = ''
DEFAULT_PASSWORD = ''
DEFAULT_PAGE_SIZE = 2000
DEFAULT_DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
DEFAULT_SORT_KEY = 'start_date'


log = logging.getLogger('https_s3_cmems')
log.setLevel(logging.DEBUG)


class Protocol_https_s3_cmems(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(self.getDefaultTimeout())

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server_address = None
        self.provider = None
        self.short_name = None
        self.page_size = DEFAULT_PAGE_SIZE
        self.start_date = None
        self.end_date = None
        self.updated_since = None
        self.date_format = DEFAULT_DATE_FORMAT
        self.sort_key = DEFAULT_SORT_KEY
        self.bounding_box = None

        self.check_infos = ['size', 'mtime', 'sensingtime']
        self._checkable_infos = ['size', 'mtime']

        self.__current_path = None

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # check for required config
        if config.path is None:
            raise DC_ConfigError('Configuration error : missing path')
        if config.server_address is None:
            raise DC_ConfigError(
                'Configuration error : missing server')
        # overwrite default config
        self.path = config.path
        self.server_address = config.server_address
        self.dataset = config.protocol_option.get('dataset', None)
        self.version = config.protocol_option.get('version', None)
        self.part = config.protocol_option.get('part', None)

        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

        if 'BoundingBox' in config.protocol_option and config.protocol_option['BoundingBox']:
            self.bounding_box = config.protocol_option['BoundingBox']

    def setDirectoryFilter(self, directory_filter):
        self.directoryFilter = directory_filter

    def setFileFilter(self, file_filter):
        self.fileFilter = file_filter


    def get_url(self) -> str:
        url = self.server_address.rstrip('/') + '/' + self.path.lstrip('/')
        if not url.startswith('http'):
            url = 'https://' + url

        return url


    def getFileInfoList(self):

        try:
            url = self.get_url()
            files = self.extract_files_from_s3(url)

            files_to_insert = []
            try:
                for filename, size, file_date in files:
                    filepath = filename.split(FILE_URL_SEPARATOR)[0]
                    mtime = file_date.timestamp()

                    interestingFile = self.fileFilter.isInteresting(
                        filepath,
                        mtime)
                    log.debug(
                        'file %s is interesting ? %s' %
                        (filepath, interestingFile))

                    # add sensingtime if necessairy
                    # sensingtime, insert = self.getSensingTime(filepath)

                    if interestingFile:
                        self.nbr_walked_files += 1
                        files_to_insert.append(File(
                            # id_execution=None,
                            filename=filename,
                            isDirectory=False,
                            isSymLink=False,
                            size=size,
                            mtime=datetime.fromtimestamp(mtime),
                            # sensingtime=sensingtime,
                        ))
            except StopIteration as msg:
                log.debug(
                    "getFileInfoList iteration stopped '%s'" %
                    (str(msg)))
                pass  # fin normale de l'iteration

            if files_to_insert:
                self.fileInfoDatabase.addFileList(files_to_insert)
        except DC_UrlError as msg:
            log.debug(
                'https_iteration error : %s. Stopping iteration.' %
                (str(msg)))
            raise msg

        self.updateValidExecutionStatus(True)

    def extract_files_from_s3(self, dataset_path):

        files = []

        if self.dataset:
            uri = self.get_catalog_uri_catalogue_of_dataset(self, self.dataset, self.version, self.part)
        else:
            uri = dataset_path

        files = self.get_product_filenames_on_s3(uri, self.getDefaultTimeout())

        return files



    def get_catalog_uri_catalogue_of_dataset(self, dataset_id, version_filter=None, part_filter=None):

        uri = ''
        msg = (f'get_catalog_uri_catalogue_of_dataset error : search by dataset id is currently not available: '
               f'copernicus_marine_client is only python compatible >= 3.9')
        log.debug(msg)
        raise DC_ExtractingError(msg)

        # copernicus_marine_client is only python compatible >= 3.9
        # from copernicus_marine_client.core_functions.describe import describe_function

        uri = None
        json_dump = describe_function(
            include_description = False,
            include_datasets = True,
            include_keywords = False,
            contains = [dataset_id],
            overwrite_metadata_cache = False,
            no_metadata_cache = False,
            disable_progress_bar = False,
            staging = False,

        )
        dump = json.loads(json_dump)
        if len(dump) == 0:
            msg = f'get_catalog_uri_catalogue_of_dataset error : dataset "{dataset_id}" not found.'
            log.debug(msg)
            raise DC_ExtractingError(msg)

        versions = dump['products'][0]['datasets'][0]['version']

        version_id = None
        if version_filter:
            for no, version in enumerate(versions):
                if version['label'] == version_filter:
                    version_id = no
                    break;
        else:
            version_id = 0
        if not version_id:
            msg = f'get_catalog_uri_catalogue_of_dataset error : version "{version_filter}" not found.'
            log.debug(msg)
            raise DC_ExtractingError(msg)

        part_id = None
        parts = versions[version_id]['parts']
        if part_filter:
            for no, part in enumerate(parts):
                if part['name'] == part_filter:
                    part_id = no
                    break
        else:
            part_id = 0
        if not part_id:
            msg = f'get_catalog_uri_catalogue_of_dataset error : part "{part_filter}" not found.'
            log.debug(msg)
            raise DC_ExtractingError(msg)

        uri = None
        services = parts[part_id]['services']
        for service in services:
            if service['service_type']['service_name'] == 'original-files':
                uri = service['uri']
                break
        if not uri:
            msg = f'get_catalog_uri_catalogue_of_dataset error : service "original-files" not found.'
            log.debug(msg)
            raise DC_ExtractingError(msg)
        return uri

    @staticmethod
    def parse_dataset_url(data_path: str) -> Tuple[str, str, str]:
        match = re.search(r'^(http|https):\/\/([\w\-\.]+)(\/.*)', data_path)
        if match:
            endpoint_url = match.group(1) + '://' + match.group(2)
            full_path = match.group(3)
            segments = full_path.split('/')
            bucket = segments[1]
            path = '/'.join(segments[2:])
            return endpoint_url, bucket, path
        else:
            raise Exception(f'Invalid data path: {data_path}')

    def get_product_filenames_on_s3(self, uri='', timeout=120):
        log.debug('https_cmems_s3 : url=%s' % uri)

        def _construct_url_with_query_params(url, query_params: dict) -> Optional[str]:
            req = PreparedRequest()
            req.prepare_url(url, query_params)
            return req.url

        def _construct_query_params_for_marine_data_store_monitoring(
                username: Optional[str] = None,
        ) -> dict:
            query_params = {'x-cop-client': CLIENT_NAME}
            if username:
                query_params['x-cop-user'] = username
            return query_params

        def _add_custom_query_param(params, context, **kwargs):
            """
            Add custom query params for MDS's Monitoring
            """
            params['url'] = _construct_url_with_query_params(
                params['url'],
                _construct_query_params_for_marine_data_store_monitoring(self.username),
            )

        endpoint_url, bucket, path = self.parse_dataset_url(uri)

        original_bucket = bucket
        if bucket in MARINE_DATA_LAKE_NON_LISTABLE_BUCKETS:
            alternative = MARINE_DATA_LAKE_NON_LISTABLE_BUCKETS[bucket]
            bucket = alternative['bucket']
            endpoint_url = alternative['endpoint_url']

        s3_session = boto3.Session()
        s3_client = s3_session.client(
            's3',
            config=botocore.config.Config(
                # Configures to use subdomain/virtual calling format.
                s3={'addressing_style': 'virtual'},
                signature_version=botocore.UNSIGNED,
            ),
            endpoint_url=endpoint_url,
        )

        s3_client.meta.events.register(
            'before-call.s3.ListObjects', _add_custom_query_param
        )

        paginator = s3_client.get_paginator('list_objects')
        page_iterator = paginator.paginate(Bucket=bucket, Prefix=path)

        s3_objects = chain(*map(lambda page: page['Contents'], page_iterator))

        files_found = []
        for s3_object in s3_objects:
            path = s3_object['Key']
            file_name = path.split('/')[-1]
            s3_uri = f's3://{original_bucket}/' + path
            file_name + FILE_URL_SEPARATOR + s3_uri
            files_found.append(
                (
                    file_name + FILE_URL_SEPARATOR + s3_uri,
                    s3_object['Size'],
                    s3_object['LastModified'],
                )

            )

        return files_found
