
import logging
import os
import sys
from datetime import datetime

from eo_dataflow_manager.dchecktools.common.basefileinfos import File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_LocalpathError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

log = logging.getLogger('localpath')
log.setLevel(logging.INFO)


class Protocol_localpath(AbstractProtocol):

    def __init__(self):
        AbstractProtocol.__init__(self)
        self.check_infos = ['size', 'mtime', 'sensingtime']
        #self.check_infos = [ 'size', 'mtime']
        self._checkable_infos = ['size', 'atime',
                                 'mtime', 'ctime', 'uid', 'gid']
        self.follow_symlink = False

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)
        # check for required config
        if config.path is not None:
            self.path = config.path
        else:
            raise DC_ConfigError('Configuration error : missing path')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

        if self.config.follow_symlink:
            self.follow_symlink = self.config.follow_symlink

    def setDirectoryFilter(self, directoryFilter):
        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):
        self.fileFilter = fileFilter

    def getFileInfoList(self):
        if os.path.exists(self.path):
            files_to_insert = self.__walkDir(self.path)
            if files_to_insert:
                self.fileInfoDatabase.addFileList(files_to_insert)
        elif len(self.directories_smart_crawler) == 0:
            error = DC_LocalpathError(
                'path does not exists : %s' % (self.path))
            log.error(error)
            sys.exit(1)
        else:
            log.warning('path does not exists : %s' % (self.path))
        self.updateValidExecutionStatus(True)

    def __walkDir(self, path):
        files_to_insert = []
        excludeDirectory = ''
        for (dirpath, dirnames, filenames) in os.walk(path, followlinks=self.follow_symlink):
            current_directory = os.path.split(dirpath)
            if excludeDirectory != '' and dirpath[:len(excludeDirectory)] == excludeDirectory:
                log.debug('excluded path root : %s' % (dirpath))
                continue

            if dirpath != path  and \
                    not self.directoryFilter.isInterestingDirectory(dirname=current_directory[1],
                                                                    dirpath=current_directory):
                log.debug('memorization of the excluded path : %s' % (dirpath))
                excludeDirectory = dirpath
                continue

            for filename in filenames:
                filePath = os.path.join(dirpath, filename)
                try:
                    fileStat = os.stat(filePath)
                    mtime = fileStat.st_mtime

                    if self.fileFilter is None or self.fileFilter.isInteresting(filePath, mtime):

                        sensingtime, insert = self.getSensingTime(filePath)

                        if insert:
                            file = File(
                                id_execution=None,
                                filename=filePath,
                                isDirectory=False,
                                isSymLink=os.path.islink(filePath),
                                size=fileStat.st_size,
                                mtime=datetime.fromtimestamp(mtime),
                                sensingtime=sensingtime,
                            )
                            # print("Adding file:", file, "in dir ", dirpath)
                            files_to_insert.append(file)
                except:
                    pass

        return files_to_insert
