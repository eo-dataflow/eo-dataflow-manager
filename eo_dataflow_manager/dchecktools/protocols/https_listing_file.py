import logging
import os
import re
import socket
import ssl
import time
import urllib.error
import urllib.parse
import urllib.request
from datetime import datetime

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR, File
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_UrlError
from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol

FILE_FORMAT_REGEXP = '([rwxdblcpsStT-]{10}). +([0-9]+) +(.*?) +(.*?) +([0-9]*?) +([a-zA-Z]{3} +[0-9]* [0-9]*:[0-9]*) (.*)\n?'
#                            rights             links   user   group    size     date (ex: Sep 17 11:04)            name
FILE_FORMAT_REGEXP2 = '([rwxdlcpsStT-]{10}). +([0-9]+) +(.*?) +(.*?) +([0-9]*?) +([a-zA-Z]{3} +[0-9]* +[0-9]*) (.*)\n?'
#                            rights             links   user   group    size     date (ex: Sep 17 2019)         name
DIR_FORMAT_REGEXP = '(.*):\n'
LNK_FORMAT_REGEXP = '(.*) -> (.*)'
lnk_regexp = re.compile(LNK_FORMAT_REGEXP)

CALMOTHS = dict((x, i + 1) for i, x in
                enumerate(('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'))
                )


DEFAULT_SERVER_PORT = 443
DEFAULT_USERNAME = ''
DEFAULT_PASSWORD = ''


log = logging.getLogger('https_listing_file')
log.setLevel(logging.DEBUG)

class Protocol_https_listing_file(AbstractProtocol):

    def __init__(self,data_server_address=''):
        AbstractProtocol.__init__(self)

        # default socket timeout
        socket.setdefaulttimeout(self.getDefaultTimeout())

        # default config
        self.port = DEFAULT_SERVER_PORT
        self.username = DEFAULT_USERNAME
        self.password = DEFAULT_PASSWORD

        # required config
        self.path = None
        self.server_address = None
        self.data_server_address = data_server_address
        self.check_certificate = True


        self.check_infos = ['size', 'mtime']
        self._checkable_infos = [
            'size',
            'mtime']

        self.__current_path = None
        self.now_ = datetime.today()

    def setConfig(self, config):
        AbstractProtocol.setConfig(self, config)

        # overwrite default config
        if config.server_port is not None:
            self.port = config.server_port

        if config.auth_username != '' and config.auth_username is not None:
            self.username = config.auth_username

        if config.auth_password != '' and config.auth_password is not None:
            self.password = config.auth_password

        # check for required config
        if config.path is not None:
            self.path = config.path
        else:
            raise DC_ConfigError('Configuration error : missing path')

        if config.server_address is not None:
            self.server_address = config.server_address
        else:
            raise DC_ConfigError(
                'Configuration error : missing server_address')

        # add default values if needed
        if config.check_infos is None:
            config.check_infos = self.check_infos

        if self.config.logLevel:
            log.setLevel(self.config.logLevel)

        if self.data_server_address == '':
            self.data_server_address = self.server_address

        if config.protocol_option and 'CheckCertificate' in config.protocol_option:
            if config.protocol_option['CheckCertificate'].lower() == 'false':
                self.check_certificate = False

    def setDirectoryFilter(self, directoryFilter):

        self.directoryFilter = directoryFilter

    def setFileFilter(self, fileFilter):

        self.fileFilter = fileFilter

    def getFileInfoList(self):
        url = self.server_address.rstrip('/') + '/' + self.path.lstrip('/')
        if not url.startswith('http'):
            url = 'https://' + url

        data_server_address = self.data_server_address.rstrip('/')
        if not data_server_address.startswith('http'):
            data_server_address = 'https://' + data_server_address + '/'

        try:
            files = self.https_get_files_from_listing_file(
                url=url,
                data_server_address=data_server_address,
                timeout=self.getDefaultTimeout()
            )

            files_to_insert = []
            try:
                for filename in files:
                    filepath = filename[0].split(FILE_URL_SEPARATOR)[0]
                    try:
                        mtime = self._parse_datetime(filename[2])
                        """
                        mtime = time.mktime(
                        time.strptime(
                            filename[2],
                            '%b %d %Y'))
                    except ValueError:
                        try:
                            mtime = time.mktime(
                                time.strptime(
                                    filename[2],
                                    '%b %d %H:%M'))
                        """
                    except:
                        raise
                    interestingFile = self.fileFilter.isInteresting(
                        filepath, mtime)
                    log.debug(
                        'file %s is interesting ? %s' %
                        (filepath, interestingFile))

                    if interestingFile:
                        self.nbr_walked_files += 1
                        files_to_insert.append(File(
                            id_execution=None,
                            filename=filename[0],
                            isDirectory=False,
                            isSymLink=False,
                            size=filename[1],
                            mtime=datetime.fromtimestamp(mtime),
                        ))
            except StopIteration as msg:
                log.debug(
                    "getFileInfoList iteration stopped '%s'" %
                    (str(msg)))
                pass  # fin normale de l'iteration

            if files_to_insert:
                self.fileInfoDatabase.addFileList(files_to_insert)
        except DC_UrlError as msg:
            log.debug(
                'https_iteration error : %s. Stopping iteration.' %
                (str(msg)))
            raise msg

        self.updateValidExecutionStatus(True)


    def getLnkSrcDst(self, lnk_str):
        m_lnk_str = lnk_regexp.match(lnk_str)
        if m_lnk_str is not None:
            name_src, name_dst = m_lnk_str.groups()
        else:
            pass
        return name_src, name_dst

    def extract_files_from_listing_file(self, data, data_server_address=''):
        current_dir = ''
        file_regexp = re.compile(FILE_FORMAT_REGEXP)
        file_regexp2 = re.compile(FILE_FORMAT_REGEXP2)
        dir_regexp = re.compile(DIR_FORMAT_REGEXP)

        files = []
        for line in data.splitlines():
            line_recognized = False
            if line.startswith('total '):
                continue
            if line.isspace():
                continue

            m = file_regexp.match(line)
            if m is not None:
                line_recognized = True
                rights, links, user, group, size, date_time, name = m.groups()
                if rights[0] == 'l':
                    name, dst = self.getLnkSrcDst(name)
                path = os.path.join(current_dir, name)
                files.append((
                    path + FILE_URL_SEPARATOR + data_server_address.rstrip('/') + '/' + path.lstrip('/'),
                    size,
                    date_time
                ))
                continue

            m = file_regexp2.match(line)
            if m is not None:
                line_recognized = True
                rights, links, user, group, size, date_time, name = m.groups()
                if rights[0] == 'l':
                    name, dst = self.getLnkSrcDst(name)
                path = os.path.join(current_dir, name)
                files.append((
                    path + FILE_URL_SEPARATOR + data_server_address.rstrip('/') + '/' + path.lstrip('/'),
                    size,
                    date_time
                ))
                continue

            m = dir_regexp.match(line)
            if m is not None:
                line_recognized = True
                dirpath = m.groups()
                dirpath = dirpath[0]
                # print('DIR_FMT : dir=%s' % (dirpath))
                size = 0
                current_dir = dirpath
                continue

            if not line_recognized:
                log.warning('Impossible to decode this line : %s' % (line))
                continue

        return files

    def https_get_files_from_listing_file(self, url='', data_server_address='', timeout=120):
        log.debug('https_listing_file : url=%s' % url)
        try:
            context = None
            if not self.check_certificate:
                context = ssl._create_unverified_context()

            data = urllib.request \
                .urlopen(url, timeout=timeout, context=context) \
                .read() \
                .decode('utf-8')

        except urllib.error.URLError as msg:
            error = DC_UrlError('URL/HTTP error : %s' % (msg))
            log.debug(error)
            raise error

        return self.extract_files_from_listing_file(data, data_server_address)

    def _parse_datetime(self, datetime_: str):
        # match 'Jan  1  2020','Jan  1 20:30', 'Oct 21  2020' or 'Oct 21 10:20' ?
        date_time = re.match('([a-zA-Z]{3}) +([0-9]*) +([0-9]{2}:[0-9]{2}|[0-9]{4})', datetime_).groups()
        if date_time:
            month = CALMOTHS[date_time[0]]
            day = int(date_time[1])
            year = datetime.today().year  # by default, current year
            if len(date_time[2]) == 5:
                # the 3rd elt is the time
                hour, min = list(map(int, date_time[2].split(':')))
                # correction of the sliding year
                read_date = datetime(year, month, day, hour, min)
                if read_date > self.now_:
                    year = year - 1
            else:
                # the 3rd elt is the year
                year = int(date_time[2])
                hour, min = 0, 0
        else:

            raise ValueError(
                "Could not parse time/year in string: '%s'" %
                datetime_)

        dt = datetime(year, month, day, hour, min)
        mtime = time.mktime(dt.timetuple())
        return mtime
