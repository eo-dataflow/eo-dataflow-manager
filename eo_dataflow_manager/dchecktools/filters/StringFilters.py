#!/usr/bin/env python

import logging
import re
from datetime import datetime

log = logging.getLogger('StringFilters')
log.setLevel(logging.INFO)


class FileFilter(object):
    """
    A FileFilter object just says if a filename is interesting or not,
    based on reference regexp or string
    """

    def __init__(self, forceRegexp=None, ignoreRegexp=None, ignoreOlderThan=None, ignoreNewerThan=None, interestingByDefault=True):

        self.setForceInterestingRegex(forceRegexp)
        self.setIgnoreRegex(ignoreRegexp)
        self.setIgnoreOlderThan(ignoreOlderThan)
        self.setIgnoreNewerThan(ignoreNewerThan)
        self.setDefault(interestingByDefault)

    def setForceInterestingRegex(self, rgx):
        rgxList = rgx if isinstance(rgx, list) else [rgx]
        self.__forceRegexp = [re.compile(r) for r in rgxList if r is not None]

    def setIgnoreRegex(self, rgx):
        rgxList = rgx if isinstance(rgx, list) else [rgx]
        self.__ignoreRegexp = [re.compile(r) for r in rgxList if r is not None]

    def setIgnoreOlderThan(self, date):
        self.__ignoreOlderThan = date

    def setIgnoreNewerThan(self, date):
        self.__ignoreNewerThan = date

    def setDefault(self, interesting):
        self.__interestingByDefault = interesting

    def isInteresting(self, file, mtime=None):
        """
        Input : String file
        Output : Boolean
        Test whether a file string is interesting or not, according to
        current object configuration. Priority order :
        forceFiles(Regexp) > ignoreFiles(Regexp) > date related checks > interestingByDefault
        """

        if self.__ignoreNewerThan is not None and mtime is not None and \
                datetime.fromtimestamp(mtime) > self.__ignoreNewerThan:
            return False

        if self.__ignoreOlderThan is not None and mtime is not None and \
                datetime.fromtimestamp(mtime) < self.__ignoreOlderThan:
            return False

        # check ignore file regexp
        for regexp in self.__ignoreRegexp:
            if regexp.match(file):
                return False

        # check force file regexp
        for regexp in self.__forceRegexp:
            if regexp.match(file):
                return True

        return self.__interestingByDefault

    def isInterestingDirectory(self, dirname, mtime=None, dirpath=None):
        """
        Input : String file
        Output : Boolean
        Test whether a file string is interesting or not, according to
        current object configuration. Priority order :
        forceFiles(Regexp) > ignoreFiles(Regexp) > date related checks > interestingByDefault
        """

        if self.__ignoreNewerThan is not None and mtime is not None and \
                datetime.fromtimestamp(mtime) > self.__ignoreNewerThan:
            return False

        if self.__ignoreOlderThan is not None and mtime is not None and \
                datetime.fromtimestamp(mtime) < self.__ignoreOlderThan:
            return False

        # check ignore file regexp
        for regexp in self.__ignoreRegexp:
            if regexp.search(dirname):
                return False
        # check force file regexp
        for regexp in self.__forceRegexp:
            if regexp.search(dirname):
                return True

        return self.__interestingByDefault
