#!/usr/bin/env python

import logging
import os
import sys
from datetime import datetime
from optparse import OptionParser

from eo_dataflow_manager.dchecktools.common import workingdir
from eo_dataflow_manager.dchecktools.common.basefileinfos import SqliteBaseFilesInfos
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError
from eo_dataflow_manager.dchecktools.common.spliturl import setConfigFromUrl
from eo_dataflow_manager.dchecktools.filters.StringFilters import FileFilter
from eo_dataflow_manager.dchecktools.reports import Report_ListFiles

"""Start DReport (and end) here - read arguments, set global settings, etc."""

logging.basicConfig()
log = logging.getLogger('dreport')
log.setLevel(logging.INFO)

VERSION = '0.7b'


class Config:

    def __init__(self):
        self.report = None
        self._start_time = None
#        self.date_from = None
#        self.date_to = None
#        self.configfile = None
#
#        self.output_path = None
#
        # default workspace
        self.default_workspace = workingdir.DEFAULT_CONFIG_WORKING_DIR

        # database configuration
        self.database_path = None
        self.check_infos = None
        self.output_filepath = None
        self.timestamp_filepath = None

        # misc, unused here
        self.test_mode = False
        self.ignore_config_diff = False

        self.logLevel = logging.INFO

        # filters
        self.forceFiles = []
        self.forceFilesRegexp = []
        self.ignoreFiles = []
        self.ignoreFilesRegexp = []
        self.default_file_accept = True

        self.forceDirectories = []
        self.forceDirectoriesRegexp = []
        self.ignoreDirectories = []
        self.ignoreDirectoriesRegexp = []
        self.default_directory_accept = True

    def getNodeString(self, nodename):
        nodestr = ''
        if isinstance(
                self.__dict__[nodename],
                list) and len(
                self.__dict__[nodename]) == 0:
            return nodestr
        elif self.__dict__[nodename] is not None:
            return '<%s>%s</%s>' % (nodename,
                                    self.__dict__[nodename], nodename)

        return nodestr

    def loadFromModule(self, modulepath):
        (moddir, modname) = os.path.split(modulepath)
        sys.path.append(moddir)
        log.debug('Module config file : %s' % (modulepath + '.py'))
        try:
            module = __import__(modname)
        except ImportError as msg:
            error_str = 'Cannot import config file : %s (%s)' % (
                modulepath + '.py', msg)
            error = DC_ConfigError(error_str)
            log.error(error)
            raise Exception(error_str)  # Pour integration dans le eo-dataflow-manager
            # sys.exit(1)
        module_attr = list(module.__dict__.keys())
        self.configfile = modulepath

        # default workspace
        if 'DEFAULT_WORKSPACE' in module_attr:
            self.default_workspace = module.DEFAULT_WORKSPACE

        if 'REPORT' in module_attr:
            self.report = module.REPORT

        if 'TIMESTAMP_FILE' in module_attr:
            self.timestamp_filepath = module.TIMESTAMP_FILE

        if 'OUTPUT_FILE' in module_attr:
            self.output_filepath = module.OUTPUT_FILE

        if 'IGNORE_CONFIG_DIFFERENCES' in module_attr:
            self.ignore_config_diff = module.IGNORE_CONFIG_DIFFERENCES

        # File filters
        if 'FORCE_FILES' in module_attr:
            self.forceFiles = module.FORCE_FILES
        if 'FORCE_FILES_REGEXP' in module_attr:
            self.forceFilesRegexp = module.FORCE_FILES_REGEXP
        if 'IGNORE_FILES' in module_attr:
            self.ignoreFiles = module.IGNORE_FILES
        if 'IGNORE_FILES_REGEXP'in module_attr:
            self.ignoreFilesRegexp = module.IGNORE_FILES_REGEXP
        if 'DEFAULT_FILE_ACCEPT' in module_attr:
            self.default_file_accept = module.DEFAULT_FILE_ACCEPT

        # Directory filters
        if 'FORCE_DIRECTORIES' in module_attr:
            self.forceDirectories = module.FORCE_DIRECTORIES
        if 'FORCE_DIRECTORIES_REGEXP' in module_attr:
            self.forceDirectoriesRegexp = module.FORCE_DIRECTORIES_REGEXP
        if 'IGNORE_DIRECTORIES' in module_attr:
            self.ignoreDirectories = module.IGNORE_DIRECTORIES
        if 'IGNORE_DIRECTORIES_REGEXP'in module_attr:
            self.ignoreDirectoriesRegexp = module.IGNORE_DIRECTORIES_REGEXP
        if 'DEFAULT_DIRECTORY_ACCEPT' in module_attr:
            self.default_directory_accept = module.DEFAULT_DIRECTORY_ACCEPT


#    def constConfigString(self):
#        # Attention, la constConfig ne doit pas varier dans le temps,
#        # sauf si l'utilisateur change la config
#        # les informations variables, comme _start_time sont a proscrire
#        repr = "<config>"
#        repr += self.getNodeString('configfile')
#        repr += self.getNodeString('test_mode')
#        repr += "<database>"
#        repr += self.getNodeString('database_path')
#        repr += self.getNodeString('check_infos')
#        repr += "</database>"
#        repr += "<protocol>"
#        repr += self.getNodeString('protocol')
#        repr += self.getNodeString('path')
#        repr += self.getNodeString('server_address')
#        repr += self.getNodeString('server_port')
#        repr += self.getNodeString('auth_username')
#        repr += self.getNodeString('auth_password')
#        repr += "</protocol>"
#        repr += "<filters>"
#        repr += self.getNodeString('forceFiles')
#        repr += self.getNodeString('forceFilesRegexp')
#        repr += self.getNodeString('ignoreFiles')
#        repr += self.getNodeString('ignoreFilesRegexp')
#        repr += self.getNodeString('default_file_accept')
#        repr += self.getNodeString('forceDirectories')
#        repr += self.getNodeString('forceDirectoriesRegexp')
#        repr += self.getNodeString('ignoreDirectories')
#        repr += self.getNodeString('ignoreDirectoriesRegexp')
#        repr += self.getNodeString('default_directory_accept')
#        repr += "</filters>"
#        repr += "</config>"
#        return repr

class DReport(object):

    def getOptions(self, argsList=None):
        # Get a parser
        usage = 'usage: %prog [options] (-c configfile | url)'
        parser = OptionParser(usage=usage, version='%prog ' + VERSION)

        # Set need argument
        parser.add_option('-c', '--config',
                          action='store', type='string',
                          dest='configfile', metavar='FILE [REQUIRED]',
                          help='path of a python config file')
        parser.add_option('-t', '--timestamp-file',
                          action='store', type='string',
                          dest='timestamp_filepath', metavar='FILE',
                          help='path of a timestamp file')
        parser.add_option('-o', '--output',
                          action='store', type='string',
                          dest='output', metavar='FILE',
                          help='path of an output file')
        parser.add_option(
            '-d',
            '--database',
            action='store',
            type='string',
            dest='database',
            metavar='FILE',
            help='path of a sqlite database (will be created if not exists)')
        parser.add_option('', '--debug', action='store_true',
                          dest='debug_mode', metavar='',
                          help='run in debug mode (more verbose)',
                          default=False)
        parser.add_option(
            '',
            '--ignore-config-differences)',
            action='store_true',
            dest='ignore_config_diff',
            metavar='',
            help='ignore config differences when generating reports')
    #    parser.add_option("-v", "--verbose", action="store_true",
    #        help="Write output informations (not only errors).",
    #        default=False)

        if argsList is None:
            (options, args) = parser.parse_args()
        else:
            (options, args) = parser.parse_args(argsList)

        # Check if required argument are here:
        if not options.configfile and len(args) == 0:
            log.error('Missing required argument !!!')
            parser.print_help()
            sys.exit(1)

        return options, args

    def run(self, options, args, config=None):
        if config is None:
            config = Config()
        config._start_time = datetime.now()

        url = None
        if len(args) > 0:
            url = args[0]
            setConfigFromUrl(config, url)

        if options.debug_mode:
            config.logLevel = logging.DEBUG

        if options.configfile is not None:
            modulepath = options.configfile
            if modulepath.startswith('WORKSPACE'):
                modulepath = modulepath.replace(
                    'WORKSPACE', config.default_workspace, 1)
            if modulepath[-3:] == '.py':
                modulepath = modulepath[:-3]
            log.debug('Using config file : %s' % modulepath)
            config.loadFromModule(modulepath)

        if options.ignore_config_diff is not None:
            config.ignore_config_diff = options.ignore_config_diff

        if options.database is not None:
            config.database_path = options.database
        else:
            config.database_path = workingdir.getDatabasePathFromUrl(
                url, prefix=config.protocol + '_')

        if options.output is not None:
            config.output_filepath = options.output

        if options.timestamp_filepath is not None:
            config.timestamp_filepath = options.timestamp_filepath

        # set valid path for workspace
        if config.database_path and config.database_path.startswith(
                'WORKSPACE'):
            config.database_path = config.database_path.replace(
                'WORKSPACE', config.default_workspace, 1)
        if config.output_filepath and config.output_filepath.startswith(
                'WORKSPACE'):
            config.output_filepath = config.output_filepath.replace(
                'WORKSPACE', config.output_filepath, 1)
        if config.timestamp_filepath and config.timestamp_filepath.startswith(
                'WORKSPACE'):
            config.timestamp_filepath = config.timestamp_filepath.replace(
                'WORKSPACE', config.output_filepath, 1)

    #    log.debug("Config : "+config.constConfigString())
        report = None
        if config.report is not None:
            report = config.report
        else:
            log.debug('No report specified... defaults to ListFiles report')
            report = Report_ListFiles.Report_ListFiles()
            report.check_infos = ['size', 'mtime']
            report.display_titles = False
            report.display_unmodified_files = False
            report.display_removed_files = True
            report.display_created_files = True
            report.display_modified_files = True
            report.display_file_prefix = True
            report.check_infos = ['size', 'mtime']

        report.setConfig(config)

        # Objets initialization
        bfi = SqliteBaseFilesInfos()
        try:
            bfi.setConfig(config)
        except DC_ConfigError as msg:
            log.error(msg)
            sys.exit(1)
        bfi.createEngine()
        report.setFileInfoDatabase(bfi)

        dirFilter = FileFilter(
            forceRegexp=list(config.forceDirectories) + list(config.forceDirectoriesRegexp),
            ignoreRegexp=list(config.ignoreDirectories) + list(config.ignoreDirectoriesRegexp),
            ignoreOlderThan=None,
            ignoreNewerThan=None,
            interestingByDefault=config.default_directory_accept,
        )
        report.setDirectoryFilter(dirFilter)

        fileFilter = FileFilter(
            forceRegexp=list(config.forceFiles) + list(config.forceFilesRegexp),
            ignoreRegexp=list(config.ignoreFiles) + list(config.ignoreFilesRegexp),
            ignoreOlderThan=None,
            ignoreNewerThan=None,
            interestingByDefault=config.default_file_accept,
        )
        report.setFileFilter(fileFilter)

        log.debug('Starting...')
        report.run()
        log.debug('End !')


def main(options=None, config=None):
    dr = DReport()
    if options is None:
        options, args = dr.getOptions()
    try:
        dr.run(options, args, config)
    except DC_ConfigError as msg:
        logging.exception(msg)
        sys.exit(1)


if __name__ == '__main__':
    main()
