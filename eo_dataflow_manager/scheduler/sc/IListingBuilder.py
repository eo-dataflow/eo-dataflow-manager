#
# -*- coding: UTF-8 -*-
#


class IListingBuilder(object):
    """
    Interface class for L{IListingBuilder}
    """

    def setDownload(self, download, download_config_file):
        raise RuntimeError(
            "setDownload is an abstract method in IListingBuilder")

    def setConfiguration(self, config_node):
        raise RuntimeError(
            "setDownloadConfiguration is an abstract method in IListingBuilder")

    def setWorkingDirectory(self, directory_path):
        raise RuntimeError(
            "setWorkingDirectory is an abstract method in IListingBuilder")

    def getListing(self, token):
        """ returns a file list to download """
        raise RuntimeError(
            "getListing is an abstract method in IListingBuilder")

    def stop(self):
        """ stop listing process """
        raise RuntimeError("stop is an abstract method in IListingBuilder")

    def ackListingStorage(self, token):
        """ acknowledgement : the listing has been stored by the controller,
        getListing can now be cleaned if needed (for example, set last date of
        listing retrieving)
        """
        raise RuntimeError(
            "getListing is an abstract method in IListingBuilder")
