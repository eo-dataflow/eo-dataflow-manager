#
# -*- coding: UTF-8 -*-
#
"""Scheduler module contain the L{Scheduler} class
"""

__docformat__ = 'epytext'

import argparse
import importlib.metadata
import logging
import os
import os.path
import signal
import sys
import threading
from socket import gethostname
from time import gmtime, sleep, strftime

from eo_dataflow_manager.scheduler.com.ext.PublishRabbitMQ import PublishRabbitMQ
from eo_dataflow_manager.scheduler.com.ext.XMLWriter import XMLWriter
from eo_dataflow_manager.scheduler.com.sys import DiskSpool
from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.com.sys.Folder import Folder
from eo_dataflow_manager.scheduler.sc import Controller, LogUtil
from eo_dataflow_manager.scheduler.sc.Download import STATE_STOPPED, Download, DownloaderException
from eo_dataflow_manager.scheduler.sc.GlobalConfig import GlobalConfig
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import STATE_DISABLED
from eo_dataflow_manager.scheduler.sc.ProviderManager.ProviderManager import ProviderManager
from eo_dataflow_manager.scheduler.sc.model.ProviderModel import ProviderModel
from eo_dataflow_manager.worker.workerUI import WorkerUI

# Define exit error codes
EXIT_OK = 0
EXIT_PB = 1
EXIT_BAD_ARGS = 2
EXIT_BAD_CONFIG = 3
EXIT_LOCKED = 4
EXIT_PB_DAEMON = 5

# Define Scheduler module/software constant
# SCHD_LOGGER_NAME = 'Scheduler' # defined in Controller, for other
# modules to retrieve it
SCHD_LOGGER_LEVEL = logging.INFO
SCHD_LOGGER_DEFAULT_FILE_NAME = 'scheduler.log'


class Scheduler(object):
    """This class is the main class implementing the Scheduler process.
    It contains an internal representation of all the downloads
    and performs an infinite loop, triggering the execution of the
    downloads.
    """

    def __init__(self):
        """Scheduler instantiation method
        """

        # poor's method to set the scheduler singleton
        Controller.SCHEDULER = self

        # Initialisation of instance attribute
        self.__version = importlib.metadata.version('eo-dataflow-manager')
        self._log = logging.getLogger('Scheduler')
        self.__globalConfig = None
        self.__verbose = None

        # 30/06/2008 FP: commenter __messagesSpool pour desactiver la gestion
        # de la bdd
        self.__messagesSpool = None
        self.__providerManager = None

        self.__downloads = []
        self.__workerUI = None

        # counter for the activatedFiles of the currentLoop: must be <
        # maxActivatedFilesByLoop
        self.__currentLoopActivatedFilesNbr = 0
        self.__mapSemaFlow = {}

        self.__reloadDownloads = False
        self.__continue = True

        self.__metrics_rabbitmq = None
        self.__jobs_rabbitmq = None
        self.__emm_rabbitmq = None

    @staticmethod
    def get_options():
        parser = argparse.ArgumentParser()

        parser.add_argument(
            '-w', '--workspace_dir',
            action='store',
            type=str,
            dest='workspace_dir',
            metavar='PATH',
            help='Set PATH as the workspace directory of the System Controller')
        parser.add_argument(
            '-a', '--appdata_dir',
            action='store',
            type=str,
            dest='appdata_dir',
            metavar='PATH',
            help='Set PATH as the application data directory of the System Controller')
        parser.add_argument(
            '-c', '--config',
            action='store',
            type=str,
            dest='config',
            metavar='FILE',
            help='Set FILE as the global System Controller configuration file')
        parser.add_argument(
            '-l', '--log-file',
            action='store',
            type=str,
            dest='log_file',
            metavar='FILE',
            help='Set FILE as the log file of the System Controller')
        parser.add_argument(
            '-d', '--daemon',
            action='store_true',
            dest='daemon',
            default=False,
            help='Activate the daemonization of the System Controller')
        parser.add_argument(
            '-v', '--verbose',
            action='store_true',
            dest='verbose',
            default=False,
            help='Activate verbose output (show log in STDOUT at DEBUG level)')
        parser.add_argument(
            '-f', '--force',
            action='store_true',
            dest='force',
            default=False,
            help='Force deletion of the lock file (if it exists)')

        options = parser.parse_args()
        return options

    def main(self, options):
        """Main method that initialize the Scheduler program:
          1. L{Initialize the screen textual log<initLog>} of the Scheduler program
          2. Get command line option
          3. Get environment variable
          4. L{Read<configureController>} the configuration
          5. L{Initialize the file textual log<initFileLog>} of the Scheduler program
          6. Init signal handling
          7. Run the L{mainLoop}

        The mainLoop should be stop by send to Scheduler
        the B{INT} or the B{USE1} kill signal
        """
        try:
            # get argument
            self.__verbose = options.verbose

            self.__globalConfig = GlobalConfig(options.config)

            # Handle workspace & appdata command line overriding
            if options.workspace_dir is not None:
                self.__globalConfig.setWorkspacePath(options.workspace_dir)
            if options.appdata_dir is not None:
                self.__globalConfig.setAppdataPath(options.appdata_dir)

            # Init logging
            from eo_dataflow_manager.ifr_lib_modules.ifr_logging import setup_logging
            setup_logging(
                config=self.__globalConfig['logs.default'],
                # If verbose option is set, the log level is DEBUG, else the log level is taken from the configuration
                root_level=logging.DEBUG if options.verbose else None,
            )
            self._log = logging.getLogger()

            if options.daemon:
                self._log.info('!' * 100)
                self._log.info('=' * 100)
                self._log.info('  System Controller - Scheduler, new pid after daemonization: %d', os.getpid())
                self._log.info('=' * 100)
                try:
                    self.create_daemon()
                except Exception:
                    self._log.error('Problem during the daemonization of the scheduler')
                    self._log.error('!' * 100)
                    sys.exit(EXIT_PB_DAEMON)

            """
            self._log.info("=" * 100)
            self._log.info("  System Controller %s - Scheduler(%d): starting the initialization",
                           self.__version, os.getpid())
            self._log.info("=" * 100)
            """
            # Check if workspace & appdata exists
            if not os.path.isdir(self.__globalConfig['paths.workspace']):
                self._log.warning(
                    "The specified workspace directory doesn't exist (path: %s ) : we create it",
                    self.__globalConfig['paths.workspace']
                )
                os.makedirs(self.__globalConfig['paths.workspace'])

            if not os.path.isdir(self.__globalConfig['paths.appdata']):
                self._log.error(
                    "the specified application data directory doesn't exist (path: %s )",
                    self.__globalConfig['paths.appdata'])
                sys.exit(EXIT_BAD_ARGS)

            self._log.info('Using %s for workspace directory', self.__globalConfig['paths.workspace'])
            self._log.info('Using %s for application data directory', self.__globalConfig['paths.appdata'])

            # Create required directories
            for name in [
                'work',
                'process_log_archive',
                'jobfile',
                'providerdir',
                'messages_to_log',
                'dynamic_plugins',
                'monitoring',
                'synchrodir',
            ]:
                path = self.__globalConfig.getPath(name)
                if not os.path.exists(path):
                    self._log.info('Creating directory %s', path)
                    os.makedirs(path)

            # Setup file logging target

            log_file = File(os.path.join(self.__globalConfig.getPath('process_log_archive'), 'scheduler.log'))
            lockfile = log_file.acquireLock('%s-%d %s %s\n' % (gethostname(),
                                                               os.getpid(),
                                                               self.__version,
                                                               strftime('%d/%m/%Y %H:%M:%S', gmtime())))

            if not lockfile and options.force:
                self._log.info('Forcing the deletion of the lock file (%s)', log_file.getName() + '.lock')
                os.remove(log_file.getName() + '.lock')
                lockfile = log_file.acquireLock('%s-%d %s %s\n' % (gethostname(),
                                                                os.getpid(),
                                                                self.__version,
                                                                strftime('%d/%m/%Y %H:%M:%S', gmtime())))

            if not lockfile:
                self._log.error('!' * 100)
                self._log.error('A session of Scheduler for this DOWNLOADER_WORKSPACE, may be already run !')
                self._log.error('Check the lock file %s before restart the scheduler',
                                log_file.getName() + '.lock')
                self._log.error('!' * 100)
                sys.exit(EXIT_LOCKED)

            LogUtil.setupFileLogger(
                self._log,
                self.__globalConfig['logs.file_log'],
                log_file.path)

            # Configure provider manager
            self.__providerManager = ProviderManager(self.__globalConfig, loggerName='ProviderManager')
            Folder(self.__globalConfig.getPath('providerdir'), create=True)
            self.__providerManager.setJobPath(self.__globalConfig.getPath('providerdir'))

            # 2018/11/05 #5 adding a startup banner
            self._log.info('[OPERATOR] %s', '=' * 89)
            self._log.info('[OPERATOR] System Controller %s - Scheduler(%d): start',
                           self.__version, os.getpid())
            self._log.info('[OPERATOR] %s', '=' * 89)
            # 2018/11/05 #5 adding a startup banner
            self._log.info(' ' * 33 + 'Start up of the scheduler programs')
            self._log.info('=' * 100)

            # read the download configuration throuth L{Download.read}
            try:
                self.__downloads = self.read_downloads_config(
                    self.list_download_config_file(), False)
            except Exception:
                self._log.exception('Bad download config')
                sys.exit(EXIT_BAD_CONFIG)

            try:
                self.__downloads.extend(
                    self.read_downloads_config(
                        self.list_test_download_config_file(), True))
            except BaseException:
                self._log.exception('Bad download config (tests)')
                sys.exit(EXIT_BAD_CONFIG)

            # create and start rabbitmq publisher threads
            self.start_rabbitmq_publisher()

            # read the Host configuration throuth L{ProviderManager.read}
            # write and Reading the ProviderManager configuration
            self.update_and_read_provider_config()

            # init the UI worker
            if self.__globalConfig['general.ui_worker']:
                try:
                    self.__workerUI = WorkerUI(Controller.SCHD_LOGGER_NAME, self.__globalConfig, options.verbose)
                except BaseException:
                    self._log.exception('Bad UI worker config')
                    sys.exit(EXIT_BAD_CONFIG)
            else:
                self._log.debug('Start eo-dataflow-manager without UI worker')

            # Init signal handler
            self.init_signal_trap()

            # 30/06/2008 FP: database management disactivated
            # Init messages spool
            self.__messagesSpool = DiskSpool.DiskSpool(
                self.__globalConfig.getPath('messages_to_log'),
                r'.*\.xml$',
                lockFileExt='.lock',
                create=True)

            # Log a header use to distinguish the new start of the runner in
            # logfile
            self._log.info('')
            self._log.info('#' * 100)
            self._log.info('*' * 100)
            self._log.info('@' * 100)
            self._log.info('*' * 100)
            self._log.info('#' * 100)
            self._log.info('')
            self._log.info('')
            self._log.info(' ' * 26 + 'New session of Scheduler of System Controller')
            self._log.info('')
            self._log.info('')
            # write summary of the configuration (INFO level)
            self.config_summary(logging.INFO)

            # start the UI worker thread
            if self.__globalConfig['general.ui_worker']:
                self.__workerUI.start()

            # start the download threads
            for d in self.__downloads:
                d.start()

            # run the main loop
            self._log.info('=' * 100)
            self._log.info(' ' * 36 + 'Start the scheduler MainLoop')
            self._log.info('-' * 100)

            try:
                self.main_loop()
            except Exception as e:
                self._log.exception(
                    'mainLoop: uncaught error: %s',
                    str(e))
                raise
            self._log.info('=' * 100)
            self._log.info(' ' * 35 + 'Exit of the scheduler programs')
            self._log.info('=' * 100)

            # Unlock the Scheduler log file
            log_file.unLock()
            self._log.debug('=' * 100)
            self._log.debug(' ' * 42 + 'Unlock log file ')
            self._log.debug('=' * 100)

            # TODO Close the log
            logging.shutdown()
            self._log.debug('=' * 100)
            self._log.debug(' ' * 42 + 'Shutdown logging')
            self._log.debug('=' * 100)

            sys.exit(EXIT_OK)

        except Exception as e:
            self._log.exception('main: uncaught error: %s', str(e))
            sys.exit(EXIT_PB)

        finally:
            if log_file.isLock():
                log_file.unLock()

    def read_downloads_config(self, ds, is_test):
        list_downloads = []
        # Begin of reading of downloads configurations ###
        if is_test:
            self._log.debug('BEGIN: reading of Test downloads configuration')
        else:
            self._log.debug('BEGIN: reading of downloads configuration')
        # Loop in DiskSpool.keys()
        for f in list(ds.keys()):
            self._log.info('Find %s as a download configuration file', f)
            dl = self.create_download(f, is_test)
            # Append the download to the __downloads list
            if dl is not None:
                list_downloads.append(dl)

        # End of reading of download configuration ###
        if is_test:
            self._log.debug('END : reading of Test downloads configuration')
        else:
            self._log.debug('END : reading of downloads configuration')

        return list_downloads

    def update_and_read_provider_config(self):
        # Check if the file sc_provider.xml exist in the 'std_config' folder
        provider_config = self.__globalConfig.getPath('providerfile')
        map_provider_max_activated_flow = self.write_provider_configuration(
            provider_config)

        for id_provider, provider in list(
                self.__providerManager.getProviderConfig().items()):
            if id_provider not in map_provider_max_activated_flow:
                provider.setState([STATE_DISABLED, ''])

        # Call the read method of the ProviderManager on the providerConfig
        # config File
        self.__providerManager.read(provider_config)

    def write_provider_configuration(self, provider_config):
        xw = XMLWriter()
        tree = xw.newXmlTree()
        attributes = {'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
        root = xw.newNode('sc_providers_config', '', '', attributes)
        xw.xmlTreeSetRoot(tree, root)
        map_provider_max_activated_flow = {}
        providers = []
        for d in self.__downloads:
            provider_model = ProviderModel(
                d.remoteStorageProviderType,
                d.remoteStorageProviderServer,
                d.remoteStorageProviderUsername,
                d.remoteStorageProviderPasswd,
                d.remoteStorageProviderPort,
                d.remoteStorageProviderOption)
            max_activated_flow = d.remoteMaxActivatedFlow
            if provider_model.getUid() in map_provider_max_activated_flow:
                max_prov = map_provider_max_activated_flow[provider_model.getUid()]
                if max_activated_flow is not None and max_activated_flow < max_prov:
                    map_provider_max_activated_flow[provider_model.getUid(
                    )] = max_activated_flow
            elif max_activated_flow is not None:
                map_provider_max_activated_flow[provider_model.getUid(
                )] = max_activated_flow

        for d in self.__downloads:
            prov_type = d.remoteStorageProviderType
            provider_model = ProviderModel(
                prov_type,
                d.remoteStorageProviderServer,
                d.remoteStorageProviderUsername,
                d.remoteStorageProviderPasswd,
                d.remoteStorageProviderPort,
                d.remoteStorageProviderOption)
            ref = prov_type.lower()
            max_activated_flow = None
            if prov_type not in [
                    'Localmove',
                    'Localpath',
                    'Localpointer',
                    'Onlynotify']:
                ref += '_' + str(provider_model.getUid())
            try:
                providers.index(provider_model.getUid())
            except ValueError:
                attr = {'id': ref, 'xsi:type': prov_type}
                node_provider = xw.newSubNode(root, 'provider', '', '', attr)
                if prov_type not in [
                        'Localmove',
                        'Localpath',
                        'Localpointer',
                        'Onlynotify']:
                    xw.newSubNode(
                        node_provider,
                        'server',
                        d.remoteStorageProviderServer)
                    xw.newSubNode(
                        node_provider,
                        'username',
                        d.remoteStorageProviderUsername)
                    xw.newSubNode(
                        node_provider,
                        'password',
                        d.remoteStorageProviderPasswd)
                    xw.newSubNode(
                        node_provider, 'port', d.remoteStorageProviderPort)
                if prov_type.startswith('Http'):
                    if 'CheckCertificate' in d.remoteStorageProviderOption:
                        options = f'CheckCertificate={d.remoteStorageProviderOption["CheckCertificate"]}'
                        xw.newSubNode(node_provider, 'options', options)

                if provider_model.getUid() in map_provider_max_activated_flow:
                    max_activated_flow = map_provider_max_activated_flow[provider_model.getUid(
                    )]
                    self._log.debug(' max_activated_flow %s ', max_activated_flow)
                    xw.newSubNode(
                        node_provider,
                        'max_activated_flow',
                        str(max_activated_flow))

                providers.append(provider_model.getUid())
            d.set_remote_storage_provider_ref(ref)
            if ref in self.__mapSemaFlow:
                self._log.debug(' sema deja connu pour le provider %s ', ref)
                sema = self.__mapSemaFlow[ref]
            else:
                self._log.debug(' nouveau sema  pour le provider %s ', ref)
                if max_activated_flow is not None:
                    self._log.debug(' nouveau sema  nb %s', max_activated_flow)
                    sema = threading.BoundedSemaphore(
                        value=int(max_activated_flow))
                else:
                    sema = threading.BoundedSemaphore(
                        value=int(self.__globalConfig['general.max_activated_flow']))
                self.__mapSemaFlow[ref] = sema

            d.set_semaphore_limit_flow(sema)

        file_config = File(provider_config)
        if file_config.exist():
            file_config.move(provider_config + '.ori')

        xw.write(tree, provider_config)
        return map_provider_max_activated_flow

    def list_download_config_file(self):
        ds = DiskSpool.DiskSpool(
            self.__globalConfig.getPath('downloads_config'),
            r'.*\.download.xml$')
        return ds

    def list_test_download_config_file(self):
        ds = DiskSpool.DiskSpool(
            self.__globalConfig.getPath('downloads_config'),
            r'.*\.download.tests.xml$')
        return ds

    def create_download(self, file_config, is_test):
        """Create a new  {Download}
        @param file_config: Download configuration file
        @param is_test: boolean : true => test
        """
        # Create a new Download
        dl = Download(
            loggerName=Controller.SCHD_LOGGER_NAME + '.' + os.path.splitext(os.path.basename(file_config))[0],
            globalConfig=self.__globalConfig,
            isTest=is_test
        )
        dl.set_debug_mode(self.__verbose)
        # Load the configuration
        try:
            path_file_config = os.path.join(
                self.__globalConfig.getPath('downloads_config'), file_config)
            # , self.__globalConfig.getPath('process_config'))
            dl.set_download_conf_file(path_file_config)
            # 15/05/2018 PMT: global limit by défaut
            dl.set_default_max_activated_files_by_loop(
                self.__globalConfig['general.max_activated_files_by_loop'])

            try:
                dl.read_configuration_file()
            except DownloaderException as e:
                raise DownloaderException(e)
            except IOError as e:
                # fileConfig => fileConfig.BAD
                self._log.error(
                    "Bad configuration file %s: disabled file (adding '.BAD' extension). Error: %s",
                    file_config, e)
                os.rename(path_file_config, path_file_config + '.BAD')
                raise DownloaderException(e)
            except Exception as e:
                # fileConfig => fileConfig.BAD
                self._log.error(
                    "Bad configuration file %s: disabled file (adding '.BAD' extension). Error: %s",
                    file_config, e)
                os.rename(path_file_config, path_file_config + '.BAD')
                raise DownloaderException(e)

            # Check if no other download already load have the same name
            for download in self.__downloads:
                if download.id == dl.id:
                    self._log.error(
                        "Bad configuration file %s: disabled file (adding '.BAD' extension)",
                        file_config)
                    self._log.error(
                        'Download configuration have the same ID as another already loaded (configuration file %s)',
                        download.download_conf_file)
                    os.rename(path_file_config, path_file_config + '.BAD')
                    raise DownloaderException('Bad configuration file %s' % file_config)  # fileConfig => fileConfig.BAD

            dl.init_file_log(self.__globalConfig['logs.file_log'], self.__globalConfig.getPath('process_log_archive'))

        except DownloaderException:
            return None

        return dl

    def update_downloads(self, downloads_config_file, test_downloads_config_file):
        # if the lists are valid
        files_configuration = []
        files_configuration_test = []
        if not downloads_config_file.nfs_check_error \
                and not test_downloads_config_file.nfs_check_error:
            files_configuration = list(downloads_config_file)
            files_configuration_test = list(test_downloads_config_file)
            self._log.debug(
                ' All download configuration files %s ',
                files_configuration)
            self._log.debug(
                ' All tests download configuration files %s ',
                files_configuration_test)
        else:
            self._log.warning(f'Path to the download configuration files is temporarily inaccessible !')

        liste_download_to_reload = []
        liste_download_to_start = []
        download_to_remove = []
        self._log.info('Current downloads state:')
        for d in self.__downloads:
            _path, file_name = os.path.split(d.download_conf_file)
            # 13/07/2018 PMT#45: Log de l'arrêt et relance d'un thread supposé
            # actif
            if not d.is_alive():
                self._log.info(
                    "  - %s: %s [details='%s']" %
                    (d.id, STATE_STOPPED, 'Dead'))
                liste_download_to_reload.append(d)
            else:
                self._log.info(
                    "  - %s: %s [details='%s']" %
                    (d.id, d.state[0], d.state[1]))
                if d.state[0] == STATE_STOPPED:
                    liste_download_to_reload.append(d)

            # if the lists are valid
            if not downloads_config_file.nfs_check_error \
                    and not test_downloads_config_file.nfs_check_error:
                if file_name in files_configuration and not d.test:
                    files_configuration.remove(file_name)
                elif file_name in files_configuration_test and d.test:
                    files_configuration_test.remove(file_name)
                else:
                    download_to_remove.append(d)

        to_reload_provider_config = len(liste_download_to_reload) > 0

        # Create a new thread Download if the configuration has been changed
        for dlToReload in liste_download_to_reload:
            self.__downloads.remove(dlToReload)
            download_conf_file = dlToReload.download_conf_file
            test = dlToReload.test
            dlToReload.join(10)
            dl = self.create_download(download_conf_file, test)
            if dl is not None:
                liste_download_to_start.append(dl)
                self.__downloads.append(dl)

        for d in download_to_remove:
            self._log.info(
                'Download configuration file %s not present =>  download %s stopped' %
                (d.download_conf_file, d.id))
            try:
                self.__downloads.remove(d)
                d.join(10)
                to_reload_provider_config = True
            except ValueError:
                pass

        # if the lists are valid
        if not downloads_config_file.nfs_check_error \
                and not test_downloads_config_file.nfs_check_error:
            # new configuration file
            for f in files_configuration:
                self._log.info('New Download configuration file %s found', f)
                dl = self.create_download(f, False)
                # Append the download to the __downloads list
                if dl is not None:
                    to_reload_provider_config = True
                    self.__downloads.append(dl)
                    liste_download_to_start.append(dl)

            # new tests configuration file
            for f in files_configuration_test:
                self._log.info('New Test Download configuration file %s found', f)
                dl = self.create_download(f, True)
                # Append the download to the __downloads list
                if dl is not None:
                    to_reload_provider_config = True
                    self.__downloads.append(dl)
                    liste_download_to_start.append(dl)

        if to_reload_provider_config:
            # Update once time provider file conf
            self.update_and_read_provider_config()

        for dlToReload in liste_download_to_start:
            self._log.debug(
                'Download  [%s] start %s' %
                (dlToReload.id, dlToReload.download_conf_file))
            dlToReload.config_summary(logging.DEBUG)
            dlToReload.start()

        self._log.debug('End of updateDownloads')

    def start_rabbitmq_publisher(self):
        if 'rabbitmq' in self.__globalConfig['metrics.targets']:
            self.__metrics_rabbitmq = PublishRabbitMQ(
                host=self.__globalConfig['metrics.rabbitmq.host'],
                port=self.__globalConfig['metrics.rabbitmq.port'],
                ssl=self.__globalConfig['metrics.rabbitmq.ssl'],
                user=self.__globalConfig['metrics.rabbitmq.user'],
                password=self.__globalConfig['metrics.rabbitmq.password'],
                virtual_host=self.__globalConfig['metrics.rabbitmq.virtual_host'],
                queue_name=self.__globalConfig['metrics.rabbitmq.queue_name'],
                routing_key=self.__globalConfig['metrics.rabbitmq.routing_key'],
                message_type=self.__globalConfig['metrics.rabbitmq.message_type'],
            )
            self.__metrics_rabbitmq.start()

        if 'rabbitmq' in self.__globalConfig['jobs.targets']:
            self.__jobs_rabbitmq = PublishRabbitMQ(
                host=self.__globalConfig['jobs.rabbitmq.host'],
                port=self.__globalConfig['jobs.rabbitmq.port'],
                ssl=self.__globalConfig['jobs.rabbitmq.ssl'],
                user=self.__globalConfig['jobs.rabbitmq.user'],
                password=self.__globalConfig['jobs.rabbitmq.password'],
                virtual_host=self.__globalConfig['jobs.rabbitmq.virtual_host'],
                queue_name=self.__globalConfig['jobs.rabbitmq.queue_name'],
                routing_key=self.__globalConfig['jobs.rabbitmq.routing_key'],
                message_type=self.__globalConfig['jobs.rabbitmq.message_type'],
            )
            self.__jobs_rabbitmq.start()

        if 'rabbitmq' in self.__globalConfig['emm.targets']:
            self.__emm_rabbitmq = PublishRabbitMQ(
                host=self.__globalConfig['emm.rabbitmq.host'],
                port=self.__globalConfig['emm.rabbitmq.port'],
                ssl=self.__globalConfig['emm.rabbitmq.ssl'],
                user=self.__globalConfig['emm.rabbitmq.user'],
                password=self.__globalConfig['emm.rabbitmq.password'],
                virtual_host=self.__globalConfig['emm.rabbitmq.virtual_host'],
                queue_name=self.__globalConfig['emm.rabbitmq.queue_name'],
                routing_key=self.__globalConfig['emm.rabbitmq.routing_key'],
                message_type=self.__globalConfig['emm.rabbitmq.message_type'],
            )
            self.__emm_rabbitmq.start()

    def config_summary(self, level):
        """Write on text log a summary of the L{Scheduler} configuration

        @param level: log level to write information
        @type level: C{int}
        """
        self._log.log(level, '=' * 100)
        self._log.log(level, '  System Controller - Scheduler: Configuration summary')
        self._log.log(level, '=' * 100)
        # self._log.log(level, "System Controller global configuration version %s", self.__version)
        self._log.log(level, '- mainloop timeout = %d', self.__globalConfig['general.mainloop_timeout'])
        self._log.log(level, '- max activated files by loop = %s',
                      self.__globalConfig['general.max_activated_files_by_loop'])
        self._log.log(level, '- default max activated flow = %s',
                      self.__globalConfig['general.max_activated_flow'])
        self._log.log(level, '- ui worker = %s', self.__globalConfig['general.ui_worker'])
        self._log.log(level, '- message emm = %s', self.__globalConfig['emm.targets'])
        self._log.log(level, '- message jobs = %s', self.__globalConfig['jobs.targets'])

        self._log.log(level, '- List of path:')
        for pathname in ['downloads_config', 'work', 'process_log_archive',
                         'jobfile', 'providerdir', 'messages_to_log']:
            self._log.log(level, "  - '%s' -> %s", pathname, self.__globalConfig.getPath(pathname))

        self.__providerManager.configSummary(level)

        self._log.log(level, '-' * 100)
        self._log.log(level, 'Download Manager configuration:')
        for download in self.__downloads:
            download.config_summary(level)

        self._log.log(level, 'List of registered administrators:')
        for a in self.__globalConfig.administrators:
            self._log.log(level, '  - %s <%s>', a[0], a[1])

    def handler_signal(self, signum, _frame):
        """Handler for signal that stop the main loop

        @param signum: number of the signal handled
        @type signum: C{int}
        @param _frame: the current stack _frame
        @type _frame: _frame U{python internal type
        """
        if signum in [signal.SIGINT, signal.SIGTERM]:
            if self.__continue is True:
                self._log.info(
                    'Received signal (SIGINT|SIGTERM): gracefully stopping scheduler...')
                self.__continue = False
            else:
                self._log.info(
                    'Received signal (SIGINT|SIGTERM): exiting process')
                sys.exit(255)
        elif signum == signal.SIGHUP:
            self._log.info(
                'Received signal SIGHUP: reloading configurations is not supported')
            self.__reloadDownloads = True
        else:
            self._log.info('Received unknown signal: %s (do nothing)' % signum)

    def init_signal_trap(self):
        """Init signal handler
        """
        signal.signal(signal.SIGINT, self.handler_signal)
        signal.signal(signal.SIGTERM, self.handler_signal)
        signal.signal(signal.SIGHUP, self.handler_signal)

    def create_daemon(self):
        """Detach a process from the controlling terminal and run it in the
        background as a daemon.
        """

        pid = None

        try:
            # Fork a child process so the parent can exit.  This will return control
            # to the command line or shell.  This is required so that the new process
            # is guaranteed not to be a process group leader.  We have this guarantee
            # because the process GID of the parent is inherited by the child, but
            # the child gets a new PID, making it impossible for its PID to equal its
            # PGID.
            pid = os.fork()
        except OSError:
            self._log.exception('Problem during the first fork')

        if pid == 0:       # The first child.

            # Next we call os.setsid() to become the session leader of this new
            # session.  The process also becomes the process group leader of the
            # new process group.  Since a controlling terminal is associated with a
            # session, and this new session has not yet acquired a controlling
            # terminal our process now has no controlling terminal.  This shouldn't
            # fail, since we're guaranteed that the child is not a process group
            # leader.
            os.setsid()

            # When the first child terminates, all processes in the second child
            # are sent a SIGHUP, so it's ignored.
            signal.signal(signal.SIGHUP, signal.SIG_IGN)

            try:
                # Fork a second child to prevent zombies.  Since the first child is
                # a session leader without a controlling terminal, it's possible for
                # it to acquire one by opening a terminal in the future.  This second
                # fork guarantees that the child is no longer a session leader, thus
                # preventing the daemon from ever acquiring a controlling
                # terminal.
                pid = os.fork()        # Fork a second child.
            except OSError:
                self._log.exception('Problem during the second fork')

            if pid == 0:      # The second child.
                # Ensure that the daemon doesn't keep any directory in use.  Failure
                # to do this could make a filesystem unmountable.
                os.chdir('/')
                # Give the child complete control over permissions.
                os.umask(0)
            else:
                # Exit parent (the first child) of the second child.
                sys.exit(0)  # pylint: disable=protected-access
        else:
            # Exit parent of the first child.
            sys.exit(0)  # pylint: disable=protected-access

    def clean_workspace(self):
        for pc in self.__downloads:
            pc.clean_workspace()

    def clean_job_file(self):
        list_path = [self.__globalConfig.getPath('providerdir'), self.__globalConfig.getPath('jobfile')]

        for path in list_path:
            folder = Folder(path)
            try:
                folder.scan()
            except OSError:
                self._log.warning(f'Path {path} is temporarily inaccessible !')
                continue
            for job_file in list(folder.keys()):
                job_path = os.path.join(path, job_file)
                _name, file_extension = os.path.splitext(job_path)
                if file_extension in ['.job', '.tests']:
                    f = File(job_path)
                    try:
                        f.remove()
                    except OSError:
                        pass

    def clean_ending_sub_process(self, nowait=True):
        # self._log.debug("Scheduler::cleanEndingSubProcess: nowait=%s"%(str(nowait)))
        if nowait:
            wait_mode = os.WNOHANG
        else:
            wait_mode = 0  # normal operation for the os.waitpid command

        # Clean ending subprocess
        list_input = self.__providerManager.cleanEndSubProcess(wait_mode)
        for i in list_input:
            # get lock file
            # open and check if it's not operator
            # => remove lock file
            if i.exist():
                lock_file = File(i.getName() + '.lock')
                if lock_file.exist():
                    try:
                        lock_file.open()
                        text = lock_file.readline()
                    except BaseException:
                        if lock_file.exist():
                            # if lock already exist => reading problems => remove them
                            # => put not_operator in text for next step
                            text = 'not_operator'
                        else:
                            text = 'operator'
                    finally:
                        lock_file.close()

                    if text != 'operator':
                        try:
                            i.unLock()
                        except Exception as e:
                            self._log.error("Scheduler::cleanEndingSubProcess: unLock failed: '%s'", e)

                else:
                    pass
        # End For loop

    def main_loop(self):
        """Scheduler MainLoop
        """

        # save the last workspace, and create an emtpy one to start the
        # processing with a clean workspace
        self.clean_workspace()

        loop_number = 1
        while self.__continue:

            # Clean ending subprocess
            # self.cleanEndingSubProcess()

            # reset the activatedFiles counter
            self.__currentLoopActivatedFilesNbr = 0

            # Log message in spool
            # self.registerLogMessages()

            # Check providers availability
            self.__providerManager.checkProvidersAvailability()

            # update downloads

            try:
                self.update_downloads(
                    self.list_download_config_file(),
                    self.list_test_download_config_file())
            except Exception as e:
                self._log.error(f'error when updating downloads ({str(e)})')

            self._log.info(
                '# End of the loop **%08d**, sleep for %d sec [%s] %s ',
                loop_number,
                self.__globalConfig['general.mainloop_timeout'],
                strftime('%d/%m/%Y %H:%M:%S +0000', gmtime()),
                '#' * 22)
            loop_number += 1

            sleep(self.__globalConfig['general.mainloop_timeout'])

        # End While loop
        self._log.info('Wait for all subprocess')

        join_timeout = 10
        # terminate the download threads
        for d in self.__downloads:
            d.stop_download()
        for d in self.__downloads:
            d.join(join_timeout)

        # terminate the UI worker
        if self.__globalConfig['general.ui_worker']:
            if self.__workerUI.isAlive():
                self._log.debug('workerUI is alive')
                self.__workerUI.join(join_timeout)
            else:
                threading.Thread.join(self.__workerUI, join_timeout)

        # teminate the rabbitmq publisher threads
        for rabbitmq_publisher in PublishRabbitMQ.queues:
            PublishRabbitMQ.queues[rabbitmq_publisher].join()
            self._log.debug(f'RabbitMQ publisher {rabbitmq_publisher} stopped.')

        self.clean_ending_sub_process(nowait=False)
        self.clean_job_file()
        self._log.info('End of mainloop')

    maxActivatedFilesByLoop = property(
        lambda self: self.__globalConfig['general.max_activated_files_by_loop'],
        None,
        None,
        'maximum number of files to process in 1 loop for the scheduler')

    def get_download(self, download_id):
        """Returns the processingChain Object, searching it with his name. Raise if processingChainName not found.
        """
        for download in self.__downloads:
            if download.id == download_id:
                return download
        raise RuntimeError(
            "Scheduler::get_download(): No download for id: '%s'" % download_id)


def scheduler_main():
    scd = Scheduler()
    scd.main(Scheduler.get_options())
