#
# -*- coding: UTF-8 -*-
#
"""Controller module contain the L{Controller} class
"""

__docformat__ = 'epytext'

import os
import tempfile

from eo_dataflow_manager.emm.api import messages

SCHEDULER = None
SCHD_LOGGER_NAME = ''
DOWNLOAD_LOGGER_NAME = 'download'

BASE_LOGGER_FILE_MAX_SIZE = 500000

shared_dir = '/export/home/application_data/emm/spool/'
urgency_dir = os.path.join(tempfile.gettempdir(), 'emm_spool')
EMM_STORAGES = [shared_dir, urgency_dir]  # local_dir,

# TODO : mettre certaines informations en configuration ! Utiliser un
# message template xml ?
emm_factory = messages.MessageFactory()
TEMPLATE_DOWNLOAD_MESSAGE = emm_factory.create({
    'type': 'downloadMessage',
    'sender_name': 'CERSAT',
    'sender_type': 'auto',
    'recipient_name': 'CERSAT',
    'recipient_type': 'auto',
    'project': 'CERSAT',
    'processing_process_id': 'Downloader',
})
