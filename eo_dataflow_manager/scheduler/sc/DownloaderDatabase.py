#
# -*- coding: UTF-8 -*-
#

""" Database module contains the L{DownloaderDatabase} class, used to manage downloads history
"""

import datetime
import logging
import time

from sqlalchemy import Column, DateTime, Integer, MetaData, String, Table, and_, create_engine  # , Index, or_
from sqlalchemy.exc import DBAPIError, ProgrammingError
from sqlalchemy.orm import mapper, sessionmaker  # , relation, class_mapper

LOGGER_NAME = 'DownloaderDatabase'
LOGGER_LEVEL = logging.INFO

# 15/05/2018 PMT : ajout de MAX_ERROR_DOWNLOAD et modification de
# STATE_TO_RETRY
MAX_ERROR_DOWNLOAD = 3
# 24/04/2018 PMT : ajout de STATE_TO_RETRY
STATE_TO_RETRY = list(range(-(MAX_ERROR_DOWNLOAD), 0))

STATE_TO_DOWNLOAD = 0
STATE_DOWNLOAD_ERROR = 1
STATE_DOWNLOAD_SUCCESS = 2
STATE_TO_REDOWNLOAD = 3

# SQLAlchemy DB Objects : DO NOT REMOVE !
# class DBDownload(object):
#    pass


#metadata = MetaData()
# downloadTable = Table('download', metadata,
#                Column('id', Integer, primary_key=True),
#                Column('filepath', String(2048), nullable=False, index=True, unique=True),
#                Column('state', Integer, nullable=False),
#                Column('ignition_date', DateTime, nullable=False),
#                Column('last_update', DateTime, nullable=False),
#                Column('size', Integer, nullable=True),
#                Column('mtime', DateTime, nullable=True),
#                Column('sensingtime', DateTime, nullable=True),
#            )
#
#mapper(DBDownload, downloadTable)


class DownloaderDatabase(object):

    def __init__(
            self,
            download_id,
            database_path=None,
            loggerName=None,
            loggerLevel=None):
        if loggerName is not None:
            self._log = logging.getLogger(loggerName)
        else:
            self._log = logging.getLogger(LOGGER_NAME)

        if loggerLevel is not None:
            self._log.setLevel(loggerLevel)
        else:
            self._log.setLevel(LOGGER_LEVEL)

        self.download_id = download_id
        self.db = None

        self.database_path = database_path
        self.__downloadTable = None

        self.metadata = None
        self.__download_mapper = None

    def createEngine(self, db_must_exist=True):
        #create = db_must_exist
        create = True

        self.db = create_engine('%s' % (self.database_path), echo=False)

        try:
            self.db.connect()
        except DBAPIError as msg:
            raise Exception('Cannot connect to database : %s (%s)' %
                            (self.database_path, msg))
        self.db.echo = False
        self.metadata = MetaData(self.db)
        self.metadata.bind = self.db

        if create:
            self.__downloadTable = Table('download_%s' % (self.download_id), self.metadata,
                                         Column('id', Integer,
                                                primary_key=True),
                                         Column('filepath', String(
                                             2048), nullable=False, index=True, unique=True),
                                         Column('state', Integer,
                                                nullable=False),
                                         Column('ignition_date',
                                                DateTime, nullable=False),
                                         Column('last_update', DateTime,
                                                nullable=False),
                                         # 29/01/2018 PMT ID#28 : ajout
                                         # attributs size et mtime
                                         Column('size', Integer),
                                         Column('mtime', DateTime),
                                         Column('sensingtime', DateTime)
                                         )
            try:
                self.__downloadTable.create(checkfirst=True)
            except ProgrammingError:
                self.__downloadTable.create()

        else:
            self.__downloadTable = Table('download_%s' % (
                self.download_id), self.metadata, autoload=True)

        self.getSession()

    def getSession(self):
        #mapper(DBDownload, self.__downloadTable)
        SessionMaker = sessionmaker(bind=self.db)
        self.session = SessionMaker()
        return self.session

    def closeSession(self):
        self.session.close()

    def __addDownload(
            self,
            filepath,
            sensingtime,
            state,
            ignition_date=None,
            size=None,
            mtime=None,
            nbTry=0):
        class DBDownload(object):
            pass
        mapper(DBDownload, self.__downloadTable)

        self._log.debug(
            'addDownload(filepath=%s, state=%s, ignition_date=%s sensingtime=%s )' %
            (filepath, state, ignition_date, sensingtime))
        date_now = datetime.datetime.utcnow()
        if ignition_date is None:
            ignition_date = date_now
        dl = DBDownload()
        dl.filepath = filepath
        dl.state = state
        dl.ignition_date = ignition_date
        dl.sensingtime = sensingtime
        dl.last_update = date_now
        # 29/01/2018 PMT ID#28 : ajout attributs size et mtime
        dl.size = size
        dl.mtime = mtime
        dl.id = None
        self._add_db_object(dl)
        self.commit()
        id = dl.id
        if id is None:
            raise Exception(
                'addDownload failure : id is None after commit [filepath=%s, state=%s, ignition_date=%s]' %
                (filepath, state, ignition_date))
        self._log.debug(' => id=%s' % (id))

    def __updateDownload(
            self,
            download,
            filepath,
            state,
            ignition_date=None,
            size=None,
            mtime=None,
            sensingtime=None):
        self._log.debug(
            'updateDownload(filepath=%s, state=%s, ignition_date=%s)' %
            (filepath, state, ignition_date))
        date_now = datetime.datetime.utcnow()
        dl = self.getDownload(filepath)
        if dl is None:
            dl = download

        # 24/04/2018 PMT : ajout de la gestion des re-essais
        # si le téchargement est en erreur,
        #      si c'est la première erreur, on initialise state à -1,
        #      sinon state est décrementé de 1 (en correspondance avec le nombre d'erreurs rencontré).
        # si state n'est plus dans un état de re-essai, le fichier est en
        # erreur de téléchargement
        if state == STATE_DOWNLOAD_ERROR:
            if dl.state in [STATE_TO_DOWNLOAD, STATE_TO_REDOWNLOAD]:
                dl.state = -1
            else:
                dl.state -= 1
            if dl.state not in STATE_TO_RETRY:
                dl.state = STATE_DOWNLOAD_ERROR
        else:
            dl.state = state

        if sensingtime is not None:
            dl.sensingtime = sensingtime
        if ignition_date is not None:
            dl.ignition_date = ignition_date
        if size is not None:
            dl.size = size
        if mtime is not None:
            dl.mtime = mtime
        dl.last_update = date_now
        self.commit()

        return dl.state

    def resetDownload(self, filepath):
        self._log.debug('resetDownload(filepath=%s)' % (filepath))
        date_now = datetime.datetime.utcnow()
        dls = self.getDownloadFilepath(filepath)
        filepath = []
        for dl in dls:
            dl.state = STATE_TO_REDOWNLOAD
            dl.last_update = date_now
            filepath.append(dl.filepath)

        if len(filepath) != 0:
            self.commit()

        return filepath

    def getDownloadFilepath(self, filename_template, nbTry=0):
        template = filename_template.replace('*', '%')

        class DBDownload(object):
            pass
        mapper(DBDownload, self.__downloadTable)
        try:
            q = self.session.query(DBDownload)
            q = q.filter(DBDownload.filepath.like(template))  # pylint: disable=no-member
            result = q.all()
            return result
        except Exception as e:
            nbTry += 1
            if nbTry < 3:
                time.sleep(10)
                self.getDownloadFilepath(template, nbTry)
            else:
                raise e

    # 29/01/2018 PMT ID#28 : ajout attributs size et mtime
    def setDownload(
            self,
            filepath,
            state,
            sensingtime=None,
            ignition_date=None,
            size=None,
            mtime=None):
        self._log.debug(
            'setDownload(filepath=%s, state=%s, ignition_date=%s)' %
            (filepath, state, ignition_date))
        finalState = state
        if state == STATE_TO_DOWNLOAD and ignition_date is None:
            ignition_date = datetime.datetime.utcnow()

        isRegistered, dl = self.isRegistered(filepath)
        if isRegistered:
            if state == STATE_TO_REDOWNLOAD:
                finalState = self.__updateDownload(
                    dl, filepath, state, ignition_date, size, mtime)
            else:
                finalState = self.__updateDownload(
                    dl, filepath, state, ignition_date, sensingtime=sensingtime)
        else:
            self.__addDownload(filepath, sensingtime, state,
                               ignition_date, size, mtime)

        return finalState

    def commit(self):
        self.session.commit()
        self._log.debug('commit')

    def _add_db_object(self, db_object, flush=True):
        self.session.add(db_object)
        if flush:
            self.session.flush()

    # Search queries
    def isDownloaded(self, filepath):
        res = self.getDownload(
            filepath, state=[STATE_DOWNLOAD_SUCCESS, STATE_TO_REDOWNLOAD])
        if res is not None:
            return True
        else:
            return False

    def isRegistered(self, filepath, state=None):
        res = self.getDownload(filepath, state)
        if res is not None:
            return True, res
        else:
            return False, res

    def getDownload(self, filepath, state=None, ignition_date=None, nbTry=0):
        class DBDownload(object):
            filepath = None
            state = None
            ignition_date = None
        mapper(DBDownload, self.__downloadTable)
        try:
            query = self.session.query(DBDownload)
            qfilter = True
            qfilter = and_(qfilter, DBDownload.filepath == filepath)
            if state is not None:
                qfilter = and_(qfilter, DBDownload.state.in_(state))
            if ignition_date is not None:
                qfilter = and_(
                    qfilter, DBDownload.ignition_date <= ignition_date)
            res = query.filter(qfilter).all()
            if res is not None and len(res) > 0:
                assert len(res) == 1
                return res[0]
            else:
                return None
        except Exception as e:
            nbTry += 1
            if nbTry < 3:
                time.sleep(10)
                self.getDownload(filepath, state, ignition_date, nbTry)
            else:
                raise e

    def getFilesToDownload(self, care_about_ignition=True):
        if care_about_ignition:
            date_now = datetime.datetime.utcnow()
        else:
            date_now = None
        res = self.getDownloads(
            state=[STATE_TO_DOWNLOAD], ignition_date=date_now)
        return res

    def getFilesToReDownload(self, care_about_ignition=True):
        if care_about_ignition:
            date_now = datetime.datetime.utcnow()
        else:
            date_now = None
        res = self.getDownloads(
            state=[STATE_TO_REDOWNLOAD], ignition_date=date_now)
        return res

    # 24/04/2018 PMT : ajout de getFilesWithError
    # Recherche des fichiers dont state est à un été de re-essai.

    def getFilesWithError(self, care_about_ignition=True):
        if care_about_ignition:
            date_now = datetime.datetime.utcnow()
        else:
            date_now = None
        res = self.getDownloads(state=STATE_TO_RETRY, ignition_date=date_now)
        return res

    def getFilesInError(self, care_about_ignition=True):
        if care_about_ignition:
            date_now = datetime.datetime.utcnow()
        else:
            date_now = None
        res = self.getDownloads(state=[STATE_DOWNLOAD_ERROR], ignition_date=date_now)
        return res

    def getDownloads(self, state=None, ignition_date=None, nbTry=0):

        class DBDownload(object):
            pass
        mapper(DBDownload, self.__downloadTable)
        try:
            q = self.session.query(DBDownload)
            f = True
            if state is not None:
                f = and_(f, DBDownload.state.in_(state))
            if ignition_date is not None:
                f = and_(f, DBDownload.ignition_date <= ignition_date)
            q = q.filter(f)
            q = q.order_by(DBDownload.sensingtime.desc())

            res = q.all()

            return res
        except RuntimeError as e:
            nbTry += 1
            if nbTry < 3:
                time.sleep(10)
                self.getDownloads(state, ignition_date, nbTry)
            else:
                raise e
        except AttributeError as e:
            nbTry += 1
            if nbTry < 3:
                time.sleep(10)
                self.getDownloads(state, ignition_date, nbTry)
            else:
                raise e

    # 01/02/2018 PMT#33 : recherche des fichiers ayant le même repertoire ou
    # bout de répertoire
    def getFilesInFolder(self, folder=None, nbTry=0):

        class DBDownload(object):
            pass
        mapper(DBDownload, self.__downloadTable)
        try:
            q = self.session.query(DBDownload)
            f = True
            if folder is not None:
                f = and_(f, DBDownload.filepath.like('%/' + folder + '/%'))
            q = q.filter(f)
            q = q.order_by(DBDownload.state.desc())
            res = q.all()

            return res
        except RuntimeError as e:
            nbTry += 1
            if nbTry < 3:
                time.sleep(10)
                self.getFilesInFolder(folder, nbTry)
            else:
                raise e
        except AttributeError as e:
            nbTry += 1
            if nbTry < 3:
                time.sleep(10)
                self.getFilesInFolder(folder, nbTry)
            else:
                raise e
