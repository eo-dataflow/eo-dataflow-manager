#
# -*- coding: UTF-8 -*-
#
"""Download module contain the L{Download} class
"""

__docformat__ = 'epytext'

import datetime
import grp
import json
import logging
import os
import os.path
import pwd
import random
import shutil
import stat
import threading
import time
from queue import Queue
from socket import gethostname
from string import capwords
from xml.etree.ElementTree import ParseError
from xml.parsers.expat import ExpatError

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR
from eo_dataflow_manager.emm.EMMWriter import EMMWriter
from eo_dataflow_manager.emm.api import messages
from eo_dataflow_manager.ifr_lib_modules.ifr_os import is_io_locked
from eo_dataflow_manager.scheduler.com.ext.PatternFilter import PatternFilter
from eo_dataflow_manager.scheduler.com.ext.PublishRabbitMQ import PublishRabbitMQ
from eo_dataflow_manager.scheduler.com.ext.XMLReader import XMLReader
from eo_dataflow_manager.scheduler.com.sys import DiskSpool
from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.com.sys.Folder import Folder, nfs_check
from eo_dataflow_manager.scheduler.plugins import Factory
from eo_dataflow_manager.scheduler.plugins.PostProcessing.ManageValidation import ManageValidation
from eo_dataflow_manager.scheduler.sc import Controller
from eo_dataflow_manager.scheduler.sc.ConfigurationFileUtil import ConfigurationFileUtil
from eo_dataflow_manager.scheduler.sc.DownloaderDatabase import (
    STATE_DOWNLOAD_ERROR,
    STATE_DOWNLOAD_SUCCESS,
    STATE_TO_DOWNLOAD,
    STATE_TO_REDOWNLOAD,
    DownloaderDatabase,
)
from eo_dataflow_manager.scheduler.sc.LogUtil import setupFileLogger
from eo_dataflow_manager.scheduler.sc.ProviderManager import ProviderManager
from eo_dataflow_manager.scheduler.sc.job_state import JobState
from eo_dataflow_manager.scheduler.sc.job_targets.JobsElastic import JobsElastic

LOGGER_DEFAULT_FILE_NAME = 'download_'
DOWNLOAD_LOGGER_LEVEL = logging.DEBUG

# Download main states
STATE_INIT = 'Initializing'
STATE_WAITING_PROVIDER = 'Waiting Provider'
STATE_RUNNING = 'Running'
STATE_IDLE = 'Idle'
STATE_STOPPED = 'Stopped'

STATE_TEST_SUCCESS = 'Success'
STATE_TEST_WARNING = 'Warning'
STATE_TEST_PROGRESS = 'Testing In Progress'
STATE_TEST_NO_FILE_TO_DOWNLOAD = 'No file to download'
STATE_TEST_FAILED = 'Failed'

# Running states
STATE_RUNNING_SCANNING = 'Scanning'
STATE_RUNNING_WAIT_BETWEEN_DOWNLOADS = 'Wait %ss before starting next download'
STATE_RUNNING_CONNECTING = 'Connecting'
STATE_RUNNING_DOWNLOADING = 'Downloading %s'
STATE_RUNNING_STORING = 'Storing %s'
STATE_RUNNING_VALIDATING = 'Validating %s'

FOLDER_TEMPORARY = 'temporary'
FOLDER_TO_STORE = 'to_store'
FOLDER_WAITING_TO_COMPLETE = 'safe'
FOLDER_TO_SPOOL_FOR_SAFE = 'spool'
FOLDER_DOWNLOADED_FILES = 'to_store'

FOLDER_LISTINGS_AUTO = 'listings_auto'
FOLDER_LISTINGS_MANUAL = 'listings_manual'
FOLDER_LISTINGS_HISTORY = 'listings_history'
FOLDER_COMMANDS = 'commands'
FOLDER_INTERNAL = 'internal'

# Options
# fcad: add session timeout parameter-> TODO : add in configuration file
SESSION_TIMEOUT_SEC = 120
# MUST BE COMPATIBLE WITH EMM MESSAGES DATE FORMAT !!!
DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
# MUST BE COMPATIBLE WITH EMM MESSAGES DATA_FOR_DAY FORMAT !!!
DATA_FOR_DAY_FORMAT = '%Y-%m-%d'
# MUST BE COMPATIBLE WITH MONITORING MESSAGES DATE TIME FORMAT !!!
JSON_DATE_FORMAT = '%Y%m%dT%H%M%SZ'

FILE_ACCESS_RIGHT = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH
DIRECTORY_ACCESS_RIGHT = stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH

# Default values
LISTING_AUTO_MAX_LINES_DEFAULT = 100
CHECK_SOURCE_AVAILABILITY_DEFAULT = True
KEEP_LAST_SCAN_DEFAULT = True
NB_RETRIES_DEFAULT = 3
NB_PARALLEL_DOWNLOADS_DEFAULT = 0
MAX_NB_FILE_PER_CYCLE_DEFAULT = 10
CYCLE_LENGTH_DEFAULT = 60
DELAY_BEFORE_DOWNLOAD_START_DEFAULT = 120
DELAY_BETWEEN_SCANS_DEFAULT = 600
DELAY_BETWEEN_DOWNLOADS_DEFAULT = 0

ORGANIZATION_TYPE_DEFAULT = 'spool'

SOURCE_SELECTION_UPDATE_DEFAULT = 'modified'
SOURCE_LOGIN_DEFAULT = 'anonymous'
SOURCE_PASSWORD_DEFAULT = 'anonymous'
SOURCE_ROOT_PATH_DEFAULT = '/'


# TODO : Ajouter un nettoyeur de dossiers vides pour les differnts dossiers du workspace
#        (au demarrage par exemple, # apres avoir nettoye les dossiers des fichiers .tmp restants)
# A verifier, mais a priori c'est ok avec le cleanWorkspace...


class DownloaderException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class DownloadedFile(File):

    #    def __init__(self, local_filepath, remote_filepath, data_reader):
    def __init__(self, remote_filepath, data_reader=None, rooth_path=None):
        self.__localFile = None
        self.__localFolder = None
        self.__remote_filepath = remote_filepath
        self.__data_reader = data_reader

        # 29/01/2018 PMT#28 : add attributs
        self.filesize = None
        self.mtime = None
        self.downloadEndTime = None

        self.localFilename = None
        self.isFolder = False

        self.__storageName = None
        self.__relativePath = None
        self.__validRelativePath = False
        self.__roothPath = rooth_path

        if remote_filepath is not None:
            self.__relativePath = self.__get_relative_path()

        # infos destinees a etre mises dans le message operateur, dans le
        # <download_file><misc_infos>
        self.download_time = None
        self.data_for_day = None

        self.isDownloaded = False
        self.isStoredInternal = False
        self.isStoredFinal = False
        self.noChecksumFile = False

    def __set_local_file(self, local_file):
        assert isinstance(local_file, File)
        self.__localFile = local_file

    def __set_local_folder(self, local_folder):
        assert isinstance(local_folder, Folder)
        self.__localFolder = local_folder

    def __get_remote_name(self):
        _, filename = os.path.split(self.remoteFilepath)
        return filename

    def __set_remote_filepath(self, remote_filepath):
        self.__remote_filepath = remote_filepath

    def getDate(self):
        ret = self.__data_reader.getDate(self.remoteFilepath)
        if ret is not None:
            self.data_for_day = ret
        return ret

    def __get_relative_path(self):
        if self.__localFile is not None:  # in case getRelativePath would need a local file
            rel_path = self.__data_reader.getRelativePath(
                self.__localFile.getName())
            self.__validRelativePath = True
        else:
            rel_path = self.__data_reader.getRelativePath(
                self.remoteFilepath.split(FILE_URL_SEPARATOR)[0], self.__roothPath)

            if not self.__data_reader.ONLY_LOCAL:
                self.__validRelativePath = True

        return rel_path

    def getRelativePath(self, name_only=False, dir_only=False):

        if self.__relativePath is None or not self.__validRelativePath:
            self.__relativePath = self.__get_relative_path()

        if dir_only:
            return os.path.split(self.__relativePath)[0]
        if name_only:
            return os.path.split(self.__relativePath)[1]
        else:
            return self.__relativePath

    localFile = property(lambda self: self.__localFile, __set_local_file, None,
                         'local file path of the download file')
    localFolder = property(lambda self: self.__localFolder, __set_local_folder, None,
                           'local result path of the download file')

    remoteFilepath = property(
        lambda self: self.__remote_filepath,
        __set_remote_filepath,
        None,
        'remote filepath of the download file')

    remoteFilename = property(__get_remote_name, None, None,
                              'remote filename of the download file')


class Download(threading.Thread):
    """This class implements a processing chain, consisting of a tree
    of processes linked together by their input/output data exchanged
    through spools. The main operation performed by a processing chain
    is to run its processes on time or when input data are available,
    and to distribute the data from one processing node to the others,
    which is ensured by the L{sequence()<sequence>} method.
    """

    def __init__(
            self,
            loggerName,
            globalConfig,
            isTest,
            sema=None):
        """
        Download instantiation method

        @param loggerName: Name of text logger to use
        @param globalConfig: scheduler configuration
        @param isTest:  True => test
        @param sema: semaphore of provider
        """

        threading.Thread.__init__(self)
        self.name = f'{self.name}-{os.path.splitext(loggerName[len(Controller.SCHD_LOGGER_NAME) + 1:])[0]}'

        self.__id = ''  # set in read()
        self.__isTest = isTest

        self.__globalConfig = globalConfig

        self.__downloadPath = None

        self.__downloadDirs = {
            FOLDER_TEMPORARY: 'data/temporary',
            FOLDER_TO_STORE: 'data/to_store',
            FOLDER_DOWNLOADED_FILES: 'data/to_store',
            FOLDER_LISTINGS_AUTO: 'orders/listings/auto',
            FOLDER_LISTINGS_MANUAL: 'orders/listings/manual',
            FOLDER_LISTINGS_HISTORY: 'internal/listings/history',
            FOLDER_COMMANDS: 'orders/commands',
            FOLDER_INTERNAL: 'internal',
            FOLDER_WAITING_TO_COMPLETE: 'data/safe',
            FOLDER_TO_SPOOL_FOR_SAFE: 'data/to_spool'
        }
        self.__downloadFolders = {
            FOLDER_TEMPORARY: None,
            FOLDER_TO_STORE: None,
            FOLDER_DOWNLOADED_FILES: None,
            FOLDER_LISTINGS_AUTO: None,
            FOLDER_LISTINGS_MANUAL: None,
            FOLDER_LISTINGS_HISTORY: None,
            FOLDER_COMMANDS: None,
            FOLDER_INTERNAL: None,
            FOLDER_WAITING_TO_COMPLETE: None,
            FOLDER_TO_SPOOL_FOR_SAFE: None
        }
        self.__post_processing = []
        # Store localy the instance for log (in file and in db)
        self._log = logging.getLogger(loggerName)
        self.__logFile = None

        self.__state = (STATE_INIT, '')

        self.__current_mtime_download_conf_file = None
        # config parameters
        self.__download_conf_file = None
        self.__configuration = None
        self.__direction = None
        self.__data_reader = None
        self.__data_reader_node = None
        self.__data_reader_name = None
        self.__data_reader_subpath = None
        self.__listing_builder = None
        self.__listing_builder_node = None
        self.__listing_builder_name = None
        self.__checksum = None
        self.__compression_action = None
        self.__compression_type = None
        self.__file_ignored = None
        self.__tar_action = None
        self.__tar_add_sub_directory = False
        self.__zip_add_sub_directory = False
        self.__remote_storage_provider_ref = None
        self.__remote_storage_repository = None
        self.__remote_storage_server = None
        self.__remote_storage_username = None
        self.__remote_storage_passwd = None
        self.__remote_storage_port = None
        self.__remote_storage_type = None
        self.__remote_max_activated_flow = None

        self.__local_storage_repository = None
        self.__local_storage_type = None
        self.__local_added_spool_links = None
        self.__local_optional_storage_spool = None
        self.__misc_param_check_provider_before_download = CHECK_SOURCE_AVAILABILITY_DEFAULT
        self.__misc_param_nb_retry = NB_RETRIES_DEFAULT
        self.__misc_param_loop_delay = CYCLE_LENGTH_DEFAULT
        self.__misc_param_getlisting_delay = DELAY_BETWEEN_SCANS_DEFAULT
        self.__misc_param_wait_before_download_delay = DELAY_BEFORE_DOWNLOAD_START_DEFAULT
        self.__misc_param_wait_between_downloads = DELAY_BETWEEN_DOWNLOADS_DEFAULT
        self.__misc_param_max_activated_files_by_loop = MAX_NB_FILE_PER_CYCLE_DEFAULT
        self.__misc_param_parallel = False
        self.__misc_param_nb_parallel = NB_PARALLEL_DOWNLOADS_DEFAULT
        self.__misc_param_purge_scans_older_than = None
        self.__misc_param_keep_last_scan = True
        self.__listing_auto_max_lines = LISTING_AUTO_MAX_LINES_DEFAULT
        # 31/01/2018 PMT#33 : ajout de integrity_check
        self.__misc_param_integrity_check = None
        self.__prefix_dir = None

        self.__dp_redownload_accepted = None
        self.__dp_storage_temporal_switch_activated = None
        self.__dp_pattern_filter = None
        self.__dp_pattern_filter_destination = None
        self.__file_selector_mode = None
        self.__file_selector_filters = None
        self.__flow_ctrl_files_by_loop = None
        self.__dc_ignore_before = None
        self.__dc_ignore_after = None
        self.__dc_ignore_older_than = None

        self.__provider = None

        # 31/01/2018 PMT#33 : ajout de la sauvegarde du dernier  local path
        # temporaire
        self.__save_local_path = None

        self.__database = None
        self.__db_prefix = None
        self.__db_id = None
        self.__db_rel_path = None

        self.__jobFile = None
        self.__jobstate = None

        if self.test:
            self.__jobTestFile = None
            self.__teststate = None

        self._stopevent = threading.Event()

        self.__endingMessage = ''
        self.__endingMStatus = 'INFO'

        # emm_dir = [self.__globalConfig.getPath('messages_to_log')]
        # emm_dir.append(Controller.EMM_STORAGES)

        self.__emm_writer = EMMWriter(globalConfig)
        self.__emm_start_date = datetime.datetime.utcnow().strftime(messages.MSG_DATE_FORMAT)

        self.__session = None
        self.__sema = sema
        self.__nbFileToDownload = 0
        self.__isWaitSema = False
        # Ressources semaphore (parralel download)
        self.__semaState = threading.Semaphore()

        # 28/02/2018 PMT : ajout de verbose (mode debug)
        self.__verbose = False

        self.__groupid = -1

        self.__jobstateFile = None

        self.__monitoring = None
        self.__monitoringFilePath = None
        self.__jsonFileName = None

        # Setup Elasticsearch job upload
        if 'elasticsearch' in self.__globalConfig['jobs.targets']:
            self.__jobsElastic = JobsElastic(
                hosts=self.__globalConfig['jobs.elasticsearch.hosts'],
                scheme=self.__globalConfig['jobs.elasticsearch.scheme'],
                user=self.__globalConfig['jobs.elasticsearch.user'],
                password=self.__globalConfig['jobs.elasticsearch.password'],
                indexNameTemplate=self.__globalConfig['jobs.elasticsearch.index'],
                sniffCluster=self.__globalConfig['jobs.elasticsearch.sniff_cluster'],
            )
            self._log.debug('Jobs will be pushed to ElasticSearch')
        else:
            self.__jobsElastic = None
        # Setup RabbitMQ job upload
        self.__jobsRabbitMQ = PublishRabbitMQ

        self.__metricsRabbitMQ = None
        self.__metricsFilesystem = None

    def init_file_log(self, file_log_config, path):
        self.set_file_log_name(path)
        setupFileLogger(self._log, file_log_config, self.__logFile.getName())

        # If verbose option is set, the log level is forced to DEBUG
        if self.__verbose or self.__configuration.debug:
            self._log.setLevel(logging.DEBUG)

        # If the is no log level defined, set the log level to INFO
        if self._log.getEffectiveLevel() == logging.NOTSET:
            self._log.setLevel(logging.INFO)

    def set_file_log_name(self, path):
        file_name = [LOGGER_DEFAULT_FILE_NAME]
        file_name.append(self.__id)
        if self.test:
            file_name.append('_test')
        file_name.append('.log')
        self.__logFile = File(os.path.join(path, ''.join(file_name)))

    def init_downloader_database(self):
        self.__db_id = self.id  # TODO : ne pas utiliser le self._id mais un attribut de la conf
        self.__db_rel_path = 'downloader_%s.db' % self.id
        self.__db_prefix = 'sqlite:///'

        dp = os.path.join(self.__get_folder(
            FOLDER_INTERNAL).getPath(), self.__db_rel_path)
        db_url = self.__db_prefix + dp

        self.__database = DownloaderDatabase(self.__db_id, db_url)
        self.__database.createEngine(db_must_exist=False)

    def init_jobstate_file(self):
        job_file_name = '%s-%s.job' % (self.__id,
                                       gethostname())

        self.__jobFile = File(os.path.join(self.__globalConfig.getPath('jobfile'), job_file_name))
        self.__set_state(self.__state)

    def __set_state(self, state):
        tooked_semaphore = False
        try:
            if self.__jobstate is None:
                try:
                    self.__jobstate = JobState(
                        self.__jobFile,
                        mode='w',
                        jobsElastic=self.__jobsElastic,
                        jobsRabbitMQ=self.__jobsRabbitMQ
                    )
                except BaseException:
                    err_msg = 'Impossible to create JobState instance'
                    self._log.error(f'{err_msg}: {str(self.__jobFile)}')
                    self._log.exception(err_msg)
                    self.__jobstate = None
                    self.__jobstateFile = None
            else:
                self.__jobstate['Download'] = self.__id
                self.__jobstate['Host'] = gethostname()
                self.__jobstate['Pid'] = os.getpid()
                self.__jobstate['State'] = state[0]
                self.__jobstate['Details'] = state[1]
                self.__jobstate['Date'] = datetime.datetime.utcnow().strftime(
                    messages.MSG_DATE_FORMAT)
                self.__semaState.acquire()
                tooked_semaphore = True
                self.__jobstate.sync()
                self.__semaState.release()
        except Exception:
            self._log.error(
                "Cannot update jobstate File for download '%s' (state=%s)" %
                (self.__id, state))
            self._log.exception('Skip update')
            if tooked_semaphore:
                self.__semaState.release()

        self.__state = state

    def init_test_state_file(self):
        self._log.debug('initTestStateFile')
        job_test_file_name = '%s.test' % self.__id

        self.__jobTestFile = File(os.path.join(
            self.__globalConfig.getPath('jobfile'), job_test_file_name))
        self._log.debug('initTestStateFile %s' % self.__jobTestFile)

        # TODO changer
        self.__set_job_test_state(self.__state)

    def __set_job_test_state(self, state):
        self._log.debug('setJobTestState %s' % str(state[0]))
        tooked_semaphore = False
        try:
            if self.__teststate is None:
                try:
                    self._log.debug('self.__teststate == None')
                    self.__teststate = JobState(
                        self.__jobTestFile,
                        mode='w',
                        jobsElastic=self.__jobsElastic,
                        jobsRabbitMQ=self.__jobsRabbitMQ
                    )
                except BaseException:
                    err_msg = 'Impossible to create JobState instance'
                    self._log.error(err_msg + ' : %s' %
                                    (str(self.__jobTestFile)))
                    self._log.exception(err_msg)
                    self.__teststate = None
                    self.__jobTestFile = None
            else:
                self.__teststate['Download'] = self.__id
                self.__teststate['Host'] = gethostname()
                self.__teststate['Pid'] = os.getpid()
                self.__teststate['State'] = state[0]
                self.__teststate['Details'] = state[1]
                self.__teststate['Date'] = datetime.datetime.utcnow().strftime(
                    messages.MSG_DATE_FORMAT)

                self.__semaState.acquire()
                tooked_semaphore = True
                self.__teststate.sync()
                self.__semaState.release()
        except Exception:
            self._log.debug('self.__teststate == None')
            self._log.error(
                "Cannot update test jobstate File for download '%s' (state=%s)" %
                (self.__id, state))
            self._log.exception('Skip update : ')
            if tooked_semaphore:
                self.__semaState.release()

    id = property(lambda self: self.__id, None, None,
                  'id of the Download')

    state = property(lambda self: self.__state, __set_state, None,
                     'state of the Download')

    remote_storage_repository = property(
        lambda self: self.__remote_storage_repository, None, None, '')
    provider = property(lambda self: self.__provider, None, None, '')
    download_conf_file = property(
        lambda self: self.__download_conf_file,
        None,
        None,
        'Download configuration file')
    patternFilter = property(
        lambda self: self.__dp_pattern_filter, None, None, 'Pattern filter')
    redownloadAccepted = property(
        lambda self: self.__dp_redownload_accepted, None, None, '')
    remoteStorageProviderRef = property(
        lambda self: self.__remote_storage_provider_ref, None, None, '')
    remoteStorageProviderRepository = property(
        lambda self: self.__remote_storage_repository, None, None, '')
    remoteStorageProviderServer = property(
        lambda self: self.__remote_storage_server, None, None, '')
    remoteStorageProviderUsername = property(
        lambda self: self.__remote_storage_username, None, None, '')
    remoteStorageProviderPasswd = property(
        lambda self: self.__remote_storage_passwd, None, None, '')
    remoteStorageProviderPort = property(
        lambda self: self.__remote_storage_port, None, None, '')
    remoteStorageProviderType = property(
        lambda self: self.__remote_storage_type, None, None, '')
    remoteStorageProviderOption = property(
        lambda self: self.__configuration.source.protocol_option, None, None, '')
    remoteMaxActivatedFlow = property(
        lambda self: self.__remote_max_activated_flow, None, None, '')
    test = property(lambda self: self.__isTest, None, None, '')
    configuration = property(lambda self: self.__configuration, None, None, '')
    globalConfig = property(lambda self: self.__globalConfig, None, None, '')

    def __get_folder(self, folder_id):
        folder = None
        if folder_id in list(self.__downloadDirs.keys()) and \
                folder_id in list(self.__downloadFolders.keys()):
            if self.__downloadFolders[folder_id] is not None:
                folder = self.__downloadFolders[folder_id]
            else:
                relative_dir = self.__downloadDirs[folder_id]
                folder = Folder(os.path.join(
                    self.__downloadPath, relative_dir), recursive=True)
                self.__downloadFolders[folder_id] = folder

        return folder

    def set_download_conf_file(self, download_conf_file):
        """Set __download_conf_file and __current_mtime_download_conf_file
        """
        self.__download_conf_file = download_conf_file

        try:
            self.__current_mtime_download_conf_file = File(
                self.__download_conf_file).getTime()
            self._log.debug('  mtime of the download configuration file : %s',
                            self.__current_mtime_download_conf_file)
        except OSError as e:
            self._log.warning(
                " the download configuration file : %s doesn't exist",
                self.__current_mtime_download_conf_file)
            raise DownloaderException(e)

    def read_configuration_file(self):
        """Read and build the L{Download} tree from
        the XML configuration subtree (recursive construction)
        """

        xr = XMLReader(self._log.name)

        self._log.debug('  --> read of the download configuration : %s',
                        self.__download_conf_file)

        try:
            xmltree = xr.open(self.__download_conf_file)
        except ExpatError:
            raise IOError('invalid download_config file %s ' %
                          self.__download_conf_file)
        except IOError as e:
            self._log.warning(
                "Download_config file doesn't exist %s " %
                self.__download_conf_file)
            raise DownloaderException(e)
        except ParseError:
            self._log.warning('Download_config file parse error %s ',
                              self.__download_conf_file)
            raise IOError('invalid download_config file %s ' %
                          self.__download_conf_file)
        except Exception as e:
            self._log.warning('Read download_config file error %s (%s)',
                              self.__download_conf_file, e)
            raise IOError('invalid download_config file %s ' %
                          self.__download_conf_file)

        self.__configuration = ConfigurationFileUtil(
            self._log.name, xr, xmltree)
        self.__configuration.Read()

        self.__id = self.__configuration.id

        # ==============================================================================================================

        self._log.debug('  --> apply eo-dataflow-manager configuration : settings')

        if self.__configuration.settings.projectName is not None:
            self.__emm_writer.setProjectName(self.__configuration.settings.projectName)

        if self.__configuration.settings.nbRetries is not None:
            self.__misc_param_nb_retry = self.__configuration.settings.nbRetries

        if self.__configuration.settings.delayBetweenScans is not None:
            self.__misc_param_getlisting_delay = self.__configuration.settings.delayBetweenScans

        if self.__configuration.settings.cycleLength is not None:
            self.__misc_param_loop_delay = self.__configuration.settings.cycleLength

        if self.__configuration.settings.delayBeforeDownloadStart is not None:
            self.__misc_param_wait_before_download_delay = self.__configuration.settings.delayBeforeDownloadStart
        if self.test:
            self.__misc_param_wait_before_download_delay = 0

        if self.__configuration.settings.delayBetweenDownloads is not None:
            self.__misc_param_wait_between_downloads = self.__configuration.settings.delayBetweenDownloads

        if self.__configuration.settings.maxNbOfFilesDownloadedPerCycle is not None:
            max_activated_files_by_loop = self.__configuration.settings.maxNbOfFilesDownloadedPerCycle
            if max_activated_files_by_loop < self.__misc_param_max_activated_files_by_loop:
                self.__misc_param_max_activated_files_by_loop = max_activated_files_by_loop

        if self.__configuration.settings.nbParallelDownloads is not None:
            self.__misc_param_nb_parallel = self.__configuration.settings.nbParallelDownloads
            if self.__misc_param_nb_parallel > 0:
                self.__misc_param_parallel = True

        if self.__configuration.settings.checkSourceAvailability is not None:
            self.__misc_param_check_provider_before_download = self.__configuration.settings.checkSourceAvailability

        self.__remote_max_activated_flow = self.__configuration.settings.maxNbOfConcurrentStreams

        if self.__configuration.settings.database_purgeScansOlderThan is not None:
            self.__misc_param_purge_scans_older_than = self.__configuration.settings.database_purgeScansOlderThan

        if self.__configuration.settings.database_keepLastScan is not None:
            self.__misc_param_keep_last_scan = self.__configuration.settings.database_keepLastScan

        if self.__configuration.settings.maxNbOfLinesInAutoListing is not None:
            self.__listing_auto_max_lines = self.__configuration.settings.maxNbOfLinesInAutoListing

        if not self.__configuration.settings.monitoring:
            self.__monitoringFilePath = None
            self.__monitoring = False
        else:
            self.__set_monitoring_configuration()

        # Ensure that getlisting_delay > loop_delay
        if self.__misc_param_getlisting_delay < self.__misc_param_loop_delay:
            raise Exception(
                'Invalid configuration : getlisting delay MUST BE >= loop delay')

        # ==============================================================================================================
        self._log.debug('  --> apply eo-dataflow-manager configuration : destination')

        if isinstance(self.__configuration.destination.location, list):
            self.__local_storage_repository = self.__configuration.destination.location
        else:
            self.__local_storage_repository = self.__configuration.destination.location.split(';')
        for path in self.__local_storage_repository:
            if not os.path.isdir(path):
                raise Exception(f"Invalid destination location '{path}' : directory does not exist")

        if self.__configuration.destination.type is None:
            self.__local_storage_type = 'spool'
        elif self.__configuration.destination.type not in ['spool', 'datetree']:
            raise Exception("Unknown destination type '%s'"
                            % self.__configuration.destination.type)
        else:
            self.__local_storage_type = self.__configuration.destination.type.lower()
            if self.__local_storage_type != 'spool' and self.__configuration.destination.subpath is None:
                self.__data_reader_subpath = '%Y/%j'
            else:
                self.__data_reader_subpath = self.__configuration.destination.subpath

        if isinstance(self.__configuration.destination.spoolLink, list):
            if len(self.__configuration.destination.spoolLink) == 0 \
                    or self.__configuration.destination.spoolLink[0] is None:
                self.__local_added_spool_links = []
            else:
                self.__local_added_spool_links = self.__configuration.destination.spoolLink
        elif self.__configuration.destination.spoolLink is not None \
                and self.__configuration.destination.spoolLink != '':
            self.__local_added_spool_links = self.__configuration.destination.spoolLink.split(';')
        else:
            self.__local_added_spool_links = []
        for dir_link in self.__local_added_spool_links:
            if not os.path.isdir(dir_link):
                raise Exception(f'Invalid path for links {dir_link} : directory does not exist')

        if self.__configuration.destination.checksum is not None:
            self.__checksum = self.__configuration.destination.checksum.lower()
            if self.__checksum == 'none':
                self.__checksum = None
            elif (self.__checksum not in File.LIST_DISGEST) and (self.__checksum != 'safe'):
                raise Exception("Unknown checksum type '%s'"
                                % self.__checksum)

        if self.__configuration.destination.compression is not None:
            self.__compression_action = self.__configuration.destination.compression.lower()
            if self.__compression_action == 'none':
                self.__compression_action = None
            elif self.__compression_action not in ['compress', 'uncompress']:
                raise Exception("Unknown compression action '%s'"
                                % self.__compression_action)
            else:
                self.__compression_type = self.__configuration.destination.compression_type.lower()
                if self.__compression_type == 'none':
                    self.__compression_type = None
                elif self.__compression_type in File.LIST_COMPRESS:
                    self.__zip_add_sub_directory = self.__configuration.destination.compression_subdir
                else:
                    raise Exception("Unknown compression type '%s'"
                                    % self.__compression_type)

        if self.__configuration.destination.archive is not None:
            self.__tar_action = self.__configuration.destination.archive.lower()
            if self.__tar_action == 'none':
                self.__tar_action = None
            elif self.__tar_action not in ['extract']:
                raise Exception("Unknown archive action '%s'"
                                % self.__tar_action)
            self.__tar_add_sub_directory = self.__configuration.destination.archive_subdir

        if self.__configuration.destination.fileGroupName is not None:
            groups = [g.gr_name for g in grp.getgrall() if pwd.getpwuid(
                os.getuid()).pw_name in g.gr_mem]
            if len(groups) == 0:
                raise Exception(
                    "The user '%s' does not have the ability to change groups" %
                    (pwd.getpwuid(
                        os.getuid()).pw_name))
            if self.__configuration.destination.fileGroupName in \
                    [g.gr_name for g in grp.getgrall() if pwd.getpwuid(os.getuid()).pw_name in g.gr_mem]:
                self.__groupid = grp.getgrnam(
                    self.__configuration.destination.fileGroupName).gr_gid
            else:
                raise Exception(
                    "Invalid fileGroupName configuration : the user '%s' does not belong to the group '%s'" %
                    (pwd.getpwuid(
                        os.getuid()).pw_name,
                     self.__configuration.destination.fileGroupName))

        if self.__configuration.destination.keepParentFolder and self.__configuration.source.plugin != 'TreeDataReader':
            self.__misc_param_integrity_check = 'SAFE'

        if isinstance(self.__configuration.destination.optionalSpool, list):
            if len(self.__configuration.destination.optionalSpool) == 0 \
                    or self.__configuration.destination.optionalSpool[0] is None:
                self.__local_optional_storage_spool = []
            else:
                self.__local_optional_storage_spool = self.__configuration.destination.optionalSpool
        elif self.__configuration.destination.optionalSpool is not None \
                and self.__configuration.destination.optionalSpool != '':
            self.__local_optional_storage_spool = self.__configuration.destination.optionalSpool.split(';')
        else:
            self.__local_optional_storage_spool = []
        for path in self.__local_optional_storage_spool:
            if not os.path.isdir(path):
                raise Exception(f'Invalid destination optional location {path} : directory does not exist')

        # ==============================================================================================================
        self._log.debug('  --> apply eo-dataflow-manager configuration : source')

        self.__remote_storage_type = capwords(
            self.__configuration.source.protocol)
        self.__remote_storage_repository = self.__configuration.source.rootPath
        if self.__remote_storage_repository is None:
            self.__remote_storage_repository = '/'
        self.__remote_storage_server = self.__configuration.source.server
        self.__remote_storage_username = self.__configuration.source.login
        if self.__remote_storage_username is None:
            self.__remote_storage_username = 'anonymous'
        self.__remote_storage_passwd = self.__configuration.source.password
        if self.__remote_storage_passwd is None:
            self.__remote_storage_passwd = 'anonymous'
        self.__remote_storage_port = self.__configuration.source.port

        if self.__configuration.source.update == 'new':
            self.__dp_redownload_accepted = False
        else:
            self.__dp_redownload_accepted = True

        if not self.__configuration.source.date_pattern:
            try:
                if self.__configuration.source.date_maxDate \
                        and self.__configuration.source.date_maxDate.lower() != 'none':
                    self.__dc_ignore_before = datetime.datetime.strptime(
                        self.__configuration.source.date_maxDate, '%Y%m%d')
            except BaseException:
                raise Exception(
                    "max_date (%s) in date_folders does not have the expected format '%s'" %
                    (self.__configuration.source.date_maxDate, '%Y%m%d'))

            try:
                if self.__configuration.source.date_minDate \
                        and self.__configuration.source.date_minDate.lower() != 'none':
                    self.__dc_ignore_after = datetime.datetime.strptime(
                        self.__configuration.source.date_minDate, '%Y%m%d')
            except BaseException:
                raise Exception(
                    "min_date (%s) in date_folders does not have the expected format '%s'" %
                    (self.__configuration.source.date_minDate, '%Y%m%d'))

            self.__dc_ignore_older_than = self.__configuration.source.date_backlogInDays

        self._log.debug('  --> configure ListingBuilder')
        self.__listing_builder_name = 'DCheckReportListingBuilder'
        self.__listing_builder = Factory.ListingBuilderFactory(
            self.__listing_builder_name, self._log.name)
        self.__listing_builder.setDownload(self, self.__download_conf_file)
        if self.__configuration.source.update is None:
            self.__listing_builder.setConfiguration('modified')
        else:
            self.__listing_builder.setConfiguration(
                self.__configuration.source.update)

        # If download configuration is set to debug, the logger level is set to debug
        if self.__configuration.debug:
            self._log.setLevel(logging.DEBUG)
            self.__listing_builder.set_mode_debug(True)
        # If download configuration is set to no debug, the logger level is forced to debug
        # only if the verbose mode is set
        else:
            self.__listing_builder.set_mode_debug(self.__verbose)
            if self.__verbose:
                self._log.setLevel(logging.DEBUG)
            else:
                self._log.setLevel(logging.INFO)

        self._log.debug('  --> configure DataReader')
        try:
            self.__data_reader = Factory.DataReaderFactory(
                self.__configuration.source.plugin,
                self._log.name,
                self.__configuration.source.dateRegexp,
                self.__configuration.source.dateFormat,
                self.__data_reader_subpath)
        except Exception as e:
            raise Exception("Invalid '%s' plugin configuration : %s" %
                            (self.__configuration.source.plugin, e))
        else:
            if self.__data_reader.DIRECTORY:
                self._log.debug(f'      DataReader :{self.__configuration.source.plugin}')
                self.__data_reader.setContainer(
                    self.__configuration.destination.keepParentFolder)

        self._log.debug('  --> configure Filters')
        # Download Policies
        try:
            self.__dp_pattern_filter = PatternFilter()
            self.__dp_pattern_filter.add(
                'filename',
                self.__configuration.source.files_regexp,
                self.__configuration.source.files_ignoreRegexp)
        except Exception as e:
            raise Exception(
                "(file) regexp/ignore_regexp '%s' / '%s'  invalid : '% s'" %
                (self.__configuration.source.files_regexp,
                 self.__configuration.source.files_ignoreRegexp,
                 e))

        try:
            self.__dp_pattern_filter.add(
                'path',
                self.__configuration.source.directories_regexp,
                self.__configuration.source.directories_ignoreRegexp)
        except Exception as e:
            raise Exception(
                "(directory) regexp/ignore_regexp '%s' / '%s'  invalid : '% s" %
                (self.__configuration.source.directories_regexp,
                 self.__configuration.source.directories_ignoreRegexp,
                 e))
        try:
            self.__dp_pattern_filter_destination = PatternFilter()
            self.__dp_pattern_filter_destination.add(
                'filename',
                self.__configuration.destination.files_regexp,
                self.__configuration.destination.files_ignoreRegexp)
        except Exception as e:
            raise Exception(
                "(file) regexp/ignore_regexp '%s' / '%s'  invalid : '% s'" %
                (self.__configuration.destination.files_regexp,
                 self.__configuration.destination.files_ignoreRegexp,
                 e))

        self._log.debug('  --> apply download configuration : path')

        # Build the download path and if not exists, create it
        # Make a normalize folder name (only alphanum and space ( space will be
        # replace by '_')
        tmp_name = ''
        for i in self.__id:
            if i.isalnum() or i.isspace():
                tmp_name = tmp_name + i
            else:
                tmp_name = tmp_name + ' '
        # make the download workspace path
        self.__downloadPath = os.path.join(
            self.__globalConfig.getPath('work'),
            '_'.join(tmp_name.split())
        )
        # Make the list of the needed folders for the download workspace
        workspace_path_list = [self.__downloadPath]
        for d in list(self.__downloadDirs.keys()):
            download_folder_path = os.path.join(
                self.__downloadPath, self.__downloadDirs[d])
            workspace_path_list += [download_folder_path]

        # Add the path of the json files if the generation of the files is
        # requested
        if self.__monitoringFilePath is not None:
            workspace_path_list += [self.__monitoringFilePath]

        # Try to build needed directories for download workspace
        for workspace_path in workspace_path_list:
            if not os.path.isdir(workspace_path):
                self._log.info(
                    "folder '%s' doesn't exist... build it" % workspace_path)
                try:
                    os.makedirs(workspace_path)
                except Exception as e:
                    self._log.error("can't build folder %s ! abort (%s)" % (
                        workspace_path, str(e)))
                    raise e

        self._log.debug('  --> apply download configuration : Post Processing')

        try:
            for config in self.dict_plugins_config():
                if config['kwargs']['activate']:
                    self.__post_processing.append(
                        Factory.PostProcessingFactory(config['classname'], self._log, self.__id, **config['kwargs'])
                    )
        except Exception as e:
            raise e

        # End of read()
        self._log.debug('  download configuration done.')

    def dict_plugins_config(self):
        plugin_configs = [
            {
                'classname': 'ManageCompression',
                'kwargs': {
                    'activate': bool(self.__compression_action is not None \
                                     and self.__remote_storage_type != 'LocalpointerProvider'),
                    'downloadDirs': self.__downloadDirs,
                    'downloadFolders': self.__downloadFolders,
                    'downloadPath': self.__downloadPath,
                    'misc_param_integrity_check': self.__misc_param_integrity_check,
                    'zip_add_sub_directory': self.__zip_add_sub_directory,
                    'compression_action': self.__compression_action,
                    'compression_type': self.__compression_type
                }
            },
            {
                'classname': 'ManageTar',
                'kwargs': {
                    'activate': bool(self.__tar_action is not None \
                                     and self.__remote_storage_type != 'LocalpointerProvider'),
                    'downloadDirs': self.__downloadDirs,
                    'downloadFolders': self.__downloadFolders,
                    'downloadPath': self.__downloadPath,
                    'misc_param_integrity_check': self.__misc_param_integrity_check,
                    'tar_action': self.__tar_action,
                    'tar_add_sub_directory': self.__tar_add_sub_directory
                }
            },
            {
                'classname': 'ManageValidation',
                'kwargs': {
                    'activate': bool(self.__configuration.destination.validation),
                    'validation': self.__configuration.destination.validation
                }
            },
            {
                'classname': 'FileFilter',
                'kwargs': {
                    'activate': bool(self.__configuration.destination.files_regexp or self.__configuration.destination.files_ignoreRegexp),
                    'dpPatternFilterDestination': self.__dp_pattern_filter_destination
                }
            }
        ]
        return plugin_configs

    # 24/03/2020 #37 : monitoring - global configuration
    def __set_monitoring_configuration(self):
        # Setup RabbitMQ metrics upload
        self.__metricsRabbitMQ = PublishRabbitMQ

        # Setup Filesystem metrics upload
        if 'filesystem' in self.__globalConfig['metrics.targets']:
            self.__monitoringFilePath = os.path.join(self.__globalConfig['paths.workspace'],
                                                     self.__globalConfig['metrics.filesystem.path'])
            self.__metricsFilesystem = True
        else:
            self.__monitoringFilePath = None

        if self.__monitoringFilePath is None and self.__metricsRabbitMQ is None:
            self.__monitoring = False
        else:
            self.__monitoring = True

    # 15/05/2018 PMT : limite globale par défaut
    def set_default_max_activated_files_by_loop(self, max_activated_files_by_loop):
        if max_activated_files_by_loop is not None:
            self.__misc_param_max_activated_files_by_loop = int(
                max_activated_files_by_loop)

    # 15/05/2018 PMT : limite globale par défaut
    def set_debug_mode(self, verbose):
        self.__verbose = verbose
        self._log.debug('  Verbose mode : %s' % verbose)

    def __get_session(self):
        if self.__session is None:
            self.state = (STATE_RUNNING, STATE_RUNNING_CONNECTING)
            # fcad: add session timeout parameter
            session = self.__provider.getSession(timeout_=SESSION_TIMEOUT_SEC)
            if session is None:
                self._log.info("Invalid session for download '%s'" % self.id)
            self._log.debug("Initiate session for download '%s'" % self.__id)
            self.__session = session
            self._log.debug("session '%s'" % self.__session)
        return self.__session

    def __close_session(self, session):
        self.__provider.closeSession(session)
        self.__session = None

    def __verify_checksum(self, downloaded_file, downloaded_checksum_file):
        self._log.debug(
            'verifyChecksum : downloaded_file.remoteFilename=%s, downloaded_checksum_file=%s' %
            (downloaded_file.remoteFilename, downloaded_checksum_file.remoteFilename))
        checksum_ok = False
        try:
            downloaded_checksum_file.localFile.open()
            chksum_file_content = downloaded_checksum_file.localFile.readlines()
            downloaded_checksum_file.localFile.close()
            file_chksum_value = None
            for chksum_file_line in chksum_file_content:
                if len(chksum_file_line.split()) > 1:
                    chksum_value, filename = chksum_file_line.split(' ', 1)
                else:
                    chksum_value = chksum_file_line
                    filename = downloaded_file.remoteFilename
                # recuperation du nom de fichier uniquement, pour eviter les pbs avec les
                # checksum contenant des noms de fichiers type './filename'
                filename = os.path.basename(filename.strip())
                if filename == downloaded_file.remoteFilename:
                    file_chksum_value = chksum_value
            if file_chksum_value is not None and downloaded_file.localFile.checkIntegrity(
                    file_chksum_value, self.__checksum):
                checksum_ok = True
                self._log.debug('CheckIntegrity : OK !')
            else:
                self._log.info(
                    "Checksum failure : file : '%s', md5 value for file should be : '%s'" %
                    (downloaded_file.localFile.getCheckSum(), file_chksum_value))
        except Exception as e:
            self._log.exception(e)
            raise e
        finally:
            downloaded_checksum_file.localFile.close()

        return checksum_ok

    def __get_safe_checksum(self, downloaded_checksum_file):
        xr = XMLReader(self._log.name)
        xfdumanifest = xr.open(downloaded_checksum_file)
        xfdu = xr.getRootNode(logging.ERROR, xfdumanifest)
        nodes = xr.getAllSubNode(xr.getSubNode(
            logging.ERROR, xfdu, 'dataObjectSection'), 'dataObject')
        checksums = {}
        for node in nodes:
            file_node = xr.getSubNode(logging.ERROR, xr.getSubNode(
                logging.ERROR, node, 'byteStream'), 'fileLocation')
            file_name = os.path.split(xr.getAttributeValue(
                logging.ERROR, file_node, 'href'))[1]
            checksum_node = xr.getSubNode(logging.ERROR, xr.getSubNode(
                logging.ERROR, node, 'byteStream'), 'checksum')
            checksum_name = xr.getAttributeValue(
                logging.ERROR, checksum_node, 'checksumName').lower()
            checksum_value = xr.getNodeValue(checksum_node)
            checksums[file_name] = (checksum_name, checksum_value)
        return checksums

    def __verify_safe_checksum(self, downloaded_file, checksum_dict):
        self._log.debug('verifyChecksum : downloaded_file=%s' %
                        (downloaded_file.getName(short=True)))
        checksum_ok = False
        try:
            checksum = checksum_dict.get(downloaded_file.getName(short=True))
            if checksum is not None:
                file_checksum = downloaded_file.getCheckSum(checksum[0])
                if file_checksum == checksum[1]:
                    checksum_ok = True
                    self._log.debug('CheckIntegrity : OK !')
                else:
                    self._log.info(
                        "Checksum failure : file : '%s', %s value for file should be : '%s'" %
                        (file_checksum, checksum[0], checksum[1]))
            else:
                checksum_ok = True
                self._log.debug(
                    "Checksum file : '%s', no reference value found for this file" %
                    (downloaded_file.getName(
                        short=True)))
        except Exception as e:
            self._log.exception(e)
            raise e

        return checksum_ok

    def set_remote_storage_provider_ref(self, ref):
        self.__remote_storage_provider_ref = ref
        self._log.debug("Remote storage provider ref : %s':" %
                        self.__remote_storage_provider_ref)

    def set_semaphore_limit_flow(self, sema):
        self.__sema = sema

    def get_listing_remote(self):
        """ Renvoie la liste des fichiers dont le telechargement est demande """

        # fabrication d'un token, qui permet de s'assurer que l'acquittement envoye
        # au listing_builder correspond bien au listing demande. Ce token est egalement
        # utilise pour la generation du nom de listing (pour le rendre unique)
        # FP 13/10/09 : '%f' ne fonctionne qu'a partir de python 2.6..., je met un random
        # a la place, pour eviter le risque de collision (qui n'aurait pas vraiment d'incidence
        # dans tous les cas, en dehors de rajouter des lignes a un listing existant...)
        token = datetime.datetime.utcnow().strftime('%Y%m%d_%H%M%S')
        token = token + '.%s' % (str(random.randrange(0, 10000000000)))
        listing_filepath = None
        try:
            # demande au plugin listing builder de recuperer le listing des
            # fichiers a eo-dataflow-manager
            self.__listing_builder.check_provider()
            self.__listing_builder.setWorkingDirectory(
                self.__get_folder(FOLDER_INTERNAL).getPath())
            listing = self.__listing_builder.getListing(token)
            # store the new listing, and send acknowledgment to listing builder. It is more secure :
            # avoid to loose a listing before it is stored in the listing folder. If a listing is
            # not acknowledged, the listing_builder should send the same listing (at least!) next time
            # getListing() is called

            if len(listing) > 0:
                listing_filename = 'Listing_%s_%s.list' % (
                    gethostname(), token)
                listing_filepath = os.path.join(self.__get_folder(
                    FOLDER_LISTINGS_AUTO).getPath(), listing_filename)
                with open(listing_filepath, mode='a+') as f:
                    for line in listing:
                        line = line.strip()
                        f.write('%s\n' % line)
        except Exception as e:
            # acquittement non envoye => le getListing sera relance avec les memes parametres par la suite.
            # on choppe l'exception ici parce que ce n'est pas grave, et qu'on veut qd meme executer la
            # suite du code
            msg = '[%s] getListing anomaly, will be retried later' % self.id
            self._log.exception('OPERATEUR : ' + msg)
            # On envoie un warning a l'operateur qd meme, pour signaler un pb
            # sur ce download
            m = messages.MessageFactory().create({
                'type': 'downloadMessage',
                'level': 'WARNING',
                'summary': '[%s] get listing anomaly : %s' % (self.id, str(e)),
                'details': '%s' % (msg + ' : ' + str(e)),
                'processing_start_date': self.__emm_start_date,
                'processing_hostname': gethostname(),
                'processing_pid': os.getpid(),
                'download_name': self.id,
                'download_provider': self.__remote_storage_provider_ref,
            }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)
            if listing_filepath is not None:  # si un listing a ete cree, on l'ajoute au message
                m.reference = messages.Reference(
                    name=os.path.basename(listing_filepath), type='listing')
                m.details += ' Listing path : %s' % listing_filepath
            try:
                self.__emm_writer.writeMessage(m, self.__id)
            except Exception as e:
                self._log.exception('Message write error : %s' % (str(e)))
        else:
            # OK, listing is written, we can acknowledge it
            self.__listing_builder.ackListingStorage(token)

        # On controle les listings du dossier 'auto', qui ne doivent pas depasser un certain nombre de lignes
        # par securite (probablement un probleme de getlisting si le nombre est grand). En revanche, on ne fait pas
        # ce controle pour les listings du dossier 'manual'
        folder_listings_auto_path = self.__get_folder(
            FOLDER_LISTINGS_AUTO).getPath()
        ds = DiskSpool.DiskSpool(folder_listings_auto_path, r'.*\.list$',
                                 lockFileExt='.lock')
        for listing in list(ds.keys()):
            listing_file = File(os.path.join(
                folder_listings_auto_path, listing))
            listing_file.open()
            lines = listing_file.readlines()
            listing_file.close()
            if len(lines) > self.__listing_auto_max_lines:
                listing_file.acquireLock('eo-dataflow-manager::getListing: too_many_lines')
                msg = ("[%s] Listing '%s' locked : too much files asked for download in mode 'auto' " +
                       "(use 'manual' mode to force the download if needed)") \
                      % (self.id, listing_file.getName())
                self._log.error('OPERATOR : ' + msg)
                m = messages.MessageFactory().create({
                    'type': 'downloadMessage',
                    'level': 'ERROR',
                    'summary': '[%s] Listing validation error : %s' % (self.id, listing_file.getName(short=True)),
                    'details': '%s' % msg,
                    'reference_type': 'listing',
                    'reference_name': os.path.basename(listing_file.getName(short=True)),
                    'processing_start_date': self.__emm_start_date,
                    'processing_hostname': gethostname(),
                    'processing_pid': os.getpid(),
                    'download_name': self.id,
                    'download_provider': self.__remote_storage_provider_ref,
                }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)
                try:
                    self.__emm_writer.writeMessage(m, self.__id)
                except Exception as e:
                    self._log.exception('Message write error : %s' % (str(e)))

    def get_listing(self, get_remote_listing):
        # generation des listings auto
        if get_remote_listing and not self.test:
            self._log.info("Scanning '%s'", self.id)
            self.get_listing_remote()

        # recupere la liste des listings a traiter, a partir de tous les
        # fichiers qui ne sont pas en lock
        listing_list = []

        for folder in [self.__get_folder(FOLDER_LISTINGS_AUTO),
                       self.__get_folder(FOLDER_LISTINGS_MANUAL)]:

            if self.test:
                ds = DiskSpool.DiskSpool(folder.getPath(), r'.*\.list.test$',
                                         lockFileExt='.lock')
                self._log.debug(
                    " Listing du fichier test '%s' ", list(ds.keys()))
            else:
                ds = DiskSpool.DiskSpool(folder.getPath(), r'.*\.list$',
                                         lockFileExt='.lock')
            for listing in list(ds.keys()):
                listing_path = os.path.join(ds.getPath(), listing)
                self._log.debug("Find '%s' as a listing file", listing_path)
                listing_list += [listing_path]

            ds = DiskSpool.DiskSpool(folder.getPath(), r'.*\.list.sync$',
                                     lockFileExt='.lock')

            if not self.test:
                for listing in list(ds.keys()):
                    listing_path = os.path.join(ds.getPath(), listing)
                    self._log.debug(
                        "Find '%s' as a listing file", listing_path)
                    listing_list += [listing_path]

        if self.test and not listing_list:
            self.__set_job_test_state([STATE_TEST_NO_FILE_TO_DOWNLOAD, ''])

        self._log.debug(" get Listing list '%s' ", listing_list)
        return listing_list

    def store_listing(self, listing_path, is_sync=False):
        """ Verifie que le listing est acceptable, et stocke chaque element dans la base persistente """
        nbr_files_to_download = 0

        self._log.debug("Start listing storage for '%s'", listing_path)
        # verifie que les fichiers a telecharger correspondent bien aux patterns autorises, sinon on
        # lock le listing et on envoie un message operateur
        listing_file = File(listing_path)
        listing_file.open('r')
        lines = listing_file.readlines()
        listing_file.close()

        file_map = {}
        for line in lines:
            line_infos = line.split(', ')
            filepath = line_infos[0].strip()

            if filepath == '':
                continue
            is_ignored, sensingtime = self.is_file_ignored(
                filepath.split(FILE_URL_SEPARATOR)[0])
            if not is_sync and is_ignored:
                self._log.info('File ignored : %s' % filepath)
                continue
            accepted, is_file_already_downloaded = self.is_file_to_download(
                filepath.split(FILE_URL_SEPARATOR)[0], validate_listing_mode=True)
            self._log.debug("  isAccepted '%s' : %s" % (filepath, accepted))
            if not accepted:
                listing_file.acquireLock('eo-dataflow-manager::storeListing: refusedfile')
                raise Exception(
                    "Invalid listing '%s' (contains a refused file : '%s')" %
                    (listing_file.getName(), filepath))

            line_mtime = None
            line_size = None
            for info in line_infos[1:]:
                info_split = info.split('=')
                if len(info_split) > 1:
                    if info_split[0].strip() == 'mtime':
                        line_mtime = info_split[1]
                    elif info_split[0].strip() == 'size':
                        line_size = info_split[1]
            file_map[filepath] = [sensingtime, line_size, line_mtime, is_file_already_downloaded]

        # OK, all files are accepted, we store the whole listing in the database
        # Remarque : en cas de plantage ici, pas grave, le listing sera locke et un message
        # sera envoye a l'operateur (cf. try/except qui englobe le storeListing() dans le runDownload)
        # On verifie les download policies pour chaque fichier. Si certains sont refuses, on ne les ajoute pas
        # pour le download, et on averti l'operateur
        # for file in fileList:
        for file in list(file_map.keys()):
            sensingtime = file_map[file][0]
            is_file_already_downloaded = file_map[file][3]
            if file_map[file][1] is not None:
                size = int(file_map[file][1])
            else:
                size = None
            if file_map[file][2] is not None:
                try:
                    mtime = datetime.datetime.strptime(file_map[file][2].rstrip(), '%Y-%m-%d %H:%M:%S.%f')
                except ValueError:
                    mtime = datetime.datetime.strptime(file_map[file][2].rstrip(), '%Y-%m-%d %H:%M:%S')
            else:
                mtime = None

            nb_try = 0
            if not is_file_already_downloaded or self.__dp_redownload_accepted:
                if is_file_already_downloaded:
                    state = STATE_TO_REDOWNLOAD
                else:
                    state = STATE_TO_DOWNLOAD
                try:
                    self.set_file_to_download(
                        file, sensingtime, state, size, mtime)
                except Exception as e:
                    if nb_try < 3:
                        time.sleep(10)
                        self.set_file_to_download(
                            file, sensingtime, state, size, mtime)
                        nb_try += 1
                    else:
                        raise e
            else:
                self._log.warning("Download '%s' : \
                                   file '%s' already downloaded, and redownload not accepted => policy violation."
                                  % (self.id, file))
                msg = ("[%s] Download policy violation for file '%s' : will not be downloaded. " +
                       'You should check ListingBuilder config file or download config file.') \
                      % (self.id, file)
                self._log.warning('OPERATOR : ' + msg)
                m = messages.MessageFactory().create({
                    'type': 'downloadMessage',
                    'level': 'WARNING',
                    'summary': '[%s] Download policy violation for file %s' % (self.id, os.path.basename(file)),
                    'details': '%s' % msg,
                    'reference_type': 'data',
                    'reference_name': os.path.basename(file),
                    'processing_start_date': self.__emm_start_date,
                    'processing_hostname': gethostname(),
                    'processing_pid': os.getpid(),
                    'download_name': self.id,
                    'download_provider': self.__remote_storage_provider_ref,
                }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)
                try:
                    self.__emm_writer.writeMessage(m, self.__id)
                except Exception as e:
                    self._log.exception('Message write error : %s' % (str(e)))

                self.update_state_test(
                    [STATE_TEST_FAILED, 'Download policy violation'])

            nbr_files_to_download += 1
            if nbr_files_to_download % 500 == 0:
                time.sleep(10)

        return nbr_files_to_download

    def is_file_ignored(self, filepath):
        try:
            fd = self.__data_reader.getDate(filepath)
        except Exception as e:
            self._log.exception(
                'Cannot check if file is ignored, getDate() error : %s' %
                (str(e)))
            raise e

        if fd is not None:

            if self.__dc_ignore_before is not None and self.__dc_ignore_before > fd:
                return True, fd

            if self.__dc_ignore_after is not None and self.__dc_ignore_after < fd:
                return True, fd

            if self.__dc_ignore_older_than is not None:
                ignore_date = datetime.datetime.utcnow(
                ) - datetime.timedelta(days=self.__dc_ignore_older_than)
                if ignore_date > fd:
                    return True, fd

        return False, fd

    def is_file_to_download(self, filepath, validate_listing_mode=False):
        """ Verifie a partir du chemin de fichier, s'il est autorise au telechargement ou non
            Utilise le temporal_selector pour savoir si c'est ok ou pas (le pattern_filters a
            ete utilise dans le getListing, il s'agit donc ici d'un check plus elabore que le pattern)
        """
        # TODO : implementer les autres filtres (temporal selector etc..)
        accepted = self.__dp_pattern_filter.isAccepted(filepath)
        is_downloaded = self.is_file_already_downloaded(filepath)
        if validate_listing_mode:
            return accepted, is_downloaded
        # download policy : redownload (si le fichier a deja ete telecharge
        # avec succes,
        if accepted and self.__dp_redownload_accepted is False:
            if is_downloaded:
                self._log.warning("Download '%s' : \
                                   file '%s' already downloaded, and redownload not accepted => policy violation."
                                  % (self.id, filepath))
                accepted = False

        return accepted, is_downloaded

    def is_file_already_downloaded(self, filepath):
        # clear_mappers() # TODO : voir pourquoi y'a besoin de ca ici.. c'est
        # nul...
        self.__database.getSession()
        is_downloaded = self.__database.isDownloaded(filepath)
        self.__database.closeSession()
        return is_downloaded

    def set_file_to_download(
            self,
            filepath,
            sensingtime,
            state=STATE_TO_DOWNLOAD,
            size=None,
            mtime=None):
        """ Enregistre de maniere persistante qu'il faudra telecharger ce fichier """
        # clear_mappers() # TODO : voir pourquoi y'a besoin de ca ici.. c'est
        # nul...
        self.__database.getSession()

        delay_seconds = self.__misc_param_wait_before_download_delay
        ignition_date = datetime.datetime.utcnow(
        ) + datetime.timedelta(seconds=delay_seconds)
        self._log.info(
            'File to download in %s seconds minimum [download_id=%s] : %s' %
            (delay_seconds, self.id, filepath))
        self._log.debug(' [sensingtime=%s] state =%s ' % (sensingtime, state))
        self.__database.setDownload(
            filepath=filepath,
            state=state,
            sensingtime=sensingtime,
            ignition_date=ignition_date,
            size=size,
            mtime=mtime)
        self.__database.closeSession()
        return True

    def get_files_to_download(self):
        """ Renvoie la liste des fichiers dont le telechargement est accepte et en attente.
            Utilise les informations qui ont ete ecrites de maniere persistante par le setFileToDownload
        """
        file_list = []
        try:
            # clear_mappers()
            self.__database.getSession()

            files = self.__database.getFilesToDownload()
            if files is None:
                files = []
            files_to_redownload = self.__database.getFilesToReDownload()
            if files_to_redownload is not None:
                files.extend(files_to_redownload)

            # 24/04/2018 PMT : ajout de la reprise des errors
            # On place les fichiers marqués à re-essai en fin de liste de
            # téléchargement
            files_with_error = self.__database.getFilesWithError()
            if files_with_error is not None:
                files.extend(files_with_error)

            self.__database.closeSession()
            # 24/04/2018 PMT : optimisation de la liste des fichiers à télécharger
            # pour augmenter la lisibilité du log
            count = 1
            if len(files) > 0:
                self._log.info("Files to download for '%s' (%d): " %
                               (self.id, len(files)))
                for f in files:
                    # 2018/04/19 PMT : réduction de l'affichage de la liste des
                    # téléchargement à faire
                    if count <= 30 or count >= (len(files) - 9):
                        self._log.info(' - %s' % f.filepath)
                    if len(files) >= 50 and count == 30:
                        self._log.info(
                            ' - ... %d others files to download ...' %
                            (len(files) - 40))
                    # fileList.append(f.filepath)
                    file_list.append(f)
                    count += 1
                self._log.info("%d files to download for '%s'." %
                               (len(file_list), self.id))
        except Exception as e:
            self._log.exception(
                "[%s] getFilesToDownload error : %s (don't worry, will be retried later)" %
                (self.id, str(e)))
        # priorite effectue via la requete getFilesToDownload (order by sensing
        # time)
        return file_list

    def download_file(
            self,
            downloaded_file,
            session=None,
            download_checksum=False):
        """ Do the download for one file
            download_checksum : used to download checksumFiles
        """
        # TODO : inclure dans le downloadFile un moyen de renommer les fichiers
        # a partir d'un plugin et de leur nom/path/date_de_contenu etc...
        # Actuellement, c'est le NameBasedDataReader qui est utilise par
        # defaut...
        self._log.debug(
            'downloadFile (%s, %s)' %
            (downloaded_file.remoteFilename,
             str(download_checksum)))

        download_ok = False
        checksum_ok = False
        nb_try = 0
        downloaded_checksum_file = None

        # State : Connecting
        if session is None:
            session = self.__get_session()

        # State : Downloading
        last_exception = None  # to get last exception error, after n unsuccessful tries
        self.state = (STATE_RUNNING, STATE_RUNNING_DOWNLOADING %
                      downloaded_file.remoteFilepath)
        while download_ok is False and nb_try < self.__misc_param_nb_retry:
            nb_try += 1
            # Reset session if problem occured during previous download
            if nb_try > 1:
                # wait 5 secondes before retry
                self.__close_session(session)
                time.sleep(5)
                session = self.__get_session()
            if session is None:
                continue

            self._log.info(
                "Start download for file '%s' [try=%s/%s]" %
                (downloaded_file.remoteFilepath, nb_try, self.__misc_param_nb_retry))

            try:
                # recuperation du nom local a partir du path du fichier.
                # Reader specifique a la donnee si possible

                tmp_storage_path = os.path.join(
                    self.__get_folder(FOLDER_TEMPORARY).getPath(),
                    downloaded_file.getRelativePath())
                # creation du folder de stockage temporaire s'il n'existe pas
                # deja
                Folder(os.path.split(tmp_storage_path)[0], create=True)

                # Telechargement du fichier
                start_time = time.time()
                try:
                    # 2018/04/18 PMT#34 : remoteFilepath peut contenir une URL après le séparateur FILE_URL_SEPARATOR
                    # dans ce cas on passe l'URL, non pas
                    # downloaded_file.remoteFilepath
                    if len(downloaded_file.remoteFilepath.split(
                            FILE_URL_SEPARATOR)) == 2:
                        local_file = self.__provider.getFile(
                            session,
                            downloaded_file.remoteFilepath.split(
                                FILE_URL_SEPARATOR)[1],
                            tmp_storage_path)
                    else:
                        local_file = self.__provider.getFile(
                            session, downloaded_file.remoteFilepath, tmp_storage_path)
                        # Check if downloaded file is empty
                    if local_file is not None:
                        check = ManageValidation(self._log, self.__id)
                        check.check_downloaded_file(local_file.getName(), False)
                except Exception as e:
                    self._log.error(
                        "Download error for '%s' in provider.getFile : %s" %
                        (downloaded_file.remoteFilepath, str(e)))
                    last_exception = e
                    local_file = None

                if local_file is None:
                    # un pb a eu lieu durant le telechargement... on ne fait rien (le
                    # download va etre retente)
                    self._log.debug(
                        'An error occured while download... hope next try will be ok')
                    continue

                # Download is ok, file is retrieved
                remote_file_path = downloaded_file.remoteFilepath.split()
                if len(remote_file_path) > 1:
                    downloaded_file.remoteFilepath = remote_file_path[0]

                downloaded_file.localFile = local_file
                downloaded_file.isDownloaded = True
                if download_checksum is False:
                    downloaded_file.mtime = self.__provider.getmtime()
                    downloaded_file.filesize = self.__provider.getsize()
                    self._log.debug(f'Real downloaded mtime_file : {str(downloaded_file.mtime)},'
                                    f'Real downloaded size_file : {str(downloaded_file.filesize)},')
                stop_time = time.time()

                # Gestion du checksum, dans le cas d'un fichier normal (pas un
                # checksum)
                if downloaded_file.isDownloaded and download_checksum is False:
                    if self.__checksum is None:
                        self._log.debug('No checksum wanted. Continue')
                        checksum_ok = True
                    elif self.__checksum == 'safe':
                        self._log.debug('Checksum activated. \
                                         checking when all files in the SAFE folder have been downloaded')
                        checksum_ok = True
                    else:
                        self._log.debug(
                            "Checksum activated : '%s', download and check it" %
                            self.__checksum)
                        checksum_ok = False
                        chk_ext = '.' + self.__checksum
                        chk_filepath = downloaded_file.remoteFilepath + chk_ext

                        downloaded_checksum_file = DownloadedFile(
                            remote_filepath=chk_filepath, data_reader=self.__data_reader,
                            rooth_path=self.__configuration.source.rootPath)
                        self.download_file(
                            downloaded_checksum_file, session, download_checksum=True)

                        # verify md5 checksum
                        if downloaded_checksum_file.isDownloaded:
                            checksum_ok = self.__verify_checksum(
                                downloaded_file, downloaded_checksum_file)
                        else:
                            # Fichier md5 manquant - on force le telechargement comme s'il s'était bien passé.
                            #   on conserve neammoins
                            # Fichier md5 manquant => noChecksumFile
                            downloaded_checksum_file.isDownloaded = True
                            downloaded_file.noChecksumFile = True
                            checksum_ok = True

                # si je suis en mode de telechargement checksum, on ne verifie pas le checksum du checksum...
                #   il est donc ok, a partir du moment où on a telecharge correctement le fichier checksum
                # ici, downloaded_file == fichier de checksum
                if download_checksum and downloaded_file.isDownloaded:
                    checksum_ok = True

                if downloaded_file.isDownloaded and downloaded_file.localFile.exist():
                    if checksum_ok:
                        download_ok = True

                        self._log.debug("Download OK for file '%s'" %
                                        downloaded_file.remoteFilepath)

                        # Si on n'est pas en mode checksum, on rempli les
                        # informations du DownloadedFile et on vire le fichier
                        # checksum telecharge
                        if not download_checksum:

                            # recuperation du temps de download pour ce fichier
                            downloaded_file.download_time = int(
                                stop_time - start_time)

                            # supprime le fichier de checksum devenu inutile
                            if downloaded_checksum_file is not None and downloaded_checksum_file.localFile is not None \
                                    and downloaded_checksum_file.localFile.exist():
                                downloaded_checksum_file.localFile.remove()
                    else:
                        downloaded_file.isDownloaded = False
                        raise Exception('Checksum error')
                else:
                    raise Exception(
                        'Unknown download error (more informations may be in the eo-dataflow-manager logfile ?)')
            except Exception as e:
                # pendant les N essais, on se contente d'afficher une erreur.
                # Si les N essais sont en erreur, on raise la last_exception
                self._log.exception(
                    "Error downloading file '%s' [try %s/%s] : %s" %
                    (downloaded_file.remoteFilepath, nb_try, self.__misc_param_nb_retry, str(e)))
                last_exception = e
                downloaded_file.isDownloaded = False

        # Bilan, apres les N essais possibles de telechargement : si le fichier
        # n'est pas telecharge, on raise une erreur
        if downloaded_file.isDownloaded is False and last_exception is not None:
            raise last_exception

        if downloaded_file.isDownloaded is False and self.__provider.getLastError() is not None:
            raise Exception(self.__provider.getLastError())

        if downloaded_file.isDownloaded is False and download_checksum is False:
            raise Exception("Unknown error while downloading file '%s'" % (
                downloaded_file.remoteFilepath))

    def store_internal_downloaded_file(self, downloaded_file, integrity_check=None):

        self.state = (STATE_RUNNING, STATE_RUNNING_STORING % downloaded_file.remoteFilename)
        self._log.debug(f"[{self.id}] Store '{downloaded_file.localFile.getName()}'")
        if integrity_check == 'SAFE':
            relative_path_first_part, relative_path_last_part = \
                os.path.split(downloaded_file.getRelativePath(dir_only=True))
            if self.__local_storage_type == 'spool':
                relative_path = os.path.join('SAFE.' + os.path.split(relative_path_last_part)[1],
                                             os.path.split(relative_path_last_part)[1])
            else:
                relative_path = os.path.join(relative_path_first_part,
                                             'SAFE.' + relative_path_last_part,
                                             os.path.split(relative_path_last_part)[1])
            base_storage_folder = self.__get_folder(FOLDER_WAITING_TO_COMPLETE).getPath()
        else:
            if self.__local_storage_type == 'spool':
                relative_path = ''
            else:
                relative_path = downloaded_file.getRelativePath(dir_only=True)
            base_storage_folder = self.__get_folder(FOLDER_TO_STORE).getPath()

        storage_base_path = os.path.join(base_storage_folder, relative_path)
        dst_filepath = os.path.join(storage_base_path, downloaded_file.localFilename)
        dst_tmp_filepath = dst_filepath + '.tmp'
        os.makedirs(storage_base_path, exist_ok=True)

        # Store donwloaded file in internal folder
        try:
            nfs_check(base_storage_folder)

            downloaded_file.localFile.move(dst_tmp_filepath)
            downloaded_file.localFile = downloaded_file.localFile.copy(dst_filepath)
        except Exception as e:
            self._log.debug(f'[{self.id}]: Error while storing file'
                            f" '{downloaded_file.localFile.getName()}' in internal folder ({str(e)}).")
            os.remove(dst_tmp_filepath)
            os.remove(dst_filepath)
            raise e

        created_files = []
        stored_files = []
        src_remove = False
        try:
            # Store in internal optional spool copy
            created_files = self.store_file_in_spool(downloaded_file.localFile)
            # post processing compression archive
            self.post_processing(downloaded_file)

            if self.__file_ignored is not True:
                # For the SAFE context, the copy in the different destinations will come when the directory is complete.
                if self.__misc_param_integrity_check != 'SAFE':
                    # Store in internal temporary destination file ou folder
                    if downloaded_file.isFolder:
                        stored_files = self.store_folder(downloaded_file.localFolder, relative_path)
                    else:
                        stored_files = self.store_file(downloaded_file.localFile, relative_path)

                # Remove source file for localmove provider
                if self.__remote_storage_provider_ref == 'localmove':
                    src_remove = True
                    created_files.extend(stored_files)
                    nfs_check(os.path.dirname(os.path.abspath(downloaded_file.remoteFilepath)))
                    os.remove(downloaded_file.remoteFilepath)

        except Exception as e:
            # In case of local move, if FileNotFoundError, do nothing,
            if src_remove and isinstance(e, FileNotFoundError):
                pass
            else:
                # Deleting already created files
                for filename in created_files:
                    if os.path.isfile(filename):
                        os.remove(filename)
                    elif os.path.islink(filename):
                        os.remove(filename)
                    else:
                        shutil.rmtree(filename)
                # Deleting the original file (or folder) and the temporary copy
                if downloaded_file.isFolder:
                    downloaded_file.localFolder.remove(recursively=True)
                    downloaded_file.isFolder = False
                else:
                    downloaded_file.localFile.remove()
                if os.path.exists(dst_filepath):
                    os.remove(dst_filepath)
                os.remove(dst_tmp_filepath)
                self._log.debug(f'[{self.id}]: Error while deploying '
                                f'{downloaded_file.localFile.getName()} file in internal folder')
                raise e

        # suppress initial internal file
        os.remove(dst_tmp_filepath)
        if self.__misc_param_integrity_check != 'SAFE':
            # For the SAFE context, the original files are kept as long as the directory is not complete
            if downloaded_file.isFolder:
                shutil.rmtree(downloaded_file.localFolder.getPath())
            else:
                os.remove(downloaded_file.localFile.getName())

        if self.__file_ignored:
            return False
        else:
            return True

    def store_final_downloaded_file(self,
                                    downloaded_file,
                                    integrity_check=None):

        if integrity_check == 'SAFE':
            relative_path_first_part, relative_path_last_part = \
                os.path.split(downloaded_file.getRelativePath(dir_only=True))
            if self.__local_storage_type == 'spool':
                relative_path = 'SAFE.' + os.path.split(relative_path_last_part)[1]
            else:
                relative_path = os.path.join(relative_path_first_part,
                                             'SAFE.' + relative_path_last_part)
        else:
            if self.__local_storage_type == 'spool':
                relative_path = ''
            else:
                relative_path = downloaded_file.getRelativePath(dir_only=True)

        self._log.debug(f"[{self.id}]: Store '{downloaded_file.localFile.getName()}' in final folder")

        # Store in final destination optional spool copy
        to_store_path = self.__get_folder(FOLDER_TO_STORE).getPath()
        for destination in self.__local_optional_storage_spool:
            src_folder = os.path.join(to_store_path, destination.lstrip('/'))
            local_file = File(os.path.join(src_folder,
                                           downloaded_file.localFilename))
            self.store_file(local_file, '', final_destination=destination)

        # Store in final destination file ou folder
        for destination in self.__local_storage_repository:
            src_folder = os.path.join(to_store_path,
                                      os.path.join(destination.lstrip('/'), relative_path))
            if downloaded_file.isFolder:
                local_folder = Folder(os.path.join(src_folder,
                                                   downloaded_file.localFolder.getName()))
                local_folder.recursive = False
                targets = None
                try:
                    local_folder.scan()
                except OSError:
                    self._log.warning(f'Path {local_folder.getPath()} is temporarily inaccessible !')
                    continue
                try:
                    for real_folder in local_folder.values(restr=Folder):
                        result = self.store_folder(real_folder, relative_path, final_destination=destination)
                        if result == []:
                            raise Exception
                        if targets is None:
                            targets = result
                    for real_file in local_folder.values(restr=File):
                        result = self.store_file(real_file, relative_path, final_destination=destination)
                        if result == []:
                            raise Exception
                        if targets is None:
                            targets = result
                except Exception:
                    continue
                else:
                    local_folder.remove(recursively=True)

            else:
                local_file = File(os.path.join(src_folder,
                                               downloaded_file.localFile.getName(short=True)))
                targets = self.store_file(local_file, relative_path, final_destination=destination)

            # store links only on the first destination
            if destination == self.__local_storage_repository[0]:
                # Store in final destination file ou folder link
                if targets is not None and len(targets) > 0:
                    for dest_link in self.__local_added_spool_links:
                        src_folder = os.path.join(to_store_path, dest_link.lstrip('/'))
                        target_name = os.path.split(targets[0])[1]
                        local_target = File(os.path.join(src_folder, target_name))

                        self.store_link(local_target, final_destination=dest_link)

    def store_link(self, filepath, final_destination=None):

        if final_destination is not None:
            filepath_name = filepath.getName(short=True)
            try:
                self._log.debug(f"[{self.id}]: Store link to '{filepath.getName()}'"
                                f" in destination '{final_destination}'")

                nfs_check(final_destination)

                link_name = os.path.join(final_destination, filepath_name)

                if os.path.isfile(link_name) or os.path.islink(link_name):
                    os.remove(link_name)
                shutil.copy(filepath.getName(), link_name, follow_symlinks=False)
            except Exception as e:
                self._log.debug(f"[{self.id}]: Error store link '{filepath_name}'"
                                f" in folder '{final_destination}' ({str(e)}).")
                raise e
            else:
                os.remove(filepath.getName())
                return [link_name]
        else:

            created_links = []
            to_store_path = self.__get_folder(FOLDER_TO_STORE).getPath()
            file_path = filepath.path
            for destination in self.__local_added_spool_links:
                dst_path = os.path.join(to_store_path, destination.lstrip('/'))
                link_name = os.path.join(dst_path, os.path.split(file_path)[1])
                try:
                    self._log.debug(
                        f"[{self.id}] Store internal link to '{file_path}' into '{dst_path}'.")

                    nfs_check(to_store_path)

                    if not os.path.exists(dst_path):
                        os.makedirs(dst_path)

                    if os.path.isfile(link_name) or os.path.islink(link_name):
                        os.remove(link_name)
                    os.symlink(file_path, link_name)

                    created_links.append(link_name)

                except Exception as e:
                    for link in created_links:
                        os.remove(link)
                    self._log.warning(f"[{self.id}]: Error while storing link to '{file_path}'"
                                      f" into '{dst_path}' ({str(e)}). It will be retried later")
                    raise e

    # 13/02/2018 PMT#36 : Enregistrement d'un répertoire (tar ou SAFE)
    def store_folder(self, folder, relative_path, final_destination=None):

        if final_destination is not None:
            dst_path = os.path.join(final_destination, relative_path)
            folder_name = folder.getName()
            if folder_name.startswith('SAFE.'):
                folder_name = folder_name[len('SAFE.'):]

            dst_filepath = os.path.join(dst_path, folder_name)
            tmp_dst_filepath = dst_filepath + '.tmp'
            try:
                nfs_check(final_destination)

                if not os.path.exists(dst_path):
                    os.makedirs(dst_path)

                # First, copy the file temporarily to the destination
                self._log.debug(f"[{self.id}]: storing folder '{folder.getPath()}' into '{dst_filepath}'")
                shutil.rmtree(tmp_dst_filepath, ignore_errors=True)
                shutil.copytree(folder.getPath(), tmp_dst_filepath)

                # modify rights access and group id if necesary
                os.chmod(tmp_dst_filepath, DIRECTORY_ACCESS_RIGHT)
                if self.__groupid != -1:
                    os.chown(tmp_dst_filepath, -1, self.__groupid)
                for root, dirs, files in os.walk(tmp_dst_filepath):
                    for d in dirs:
                        os.chmod(os.path.join(root, d), DIRECTORY_ACCESS_RIGHT)
                        if self.__groupid != -1:
                            os.chown(os.path.join(root, d), -1, self.__groupid)
                    for f in files:
                        os.chmod(os.path.join(root, f), FILE_ACCESS_RIGHT)
                        if self.__groupid != -1:
                            os.chown(os.path.join(root, f), -1, self.__groupid)

            except Exception as e:
                if os.path.exists(tmp_dst_filepath):
                    shutil.rmtree(tmp_dst_filepath, ignore_errors=True)
                self._log.warning(f"[{self.id}]: Error copy folder tree '{folder.getPath()}'"
                                  f" in '{os.path.split(dst_filepath)[0]}' ({str(e)}).")
                return []

            links_created = []
            try:
                # If there are optional link spool and the destination is the first in the list
                if final_destination == self.__local_storage_repository[0]:
                    links_created = self.store_link(File(dst_filepath))

                # finaly move directory in dst
                shutil.rmtree(dst_filepath, ignore_errors=True)
                shutil.move(tmp_dst_filepath, dst_filepath)

            except Exception as e:
                if os.path.exists(tmp_dst_filepath):
                    shutil.rmtree(tmp_dst_filepath, ignore_errors=True)
                for link in links_created:
                    os.remove(link)
                self._log.warning(f"[{self.id}]: Error move final tree '{folder.getPath()}'"
                                  f" in '{os.path.split(dst_filepath)[0]}' ({str(e)})")
                return []
            else:
                folder.remove(recursively=True)
                return [dst_filepath]

        else:
            created_folder = []
            to_store_path = self.__get_folder(FOLDER_TO_STORE).getPath()
            for destination in self.__local_storage_repository:
                base_path = os.path.join(to_store_path, destination.lstrip('/'))
                dst_path = os.path.join(base_path, relative_path)
                dst_filepath = os.path.join(dst_path, folder.getName())
                tmp_dst_filepath = dst_filepath + '.tmp'

                try:
                    nfs_check(to_store_path)

                    if not os.path.exists(dst_path):
                        try:
                            Folder(dst_path, create=True)
                        except Exception:
                            pass

                    # Copie d'abord dans le repertoire temporaire cible
                    self._log.debug(f"[{self.id}]: storing folder '{folder.getPath()}' into '{dst_filepath}'")
                    shutil.rmtree(tmp_dst_filepath, ignore_errors=True)
                    shutil.copytree(folder.getPath(), tmp_dst_filepath)

                    # deplacement du repertoire temporaire cible vers repertoire cible
                    shutil.rmtree(dst_filepath, ignore_errors=True)
                    shutil.move(tmp_dst_filepath, dst_filepath)

                    created_folder.append(dst_filepath)

                except Exception as e:
                    for afolder in created_folder:
                        shutil.rmtree(afolder, ignore_errors=True)
                    self._log.exception(f"[{self.id}]: Error copy tree '{folder.getPath()}'"
                                        f" in '{os.path.split(dst_filepath)[0]}' ({str(e)}).")
                    raise e

            return created_folder

    # #71
    def store_file_in_spool(self, source, safe_ok=False):

        created_files = []

        for destination in self.__local_optional_storage_spool:
            base_path = self.__get_folder(FOLDER_TO_STORE).getPath()
            if self.__misc_param_integrity_check == 'SAFE' and not safe_ok:
                dst_path = os.path.join(base_path, os.path.split(source.getDirPath())[1])
                src_name = source.getName(short=True)
            else:
                dst_path = os.path.join(base_path, destination.lstrip('/'))
                if self.__misc_param_integrity_check == 'SAFE':
                    src_name = source.getName()
                else:
                    src_name = source.getName(short=True)
            try:
                nfs_check(base_path)

                if not os.path.exists(dst_path):
                    try:
                        Folder(dst_path, create=True)
                    except Exception:
                        pass

                dst_filepath = os.path.join(dst_path, src_name)
                dst_tmp_filepath = dst_filepath + '.tmp'
                self._log.debug(f'[{self.id}]: storage of the file to be spooled '
                                f"'{src_name}' into '{dst_path}'")

                if self.__misc_param_integrity_check == 'SAFE' and safe_ok:
                    shutil.copytree(source.getPath(), dst_tmp_filepath)
                else:
                    source.copy(dst_tmp_filepath)
                shutil.move(dst_tmp_filepath, dst_filepath)

                created_files.append(dst_filepath)

                # Create a single copy (the others, if necessary, will come when the directory is complete)
                if self.__misc_param_integrity_check == 'SAFE' and not safe_ok:
                    break
            except Exception as e:
                for filemane in created_files:
                    os.remove(filemane)
                self._log.warning(f'[{self.id}]: Error while storing optional spool file '
                                  f"'{src_name}' ({str(e)}). It will be retried later")
                raise e

        return created_files

    def store_file(self, local_file, relative_path, final_destination=None):

        if final_destination is not None:
            dst_path = os.path.join(final_destination, relative_path)
            dst_filepath = os.path.join(dst_path, local_file.getName(short=True))
            src_filepath = local_file.getName()

            links_created = []
            try:
                nfs_check(final_destination)
                if not os.path.exists(dst_path):
                    try:
                        Folder(dst_path, create=True)
                    except Exception:
                        pass

                # First, copy the file temporarily to the destination
                self._log.debug(f"[{self.id}]: storing file '{src_filepath}' into '{dst_path}'")
                tmp_dst_filepath = dst_filepath + '.tmp'
                shutil.copy(src_filepath, tmp_dst_filepath, follow_symlinks=False)
                # modify rights access and group id if necesary
                if local_file.isLink() is False:
                    os.chmod(tmp_dst_filepath, FILE_ACCESS_RIGHT)
                    if self.__groupid != -1:
                        os.chown(tmp_dst_filepath, -1, self.__groupid)
                # os.chmod(dst_path, DIRECTORY_ACCESS_RIGHT)
                if self.__groupid != -1:
                    os.chown(tmp_dst_filepath, -1, self.__groupid)

                # If there are optional link spool and the destination is the first in the list
                if final_destination == self.__local_storage_repository[0]:
                    links_created = self.store_link(File(dst_filepath))
                # Final move
                shutil.move(tmp_dst_filepath, dst_filepath)
            except Exception as e:
                for link in links_created:
                    os.remove(link)
                self._log.warning(f"[{self.id}]: Error while storing file '{local_file.getName()}'"
                                  f" into '{dst_filepath}' ({str(e)}). It will be retried later")
                return []
            else:
                os.remove(src_filepath)
                return [dst_filepath]

        else:
            created_files = []
            if self.__misc_param_integrity_check == 'SAFE':
                to_store_path = self.__get_folder(FOLDER_WAITING_TO_COMPLETE).getPath()
            else:
                to_store_path = self.__get_folder(FOLDER_TO_STORE).getPath()

            for destination in self.__local_storage_repository:
                base_path = os.path.join(to_store_path, destination.lstrip('/'))
                dst_path = os.path.join(base_path, relative_path)
                dst_filepath = os.path.join(dst_path, local_file.getName(short=True))
                dst_tmp_filepath = dst_filepath + '.tmp'

                try:
                    self._log.debug(
                        f"[{self.id}] storeFile : storing file '{local_file.getName()}' into '{dst_path}'.")

                    nfs_check(to_store_path)

                    if not os.path.exists(dst_path):
                        try:
                            Folder(dst_path, create=True)
                        except Exception:
                            pass

                    new_file = local_file.copy(dst_tmp_filepath)
                    new_file.move(dst_filepath)
                    created_files.append(dst_filepath)
                except Exception as e:
                    for filemane in created_files:
                        os.remove(filemane)
                    self._log.warning(f"[{self.id}]: Error while storing file '{local_file.getName()}'"
                                      f" into '{dst_filepath}' ({str(e)}). It will be retried later")
                    raise e

            return created_files

    def post_processing(self, downloaded_file):

        for post_processing in self.__post_processing:
            if post_processing is not None:
                self.__file_ignored = post_processing.process(downloaded_file)



    def store_safe_directories(self):
        """ Pooling du dossier FOLDER_WAITING_TO_COMPLETE (safe storage folder), pour deplacer des que possible
            les dossiers qui sont complets (SAFE) dans le storage folder """

        waiting_folders = self.__get_folder(FOLDER_WAITING_TO_COMPLETE)
        try:
            waiting_folders.scan()
        except OSError:
            self._log.warning(f'Path {waiting_folders.getPath()} is temporarily inaccessible !')
        else:
            files = waiting_folders.values(restr=File)

            if len(files) > 0:
                self._log.info(
                    'Checking the consistency of the waiting secure folders and move them to store directory :')
                folders = sorted(waiting_folders.values(restr=Folder), key=lambda x: x.getPath())
                for realfolder in folders:
                    if realfolder.getName()[:len('SAFE.')] != 'SAFE.':
                        continue  # dossier intermédiaire -> seuls les dossiers finaux sont analysés
                    logical_foldername = realfolder.getName()[len('SAFE.'):]

                    try:
                        # recherche dans la database
                        self.__database.getSession()
                        filesinfolder = self.__database.getFilesInFolder(
                            logical_foldername)
                        if filesinfolder is None:
                            filesinfolder = []
                        self.__database.closeSession()
                    except Exception:
                        self._log.debug('database not ready')
                        self._log.warning(
                            '  >> %s : database not ready - it will be retried later' %
                            logical_foldername)
                        continue  # On fera de transfert la prochaine fois

                    # on compte le nombre de fichiers déjà téléchargés et en
                    # erreur
                    nbfiledownloaded = len(filesinfolder)
                    filedownloadederror = 0
                    for fileinfolder in filesinfolder:
                        if fileinfolder.state == STATE_DOWNLOAD_SUCCESS:
                            nbfiledownloaded -= 1
                        elif fileinfolder.state == STATE_DOWNLOAD_ERROR:
                            filedownloadederror += 1

                    # ily a des fichiers en erreur de téléchargement ?
                    if filedownloadederror > 0:
                        self._log.debug(f'download of safed folder '
                                        f'{logical_foldername} is on error')
                        self._log.error(f'  >> {logical_foldername} : number of files in error : '
                                        f'{filedownloadederror}')
                        continue

                    # reste-il des fichiers non télécheargés
                    if nbfiledownloaded > 0:
                        self._log.debug(f'[{self.id}]: download of safed folder '
                                        f'{logical_foldername} is not completed, '
                                        f'number of missing files : '
                                        f'{nbfiledownloaded}/{len(filesinfolder)}')
                        self._log.info(f'  >> {logical_foldername} : not completed')
                        continue

                    self._log.debug(f'[{self.id}]: verify files in folder')
                    try:
                        realfolder.scan()
                    except OSError:
                        self._log.warning(f'[{self.id}]: Path {realfolder.getPath()} '
                                          f'is temporarily inaccessible !')
                        continue
                    # tous les fichiers téléchargés sont-ils présents dans le
                    # répertoire SAFE
                    if len(realfolder.values(restr=File)) != len(filesinfolder):
                        self._log.debug(f'  >> {logical_foldername} : not consistent '
                                        f'{len(realfolder.values(restr=File))} files found '
                                        f'for {len(filesinfolder)} files expected')
                        self._log.error(f'  >> {logical_foldername} : not consistent')
                        continue

                    # SAFE specific checksum
                    if self.__checksum == 'safe':
                        self._log.debug(f'[{self.id}]: Checksum activated. '
                                        f'Checking all files in the SAFE folder')
                        # Verify checksum of all files in the folder
                        safe_checksum_file = os.path.join(
                            realfolder.getPath(), 'xfdumanifest.xml')
                        self._log.debug(f'[{self.id}]: File checksum : {safe_checksum_file}')
                        if os.path.isfile(safe_checksum_file):
                            # read md5 digest for all files
                            checksumfiles = self.__get_safe_checksum(
                                safe_checksum_file)

                            for aFile in realfolder.values(restr=File):
                                checksum_ok = self.__verify_safe_checksum(
                                    aFile, checksumfiles)
                                if not checksum_ok:
                                    self._log.error(f'  >> Error : {aFile.getName(short=True)} not consistent')
                                    continue
                        else:
                            self._log.warning(f'  >> the checksum file ({safe_checksum_file}) '
                                              f'does not exist. Checksum checks will not be done !')

                    # The folder is valid,
                    # copy of the folder to the TO_STORE directory according to the destinations
                    storage_relative_path = realfolder.getPath()[len(waiting_folders.getPath()):]
                    relative_path = os.path.split(storage_relative_path)[0].lstrip('/')
                    created_folder = []
                    try:
                        self._log.debug(f'[{self.id}]:     ==> to spool folder')
                        created_folder = self.store_file_in_spool(realfolder, safe_ok=True)
                        # self.state = (STATE_RUNNING, STATE_RUNNING_STORING%(downloaded_file.remoteFilename)
                        self._log.debug(f'[{self.id}]:     ==> folder to {relative_path}')
                        self.store_folder(realfolder, relative_path)
                    except Exception as e:
                        for folder in created_folder:
                            shutil.rmtree(folder, ignore_errors=True)
                        self._log.info(f'[{self.id}]: storeSafeDirectories {logical_foldername} '
                                       f'-> KO : {str(e)}')
                        self._log.warning(f'[{self.id}]:  >> {logical_foldername} : '
                                          f'error when storing - it will be retried later')
                    else:
                        realfolder.remove(recursively=True)
                        self._log.info(f'  >> {logical_foldername} : ready to store')
                        self._log.debug(f'[%{self.id}] Internal storage success for SAFE folder '
                                        f"{realfolder.getName()}' (stored in "
                                        f'{created_folder})')

                self._log.info('  >> Checking done')

    def storage_pending_downloaded_files(self):
        """ Pooling du dossier FOLDER_TO_STORE (internal storage folder), pour deplacer des que possible
            les fichiers qui ont termine en erreur lors du deplacement dans le final storage folder """

        to_store_folder = self.__get_folder(FOLDER_TO_STORE).getPath()
        files_destinations = self.__local_storage_repository.copy()
        files_destinations.extend(self.__local_optional_storage_spool)

        message_to_store = ' Remaining files in folder "to_store", try to store them : '

        for destination in files_destinations:

            src_folder = os.path.join(to_store_folder, destination.lstrip('/'))
            if not os.path.exists(src_folder):
                continue

            # store folders
            store_folder = Folder(src_folder, recursive=True)
            try:
                store_folder.scan()
            except OSError:
                self._log.warning(f'[{self.id}]: Path {store_folder.getPath()} is temporarily inaccessible !')
            else:
                folders = sorted(store_folder.values(restr=Folder), key=lambda x: x.getPath())
                for folder in folders:
                    if folder.getName().startswith('SAFE.'):
                        targets = []
                        relative_folder = folder.getPath()[len(src_folder):]
                        relative_path = os.path.split(relative_folder)[0].lstrip('/')
                        folder.setRecursive(False)
                        try:
                            # scan for folders
                            folder.scan()
                            for real_folder in folder.values(restr=Folder):
                                result = self.store_folder(real_folder, relative_path, final_destination=destination)
                                if len(result) == 0:
                                    raise DownloaderException
                                targets.extend(result)
                            # scan for files
                            folder.scan()
                            for real_file in folder.values(restr=File):
                                result = self.store_file(real_file, relative_path, final_destination=destination)
                                if len(result) == 0:
                                    raise DownloaderException
                                targets.extend(result)
                        except Exception:
                            # TODO: deleting already transferred files and folders,
                            #        ==>    and restoring them to "to_store"
                            self._log.warning(f'[{self.id}]:  >> folder {folder.getName()[len("SAFE."):]} : '
                                              f'error when storing - it will be retried later')
                            continue

                        self._log.debug(f'[%{self.id}] Final storage success for folder '
                                        f"{folder.getName()}' (stored in {destination})")
                        folder.remove(recursively=True)

            # Store files
            try:
                store_folder.scan()
            except OSError:
                self._log.warning(f'[{self.id}]: Path {store_folder.getPath()} is temporarily inaccessible !')
            else:
                files = sorted(store_folder.values(restr=File), key=lambda x: x.getName())
                realfiles = []
                for file in files:
                    # exclude all files in SAFE folders
                    if file.getDirPath().find('/SAFE.') == -1:
                        # if not file.isLink():
                        realfiles.append(file)

                if len(realfiles) > 0:
                    if message_to_store:
                        self._log.info(f'[{self.id}]:{message_to_store}')
                        message_to_store = None
                    self._log.debug(f'[{self.id}]: files left in '
                                    f"'{store_folder.getPath()}' to store = {files}")

                    for file in realfiles:
                        self._log.info('  -> %s' % (file.getName()))
                        file_name = file.getName()
                        try:
                            final_storage_relative_path = file.getDirPath()[
                                                          len(store_folder.getPath()):]
                            final_storage_relative_path = final_storage_relative_path.lstrip('/')
                            dst_folderame = os.path.join(destination, final_storage_relative_path)

                            self._log.debug(f'[{self.id}]: store remaining files : {file_name}'
                                            f' -> {dst_folderame}')

                            self.store_file(file,
                                            final_storage_relative_path,
                                            final_destination=destination)

                        except Exception:
                            self._log.warning(f'[{self.id}]:  -> file= {file_name} : '
                                              f'error when storing - it will be retried later')
                        else:
                            self._log.debug(f'[{self.id}] storeRemainingFiles OK : '
                                            f'{file_name} -> {dst_folderame}')

        for final_destination in self.__local_added_spool_links:
            # store link
            src_folder = os.path.join(to_store_folder, final_destination.lstrip('/'))
            if not os.path.exists(src_folder):
                continue
            store_folder = Folder(src_folder, recursive=False)

            try:
                store_folder.scan()
            except OSError:
                self._log.warning(f'[{self.id}]: Path {store_folder.getPath()} is temporarily inaccessible !')
            else:
                files = sorted(store_folder.values(restr=File), key=lambda x: x.getName())
                folders = sorted(store_folder.values(restr=Folder), key=lambda x: x.getPath())
                reallinks = []
                for file in files:
                    if file.isLink():
                        reallinks.append(file)
                for folder in folders:
                    if os.path.islink(folder.path):
                        reallinks.append(File(folder.getPath()))

                if len(reallinks) > 0:
                    self._log.debug(f'[{self.id}]: links left in '
                                    f"'{store_folder.getPath()}' to store = {reallinks}")

                    for link in reallinks:
                        self._log.info('  -> %s' % (link.getName()))
                        try:
                            final_storage_filename = link.getName(short=True)
                            file_name = link.getName()
                            dst_filename = os.path.join(final_destination,
                                                        final_storage_filename)
                            self._log.debug(f'[{self.id}]: storeRemainingFiles : {file_name} -> {dst_filename}')

                            self.store_link(link, final_destination=final_destination)
                        except Exception:
                            self._log.warning(f'[{self.id}]: Error in Download::storeRemaingFiles '
                                              f"(file={link.getName()}). Don't worry, it will be retried")
                        else:
                            self._log.info(f'[{self.id}] storeRemainingFiles OK : '
                                           f'{file_name} -> {dst_filename}')

    def validate_file_download(
            self,
            source_file,
            msg_level,
            downloaded_file=None,
            details=None):
        filepath = source_file.filepath

        self.state = (STATE_RUNNING, STATE_RUNNING_VALIDATING % filepath)

        # Notification a l'operateur du status de download du fichier
        filename = os.path.basename(filepath)
        m = messages.MessageFactory().create({
            'type': 'downloadMessage',
            'details': 'File path : %s . ' % filepath,
            'reference_type': 'data',
            'reference_name': filename,
            'processing_start_date': self.__emm_start_date,
            'processing_hostname': gethostname(),
            'processing_pid': os.getpid(),
            'download_name': self.id,
            'download_provider': self.__remote_storage_provider_ref,
        }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)

        # recuperation des informations concernant le fichier downloaded
        if downloaded_file is not None:

            if downloaded_file.localFile.exist():
                mtime = time.strftime(DATE_FORMAT, time.gmtime(downloaded_file.localFile.getTime()))
                size = str(downloaded_file.localFile.getSize())
            else:
                mtime = source_file.mtime.strftime(DATE_FORMAT)
                size = str(source_file.size)

            msg_downloaded_file = messages.File(
                name=downloaded_file.localFile.getName(short=True),
                path=downloaded_file.localFile.getDirPath(),
                misc_infos=messages.MiscInfos({
                    'size': size,
                    'mtime': mtime,
                    'remote_path': downloaded_file.remoteFilepath
                })
            )
            if downloaded_file.download_time is not None:
                msg_downloaded_file.misc_infos.pairs['download_time'] = str(
                    downloaded_file.download_time)
            try:
                downloaded_file.getDate()
            except Exception as e:
                self._log.error("getDate failed for downloaded file '%s' : %s"
                                % downloaded_file.remoteFilepath, str(e))
                # pas grave... on continue
            else:
                if downloaded_file.data_for_day is not None:
                    msg_downloaded_file.misc_infos.pairs['data_for_day'] \
                        = downloaded_file.data_for_day.strftime(DATA_FOR_DAY_FORMAT)

            m.download.download_files = messages.DownloadFileList(
                [msg_downloaded_file])

        nochecksum = '' if (downloaded_file is None or (
                downloaded_file is not None and not downloaded_file.noChecksumFile)) else 'No checksum'

        if msg_level == 'INFO':
            dl_state = STATE_DOWNLOAD_SUCCESS
        elif msg_level == 'WARNING':
            dl_state = STATE_DOWNLOAD_SUCCESS
        else:
            dl_state = STATE_DOWNLOAD_ERROR

        self._log.debug('updateStateDownload of %s state IN : %d' % (filepath, dl_state))

        db_state = self.update_state_download(filepath, dl_state, dl_sensingtime=source_file.sensingtime)
        if db_state is None:
            self._log.debug('updateStateDownload of %s state None ?????????' % filepath)
            msg_level = 'WARNING'
        else:
            self._log.debug('updateStateDownload of %s state OUT: %d' % (filepath, db_state))

        if msg_level == 'INFO':
            m.level = messages.INFO
            m.summary = '[%s] Download success for %s %s' % (
                self.id, filename, nochecksum)
            self._log.info(
                "OPERATOR : [%s] Download success for file '%s' %s" %
                (self.id, filepath, nochecksum))
            self.update_state_test([STATE_TEST_SUCCESS, ''])

        elif msg_level == 'WARNING':
            m.level = messages.WARNING
            m.summary = '[%s] Download OK but final storage not validated for %s %s' % (
                self.id, filename, nochecksum)
            self._log.warning(
                "OPERATOR : [%s] Download OK but final storage not validated for %s %s'" %
                (self.id, filepath, nochecksum))
            self.update_state_test([STATE_TEST_WARNING, ''])
        else:
            if db_state == STATE_DOWNLOAD_ERROR:
                m.level = messages.ERROR
                self._log.error(
                    "OPERATOR : [%s] Download FAILED for file '%s' [details=%s]" %
                    (self.id, filepath, str(details)))
            else:
                m.level = messages.WARNING
                self._log.warning(
                    "OPERATOR : [%s] Download FAILED for file '%s' [details=%s]" %
                    (self.id, filepath, str(details)))
            m.summary = '[%s] Download failed for %s' % (self.id, filename)
            self.update_state_test([STATE_TEST_FAILED, details])

        if details is not None:
            m.details += details

        try:
            self.__emm_writer.writeMessage(m, self.__id)
        except Exception as e:
            self._log.exception('Message write error : %s' % (str(e)))

        # Adding a new record to the json file
        if self.__monitoring \
                and msg_level in ['INFO', 'WARNING'] \
                and not self.test:
            self.__write_json_file(source_file, downloaded_file)

    def __write_json_file(self, source_file, downloaded_file):
        mtime = source_file.mtime if downloaded_file.mtime is None else downloaded_file.mtime
        size = source_file.size if downloaded_file.filesize is None else downloaded_file.filesize
        now_date = datetime.datetime.utcnow()
        delta_time_pld = mtime - source_file.sensingtime
        delta_time_lud = now_date - mtime
        metrics = {
            'filename': os.path.split(
                source_file.filepath.split(FILE_URL_SEPARATOR)[0])[1],
            'download': self.id,
            'download_time': now_date.strftime(JSON_DATE_FORMAT),
            'sensing_time': source_file.sensingtime.strftime(JSON_DATE_FORMAT),
            'modification_time': mtime.strftime(JSON_DATE_FORMAT),
            'production_latency_delay': delta_time_pld.days * 86400 + delta_time_pld.seconds,
            'local_update_delay': delta_time_lud.days * 86400 + delta_time_lud.seconds,
            'file_volume': size,
        }
        tooked_semaphore = False
        json_file = None
        if self.__metricsFilesystem:
            try:
                metrics_text = json.dumps(metrics) + '\n'
                self._log.debug(f'[json]: {metrics_text}')
                self.__semaState.acquire()
                tooked_semaphore = True
                with open(self.__jsonFileName, 'a') as json_file:
                    json_file.write(metrics_text)
                json_file = None
                self.__semaState.release()
            except Exception as e:
                if tooked_semaphore:
                    self.__semaState.release()
                    if json_file is not None:
                        json_file.close()
                self._log.exception('Write json file : %s' % str(e))
        self.__metricsRabbitMQ.publish(self.__id, metrics, 'metrics')

    def update_state_test(self, state):
        if self.test:
            self.__set_job_test_state(state)

    def update_state_download(self, filepath, dl_state, dl_sensingtime=None):
        # Enregistrement en base du status
        db_state = None
        self.__semaState.acquire()
        try:
            # clear_mappers()
            self.__database.getSession()

            db_state = self.__database.setDownload(
                filepath=filepath,
                state=dl_state,
                sensingtime=dl_sensingtime,
            )

            self.__database.closeSession()
        except Exception:
            # on intercepte une erreur ici, parce que si le validate ne se fait pas, ca n'est pas dramatique
            # on envoie un warning a l'operateur en plus du message normal
            msg = '[%s] Registering download status failed' % self.id
            self._log.warning('OPERATOR : ' + msg +
                              ' for file %s' % filepath)
            m = messages.MessageFactory().create({
                'type': 'downloadMessage',
                'level': 'WARNING',
                'summary': msg + ' for file %s' % (os.path.basename(filepath)),
                'details': "Filepath : '%s', State : %s" % (filepath, str(dl_state)),
                'reference_type': 'data',
                'reference_name': os.path.basename(filepath),
                'processing_start_date': self.__emm_start_date,
                'processing_hostname': gethostname(),
                'processing_pid': os.getpid(),
                'download_name': self.id,
                'download_provider': self.__remote_storage_provider_ref,
            }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)
            try:
                self.__emm_writer.writeMessage(m, self.__id)
            except Exception as e:
                self._log.exception('Message write error : %s' % (str(e)))
        self.__semaState.release()

        return db_state

    def run_download(self, download_loop_number, get_remote_listing):
        """ runDownload is the main thread activity. It does :
            - getListing() : retrieve listings
            - storeListing() : validate the listing, and stores its content in the database
            - getFilesToDownload() : retrieve files to download _now_ (care about ignition date)
            - downloadFile() : do the download
            - storeDownloadedFile() : internal storage (disk space should be local, available)
            - storeDownloadedFile() : final storage (disk space may be unavailable...)
            - validateFileDownload() : update database to known download file state (ok or error)
        """
        if self.test:
            self.__logFile.open('w')
            self.__logFile.close()
            self.config_summary(DOWNLOAD_LOGGER_LEVEL)
        self._log.info("Executing download '%s' (loop number = %s)" %
                       (self.id, download_loop_number))

        # TODO (maybe) : Supprimer les fichiers téléchargés qui terminent en
        # erreur !

        # 1. Detection d'une nouvelle configuration
        # self.isNewConfiguration()

        # 2. State : Scanning
        # Recuperation de la liste des fichiers a telecharger dans un fichier listing
        # Dans le cas des listings auto (c'est le cas ici, si get_remote_listing==True), une validation basique
        # du nombre de lignes de chaque listing est faite ici (contrairement
        # aux listings manuels)
        self.state = (STATE_RUNNING, STATE_RUNNING_SCANNING)
        valid_listings = self.get_listing(get_remote_listing=get_remote_listing)

        # Managing listing files : validation and put in listing history folder
        for listing_path in valid_listings:
            self._log.debug(" Valid Listing : '%s' (loop number = %s)" % (
                listing_path, download_loop_number))
            listing_file = File(listing_path)
            is_sync = (listing_path[-5:] == '.sync')
            # TODO : storeListing devrait stocker dans un nom de fichier,
            # dont le path est indique en cas d'erreur !
            # (manual ou auto) et des patterns des fichiers
            try:
                self._log.info("Store listing '%s' (loop number = %s)" %
                               (self.id, download_loop_number))
                nbr_files_to_download = self.store_listing(listing_path, is_sync)
            except Exception as e:
                # Un probleme a eu lieu au storage, ou le listing n'est pas acceptable. On lock
                # le listing, et on envoie un message a l'operateur avec le message
                # d'erreur. Fin du traitement pour ce listing, on continue avec
                # le suivant
                m = messages.MessageFactory().create({
                    'type': 'downloadMessage',
                    'level': 'ERROR',
                    'summary': '[%s] Listing validation error : %s' % (self.id, listing_path),
                    'details': 'Error : %s' % (str(e)),
                    'reference_type': 'listing',
                    'reference_name': os.path.basename(listing_path),
                    'processing_start_date': self.__emm_start_date,
                    'processing_hostname': gethostname(),
                    'processing_pid': os.getpid(),
                    'download_name': self.id,
                    'download_provider': self.__remote_storage_provider_ref,
                }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)
                try:
                    self.__emm_writer.writeMessage(m, self.__id)
                except Exception as exc:
                    self._log.exception('Message write error : %s' % (str(exc)))

                listing_file.acquireLock('eo-dataflow-manager::storeListing: exception')
                self.update_state_test([STATE_TEST_FAILED, ''])
                msg = '[%s] Error in storeListing : %s. Listing has been locked' % (self.__id, e)
                self._log.exception(msg)
                continue

            m = messages.MessageFactory().create({
                'type': 'downloadMessage',
                'level': 'INFO',
                'summary': '[%s] Listing accepted and stored : %s' % (self.id, os.path.basename(listing_path)),
                'details': 'Listing contains %s files to download. Listing path : %s'
                           % (str(nbr_files_to_download), listing_path),
                'reference_type': 'listing',
                'reference_name': os.path.basename(listing_path),
                'processing_start_date': self.__emm_start_date,
                'processing_hostname': gethostname(),
                'processing_pid': os.getpid(),
                'download_name': self.id,
                'download_provider': self.__remote_storage_provider_ref,
            }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)
            try:
                self.__emm_writer.writeMessage(m, self.__id)
            except Exception as e:
                self._log.exception('Message write error : %s' % (str(e)))

            # on archive le listing sauf en mode test
            if not self.test:
                listing_dst_path = os.path.join(
                    self.__get_folder(FOLDER_LISTINGS_HISTORY).getPath(),
                    listing_file.getName(
                        short=True))
                if os.path.exists(listing_dst_path):
                    # si le meme nom de fichier existe deja, on utilise un
                    # numero d'increment
                    i = 0
                    exist_in_history = True
                    while exist_in_history:
                        i += 1
                        listing_dst_path = os.path.join(
                            self.__get_folder(FOLDER_LISTINGS_HISTORY).getPath(),
                            '%s_' % i + listing_file.getName(short=True))
                        exist_in_history = os.path.exists(listing_dst_path)
                self._log.debug(" Listing archived: '%s' " % listing_dst_path)
                listing_file.move(listing_dst_path)

        # In all cases, get the list of files to download
        files_accepted = self.get_files_to_download()
        self.__nbFileToDownload = len(files_accepted)
        if self.__nbFileToDownload > 0:
            # If the generation of the files is requested
            if self.__monitoringFilePath is not None:
                # One file per hour
                filename = datetime.datetime.utcnow().strftime('%Y%m%d%H') + '_' \
                           + os.path.split(self.__downloadPath)[1] + '.json'
                self.__jsonFileName = os.path.join(self.__monitoringFilePath, filename)
                self._log.debug('JSON file name : %s' % self.__jsonFileName)

            # do the download for each accepted file
            limit = min(self.__misc_param_max_activated_files_by_loop,
                        self.__nbFileToDownload)
            self._log.debug('nb files to download in this loop : %d' % limit)

            if not self.__misc_param_parallel or not self.__misc_param_nb_parallel > 0:
                self._log.info(
                    "Download and validate file '%s' (loop number = %s)" %
                    (self.id, download_loop_number))
                for filepath in files_accepted[0:limit]:
                    if self.__misc_param_wait_between_downloads > 0:
                        sleep_time_wbd = self.__misc_param_wait_between_downloads
                        self.state = (
                            STATE_RUNNING,
                            STATE_RUNNING_WAIT_BETWEEN_DOWNLOADS %
                            sleep_time_wbd)
                        self._stopevent.wait(sleep_time_wbd)

                    if not self.download_and_validate_file(filepath):
                        # self.isNewConfiguration()
                        break
                    # self.isNewConfiguration()
            else:
                if self.__misc_param_nb_parallel > 0:
                    nb_filedownloaded = 0
                    while len(files_accepted[nb_filedownloaded:]) > 0 and (
                            self.__misc_param_max_activated_files_by_loop - nb_filedownloaded) > 0:
                        limit = min(self.__misc_param_nb_parallel,
                                    len(files_accepted[nb_filedownloaded:]),
                                    self.__misc_param_max_activated_files_by_loop - nb_filedownloaded)

                        self.parallel(files_accepted[nb_filedownloaded:], limit)

                        nb_filedownloaded += limit
                else:
                    self.parallel(files_accepted, limit)

        else:
            self.update_state_test([STATE_TEST_NO_FILE_TO_DOWNLOAD, ''])

        self.store_safe_directories()

        self._log.info("End of download '%s' (loop number = %s, CPU time = %f)",
                       self.id, download_loop_number, time.thread_time())

    def parallel(self, files_accepted, limit):
        queue = Queue()
        self.__close_session(self.__session)
        self._log.debug('Parralel IN Release  ')
        self.__sema.release()
        downloads = [threading.Thread(target=self.download_and_validate_file_parallele, args=(
            queue, download_file)) for download_file in files_accepted[0:limit]]

        for download in downloads:
            download.start()
        for download in downloads:
            download.join()
        while not queue.empty():
            queue.get()

        self._log.debug('Parralel OUT Acquire ')
        self.__sema.acquire()
        self._log.debug('Parralel OUT End Acquire ')
        self.__get_session()
        # self.isNewConfiguration()

    def download_and_validate_file_parallele(self, queue, download_file):
        self._log.debug("downloadAndValidateFileParallele '%s' " %
                        download_file.filepath)
        queue.put(self.download_and_validate_file(download_file, True))

    def download_and_validate_file(self, download_file, is_parralel=False):
        # Wait between downloads time
        # time.sleep(sleep_time_wbd) equivalent, but waiting for thread
        # termination event
        session = None
        filepath = download_file.filepath

        if self._stopevent.is_set():  # si l'arret du thread a ete demande, on ne passe pas au fichier suivant
            return False

        # Return immediately if just notify
        if self.remoteStorageProviderType == 'Onlynotify':
            msg = "[%s] New file available '%s' " % (self.id, filepath)
            self._log.info('OPERATOR : ' + msg)
            m = messages.MessageFactory().create({
                'type': 'downloadMessage',
                'level': 'INFO',
                'summary': '[%s] New file available: %s' % (self.id, os.path.basename(filepath)),
                'details': '%s' % msg,
                'reference_type': 'data',
                'reference_name': os.path.basename(filepath),
                'processing_start_date': self.__emm_start_date,
                'processing_hostname': gethostname(),
                'processing_pid': os.getpid(),
                'download_name': self.id,
                'download_provider': self.__remote_storage_provider_ref,
            }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)
            try:
                self.__emm_writer.writeMessage(m, self.__id)
            except Exception as e:
                self._log.exception('Message write error : %s' % (str(e)))
            self.update_state_download(filepath, STATE_DOWNLOAD_SUCCESS)
            return

        msg_level = 'ERROR'  # level par defaut, si le telechargement ou le stockage interne echoue
        details = None

        if is_parralel:
            self._log.debug(' Parralel Acquire %s' % self.__sema)
            self.__sema.acquire()
        try:
            if is_parralel:
                self._log.debug(' Parralel End Acquire %s ' % self.__sema)
                session = self.__provider.getSession(timeout_=SESSION_TIMEOUT_SEC)
                self._log.debug("_____________session '%s' %s" %
                                (self.__session, filepath))
            else:
                session = self.__session

            try:
                # creation du downloaded file, auquel on va ajouter des infos au fur et
                # a mesure des actions a venir
                downloaded_file = DownloadedFile(
                    remote_filepath=filepath, data_reader=self.__data_reader,
                    rooth_path=self.__configuration.source.rootPath)
                self._log.debug('APPEL self.downloadFile(%s, session=%s)  ' % (
                    filepath, str(self.__session)))
                self.download_file(downloaded_file, session)
            except Exception as e:
                # Remarque : on pourrait nettoyer les fichiers qui plante,
                # mais ce n'est pas dramatique, le cleanWorkspace() fera ca
                # au prochain demarrage (des qu'il y a un fichier .tmp dans l'arbo
                # du data/temporary

                self._log.error("[%s] Download failed for file '%s': %s)" % (
                    self.id, filepath, str(e)))
                # pas la peine de mettre le filepath ici, c'est deja dans le
                # message operateur.
                details = 'Download failed. Error : %s' % (str(e))
                downloaded_file = None
                self.update_state_test([STATE_TEST_FAILED, ''])
            else:
                # si on est ici, downloaded_file n'est pas None (garanti par le
                # self.downloadFile)
                self._log.info("[%s] download success for file '%s'" %
                               (self.id, filepath))
                # si le telechargement est reussi, on depose le fichier dans le dossier
                # FOLDER_TO_STORE du workspace (internal folder). C'est une etape intermediaire, qui permet de
                # stocker temporairement les fichiers en cas d'indisponibilite du storage final.
                # Les fichiers sont immediatements deplaces dans le storage final, sauf en cas d'echec. Dans ce cas,
                # les fichiers restants dans l'internal folder seront deplaces dans le storage final des que possible
                # (cf. storagePendingDownloadedFiles())
                # La validation est effective lorsque le fichier a ete depose dans l'internal storage folder
                # (on considere que la validation s'arrete lorsque le telechargement s'est bien deroule). Un
                # fichier peut donc avoir sa validation, sans forcement etre depose
                # dans le storage folder
                try:
                    downloaded_file.localFilename = downloaded_file.localFile.getName(short=True)
                    # NetCDF Datareader Case: Searching for Sensingtime in Global NetCDF Attributes
                    if download_file.sensingtime is None:
                        download_file.sensingtime = self.__data_reader.getDate(downloaded_file.localFile.getName())

                    store_ok = self.store_internal_downloaded_file(
                        downloaded_file,
                        integrity_check=self.__misc_param_integrity_check)
                except Exception as e:
                    self._log.exception(
                        'Error in internal storing for file : %s' % (str(e)))
                    details = 'internal storage failed for file %s. Error : %s' % (
                        downloaded_file.remoteFilepath, str(e))
                    downloaded_file = None
                else:
                    if not store_ok:
                        if self.__file_ignored is True:
                            self._log.debug(
                                "The file was either ignored or not accepted by the regex.  '%s'" %
                                downloaded_file.localFile.getName())
                            msg_level = 'WARNING'
                            details = 'The file was either ignored or not accepted by the regex'
                        else:
                            self._log.exception(
                                "Internal storage error for file '%s' (will be retried later)" %
                                downloaded_file.localFile.getName())
                    elif self.__misc_param_integrity_check != 'SAFE':
                        #    and not downloaded_file.isFolder:
                        #    and not downloaded_file.localFile.getName(short=True).startswith('$'):
                        self._log.debug(f'[{self.id}] Internal storage success for file '
                                        f"'{downloaded_file.remoteFilepath}' "
                                        f'(stored in {downloaded_file.localFile.getName()})')
                        # si on est ici, au pire, c'est un warning dans le cas ou
                        # le storage final echoue (il sera retente plus tard)
                        msg_level = 'WARNING'
                        # try to move file into final storage folder, but do not care if it fails :
                        # it will be moved later, in the storeRemainingFiles()
                        try:
                            self.store_final_downloaded_file(
                                downloaded_file,
                                integrity_check=self.__misc_param_integrity_check)
                        except Exception as e:
                            self._log.warning(
                                "Final storage failure for file '%s' (will be retried later) : %s" %
                                (downloaded_file.localFile.getName(), str(e)))
                        else:
                            self._log.debug(f'[{self.id}] Final storage success for file '
                                            f"'{downloaded_file.remoteFilepath}' "
                                            f'(stored in {downloaded_file.localFile.getName()})')
                            msg_level = 'INFO'  # tout s'est bien passe, c'est une info qui est envoyee a l'operateur

                    elif self.__misc_param_integrity_check == 'SAFE':
                        self._log.debug(f'[{self.id}] no final storage for file SAFE '
                                        f"'{downloaded_file.remoteFilepath}' "
                                        '(wait until all files in the secure directory are downloaded)')
                        msg_level = 'INFO'  # tout s'est bien passe, c'est une info qui est envoyee a l'operateur
                    elif downloaded_file.isFolder:
                        # downloaded_file.localFile.getName(short=True).startswith('$'):
                        self._log.debug(f'[{self.id}] no final storage for tar file '
                                        f"'{downloaded_file.remoteFilepath}' "
                                        f'(using the secure storage procedure)')
                        msg_level = 'INFO'  # tout s'est bien passe, c'est une info qui est envoyee a l'operateur

            # enregistrement du bon deroulement ou non du download + storage interne de ce fichier, avec
            # message operateur si tout se passe bien (si ca se passe mal parce que le stockage
            # final n'est pas accessible, c'est pas grave, on attend qu'il redevienne disponible. Le message
            # recu par l'operateur dans ce cas indique un succes du telechargement)
            self.validate_file_download(
                download_file,
                msg_level,
                downloaded_file=downloaded_file,
                details=details)
        finally:
            if is_parralel:
                self._log.debug(' Parralel Release sema  and close session .')
                if session:
                    self.__provider.closeSession(session)
                self.__sema.release()

            return True

    def is_new_configuration(self):
        """ isNewConfiguration detect if download configuration file is modified.
            If it is modified, the current thread is stopped.
        """
        self._log.debug('Detect if a new configuration is available')

        try:
            if not is_io_locked(os.path.split(self.__download_conf_file)[0], timeout=2.0):
                mtime = File(self.__download_conf_file).getTime()
                is_new_configuration = (
                        mtime != self.__current_mtime_download_conf_file)
                if is_new_configuration:
                    self._log.debug(
                        'New configuration detected %s, new mtime : %s, old mtime : %s ' %
                        (self.__download_conf_file, mtime, self.__current_mtime_download_conf_file))
                    self.__current_mtime_download_conf_file = mtime
                    self.__endingMessage = 'A new configuration detected'
                    self.__endingMStatus = 'WARNING'
                    self._stopevent.set()
            else:
                # continue with the same configuration
                # log trace with "WARNING" level instead of "INFO"
                self._log.warning(f'[{self.id}] Download configuration directory is temporarily inaccessible !')

        except OSError:
            self.__endingMessage = 'The configuration file is not found'
            self._log.warning(f'[{self.id}] The configuration file is not found')
            self.__endingMStatus = 'WARNING'
            self._stopevent.set()

    # Thread entry point, called by YourDownload.start()
    def run(self):
        self._log.info('OPERATOR : Start %s downloader', self.id)
        # init thread jobstate file
        self._log.debug('[download_id=%s] : initJobstateFile()' % self.id)
        self.init_jobstate_file()
        if self.test:
            self._log.debug(
                '[download_id=%s] : initTestStateFile()' % self.id)
            self.init_test_state_file()
            self.update_state_test([STATE_TEST_PROGRESS, ''])

        # init database
        self._log.debug(
            '[download_id=%s] : initDownloaderDatabase()' % self.id)
        self.init_downloader_database()

        # get provider object for this download
        self._log.debug('[download_id=%s] : getProvider()' % self.id)

        pm = ProviderManager.ProviderManager(globalConfig=self.__globalConfig)
        self.__provider = pm.getProvider(self.__remote_storage_provider_ref)
        if self.__provider is None:
            self._log.error(
                "Bad configuration file %s : disabled file (adding '.BAD' extension)" %
                self.__download_conf_file)
            os.rename(self.__download_conf_file,
                      self.__download_conf_file + '.BAD')
            self.update_state_test([STATE_TEST_FAILED, 'Invalid provider'])
            self._log.error(
                "Invalid remote provider_ref for download '%s' : stopping download !" %
                self.id)
            self.stop_running()
            return
        self.__provider.setOption(self.__configuration.source.protocol_option)
        self.__provider.set_timeout(self.__configuration.settings.protocolTimeout)

        # we want to do the getlisting at startup, (expected date of next
        # listing is now)
        nextdate_getlisting = datetime.datetime.utcnow()

        # main download loop thread
        download_loop_number = 0
        self._log.info(
            'Control if max activated flow is get. [download_id=%s] : wait .' %
            self.id)

        self.__close_session(self.__session)
        self.__isWaitSema = True
        self._log.debug('-- isWaitSema : %s' % self.__isWaitSema)
        self.__sema.acquire()
        self.__isWaitSema = False
        self._log.debug('-- isWaitSema : %s' % self.__isWaitSema)
        self._log.info(
            '[download_id=%s] acquire semaphore. (Wait finished)' % self.id)
        self.__get_session()

        # Catch the untreated exception, print the Traceback and stop the download
        try:
            while not self._stopevent.is_set():
                if self.__isWaitSema:
                    self._log.debug(
                        'Loop Control if max activated flow is get. [download_id=%s] : wait .' %
                        self.id)
                    self._log.debug('-- CloseSession')
                    self.__close_session(self.__session)
                    self._log.debug('-- Sema.acquire')
                    self.__sema.acquire()
                    self._log.debug('-- GetSession')
                    self.__get_session()
                    self.__isWaitSema = False
                    self._log.debug('-- isWaitSema : %s' % self.__isWaitSema)
                    self._log.debug(
                        'Loop Control  [download_id=%s] acquire sema. (Wait finished)' %
                        self.id)

                # try to store previously downloaded files in the final storage folder,
                # if some files,folders or links are still in the internal storage folder
                self.storage_pending_downloaded_files()

                # 15/03/2018 PMT : ajout des liens complémentaires sur les téléchargements finaux
                # si des liens temporaires existent, ils sont copies dans les
                # répertoires finaux.
                # self.storeRemainingLinks()

                details = ''
                if not self.__misc_param_check_provider_before_download:
                    # consider that provider is available, so forces the download. Should only be
                    # used in special cases, to avoid connections flood (which
                    # timeout on some servers...)
                    provider_ok = True
                else:
                    try:
                        provider_ok = self.__provider.getAvailability()
                    except Exception as e:
                        # la string d'erreur est affiche dans le self.state
                        details = str(e)
                        provider_ok = False

                # update the next getlisting flag (expected date of next
                # listing < current date ?)
                next_getlisting = (
                                          nextdate_getlisting -
                                          datetime.datetime.utcnow()).total_seconds() <= 0.0

                # we do not want to run the download if the provider is not
                # available
                if not provider_ok:
                    self.state = (STATE_WAITING_PROVIDER, details)
                    self._log.info("%s(%s) '%s' " %
                                   (STATE_WAITING_PROVIDER, details, self.id))
                else:
                    download_loop_number += 1
                    # run download with getlisting (remote or not)
                    do_get_remote_listing = False
                    if next_getlisting:
                        do_get_remote_listing = True
                        nextdate_getlisting = datetime.datetime.utcnow(
                        ) + datetime.timedelta(seconds=self.__misc_param_getlisting_delay)

                    try:
                        # do the download operations : getlisting (optionnal),
                        # download, storage ...
                        self.run_download(
                            download_loop_number,
                            get_remote_listing=do_get_remote_listing)

                    except Exception as e:
                        self._log.exception(
                            "Error in runDownload [download_id='%s'] : %s" %
                            (self.id, str(e)))

                    self.state = (STATE_IDLE, '')

                # loop sleep, equivalent to time.sleep(self.__misc_param_loop_delay), but waiting for
                # thread termination event
                self._stopevent.wait(self.__misc_param_loop_delay)

                self._log.debug(
                    '[download_id=%s] _nb file to download = %s.' %
                    (self.id, self.__nbFileToDownload))
                if self.__nbFileToDownload == 0 and not self.__isWaitSema:
                    self._log.debug(
                        'End Loop nbfiledownload = 0 [download_id=%s] Release semaphore.' %
                        self.id)
                    self.__sema.release()
                    self.__isWaitSema = True
                    self._log.debug('-- isWaitSema : %s' % self.__isWaitSema)
                self._log.debug(
                    '[download_id=%s] : next getlisting will occur at %s (if provider is available).' %
                    (self.id, nextdate_getlisting.strftime('%Y/%d/%m %H:%M:%S')))

                # find new cofiguration
                self.is_new_configuration()

        except BaseException:
            self._log.exception('Download thread %s is dead !' % self.__id)
            self.__endingMessage = 'A not catchable error has occurred'
            self.__endingMStatus = 'ERROR'

        if not self.__isWaitSema:
            self._log.debug(' [download_id=%s] Release sema .' % self.id)
            self.__sema.release()
            self.__isWaitSema = True
            self._log.debug('-- isWaitSema : %s' % self.__isWaitSema)

        if self.__endingMStatus == 'ERROR':
            self._log.error(f'[{self.id}] the download is stopped ({self.__endingMessage})')
        elif self.__endingMStatus == 'WARNING':
            self._log.warning(f'[{self.id}] the download is stopped ({self.__endingMessage})')
        else:
            self._log.info(f'[{self.id}] the download is stopped ({self.__endingMessage})')

        m = messages.MessageFactory().create({
            'type': 'downloadMessage',
            'level': self.__endingMStatus,
            'summary': '[%s] %s' % (self.id, self.__endingMessage),
            'details': 'the download is stopped',
            'reference_type': 'data',
            'reference_name': self.__download_conf_file,
            'processing_start_date': self.__emm_start_date,
            'processing_hostname': gethostname(),
            'processing_pid': os.getpid(),
            'download_name': self.id,
            'download_provider': self.__remote_storage_provider_ref,
        }, template=Controller.TEMPLATE_DOWNLOAD_MESSAGE)
        try:
            self.__emm_writer.writeMessage(m, self.__id)
        except Exception as e:
            self._log.exception('Message write error : %s' % (str(e)))

        # Thread termination
        self._log.info('OPERATOR : Stop %s downloader', self.id)
        self.__close_session(self.__session)
        self.stop_running()

    def stop_running(self):
        self.state = (STATE_STOPPED, '')
        self.__jobstate.close()  # just in case... should not be necessary
        # To avoid the multiplication of handlers
        for handler in self._log.handlers:
            if str(self.__logFile) == str(handler.baseFilename):
                self._log.removeHandler(handler)

    def join(self, timeout=None):
        """
        Stop the thread
        """
        self.__endingMessage = 'The stop of download is requested'
        self.__endingMStatus = 'INFO'
        self._stopevent.set()
        while self.is_alive():
            self._log.info("  waiting end of download '%s' (state = %s)" % (
                self.__id, self.state))
            if not self.__isWaitSema:
                self.__sema.release()
                self.__isWaitSema = True
                self._log.debug(
                    "  Join download '%s' release semaphore " % self.__id)
                self.__close_session(self.__session)

                self._log.debug('-- isWaitSema : %s' % self.__isWaitSema)

            if self.state[1] == STATE_RUNNING_SCANNING:
                self._log.info("  stop download '%s' (state = %s)" %
                               (self.__id, self.state))
                self.__listing_builder.stop()
            threading.Thread.join(self, timeout)
        self._log.info(' %s is not alive' % self.__id)

    def stop_download(self):
        """
        Stop the thread
        """
        self._log.info('[download_id=%s] The stop of download is requested' % self.id)
        self.__endingMessage = 'The stop of download is requested'
        self.__endingMStatus = 'INFO'
        self._stopevent.set()

    def config_summary(self, level):
        """Write on text log a summary of the L{Download} configuration

        @param level: log level to write information (see
        U{logging module documentation<http://docs.python.org/lib/module-logging.html>}
        for good value)
        @type level: C{int}
        """
        self._log.log(level, ' ' + '-' * 99)
        self._log.log(level, " - Download '%s'" % self.id)
        self._log.log(level, "     test mode : '%s'" % self.test)
        self._log.log(level, "     log debug : '%s'" %
                      self.__configuration.debug)
        self._log.log(level, "     Download configuration file : '%s'" %
                      self.download_conf_file)
        self._log.log(level, '     data source : ')
        self._log.log(level, '       location : ')
        self._log.log(level, "         protocol : '%s'" %
                      self.__configuration.source.protocol)
        self._log.log(level, "         protocol option: '%s'" %
                      self.__configuration.source.protocol_option)
        self._log.log(level, "         rootPath : '%s'" %
                      self.__configuration.source.rootPath)
        self._log.log(level, "         server : '%s'" %
                      self.__configuration.source.server)
        self._log.log(level, "         login : '%s'" %
                      self.__configuration.source.login)
        self._log.log(level, "         password : '%s'" %
                      self.__configuration.source.password)
        self._log.log(level, "         port : '%s'" %
                      self.__configuration.source.port)
        self._log.log(level, '       data_reader :')
        self._log.log(level, "         plugin : '%s'" %
                      self.__configuration.source.plugin)
        self._log.log(level, "         date regexp : '%s'" %
                      self.__configuration.source.dateRegexp)
        self._log.log(level, "         date format : '%s'" %
                      self.__configuration.source.dateFormat)
        self._log.log(level, '       selection :')
        self._log.log(level, "         update : '%s'" %
                      self.__configuration.source.update)
        self._log.log(level, '         by date :')
        self._log.log(level, "           pattern : '%s'" %
                      self.__configuration.source.date_pattern)
        self._log.log(level, "           backlog in days : '%s'" %
                      self.__configuration.source.date_backlogInDays)
        self._log.log(level, "           maxDate : '%s'" %
                      self.__configuration.source.date_maxDate)
        self._log.log(level, "           minDate : '%s'" %
                      self.__configuration.source.date_minDate)
        self._log.log(level, '         by directories :')
        self._log.log(level, "           ignore newer than : '%s'" %
                      self.__configuration.source.directories_ignoreNewerThan)
        self._log.log(level, "           ignore older than : '%s'" %
                      self.__configuration.source.directories_ignoreOlderThan)
        self._log.log(level, "           ignore regexp : '%s'" %
                      self.__configuration.source.directories_ignoreRegexp)
        self._log.log(level, "           regexp : '%s'" %
                      self.__configuration.source.directories_regexp)
        self._log.log(level, '         by files :')
        self._log.log(level, "           ignore older than : '%s'" %
                      self.__configuration.source.files_ignoreOlderThan)
        self._log.log(level, "           ignore regexp : '%s'" %
                      self.__configuration.source.files_ignoreRegexp)
        self._log.log(level, "           regexp : '%s'" %
                      self.__configuration.source.files_regexp)
        self._log.log(level, '         specific opensearch :')
        self._log.log(level, "           area : '%s'" %
                      self.__configuration.source.opensearch_area)
        self._log.log(level, "           dataset : '%s'" %
                      self.__configuration.source.opensearch_dataset)
        self._log.log(level, "           request format : '%s'" %
                      self.__configuration.source.opensearch_requestFormat)
        self._log.log(level, '     data destination : ')
        self._log.log(level, "       location : '%s'" %
                      self.__configuration.destination.location)
        self._log.log(level, '       organization : ')
        self._log.log(level, "         type : '%s'" %
                      self.__configuration.destination.type)
        self._log.log(level, "         subpath : '%s'" %
                      self.__configuration.destination.subpath)
        self._log.log(level, "         keep parent folder : '%s'" %
                      self.__configuration.destination.keepParentFolder)
        self._log.log(level, "       optional spool location : '%s'" %
                      self.__configuration.destination.optionalSpool)
        self._log.log(level, '       post processing : ')
        self._log.log(level, "         checksum : '%s'" %
                      self.__configuration.destination.checksum)
        self._log.log(level, "         compression : '%s'" %
                      self.__configuration.destination.compression)
        self._log.log(level, "           type : '%s'" %
                      self.__configuration.destination.compression_type)
        self._log.log(level, "           subdir : '%s'" %
                      self.__configuration.destination.compression_subdir)
        self._log.log(level, "         archive : '%s'" %
                      self.__configuration.destination.archive)
        self._log.log(level, "           subdir : '%s'" %
                      self.__configuration.destination.archive_subdir)
        self._log.log(level, "         file group name : '%s'" %
                      self.__configuration.destination.fileGroupName)
        self._log.log(level, "         optional link spool : '%s'" %
                      self.__configuration.destination.spoolLink)
        self._log.log(level, '     settings  : ')
        self._log.log(level, '       database :')
        self._log.log(level, "         path : '%s'" %
                      self.__configuration.settings.database_path)
        self._log.log(
            level, "         purge scans older than : '%s'" %
                   self.__configuration.settings.database_purgeScansOlderThan)
        self._log.log(
            level, "         keep last scan : '%s'" %
                   self.__configuration.settings.database_keepLastScan)  # default True
        self._log.log(
            level, "       check source availability : '%s'" %
                   self.__configuration.settings.checkSourceAvailability)  # default True
        self._log.log(
            level, "       delay between downloads : '%s'" %
                   self.__configuration.settings.delayBetweenDownloads)  # default 0
        self._log.log(level, "       cycle length : '%s'" %
                      self.__configuration.settings.cycleLength)
        self._log.log(level, "       number of retries : '%s'" %
                      self.__configuration.settings.nbRetries)
        self._log.log(level, "       number of parallel downloads : '%s'" %
                      self.__configuration.settings.nbParallelDownloads)
        self._log.log(
            level, "       max nb of files downloads per cycle : '%s'" %
                   self.__configuration.settings.maxNbOfFilesDownloadedPerCycle)
        self._log.log(level, "       delay before download start : '%s'" %
                      self.__configuration.settings.delayBeforeDownloadStart)
        self._log.log(level, "       delay between scans : '%s'" %
                      self.__configuration.settings.delayBetweenScans)
        self._log.log(level, "       max nb of concurrent streams : '%s'" %
                      self.__configuration.settings.maxNbOfConcurrentStreams)
        self._log.log(level, "       max nb of lines in auto listing : '%s'" %
                      self.__configuration.settings.maxNbOfLinesInAutoListing)  # default LISTING_AUTO_MAX_LINES_DEFAULT
        self._log.log(level, "       monitoring : '%s'" %
                      self.__configuration.settings.monitoring)  # default None
        self._log.log(level, "       protocol timeout : '%s'" %
                      self.__configuration.settings.protocolTimeout)  # default None
        self._log.log(level, ' ' + '-' * 99)

    def clean_workspace(self):
        """
            Clean the workspace :
             - save the data/temporary folder in another directory and create
               an empty directory as new current temporary folder. Called at init, to start
               with an empty workspace.
        """
        temp_folder = Folder(self.__get_folder(
            FOLDER_TEMPORARY).getPath(), recursive=True)
        try:
            temp_folder.scan()
        except OSError:
            self._log.warning(
                f'Path {self.__get_folder(FOLDER_TEMPORARY).getPath()} is temporarily inaccessible !')
        else:
            temp_files = temp_folder.values(restr=File)
            if len(temp_files) > 0:
                self._log.debug(
                    'Clean workspace : %s files in temporary folder' %
                    (len(temp_files)))
                temporary_path = self.__get_folder(FOLDER_TEMPORARY).getPath()
                save_temporary_path = temporary_path + '_save'
                Folder(save_temporary_path, create=True)
                save_temporary_path = os.path.join(
                    save_temporary_path, time.strftime(
                        '%Y%m%d_%H%M%S', time.gmtime()))

                temp_folder.move(save_temporary_path)
                Folder(self.__get_folder(FOLDER_TEMPORARY).getPath(), create=True)
