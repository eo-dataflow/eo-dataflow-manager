#
# -*- coding: UTF-8 -*-
#
"""ProviderFactory module contain the L{ProviderFactory} function
"""

__docformat__ = 'epytext'

know_classes = {}


def my_import(name):
    mod = __import__(name)
    components = name.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def ProviderFactory(provType, provID, job_path, globalConfig):
    """ProviderFactory instanciate a subclass of AbstractProvider in
    function of the system name.

    If module or class is unreachable log the error and raise
    a RuntimeError exception.

    @param system: The type of system
    @type system: C{str}
    @param name: Name or address of the host
    @type name: C{str}
    @param remoteShell: Name of remote shell use to run distant program
    @type remoteShell: C{str}
    @param preEnvScript: Name of the script use to define SystemController environnement variable
    @type preEnvScript: C{str}

    @rtype: Subclass of L{AbstractProvider}
    """

    # 2018/12/18 PMT#14 protocol in lowercase: the plugin must have the first
    # character uppercase
    providerType = str.capitalize(provType)

    # 2018/04/09 PMT#34 mise à jour du type de provider (pour https_opensearch
    # -> https)

    if providerType == 'Https_eop_os':
        provType = 'Eumdac'
    elif providerType == 'Https_s3_cmems':
        provType = 'S3'
    elif providerType[:5] == 'Https':
        provType = 'Https'
    elif providerType[:4] == 'Ftps':
        provType = 'Ftps'
    elif providerType == 'Eodms':
        provType = 'Https'

    realname = provType + 'Provider'

    # If the class is unknow, try to load it
    if realname not in know_classes:
        # Try to import the modules "realname

        try:
            tmpModule = my_import(
                'eo_dataflow_manager.scheduler.sc.ProviderManager.' + realname)
        except ModuleNotFoundError:
            # add log information
            raise RuntimeError('Module %s to import not found' % realname)

        if realname not in dir(tmpModule):
            # add log information
            raise RuntimeError(
                'Class %s unreachable in the %s module' %
                (realname, realname))

        # Store the class in the know_class dictionnary
        know_classes[realname] = tmpModule.__dict__[realname]

    # Build a instance of the class know_class[realname]
    return know_classes[realname](provID, providerType, job_path, globalConfig)
