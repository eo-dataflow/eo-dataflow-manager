#
# -*- coding: UTF-8 -*-
#
import datetime
import logging
import os
import socket
import time
from socket import gethostname

from eo_dataflow_manager.emm.api import messages
from eo_dataflow_manager.scheduler.com.ext.PublishRabbitMQ import PublishRabbitMQ
from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc import Controller
from eo_dataflow_manager.scheduler.sc.job_state import JobState
from eo_dataflow_manager.scheduler.sc.job_targets.JobsElastic import JobsElastic

__docformat__ = 'epytext'


socket.setdefaulttimeout(60)


STATE_INIT = 'Initializing'
STATE_AVAILABLE = 'Available'
STATE_NOT_AVAILABLE = 'Not available'
STATE_TESTING_AVAILABILITY = 'Testing availability'
STATE_DISABLED = 'Disabled'

DEFAULT_TIMEOUT = 120


class AbstractProvider(object):
    """
    Class AbstractProvider
    """

    def __init__(self, provID, provType, job_path, globalConfig):
        """
        AbstractProvider initialization method

        """
        self.__id = provID
        self.__type = provType
        self.__lastCheckTime = 0
        self.__available = True  # be optimist :)

        # "Provider-%s"%id en mettant ca en argument, erreur de Handler logging
        self._log = logging.getLogger(Controller.SCHD_LOGGER_NAME)
        self._jobfilePath = job_path

        self._jobstate = None
        self._state = [STATE_INIT, '']
        self._jobFile = File(os.path.join(self._jobfilePath, '%s.job' % self.__id))

        self.__option = {}

        self.__errormsg = None

        self.__timeout = DEFAULT_TIMEOUT

        # Setup Elasticsearch job upload
        if 'elasticsearch' in globalConfig['jobs.targets']:
            self.__jobsElastic = JobsElastic(
                hosts=globalConfig['jobs.elasticsearch.hosts'],
                scheme=globalConfig['jobs.elasticsearch.scheme'],
                user=globalConfig['jobs.elasticsearch.user'],
                password=globalConfig['jobs.elasticsearch.password'],
                indexNameTemplate=globalConfig['jobs.elasticsearch.index'],
                sniffCluster=globalConfig['jobs.elasticsearch.sniff_cluster'],
            )
            self._log.debug('Jobs will be pushed to ElasticSearch')
        else:
            self.__jobsElastic = None
        # Setup RabbitMQ job upload
        self.__jobsRabbitMQ = PublishRabbitMQ

        self.setState(self._state)

    provID = property(lambda self: self.__id, None, None,
                      "return the provider's id")

    provType = property(lambda self: self.__type, None, None,
                        "return the provider's type")

    def setState(self, state):
        try:
            if self._jobstate is None:
                try:
                    self._jobstate = JobState(
                        self._jobFile,
                        mode='w',
                        jobsElastic=self.__jobsElastic,
                        jobsRabbitMQ=self.__jobsRabbitMQ
                    )
                except BaseException:
                    err_msg = 'Impossible to create JobState instance'
                    self._log.error(err_msg + ' : %s' % (str(self._jobFile)))
                    self._log.exception(err_msg)
                    self._jobstate = None
                    self._jobstateFile = None
            else:
                self._jobstate['Provider'] = self.__id
                self._jobstate['Type'] = self.__type
                self._jobstate['Host'] = gethostname()
                self._jobstate['State'] = state[0]
                self._jobstate['Details'] = state[1]
                self._jobstate['Date'] = datetime.datetime.utcnow().strftime(
                    messages.MSG_DATE_FORMAT)

                self.setSpecificState()

                # self.__semaState.acquire()
                self._jobstate.sync()
                # self.__semaState.release()
        except Exception:
            self._log.error(
                "Cannot update jobstate File for Provider '%s' (state=%s)" %
                (self.__id, state))
            self._log.exception('Skip update : ')

        self._state = state

    def isAvailable(self):
        """
        return True if the host is up
        """
        # If can get load average, return True
        status = self.getAvailability()
        if status is not None:
            return status
        return False

    def getAvailability(self):
        """
        Return the provider's availability
        """
        self.__errormsg = None
        self.setState([STATE_TESTING_AVAILABILITY, 'getAvailability'])

        if not self.__available and \
           (time.time() - self.__lastCheckTime) < 300:
            self._log.debug("Host '%s' was unavailable... still waiting a few minutes before retry",
                            self.provID)
            return False

        if self.checkAvailability():
            self.__available = True
            self.setState([STATE_AVAILABLE, 'getAvailability'])
        else:
            self.setState([STATE_NOT_AVAILABLE, 'getAvailability'])
            self.__available = False

        self.__lastCheckTime = time.time()

        return self.__available

    def setSpecificState(self):
        raise RuntimeError('This method must be overloaded')

    def checkAvailability(self):
        raise RuntimeError('This method must be overloaded')

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        raise RuntimeError('This method must be overloaded')

    def closeSession(self, session):
        raise RuntimeError('This method must be overloaded')

    def getFile(self, session, remotepath, localpath):
        raise RuntimeError('This method must be overloaded')

    def putFile(self, session, localpath, remotepath):
        raise RuntimeError('This method must be overloaded')

    def getLastError(self):
        return self.__errormsg

    def getDefaultTimeout(self):
        """ get method for default timeout, see method above for more info """
        return self.__timeout

    def setOption(self, optionDict):
        self.__option = optionDict

    def getmtime(self):
        """ get method for mtime, see method above for more info """
        return None

    def getsize(self):
        """ get method for size, see method above for more info """
        return None

    def set_timeout(self, timeout):
        """ get method for default timeout, see method above for more info """
        if timeout:
            self.__timeout = timeout
