#
# -*- coding: UTF-8 -*-
#
"""FtpProvider module contain the L{FtpProvider} class
"""

__docformat__ = 'epytext'

import ftplib
import os
import socket
import ssl

from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import AbstractProvider

DEFAULT_CONNECTION_PORT = 21


class ImplicitFTP_TLS(ftplib.FTP_TLS):
    """
    FTP_TLS subclass that automatically wraps sockets in SSL to support implicit FTPS.
    Prefer explicit TLS whenever possible.
    """

    def __init__(self, *args, **kwargs):
        """Initialise self."""
        super().__init__(*args, **kwargs)
        self._sock = None

    @property
    def sock(self):
        """Return the socket."""
        return self._sock

    @sock.setter
    def sock(self, value):
        """When modifying the socket, ensure that it is SSL wrapped."""
        if value is not None and not isinstance(value, ssl.SSLSocket):
            value = self.context.wrap_socket(value)
        self._sock = value

    def ntransfercmd(self, cmd, rest=None):
        """Override the ntransfercmd method"""
        conn, size = ftplib.FTP.ntransfercmd(self, cmd, rest)
        conn = self.sock.context.wrap_socket(
            conn, server_hostname=self.host, session=self.sock.session
        )
        return conn, size


class FtpsProvider(AbstractProvider):
    """
    Class FtpProvider
    """

    def __init__(self, id, type_, job_path, globalConfig):
        """
        FtpProvider initialization method
        """
        AbstractProvider.__init__(self, id, type_, job_path, globalConfig)
        self.__server = None
        self.__username = None
        self.__password = None
        self.__port = DEFAULT_CONNECTION_PORT
        self.__ssl_protocol = type_[5:]

        self.__session = None

    server = property(lambda self: self.__server, None, None, None)
    username = property(lambda self: self.__username, None, None, None)
    password = property(lambda self: self.__password, None, None, None)
    port = property(lambda self: self.__port, None, None, None)

    def setSpecificState(self):
        self._jobstate['Server'] = self.__server
        self._jobstate['Username'] = self.__username
        self._jobstate['Password'] = self.__password
        self._jobstate['Port'] = self.__port

    def checkAvailability(self):
        #self._log.debug("Checking availability for server : %s"%(self.__server))
        self.__errormsg = None
        ret = None

        self.__session = self.getSession()
        if self.__session is None:
            return False

        try:
            ret = self.__session.getwelcome()
        except socket.error as msg:
            self._log.warning('Connection error : [socket.error : %s]' % (msg))
            self.__errormsg = str(msg)
            ret = False

        try:
            self.__closeSession()
        except Exception as msg:
            self.__errormsg = str(msg)

        if ret is not None:
            return True

        return False

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        self.__errormsg = None
        session = None
        if timeout_ is None:
            timeout_ = self.getDefaultTimeout()
        try:
            try:
                session = ImplicitFTP_TLS(timeout=self.getDefaultTimeout())
                session.connect(self.server, self.port)
            except ssl.SSLError:  # ssl.SSLZeroReturnError:
                session = ftplib.FTP_TLS(timeout=self.getDefaultTimeout())
                session.connect(self.server, self.port)
            session.login(self.username, self.password)
            session.prot_p()
        except socket.error as msg:
            self._log.info(
                'Connection error : cannot connect to remote server [socket.error : %s, (server=%s, username=%s])' %
                (msg, self.__server, self.__username))
            self.__errormsg = str(msg)
        except Exception as msg:
            self._log.info(
                'Connection error : cannot connect to remote server [exception : %s, (server=%s, username=%s])' %
                (msg, self.__server, self.__username))
            session = None
            self.__errormsg = str(msg)
        return session

    def closeSession(self, session):
        if session is None:
            return

        try:
            session.close()
        except socket.error as msg:
            self.__errormsg = str(msg)
            self._log.warning('Connection error : [socket.error : %s]' % (msg))

    def getFile(self, session, remotepath, localpath):
        self.__errormsg = None
        tmp_localpath = localpath + '.tmp'
        try:
            with open(tmp_localpath, 'wb') as fd:
                session.retrbinary(
                    'RETR ' + remotepath,
                    fd.write)
        except Exception as msg:
            self._log.warning(
                'Connection : Exception while retrieving binary : %s (file = %s)' %
                (msg, remotepath))
            self.__errormsg = str(msg)
            return None

        # move atomique, pour prise en compte du fichier complet par un systeme
        # externe eventuellement.
        try:
            os.rename(tmp_localpath, localpath)
        except OSError as msg:
            self._log.warning(
                'Cannot rename %s --> %s (%s)' %
                (tmp_localpath, localpath, msg))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(localpath):
            return File(localpath)

        return None

    def putFile(self, session, localpath, remotepath):
        raise RuntimeError('This method must be overloaded')

    def setServer(self, server):
        self.__server = server

    def setUsername(self, username):
        self.__username = username

    def setPassword(self, password):
        self.__password = password

    def setPort(self, port):
        if port is not None:
            self.__port = port

#        self._log.debug("FtpProvider::remoteLaunch on %s spawn : remoteShell='%s', cmd='%s'",
#            self.name,
#            self.remoteShell,
#            ' '.join(cmd_list))

    def __closeSession(self):
        self.closeSession(self.__session)
        self.__session = None
