#
# -*- coding: UTF-8 -*-
#
"""HttpProvider module contain the L{HttpsProvider} class
"""

__docformat__ = 'epytext'

import http.cookiejar
import os
import socket
import ssl
import urllib.error
import urllib.parse
import urllib.request

from eo_dataflow_manager.ifr_lib_modules.ifr_datetimes import parse_date
from eo_dataflow_manager.scheduler.com.sys.File import READ_BUFFER, File
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import AbstractProvider

TMP_EXT = '.tmp'


class HttpsSession(object):
    def __init__(self, server, user, pwd, option, timeout_=None):
        self.server = server
        self.user = user
        self.pwd = pwd
        self.timeout = timeout_
        self.filename = None
        self.filesize = 0
        self.option = option
        self.mtime = None

        socket.setdefaulttimeout(self.timeout)

        https_handler = urllib.request.HTTPSHandler()

        if self.option is not None and 'CheckCertificate' in self.option:
            if self.option['CheckCertificate'].lower() == 'false':
                context = ssl.create_default_context()
                context.check_hostname = False
                context.verify_mode = ssl.CERT_NONE
                https_handler = urllib.request.HTTPSHandler(context=context)

        password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(realm=None, uri=server, user=self.user, passwd=self.pwd)

        if self.option is not None and 'Redirector' in self.option:
            password_mgr.add_password(
                realm=None, uri=self.option['Redirector'], user=self.user, passwd=self.pwd)
            handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
            cookie_jar = http.cookiejar.CookieJar()
            handler_cookie = urllib.request.HTTPCookieProcessor(cookie_jar)
            self.opener = urllib.request.build_opener(https_handler, handler, handler_cookie)
        else:
            handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
            self.opener = urllib.request.build_opener(https_handler, handler)

    def close(self):
        pass  # with urllib, the connection is automatically closed when the object is disposed by the GC

    def download(self, remotepath, localpath):

        path, filename = os.path.split(os.path.abspath(localpath))
        if remotepath[:2] == '//':
            remotepath = 'https://' + self.server + remotepath[1:]
        else:
            remotepath = remotepath

        if 'AppKey' in self.option:
            remotepath += f'?appkey={self.option["AppKey"]}'

        if 'UrlLogin' in self.option:
            url_login = self.option['UrlLogin']
            if url_login[:2] == '//':
                url_login = 'https://' + url_login
            data = urllib.parse.urlencode({'email': self.user, 'passwd': self.pwd, 'action': 'login'}).encode('ascii')
            test = self.opener.open(url_login, data)
            test.close()


        if self.opener is not None:
            flow = self.opener.open(remotepath)
            info = flow.info()
            self.filesize = -1
            if 'Content-Length' in info:
                self.filesize = int(info['Content-Length'])
            if 'Last-Modified' in info:
                try:
                    self.mtime = parse_date(flow.info()['Last-Modified']).replace(tzinfo=None)
                except Exception:
                    self.mtime = None
            else:
                self.mtime = None

            # self.filesize = int(info['Content-Length'])
            # check real filename
            if 'Content-Disposition' in info \
                    and len(info['Content-Disposition'].split('filename=')) == 2:
                filename = info[
                    'Content-Disposition'].split('filename=')[1].strip('"')
            else:
                filename = filename[:-len(TMP_EXT)]

            self.filename = os.path.join(path, filename)
            toread = READ_BUFFER * 16
            done = 0
            with open(localpath, 'wb') as downloadfile:
                while True:
                    readbuffer = flow.read(toread)
                    downloadfile.write(readbuffer)
                    read = len(readbuffer)
                    done += read
                    if read != toread:
                        break
            flow.close()
        else:
            return False

        return done == self.filesize

    def check(self):
        test = self.opener.open('https://' + self.server)
        test.close()
        return True

    def getFilename(self):
        return self.filename

    def getFilemtime(self):
        return self.mtime

    def getFilesize(self):
        return self.filesize


class HttpsProvider(AbstractProvider):
    """
    Class HttpProvider
    """

    def __init__(self, provider_id, provider_type, job_path, global_config):
        """
        HttpsProvider initialization method
        """
        AbstractProvider.__init__(self, provider_id, provider_type, job_path, global_config)
        self.__server = None
        self.__username = None
        self.__password = None
        self.__session = None
        self.__port = None
        self.__option = {}
        self.__file_mtime = None
        self.__file_size = None

    server = property(lambda self: self.__server, None, None, '')
    username = property(lambda self: self.__username, None, None, '')
    password = property(lambda self: self.__password, None, None, '')
    session = property(lambda self: self.__session, None, None, '')

    def setSpecificState(self):
        self._jobstate['Server'] = self.__server
        self._jobstate['Username'] = self.__username
        self._jobstate['Password'] = self.__password

    def checkAvailability(self):
        # self._log.debug("Checking availability for server : %s"%(self.__server))
        self.__errormsg = None
        ret = True
        session = None

        try:
            session = self.getSession()

            if not session.check():
                ret = False
        except Exception as msg:
            self._log.warning('Connection error : [%s]' % msg)
            self.__errormsg = str(msg)
            ret = False

        try:
            self.closeSession(session)
        except Exception as msg:
            self.__errormsg = str(msg)
            ret = False

        return ret

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        timeout = timeout_
        if timeout_ is None:
            timeout = self.getDefaultTimeout()
        self.__session = HttpsSession(self.__server, self.__username, self.__password,
                                      self.__option, timeout)
        return self.__session

    def closeSession(self, session):
        if self.__session is not None:
            self.__session.close()

    def getFile(self, session, remotepath, localpath):

        self.__errormsg = None
        tmp_localpath = localpath + TMP_EXT

        try:
            response = session.download(remotepath, tmp_localpath)
            if not response:
                self._log.warning(
                    'download : incorrect file size (URL = %s)' % remotepath)
                self.__errormsg = 'incorrect file size'
                return None

        except Exception as msg:
            self._log.warning(
                'Connection : Exception while download binary : %s (URL = %s)' %
                (msg, remotepath), exc_info=True)
            self.__errormsg = str(msg)
            return None

        # move atomique, pour prise en compte du fichier complet par un systeme
        # externe eventuellement.
        target_file = session.getFilename()

        self.__file_mtime = session.getFilemtime()
        self.__file_size = session.getFilesize()

        try:
            os.rename(tmp_localpath, target_file)
        except OSError as msg:
            self._log.warning('Cannot rename %s --> %s (%s)' %
                              (tmp_localpath, target_file, msg))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(target_file):
            return File(target_file)

        return None

    def getFilenameAndUrl(self, remotepath):
        return remotepath.split()

    def putFile(self, session, localpath, remotepath):
        raise RuntimeError('This method must be overloaded')

    def setServer(self, server):
        self.__server = server

    def setUsername(self, username):
        self.__username = username

    def setPassword(self, password):
        self.__password = password

    def setPort(self, port):
        if port is not None:
            self.__port = port

    def setOption(self, option_dict):
        self.__option = option_dict

    def getmtime(self):
        return self.__file_mtime

    def getsize(self):
        return self.__file_size
