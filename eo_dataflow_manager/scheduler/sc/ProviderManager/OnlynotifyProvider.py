#
# -*- coding: UTF-8 -*-
#
"""OnlynotifyProvider module contain the L{OnlynotifyProvider} class
"""

__docformat__ = 'epytext'

from eo_dataflow_manager.scheduler.sc.ProviderManager.LocalpathProvider import LocalpathProvider


class OnlynotifyProvider(LocalpathProvider):
    """
    Class OnlynotifyProvider
    """

    def __init__(self, id, type, job_path, globalConfig):
        """
        OnlynotifyProvider initialization method
        """
        LocalpathProvider.__init__(self, id, type, job_path, globalConfig)

    def setSpecificState(self):
        pass

    def getFile(self, session, remotepath, localpath):
        self.__errormsg = None
        return None
