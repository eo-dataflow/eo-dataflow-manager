#
# -*- coding: UTF-8 -*-
#
"""HttpProvider module contain the L{HttpsProvider} class
"""

__docformat__ = 'epytext'

import os
import re
import socket
from typing import Optional, Tuple

import boto3
import botocore
from requests import PreparedRequest

from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import AbstractProvider

TMP_EXT = '.tmp'

DEFAULT_CLIENT_NAME = 'copernicus-marine-client'


class S3Session(object):
    def __init__(self, server, user, pwd, options, timeout_=None):
        self.server = server
        self.user = user
        self.pwd = pwd
        self.timeout = timeout_
        self.filename = None
        self.filesize = 0
        self.options = options
        self.mtime = None

        self.client_name = DEFAULT_CLIENT_NAME
        if 'ClientName' in self.options:
            self.client_name = self.options['ClientName']

        socket.setdefaulttimeout(self.timeout)

        def _add_custom_query_param(params, context, **kwargs):
            """
            Add custom query params for MDS's Monitoring
            """
            params['url'] = self.construct_url_with_query_params(
                params['url'],
                self.construct_query_params_monitoring(self.user),
            )

        self.endpoint_url = f'https://{self.server}'

        self.s3_session = boto3.Session()
        self.s3_client = self.s3_session.client(
            's3',
            config=botocore.config.Config(
                # Configures to use subdomain/virtual calling format.
                s3={'addressing_style': 'virtual'},
                signature_version=botocore.UNSIGNED,
            ),
            endpoint_url=self.endpoint_url,
        )

        # Register the botocore event handler for adding custom query params
        # to S3 HEAD and GET requests
        self.s3_client.meta.events.register(
            'before-call.s3.HeadObject', _add_custom_query_param
        )
        self.s3_client.meta.events.register(
            'before-call.s3.GetObject', _add_custom_query_param
        )


    @staticmethod
    def construct_url_with_query_params(url, query_params: dict) -> Optional[str]:
        req = PreparedRequest()
        req.prepare_url(url, query_params)
        return req.url

    def construct_query_params_monitoring(self,username: Optional[str] = None) -> dict:
        query_params = {'x-cop-client': self.client_name}
        if username:
            query_params['x-cop-user'] = username
        return query_params

    def close(self):
        pass  # with urllib, the connection is automatically closed when the object is disposed by the GC

    @staticmethod
    def parse_s3_url(data_path: str) -> Tuple[str, str]:
        match = re.search(r'^(s3):\/\/([\w\-\.]+)\/(.*)', data_path)
        if match:
            match.group(1)
            path = match.group(3)
            bucket = match.group(2)
            return bucket, path
        else:
            raise Exception(f'Invalid s3 data path: {data_path}')

    def download(self, bucket, path, localpath):

        self.s3_client.download_file(
            bucket,
            path,
            localpath,
        )

        return True

    def check(self):
        #test = self.opener.open('https://' + self.server)
        #test.close()
        return True

    def getFilename(self):
        return self.filename

    def getFilemtime(self):
        return self.mtime

    def getFilesize(self):
        return self.filesize


class S3Provider(AbstractProvider):
    """
    Class S3Provider
    """

    def __init__(self, provider_id, provider_type, job_path, global_config):
        """
        S3Provider initialization method
        """
        AbstractProvider.__init__(self, provider_id, provider_type, job_path, global_config)
        self.__server = None
        self.__username = None
        self.__password = None
        self.__session = None
        self.__port = None
        self.__options = {}
        self.__file_mtime = None
        self.__file_size = None

    server = property(lambda self: self.__server, None, None, '')
    username = property(lambda self: self.__username, None, None, '')
    password = property(lambda self: self.__password, None, None, '')
    session = property(lambda self: self.__session, None, None, '')

    def setSpecificState(self):
        self._jobstate['Server'] = self.__server
        self._jobstate['Username'] = self.__username
        self._jobstate['Password'] = self.__password

    def checkAvailability(self):
        # self._log.debug("Checking availability for server : %s"%(self.__server))
        self.__errormsg = None
        ret = True
        session = None

        try:
            session = self.getSession()

            if not session.check():
                ret = False
        except Exception as msg:
            self._log.warning('Connection error : [%s]' % msg)
            self.__errormsg = str(msg)
            ret = False

        try:
            self.closeSession(session)
        except Exception as msg:
            self.__errormsg = str(msg)
            ret = False

        return ret

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        timeout = timeout_
        if timeout_ is None:
            timeout = self.getDefaultTimeout()
        self.__session = S3Session(self.__server, self.__username, self.__password,
                                   self.__options, timeout)
        return self.__session

    def closeSession(self, session):
        if self.__session is not None:
            self.__session.close()

    def getFile(self, session, remotepath, localpath):

        self.__errormsg = None
        tmp_localpath = localpath + TMP_EXT

        try:

            bucket, path = session.parse_s3_url(remotepath)

            response = session.download(bucket, path, tmp_localpath)
            if not response:
                self._log.warning(
                    'download : incorrect file size (URL = %s)' % remotepath)
                self.__errormsg = 'incorrect file size'
                return None

        except Exception as msg:
            self._log.warning(
                'Connection : Exception while download binary : %s (URL = %s)' %
                (msg, remotepath), exc_info=True)
            self.__errormsg = str(msg)
            return None

        try:
            os.rename(tmp_localpath, localpath)
        except OSError as msg:
            self._log.warning('Cannot rename %s --> %s (%s)' %
                              (tmp_localpath, localpath, msg))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(localpath):
            return File(localpath)

        return None

    def getFilenameAndUrl(self, remotepath):
        return remotepath.split()

    def putFile(self, session, localpath, remotepath):
        raise RuntimeError('This method must be overloaded')

    def setServer(self, server):
        self.__server = server

    def setUsername(self, username):
        self.__username = username

    def setPassword(self, password):
        self.__password = password

    def setPort(self, port):
        if port is not None:
            self.__port = port

    def setOptions(self, options_dict):
        self.__options = options_dict

    def getmtime(self):
        return self.__file_mtime

    def getsize(self):
        return self.__file_size
