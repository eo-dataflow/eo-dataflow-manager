#
# -*- coding: UTF-8 -*-
#
"""LocalpathProvider module contain the L{LocalpathProvider} class
"""

__docformat__ = 'epytext'

import os
import shutil

from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import AbstractProvider


class LocalpathSession(object):
    """ Empty object, to simulate a session (methods must be overloaded)
    """


class LocalpathProvider(AbstractProvider):
    """
    Class LocalpathProvider
    """

    def __init__(self, id, type, job_path, globalConfig):
        """
        LocalpathProvider initialization method
        """
        AbstractProvider.__init__(self, id, type, job_path, globalConfig)

    def setSpecificState(self):
        pass

    def checkAvailability(self):
        self.__errormsg = None
        return True

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        session = LocalpathSession()
        return session

    def closeSession(self, session):
        pass

    def getFile(self, session, remotepath, localpath):
        self.__errormsg = None
        tmp_localpath = localpath + '.tmp'
        try:
            shutil.copy(remotepath, tmp_localpath)
        except Exception as msg:
            self._log.warning(
                'LocalpathProvider::getFile : Exception while copying : %s (file = %s)' %
                (msg, remotepath))
            self.__errormsg = str(msg)
            return None

        # move atomique, pour prise en compte du fichier complet par un systeme
        # externe eventuellement.
        try:
            os.rename(tmp_localpath, localpath)
        except OSError as msg:
            self._log.warning(
                'Cannot rename %s --> %s (%s)' %
                (tmp_localpath, localpath, msg))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(localpath):
            return File(localpath)

        return None

    def putFile(self, session, localpath, remotepath):
        self.__errormsg = None
        tmp_remotepath = remotepath + '.tmp'
        try:
            shutil.copy(localpath, tmp_remotepath)
        except Exception as msg:
            self._log.warning(
                'LocalpathProvider::putFile : Exception while copying : %s (file = %s)' %
                (msg, localpath))
            self.__errormsg = str(msg)
            return None

        # move atomique, pour prise en compte du fichier complet par un systeme
        # externe eventuellement.
        try:
            os.rename(tmp_remotepath, remotepath)
        except OSError as msg:
            self._log.warning(
                'Cannot rename %s --> %s (%s)' %
                (tmp_remotepath, remotepath, msg))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(remotepath):
            return File(remotepath)

        return None
