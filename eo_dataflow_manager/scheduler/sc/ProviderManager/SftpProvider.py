#
# -*- coding: UTF-8 -*-
#
"""FtpProvider module contain the L{FtpProvider} class
"""

__docformat__ = 'epytext'

import os
import socket

import pysftp

from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import AbstractProvider

DEFAULT_CONNECTION_PORT = 22


class SftpProvider(AbstractProvider):
    """
    Class SftpProvider
    """

    def __init__(self, id, type, job_path, globalConfig):
        """
        FtpProvider initialization method
        """
        AbstractProvider.__init__(self, id, type, job_path, globalConfig)
        self.__server = None
        self.__username = None
        self.__password = None
        self.__port = DEFAULT_CONNECTION_PORT

        self.__session = None

    server = property(lambda self: self.__server, None, None, None)
    username = property(lambda self: self.__username, None, None, None)
    password = property(lambda self: self.__password, None, None, None)
    port = property(lambda self: self.__port, None, None, None)

    def setSpecificState(self):
        self._jobstate['Server'] = self.__server
        self._jobstate['Username'] = self.__username
        self._jobstate['Password'] = self.__password
        self._jobstate['Port'] = self.__port

    def checkAvailability(self):
        #self._log.debug("Checking availability for server : %s"%(self.__server))
        self.__errormsg = None

        session = self.getSession()
        if session is None:
            return False

        try:
            self.closeSession(session)
        except Exception as msg:
            self.__errormsg = str(msg)

        return True

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        self.__errormsg = None
        session = None
        timeout = timeout_
        if timeout_ is None:
            timeout = self.getDefaultTimeout()
        try:
            cnopts = pysftp.CnOpts()
            cnopts.hostkeys = None
            cinfo = {
                'host': self.server,
                'username': self.username,
                'password': self.password,
                'port': self.port,
                'cnopts': cnopts,
                'log': False}
            session = pysftp.Connection(**cinfo)
            session.timeout = timeout
        except Exception as msg:
            self._log.info(
                'Connection error : cannot connect to remote server [exception : %s, (server=%s, username=%s])' %
                (msg, self.__server, self.__username))
            session = None
            self.__errormsg = str(msg)
        return session

    def closeSession(self, session):
        if session is None:
            return

        try:
            session.close()
        except socket.error as msg:
            self._log.warning('Connection error : [socket.error : %s]' % (msg))

    def getFile(self, session, remotepath, localpath):
        self.__errormsg = None
        tmp_localpath = localpath + '.tmp'
        try:
            session.get(remotepath, tmp_localpath, preserve_mtime=True)
        except Exception as msg:
            self._log.warning(
                'Connection : Exception while retrieving binary : %s (file = %s)' %
                (msg, remotepath))
            self.__errormsg = str(msg)
            return None

        # move atomique, pour prise en compte du fichier complet par un systeme
        # externe eventuellement.
        try:
            os.rename(tmp_localpath, localpath)
        except OSError as msg:
            self._log.warning(
                'Cannot rename %s --> %s (%s)' %
                (tmp_localpath, localpath, msg))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(localpath):
            return File(localpath)

        return None

    def putFile(self, session, localpath, remotepath):
        raise RuntimeError('This method must be overloaded')

    def setServer(self, server):
        self.__server = server

    def setUsername(self, username):
        self.__username = username

    def setPassword(self, password):
        self.__password = password

    def setPort(self, port):
        if port is not None:
            self.__port = port

#        self._log.debug("FtpProvider::remoteLaunch on %s spawn : remoteShell='%s', cmd='%s'",
#            self.name,
#            self.remoteShell,
#            ' '.join(cmd_list))

    def __closeSession(self):
        self.closeSession(self.__session)
        self.__session = None
