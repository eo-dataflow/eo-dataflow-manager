#
# -*- coding: UTF-8 -*-
#
"""HttpProvider module contain the L{HttpProvider} class
"""

__docformat__ = 'epytext'

import os
import socket
import urllib.error
import urllib.parse
import urllib.request

from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import AbstractProvider


class HttpSession(object):
    def __init__(self, server, user, pwd, timeout_=None):
        #self.auth_handler = None
        self.server = server
        self.user = user
        self.pwd = pwd
        self.timeout = timeout_

        socket.setdefaulttimeout(self.timeout)

        if self.user is not None and self.user != 'anonymous':
            self.opener = None
            raise Exception('Http authenticate not supported yet')
            #self.auth_handler = urllib.request.HTTPBasicAuthHandler()
            #self.auth_handler.add_password(realm=None, uri=server, user=self.user, passwd=self.pwd)
            #opener = urllib.request.build_opener(self.auth_handler)
            # urllib.request.install_opener(opener)
        else:
            self.opener = urllib.request.FancyURLopener({})

    def close(self):
        pass  # with urllib, the connection is automatically closed when the object is disposed by the GC

    def retrieve(self, relativepath, localpath):
        #urllib.request.urlretrieve(url, localpath)
        url = 'http://' + self.server + '/' + relativepath

        if (self.opener is not None):
            self.opener.retrieve(url, localpath)


class HttpProvider(AbstractProvider):
    """
    Class HttpProvider
    """

    def __init__(self, id, type, job_path, globalConfig):
        """
        HttpProvider initialization method
        """
        AbstractProvider.__init__(self, id, type, job_path, globalConfig)
        self.__server = None
        self.__username = None
        self.__password = None
        self.__session = None

    server = property(lambda self: self.__server, None, None, None)
    username = property(lambda self: self.__username, None, None, None)
    password = property(lambda self: self.__password, None, None, None)
    session = property(lambda self: self.__session, None, None, None)

    def setSpecificState(self):
        self._jobstate['Server'] = self.__server
        self._jobstate['Username'] = self.__username
        self._jobstate['Password'] = self.__password

    def checkAvailability(self):
        #self._log.debug("Checking availability for server : %s"%(self.__server))
        self.__errormsg = None
        ret = True

        try:
            self.__session = self.getSession()
            if self.__session is None:
                ret = False
        except Exception as msg:
            self._log.warning('Connection error : [%s]' % (msg))
            ret = False

        try:
            self.__closeSession()
        except Exception:
            ret = False

        return ret

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        if timeout_ is None:
            self.getDefaultTimeout()
        self.__session = HttpSession(
            self.__server,
            self.__username,
            self.__password,
            timeout_)
        return self.__session

    def closeSession(self, session):
        if (self.__session is not None):
            self.__session.close()

    def getFile(self, session, remotepath, localpath):
        self.__errormsg = None
        tmp_localpath = localpath + '.tmp'
        try:
            session.retrieve(remotepath, tmp_localpath)
        except Exception as msg:
            self._log.warning(
                'Connection : Exception while retrieving binary : %s (file = %s)' %
                (msg, remotepath))
            return None

        # move atomique, pour prise en compte du fichier complet par un systeme
        # externe eventuellement.
        try:
            os.rename(tmp_localpath, localpath)
        except OSError as msg:
            self._log.warning(
                'Cannot rename %s --> %s (%s)' %
                (tmp_localpath, localpath, msg))
            return None

        if os.path.exists(localpath):
            return File(localpath)

        return None

    def putFile(self, session, localpath, remotepath):
        raise RuntimeError('This method must be overloaded')

    def setServer(self, server):
        self.__server = server

    def setUsername(self, username):
        self.__username = username

    def setPassword(self, password):
        self.__password = password

    def setPort(self, port):
        if port is not None:
            self.__port = port

    def __closeSession(self):
        self.closeSession(self.__session)
        self.__session = None
