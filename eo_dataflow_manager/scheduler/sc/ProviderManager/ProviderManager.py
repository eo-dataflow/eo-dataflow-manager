#
# -*- coding: UTF-8 -*-
#
"""ProviderManager module contain the L{ProviderManager} class
"""

__docformat__ = 'epytext'

import errno
import logging
import os.path

from six import with_metaclass

from eo_dataflow_manager.scheduler.com.ext.XMLReader import XMLReader
from eo_dataflow_manager.scheduler.sc import Controller
from eo_dataflow_manager.scheduler.sc.ProviderManager import AbstractProvider
from eo_dataflow_manager.scheduler.sc.ProviderManager.ProviderFactory import ProviderFactory


class HMSingleton(type):
    def __init__(self, name, bases, dic):
        super(HMSingleton, self).__init__(name, bases, dic)
        self.instance = None

    def __call__(self, *args, **kw):
        if self.instance is None:
            self.instance = super(HMSingleton, self).__call__(*args, **kw)
        return self.instance


class ProviderManager(with_metaclass(HMSingleton)):
    """This class  implements the management of hosts usable by the System Controller,
    and manage a load balancing in the Host selection.
    """

    def __init__(self, globalConfig, loggerName=None):
        """ProviderManager instantiation method

        @param loggerName: Name of text logger to use
        @type loggerName: C{str}
        """
        if loggerName is None:
            loggerName = Controller.SCHD_LOGGER_NAME
        self.__providerConfig = {}  # key=host name value=xxxHost instance
        self.__preEnvScript = ''
        self.__pidInputFile = {}  # dict contain list of input file for a pid
        self.__inputFilePid = {}  # dict contain list of pid for an input file
        self.__pidRunner = {}  # dict contain the launched Runner for a pid
        self.__runnerPid = {}  # dict contain list of pid for a runner

        self.__job_path = None

        self.__globalConfig = globalConfig

        # get logger information
        if loggerName:
            self._log = logging.getLogger(loggerName)

    def getProviderConfig(self):
        return self.__providerConfig

    def setJobPath(self, job_path):
        self.__job_path = job_path

    def read(self, hostConfig):
        """Read and initiate the list of Host from the XML configuration subtree

        @param hostConfig: filename of the xml file contain the L{ProviderManager} configuration
        @type hostConfig: C{str}
        """

        if not os.path.isfile(hostConfig):
            self._log.error("can't find the host file configuration (%s)" % hostConfig)
            raise IOError("can't find the host file configuration (%s)" % hostConfig)

        # Get a XMLReader
        xr = XMLReader(self._log.name)
        # Open the xml tree
        xmltree = xr.open(hostConfig)
        # get the root node
        rootNode = xr.getRootNode(logging.ERROR, xmltree, 'sc_providers_config')

        ### Begin of reading of Host configuration ###
        self._log.debug('  BEGIN: reading of the Host configuration')

        if rootNode.tag != 'sc_providers_config':
            raise IOError("invalid node, '%s' instead of 'sc_providers_config'" % rootNode.tag)

        # reset config
        self.__providerConfig = {}

        # get the list of host !
        for tmpNode in xr.getAllSubNode(rootNode, 'provider'):
            options = {}
            provider_type = xr.getAttributeValue(logging.INFO, tmpNode, '{http://www.w3.org/2001/XMLSchema-instance}type')
            provider_id = xr.getAttributeValue(logging.INFO, tmpNode, 'id')

            # load the provider
            # Call the factory to build the good Host object for the system
            try:
                provider = ProviderFactory(provider_type, provider_id, self.__job_path, self.__globalConfig)
            except Exception as msg:
                self._log.error("No plugin for Provider '%s' (%s)!" % (provider_id, str(msg)))
                continue

            # load the configuration for the concerned provider_type
            if provider_type == 'Ftp':
                server = xr.getSubNodeValue(logging.ERROR, tmpNode, 'server')
                username = xr.getSubNodeValue(logging.ERROR, tmpNode, 'username')
                password = xr.getSubNodeValue(logging.ERROR, tmpNode, 'password')

                # optionnal parameter : if undefined, take the AbstractProvider default value (DEFAULT_CONNECTION_PORT)
                port = xr.getSubNodeValue(logging.ERROR, tmpNode, 'port', '')
                if port != '' and port is not None:
                    port = int(port)
                else:
                    port = None

                provider.setServer(server)
                provider.setUsername(username)
                provider.setPassword(password)
                provider.setPort(port)

            elif provider_type == 'Sftp':
                server = xr.getSubNodeValue(logging.ERROR, tmpNode, 'server')
                username = xr.getSubNodeValue(logging.ERROR, tmpNode, 'username')
                password = xr.getSubNodeValue(logging.ERROR, tmpNode, 'password')
                port = xr.getSubNodeValue(logging.ERROR, tmpNode, 'port', '')
                if port != '' and port is not None:
                    port = int(port)
                else:
                    port = None

                provider.setServer(server)
                provider.setUsername(username)
                provider.setPassword(password)
                provider.setPort(port)

            elif provider_type[:4] == 'Ftps':
                server = xr.getSubNodeValue(logging.ERROR, tmpNode, 'server')
                username = xr.getSubNodeValue(logging.ERROR, tmpNode, 'username')
                password = xr.getSubNodeValue(logging.ERROR, tmpNode, 'password')
                port = xr.getSubNodeValue(logging.ERROR, tmpNode, 'port', '')
                if port != '' and port is not None:
                    port = int(port)
                else:
                    port = None
                provider.setServer(server)
                provider.setUsername(username)
                provider.setPassword(password)
                provider.setPort(port)

            elif provider_type.find('Http') >= 0 or provider_type == 'Eodms':
                server = xr.getSubNodeValue(logging.ERROR, tmpNode, 'server')
                username = xr.getSubNodeValue(logging.ERROR, tmpNode, 'username')
                password = xr.getSubNodeValue(logging.ERROR, tmpNode, 'password')
                protocole_options = xr.getSubNodeValue(logging.ERROR, tmpNode, 'options', '')

                for option in protocole_options.split(','):
                    if option != '':
                        key, value = option.split('=')
                        if key == 'CheckCertificate' and value == 'False':
                            value = False
                        options[key] = value

                provider.setServer(server)
                provider.setUsername(username)
                provider.setPassword(password)
                provider.setOption(options)


            elif provider_type.find('Webdav') >= 0:
                server = xr.getSubNodeValue(logging.ERROR, tmpNode, 'server')
                username = xr.getSubNodeValue(logging.ERROR, tmpNode, 'username')
                password = xr.getSubNodeValue(logging.ERROR, tmpNode, 'password')
                port = xr.getSubNodeValue(logging.ERROR, tmpNode, 'port', '')

                provider.setServer(server)
                provider.setUsername(username)
                provider.setPassword(password)
                if port != '' and port is not None:
                    provider.setPort(int(port))

            elif provider_type.find('S3') >= 0:
                server = xr.getSubNodeValue(logging.ERROR, tmpNode, 'server')
                username = xr.getSubNodeValue(logging.ERROR, tmpNode, 'username')
                password = xr.getSubNodeValue(logging.ERROR, tmpNode, 'password')
                protocole_options = xr.getSubNodeValue(logging.ERROR, tmpNode, 'options', '')

                for option in protocole_options.split(','):
                    if option != '':
                        key, value = option.split('=')
                        if key == 'CheckCertificate' and value == 'False':
                            value = False
                        options[key] = value

                provider.setServer(server)
                provider.setUsername(username)
                provider.setPassword(password)
                provider.setOptions(options)

            elif provider_type == 'Localpath':
                # no configuration for this provider
                pass

            # Append the host in host config dictionnary
            self.__providerConfig[provider.provID] = provider
            self._log.debug("    Provider '%s' (type %s) is appended to the ProviderManager" % (provider.provID, provider_type))
            # Try to connect to the provider
            try:
                isAvailable = provider.isAvailable()
            except Exception as e:
                self._log.exception("    Exception while checking provider availability for '%s' : %s" % (provider.provID, e))
                isAvailable = False

            if isAvailable is False:
                self._log.warning("    Provider '%s' is not available !" % (provider.provID))
            else:
                self._log.info("    Provider '%s' is available." % (provider.provID))

        ### End of reading of Host configuration ###
        self._log.debug('  END  : reading of the Host configuration')

    def configSummary(self, level):
        """Write on text log a summary of the L{ProviderManager} configuration

        @param level: log level to write information (see
        U{logging module documentation<http://docs.python.org/lib/module-logging.html>}
        for good value)
        @type level: C{int}
        """
        self._log.log(level, '-' * 100)
        self._log.log(level, '  ProviderManager configuration:')
        self._log.log(level, '  List of registered providers :')
        self._log.log(
            level,
            '    |--------------------------------------------|--------------------------------|')
        self._log.log(
            level,
            '    | ID                                         | Type                           |')
        self._log.log(
            level,
            '    |--------------------------------------------|--------------------------------|')
        for p in list(self.__providerConfig.values()):
            self._log.log(level, '    | %-42s | %-30s |' %
                          (p.provID, str(type(p)).split('.')[-1][:-2]))
        self._log.log(
            level,
            '    |--------------------------------------------|--------------------------------|')

    def hasProvider(self, provider_ref):
        """Return true if the hostname is know

        @rtype: C{bool}
        """
        return provider_ref in self.__providerConfig

    def getProvider(self, provider_ref):
        if self.hasProvider(provider_ref):
            return self.__providerConfig[provider_ref]

    def checkProvidersAvailability(self):
        self._log.info('Providers availability :')
        for p in list(self.__providerConfig.values()):

            try:
                p.setState(
                    [AbstractProvider.STATE_TESTING_AVAILABILITY, 'checkProvidersAvailability'])
                status = p.checkAvailability()
            except Exception as e:
                self._log.error(
                    "  - '%s' : check availability error : %s" %
                    (p.provID, str(e)))
                status = False
            if status:
                self._log.info("  - '%s' is available" % (p.provID))
                p.setState([AbstractProvider.STATE_AVAILABLE,
                            'checkProvidersAvailability'])
            else:
                self._log.info("  - '%s' is not available" % (p.provID))
                p.setState([AbstractProvider.STATE_NOT_AVAILABLE,
                            'checkProvidersAvailability'])

    def cleanEndSubProcess(self, waitMode=os.WNOHANG):
        """
        """
        listInputFile = []
        runner = None

        loopFlag = True
        while loopFlag:
            try:
                ret = os.waitpid(-1, waitMode)
                if ret == (0, 0):
                    loopFlag = False
                else:
                    nbr_processed_files = 0
                    if ret[0] in self.__pidInputFile:
                        for f in self.__pidInputFile[ret[0]]:
                            if f in self.__inputFilePid and ret[0] in self.__inputFilePid[f][1]:
                                self.__inputFilePid[f][1].remove(ret[0])
                                if len(self.__inputFilePid[f][1]) == 0:
                                    listInputFile.append(f)
                                    nbr_processed_files += 1
                                    del self.__inputFilePid[f]
                        del self.__pidInputFile[ret[0]]
                    else:
                        # Ne devrait jamais arriver !,
                        self._log.exception(
                            "ProviderManager::cleanEndSubProcess : No InputFile for PID '%s'" %
                            (ret[0]))

                    if ret[0] in self.__pidRunner:
                        runner = self.__pidRunner[ret[0]]
                        if runner in self.__runnerPid and ret[0] in self.__runnerPid[runner]:

                            assert 0, 'Dead code'
                            # Controller.SCHEDULER.getProcessingChain(
                            #     runner.processingChainName).runningFilesCurrentNb -= nbr_processed_files
                            # self._log.debug(
                            #     "ProviderManager::cleanEndSubProcess : cleaned files = %s, runningFilesCurrentNb = %s  [processingChainName=%s]" %
                            #     (nbr_processed_files, Controller.SCHEDULER.getProcessingChain(
                            #         runner.processingChainName).runningFilesCurrentNb, runner.processingChainName))

                            # self.__runnerPid[runner].remove(ret[0])
                            # if not self.__runnerPid[runner]:
                            #     del self.__runnerPid[runner]
                    else:
                        # Ne devrait jamais arriver !,
                        self._log.exception(
                            "ProviderManager::cleanEndSubProcess : No Runner for PID '%s'" %
                            (ret[0]))

            except OSError as e:
                if e.errno == errno.ECHILD:
                    # comportement normal a chaque iteration : '[Errno 10] No
                    # child processes'
                    pass
                else:
                    self._log.exception(
                        "ProviderManager::cleanEndSubProcess : OSError while removing pid file : '%s'",
                        str(e))
                loopFlag = False
            except Exception as e:
                self._log.exception(
                    "ProviderManager::cleanEndSubProcess : exception while removing pid file : '%s'",
                    str(e))
                loopFlag = False

        return listInputFile
