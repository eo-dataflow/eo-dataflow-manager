#
# -*- coding: UTF-8 -*-
#
"""LocalpointerProvider module contain the L{LocalpointerProvider} class
"""

__docformat__ = 'epytext'

import os

from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc.ProviderManager.LocalpathProvider import LocalpathProvider


class LocalpointerProvider(LocalpathProvider):
    """
    Class LocalpointerProvider
    """

    def __init__(self, id, type, job_path, globalConfig):
        """
        LocalpointerProvider initialization method
        """
        LocalpathProvider.__init__(self, id, type, job_path, globalConfig)

    def setSpecificState(self):
        pass

    def getFile(self, session, remotepath, localpath):
        self.__errormsg = None
        tmp_localpath = localpath
        self._log.debug(
            'LocalpointerProvider::getFile symbolic link  %s (file = %s)' %
            (tmp_localpath, remotepath))
        try:
            if os.path.lexists(tmp_localpath):
                os.remove(tmp_localpath)
            os.symlink(remotepath, tmp_localpath)
        except Exception as msg:
            self._log.warning(
                'LocalpointerProvider::getFile : Exception while symbolic linking  : %s (file = %s)' %
                (msg, remotepath))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(localpath):
            return File(localpath)

        return None
