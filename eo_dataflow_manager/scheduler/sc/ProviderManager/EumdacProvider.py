#
# -*- coding: UTF-8 -*-
#
"""EumdacProvider module contain the EumdacProvider class and classes to handle token authentication
"""

__docformat__ = 'epytext'

import logging
import os
import time
import zipfile
from datetime import datetime
from typing import Iterable, NamedTuple, Optional

import requests
from requests.auth import AuthBase, HTTPBasicAuth

from eo_dataflow_manager.scheduler.com.ext.XMLReader import XMLReader
from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc import Controller
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import AbstractProvider

DOWNLOAD_CHUNK_SIZE = 100000
TMP_EXT = '.tmp'


class Credentials(NamedTuple):
    consumer_key: str
    consumer_secret: str


class HTTPBearerAuth(AuthBase):
    """Attaches HTTP Bearer Authentication to the given Request object.

    Attributes:
        token: Bearer token

    Arguments:
        token: Bearer token
    """

    def __init__(self, token: str) -> None:
        self.token = token

    def __call__(self, request: requests.PreparedRequest) -> requests.PreparedRequest:
        request.headers['authorization'] = f'Bearer {self.token}'
        return request


class AccessToken:
    """Eumetsat API access token.

    Used to handle requesting of API tokens and there refreshment after expiration.

    Attributes:
        request_margin: seconds before expiration to start re-requesting
        expires_in: seconds before token expiration
        access_token: the value of the access token

    Arguments:
        cache (default: true): if true, use data from previous request until expiration
    """

    request_margin: int = 15  # seconds
    _expiration: int = 0
    _access_token: str = ''

    credentials: Credentials
    token_url: str
    cache: bool

    def __init__(
        self, credentials: Iterable[str], apis_endpoint: str, cache: bool = True,
    ) -> None:
        self.credentials = Credentials(*credentials)
        self.token_url = apis_endpoint + '/token'
        self.cache = cache

    def __str__(self) -> str:
        return self._access_token

    @property
    def expiration(self) -> datetime:
        """Expiration of the current token string"""
        return datetime.fromtimestamp(self._expiration)

    @property
    def access_token(self) -> str:
        """Token string"""
        expires_in = self._expiration - time.time()
        if not self.cache or expires_in < self.request_margin:
            self._update_token_data()
        return self._access_token

    @property
    def auth(self) -> AuthBase:
        return HTTPBearerAuth(self.access_token)

    def _update_token_data(self) -> None:
        auth = HTTPBasicAuth(*self.credentials)
        now = time.time()
        response = requests.post(
            self.token_url, auth=auth, data={'grant_type': 'client_credentials'}
        )
        response.raise_for_status()
        token_data = response.json()
        self._expiration = now + token_data['expires_in']
        self._access_token = token_data['access_token']


class EumdacProduct:
    """
    Class for storing Eumdac product metadata :
        data_size : size of the data file
        data_path : path (inside the archive product) of the data file
        manifest_size : size of the xfdu manifest file
        data_path : path (inside the archive product) of the xfdu manifest file
        mtime : modification time of the data file
        checksum : checksum of the data file
    """
    data_size: int
    data_path: str
    manifest_size: int
    manifest_path: str
    mtime: datetime
    checksum: str

    def __init__(
            self, data_size: int, data_path: str, manifest_path: str, manifest_size: int
    ) -> None:
        self.data_size = data_size
        self.data_path = data_path
        self.manifest_path = manifest_path
        self.manifest_size = manifest_size
        self.checksum = None
        self.mtime = None


class EumdacProvider(AbstractProvider):
    """
    Class EumdacProvider for downloading files from EUMETSAT DATASTORE
    """

    def __init__(self, id, type, job_path, globalConfig):
        """
        EumdacProvider initialization method
        """
        AbstractProvider.__init__(self, id, type, job_path, globalConfig)
        self.__server = None
        self.__consumer_key = None
        self.__consumer_secret = None
        self.__access_token = None
        self.__option = None
        self.__file_mtime = None
        self.__file_size = None

    server = property(lambda self: self.__server, None, None, None)
    username = property(lambda self: self.__consumer_key, None, None, None)
    password = property(lambda self: self.__consumer_secret, None, None, None)
    access_token = property(lambda self: self.__access_token, None, None, None)

    def setSpecificState(self):
        self._jobstate['Server'] = self.__server
        self._jobstate['Username'] = self.__consumer_key
        self._jobstate['Password'] = self.__consumer_secret

    def checkAvailability(self):
        #self._log.debug("Checking availability for server : %s"%(self.__server))
        self.__errormsg = None
        ret = True
        session = None
        try:
            session = self.getSession()
        except Exception as msg:
            self._log.warning('Connection error : [%s]' % (msg))
            self.__errormsg = str(msg)
            ret = False
        try:
            session.auth
        except Exception as msg:
            self._log.warning('Connection error : [%s]' % (msg))
            self.__errormsg = str(msg)
            ret = False

        try:
            self.closeSession(session)
        except Exception as msg:
            self.__errormsg = str(msg)
            ret = False

        return ret

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        if timeout_ is not None:
            self.set_timeout(timeout_)

        credentials = (self.__consumer_key, self.__consumer_secret)
        apis_endpoint = 'https://' + self.server
        self.__access_token = AccessToken(credentials=credentials, apis_endpoint=apis_endpoint)

        return self.__access_token

    def closeSession(self, session):
        pass

    def open_manifest(self,
                      product_url: str,
                      api_token: AccessToken
                      ) -> bytes:
        """Opens a stream to download the product manifest.

        Note:
            A Data Store product refers to a zip archive containing the data.

        Arguments:
            api_token: the Eumetsat api access token.
            product_url: the product URL.
        Returns:
            the manifest data
        """
        entry = 'manifest.xml'
        product_url += '/entry'
        params = {'name': entry}

        try:
            with requests.get(product_url, auth=api_token.auth,
                              params=params, stream=True, timeout=self.getDefaultTimeout()) as response:
                response.raise_for_status()
                response.raw.decode_content = True
                return response.content
        except Exception:
            return bytearray()

    def read_manifest(self,
                      manifest_data: bytes
                      ) -> EumdacProduct:
        """Read the product manifest.
        Arguments:
            manifest_data: the manifest file content
        Returns:
            the product metadata containing NetCDF file and xfdu manifest sizes and paths
        """

        # Read the size and the data file name from the manifest
        xr = XMLReader(Controller.SCHD_LOGGER_NAME)
        try:
            rootNode = xr.read_from_string(manifest_data)
        except Exception:
            raise IOError('Manifest data reading failure')

        nodes = xr.getAllSubNode(xr.getSubNode(
            logging.ERROR, rootNode, 'dataSection'), 'dataObject')

        # Search the node containing NetCDF file and xfdu manifest sizes and paths
        data_path = []
        data_size = 0
        manifest_path = None
        manifest_size = 0
        for node in nodes:
            format = xr.getSubNodeValue(level=logging.ERROR, node=node, subnodeName='format')
            fileType = xr.getSubNodeValue(level=logging.ERROR, node=node, subnodeName='fileType')
            size = int(xr.getSubNodeValue(level=logging.ERROR, node=node, subnodeName='size'))
            path = xr.getSubNodeValue(level=logging.ERROR, node=node, subnodeName='path')
            if format == 'XML' and fileType == 'Data':
                manifest_path = path
                manifest_size = size
            elif fileType == 'Data':  # NetDCF / Native / GRIB2...
                data_path.append(path)
                data_size += size
        if len(data_path) == 0 or data_size == 0:
            raise IOError('Product not found in the manifest')
        if len(data_path) > 1:
            if manifest_path is None or manifest_size is None:
                raise IOError('Product manifest not found in the manifest')

        eumdac_product = EumdacProduct(data_size=data_size, data_path=data_path,
                                       manifest_size=manifest_size, manifest_path=manifest_path)

        return eumdac_product

    def open_product_manifest(self,
                              product_url: str,
                              eumdac_product: EumdacProduct,
                              api_token: AccessToken
                              ) -> bytes:
        """Opens a stream to download the product xfdu manifest.

        Note:
            A Data Store product refers to a zip archive containing the data.

        Arguments:
            api_token: the Eumetsat api access token.
            product_url: the product URL.
        Returns:
            the xfdu manifest data
        """
        product_url += '/entry'
        params = {'name': eumdac_product.manifest_path}

        try:
            with requests.get(product_url, auth=api_token.auth,
                              params=params, stream=True, timeout=self.getDefaultTimeout()) as response:
                response.raise_for_status()
                response.raw.decode_content = True
                return response.content
        except Exception:
            return bytearray()

    def read_product_manifest(self,
                              manifest_data: bytes,
                              eumdac_product: EumdacProduct
                              ) -> EumdacProduct:
        """Read the product xfdumanifest.

        Arguments:
            manifest_data: the xfdumanifest file content
            eumdac_product: the Eumdac product metadata containing the mtime and the checksum of the data file.
        """

        # Read the modification time and the checksum from the xfdumanifest
        xr = XMLReader(Controller.SCHD_LOGGER_NAME)
        try:
            rootNode = xr.read_from_string(manifest_data)
        except Exception:
            raise IOError('Xfdumanifest data reading failure')

        # Search the node containing the checksum of the data file
        nodes = xr.getAllSubNode(xr.getSubNode(
            logging.ERROR, rootNode, 'dataObjectSection'), 'dataObject')

        # <byteStream mimeType="application/x-netcdf" size="24714215">
        byteStreamNode = xr.getSubNode(level=logging.ERROR, node=nodes[0], subnodeName='byteStream')
        checksum = xr.getSubNodeValue(level=logging.ERROR, node=byteStreamNode, subnodeName='checksum')
        if checksum is None:
            raise IOError('Checksum not found in the Xfdumanifest')

        eumdac_product.checksum = checksum

        # Search the node containing the modification time
        #<sentinel3:creationTime>20220505T125854</sentinel3:creationTime>
        mtime = None
        nodes = rootNode.findall(".//*[@ID='generalProductInformation']/metadataWrap/xmlData")
        general_product_information = xr.getAllSubNode(nodes[0])[0]
        nodename = xr.getNodeTagName(general_product_information).split('}')
        if len(nodename) > 0:
            namespace = ''
            if len(nodename) > 1:
                namespace = nodename[0] + '}'
            mtime = xr.getSubNodeValue(level=logging.ERROR,
                                       node=general_product_information,
                                       subnodeName=namespace + 'creationTime')
        if mtime is None:
            raise IOError('Modification time not found in the Xfdumanifest')

        eumdac_product.mtime = datetime.strptime(mtime, '%Y%m%dT%H%M%S')

        return eumdac_product

    def download_zipped_product(self,
                         product_url: str,
                         eumdac_product: EumdacProduct,
                         local_path: str,
                         api_token: AccessToken
                         ):
        """Download the product zip.

        Note:
            A Data Store product refers to a zip archive containing the data.

        Arguments:
            local_path: The local path to the file.
            eumdac_product: The zipped product to download.
            api_token: The Eumetsat api access token.
            product_url: The product URL.
        Returns:
            True if size of archive file is equal to size in manifest (data file + xfdu manifest).
        """

        # Get the chunks from the product URL
        with requests.get(product_url, auth=api_token.auth,
                          stream=False, timeout=self.getDefaultTimeout()) as response:
            response.raise_for_status()
            response.raw.decode_content = True
            chunks = response.iter_content(chunk_size=DOWNLOAD_CHUNK_SIZE)

        # Write them into local file
        zip_product = local_path+'.zip'
        with open(zip_product, 'wb') as downloadfile:
            total_read = 0
            for chunk in chunks:
                downloadfile.write(chunk)
                total_read += len(chunk)

        # Create the product archive (removing unwanted files from the original archive file)
        try:
            archive_size = EumdacProvider.create_product_archive(zip_product, eumdac_product, local_path)
        except:
            raise IOError('Create product archive failure')
        # Remove the downloaded zip
        f = File(zip_product)
        try:
            f.remove()
        except OSError:
            pass

        return archive_size == eumdac_product.data_size + eumdac_product.manifest_size

    @staticmethod
    def create_product_archive(zip_filename: str,
                               eumdac_product: EumdacProduct,
                               archive_filename: str
                               ) -> int:
        """ Create the archive from the product zip.

          Arguments:
              zip_filename: The path to the product zip
              eumdac_product: The list of products to save.
              archive_filename: The path to the archive to create.
          Returns:
              The total size (uncompressed) of the files in the archive.
          """
        archive = zipfile.ZipFile(archive_filename, 'w')
        product_size = 0
        with zipfile.ZipFile(zip_filename) as zip_file:
            for member in zip_file.infolist():
                if member.filename in eumdac_product.data_path \
                        or member.filename == eumdac_product.manifest_path:
                    # copy file in the new archive
                    buffer = zip_file.read(member.filename)
                    product_size += member.file_size
                    archive.writestr(member, buffer)
                    if eumdac_product.mtime is None:
                        eumdac_product.mtime = datetime(*member.date_time)
        archive.close()
        return product_size

    def getFile(self,
                session, remotepath, localpath) \
            -> Optional[File]:
        """
        Download a product file from Eumdac
        Args:
            session: the token for eumdac access
            remotepath: the url of the remote file
            localpath: the path to store the downloaded file
        Returns:
            The downloaded file.
        """

        self.__errormsg = None
        tmp_localpath = localpath + TMP_EXT

        # Get the manifest file
        manifest_file = self.open_manifest(product_url=remotepath, api_token=session)
        if len(manifest_file) == 0:
            self._log.warning(
                'Connection : Exception while downloading product manifest (URL: %s)' % remotepath, exc_info=True)
            return None

        try:
            eumdac_product = self.read_manifest(manifest_file)
        except IOError as msg:
            self._log.warning(
                'Connection : Exception while reading product manifest : %s (URL: %s)' % (msg, remotepath),
                exc_info=True)
            self.__errormsg = str(msg)
            return None

        if eumdac_product.manifest_path is not None:
            # if SAFE format : Get the xfdumanifest file
            xfdumanifest_file = self.open_product_manifest(product_url=remotepath, eumdac_product=eumdac_product,
                                                           api_token=session)
            if len(xfdumanifest_file) == 0:
                self._log.warning(
                    'Connection : Exception while downloading product xfdumanifest (URL: %s)' % remotepath, exc_info=True)
                return None

            try:
                eumdac_product = self.read_product_manifest(xfdumanifest_file, eumdac_product)
            except IOError as msg:
                self._log.warning(
                    'Connection : Exception while reading product xfdumanifest : %s (URL: %s)' % (msg, remotepath),
                    exc_info=True)
                self.__errormsg = str(msg)
                return None

        # Download the data file
        try:
            response = self.download_zipped_product(product_url=remotepath, eumdac_product=eumdac_product,
                                             local_path=tmp_localpath, api_token=session)
            if not response:
                self._log.warning(
                    'download : incorrect file size (URL = %s)' % (remotepath))
                self.__errormsg = 'incorrect file size'
                return None

        except Exception as msg:
            self._log.warning(
                'Connection : Exception while download binary : %s (URL = %s)' %
                (msg, remotepath), exc_info=True)
            self.__errormsg = str(msg)
            return None

        # move atomique, pour prise en compte du fichier complet par un systeme
        # externe eventuellement.
        target_file = localpath + '.zip'

        self.__file_mtime = eumdac_product.mtime
        self.__file_size = eumdac_product.data_size

        try:
            os.rename(tmp_localpath, target_file)
        except OSError as msg:
            self._log.warning('Cannot rename %s --> %s (%s)' %
                              (tmp_localpath, target_file, msg))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(target_file):
            return File(target_file)
        return None

    def getFilenameAndUrl(self, remotepath):
        return(remotepath.split())

    def putFile(self, session, localpath, remotepath):
        raise RuntimeError('This method must be overloaded')

    def setServer(self, server):
        self.__server = server

    def setUsername(self, consumer_key):
        self.__consumer_key = consumer_key

    def setPassword(self, consumer_secret):
        self.__consumer_secret = consumer_secret

    def setOption(self, optionDict):
        pass

    def getmtime(self):
        return self.__file_mtime

    def getsize(self):
        return self.__file_size
