#
# -*- coding: UTF-8 -*-
#
"""WebdavProvider module contain the L{WebdavProvider} class
"""

__docformat__ = 'epytext'

import os

import requests

from eo_dataflow_manager.scheduler.com.sys.File import READ_BUFFER, File
from eo_dataflow_manager.scheduler.sc.ProviderManager.AbstractProvider import AbstractProvider

DEFAULT_PROTOCOL = 'http'
DEFAULT_PROTOCOL_SECURE = 'https'
DEFAULT_SERVER_PORT_SECURE = 443
TMP_EXT = '.tmp'


class WebdavSession(object):
    def __init__(self, server, user, pwd, port=DEFAULT_SERVER_PORT_SECURE, timeout_=None):
        self.server = server
        self.user = user
        self.pwd = pwd
        self.port = port
        self.timeout = timeout_
        self.filename = None
        self.filesize = 0
        if self.port == DEFAULT_SERVER_PORT_SECURE:
            self.protocol = DEFAULT_PROTOCOL_SECURE
        else:
            self.protocol = DEFAULT_PROTOCOL

        self.__session = requests.session()
        self.__session.verify = True
        self.__session.stream = True
        self.__session.auth = (self.user, self.pwd)

    def close(self):
        pass  # with urllib, the connection is automatically closed when the object is disposed by the GC

    def download(self, remotepath, localpath):

        if (remotepath[:2] == '//'):
            remotepath = 'https://' + self.server + remotepath[1:]
        elif (remotepath[:1] == '/'):
            remotepath = '{0}://{1}:{2}'.format(self.protocol, self.server, self.port) + remotepath
        else:
            remotepath = remotepath

        if self.__session is not None:
            response = self.__session.request('GET', remotepath, allow_redirects=False, timeout=self.timeout)
            if response.status_code != 200:  # MULTI_STATUS
                return False, 'Error request return status code : ' + str(response.status_code)

            with open(localpath, 'wb') as downloadfile:
                done = 0
                for readbuffer in response.iter_content(READ_BUFFER * 16):
                    read = len(readbuffer)
                    done += read
                    downloadfile.write(readbuffer)
            return True, None
        else:
            return False, 'Error : no session'

    def check(self):
        self.__session.request('GET', '{0}://{1}:{2}'.format(self.protocol, self.server, self.port),
                               allow_redirects=False, timeout=self.timeout)
        return True


class WebdavProvider(AbstractProvider):
    """
    Class WebdavProvider
    """

    def __init__(self, id, type, job_path, globalConfig):
        """
        WebdavProvider initialization method
        """
        AbstractProvider.__init__(self, id, type, job_path, globalConfig)
        self.__server = None
        self.__username = None
        self.__password = None
        self.__session = None
        self.__port = None

    server = property(lambda self: self.__server, None, None, None)
    username = property(lambda self: self.__username, None, None, None)
    password = property(lambda self: self.__password, None, None, None)
    session = property(lambda self: self.__session, None, None, None)
    port = property(lambda self: self.__port, None, None, None)

    def setSpecificState(self):
        self._jobstate['Server'] = self.__server
        self._jobstate['Username'] = self.__username
        self._jobstate['Password'] = self.__password

    def checkAvailability(self):
        #self._log.debug("Checking availability for server : %s"%(self.__server))
        self.__errormsg = None
        ret = True

        try:
            session = self.getSession()

            if not session.check():
                ret = False
        except Exception as msg:
            self._log.warning('Connection error : [%s]' % (msg))
            self.__errormsg = str(msg)
            ret = False

        try:
            self.closeSession(session)
        except Exception as msg:
            self.__errormsg = str(msg)
            ret = False

        return ret

    def getSession(self, timeout_=None):  # fcad: add session timeout parameter
        timeout = timeout_
        if timeout_ is None:
            timeout = self.getDefaultTimeout()
        self.__session = WebdavSession(self.__server, self.__username, self.__password, self.__port, timeout)
        return self.__session

    def closeSession(self, session):
        if self.__session is not None:
            self.__session.close()

    def getFile(self, session, remotepath, localpath):

        self.__errormsg = None
        tmp_localpath = localpath + TMP_EXT

        try:
            response, self.__errormsg = session.download(remotepath, tmp_localpath)
            if not response:
                self._log.warning('error %s (URL = %s)' % (self.__errormsg, remotepath))
                return None

        except Exception as msg:
            self._log.warning('Connection : Exception while download binary : %s (URL = %s)' % (msg, remotepath))
            self.__errormsg = str(msg)
            return None

        try:
            os.rename(tmp_localpath, localpath)
        except OSError as msg:
            self._log.warning('Cannot rename %s --> %s (%s)' % (tmp_localpath, localpath, msg))
            self.__errormsg = str(msg)
            return None

        if os.path.exists(localpath):
            return File(localpath)

        return None

    def getFilenameAndUrl(self, remotepath):
        return(remotepath.split())

    def putFile(self, session, localpath, remotepath):
        raise RuntimeError('This method must be overloaded')

    def setServer(self, server):
        self.__server = server

    def setUsername(self, username):
        self.__username = username

    def setPassword(self, password):
        self.__password = password

    def setPort(self, port):
        if port is not None:
            self.__port = port

    def __closeSession(self):
        self.closeSession(self.__session)
        self.__session = None
