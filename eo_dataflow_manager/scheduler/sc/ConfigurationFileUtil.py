#
# -*- coding: UTF-8 -*-
#

# Log file management
import logging
from string import capwords

from eo_dataflow_manager.ifr_lib_modules.ifr_yaml import YamlConfig

"""
from eo-dataflow-manager.scheduler.com.ext.XMLReader import XMLReader
from xml.parsers.expat import ExpatError
"""


# log = logging.getLogger('ConfigurationFileUtil')
# log.setLevel(logging.INFO)


def read_integer(log, reader, node, sub_node_name):
    result = True
    value = None

    if reader.haveSubNode(node, sub_node_name):
        a_buffer = reader.getSubNodeValue(logging.WARNING, node, sub_node_name)

        if a_buffer != '' and a_buffer is not None:
            if a_buffer.isdigit():
                value = int(a_buffer)
            else:
                log.error(
                    'the value (%s) of the subNode %s must be a integer' %
                    (a_buffer, sub_node_name))
                value = a_buffer
                result = False

    return value, result


def read_dict(log, reader, node, sub_node_name):
    result = True

    value = {}

    if reader.haveSubNode(node, sub_node_name):
        a_buffer = reader.getSubNodeValue(logging.WARNING, node, sub_node_name)

        if a_buffer != '' and a_buffer is not None:
            if a_buffer[0] != '{':
                dictionary = '{' + a_buffer + '}'
            else:
                dictionary = a_buffer
            try:
                value = YamlConfig(content=dictionary)
            except:
                log.error(
                    'the value (%s) of the subNode %s must be a dict' %
                    (a_buffer, sub_node_name))
                result = False

    return value, result


"""
def getLocation(location, workspacepath, appdatapath):
    # Get location attribute
    if location == "workspace":
        appendPath = workspacepath
    elif location == "appdata":
        appendPath = appdatapath
    else:
        appendPath = ""

    return appendPath
"""


class ConfigurationFileUtil(object):

    def __init__(self, logger_name, xml_reader, xmltree):
        """
        ConfigurationFileUtil instantiation method

        @param logger_name: Name of text logger to use
        @type logger_name: C{Logger}
        @param xml_reader : XMLReader
        @type xml_reader: C{XMLReader}
        @param xmltree: XML tree of configuration file.
        @type xmltree: C{tree}
       """
        self._log = logging.getLogger(logger_name)

        self.__id = None
        self.__source = None
        self.__destination = None
        self.__settings = None

        #        self._log = log
        self.__xr = xml_reader
        self.__xmltree = xmltree

        self.__debug = None

    def Read(self):

        config_source = False
        config_destination = False
        config_setting = False

        # Get the root node
        xml_node = self.__xr.getRootNode(
            logging.ERROR, self.__xmltree, 'download_config')
        if 'id' in xml_node.attrib:
            self.__id = self.__xr.getAttributeValue(
                logging.ERROR, xml_node, 'id', '')
        else:
            self._log.error(
                "the node '%s' don't have the required attribute '%s'" %
                (xml_node.tag, 'id'))

        self.__debug = False
        if self.__xr.haveSubNode(xml_node, 'debug'):
            value = self.__xr.getSubNodeValue(logging.ERROR, xml_node, 'debug')
            if value is not None and value.upper() == 'TRUE':
                self.__debug = True

        if self.__xr.haveSubNode(xml_node, 'data_source'):
            source_node = self.__xr.getSubNode(
                logging.ERROR, xml_node, 'data_source')

            self.__source = ConfigurationSource(
                self._log, self.__xr, source_node)
            config_source = self.__source.read()

        if self.__xr.haveSubNode(xml_node, 'data_destination'):
            destination_node = self.__xr.getSubNode(
                logging.ERROR, xml_node, 'data_destination')

            self.__destination = ConfigurationDestination(
                self._log, self.__xr, destination_node)
            config_destination = self.__destination.read()

        if self.__xr.haveSubNode(xml_node, 'download_settings'):
            settings_node = self.__xr.getSubNode(
                logging.ERROR, xml_node, 'download_settings')

            self.__settings = ConfigurationSettings(
                self._log, self.__xr, settings_node)
            config_setting = self.__settings.read()

        if not self.__id or not config_source or not config_destination or not config_setting:
            self._log.error(
                '  Source node OK : %s, Destination node OK : %s, Settings node OK : %s ' %
                (config_source, config_destination, config_setting))
            raise IOError

    id = property(lambda self: self.__id, None, None, 'read only id variable')
    debug = property(lambda self: self.__debug, None, None, 'read only debug variable')
    source = property(lambda self: self.__source, None,
                      None, 'read only source variable')
    destination = property(
        lambda self: self.__destination,
        None,
        None,
        'read only destination variable')
    settings = property(lambda self: self.__settings, None,
                        None, 'read only settings variable')


class ConfigurationSource(object):

    def __init__(self, log, xml_reader, node):
        """
        ConfigurationSource instantiation method

        @param log: Name of text logger to use
        @type log: C{Logger}
        @param xml_reader : XMLReader
        @type xml_reader: C{XMLReader}
        @param node: Source node of  configuration file.
        @type node: C{str}
        """
        self._log = log
        self.__xr = xml_reader
        self.__node = node

        self.__protocol = None
        self.__protocol_option = None
        self.__rootPath = None
        self.__server = None
        self.__login = None
        self.__password = None
        self.__port = None
        self.__plugin = None
        self.__dateRegexp = None
        self.__dateFormat = None
        self.__update = None
        self.__date_pattern = None
        self.__date_backlogInDays = None
        self.__date_maxDate = None
        self.__date_minDate = None
        self.__directories_ignoreNewerThan = None
        self.__directories_ignoreOlderThan = None
        self.__directories_ignoreRegexp = None
        self.__directories_regexp = None
        self.__files_ignoreOlderThan = None
        self.__files_ignoreSensingtimeOlderThan = None
        self.__files_ignoreRegexp = None
        self.__files_regexp = None
        self.__opensearch_area = None
        self.__opensearch_dataset = None
        self.__opensearch_requestFormat = None
        self.__geotemporal_list= None

    def read(self):

        result = True

        if self.__xr.haveSubNode(self.__node, 'location'):
            location_node = self.__xr.getSubNode(
                logging.ERROR, self.__node, 'location')

            if self.__xr.haveSubNode(location_node, 'protocol'):
                self.__protocol = self.__xr.getSubNodeValue(
                    logging.ERROR, location_node, 'protocol')
                self.__protocol_option, readOK = read_dict(
                    self._log, self.__xr, location_node, 'protocol_option')
                if not readOK:
                    result = False
            else:
                self._log.error(
                    "the node '%s' don't have the required node '%s'" %
                    (location_node.tag, 'protocol'))
                result = False

            if self.__xr.haveSubNode(location_node, 'rootpath'):
                self.__rootPath = self.__xr.getSubNodeValue(
                    logging.ERROR, location_node, 'rootpath')

            if capwords(
                    self.__protocol) not in [
                'Localpath',
                'Localmove',
                'Localpointer',
                'Onlynotify']:
                self.__server = self.__xr.getSubNodeValue(
                    logging.ERROR, location_node, 'server')
                if self.__xr.haveSubNode(location_node, 'login'):
                    self.__login = self.__xr.getSubNodeValue(
                        logging.ERROR, location_node, 'login')
                if self.__xr.haveSubNode(location_node, 'password'):
                    self.__password = self.__xr.getSubNodeValue(
                        logging.ERROR, location_node, 'password')
                if self.__xr.haveSubNode(location_node, 'port'):
                    self.__port = self.__xr.getSubNodeValue(
                        logging.ERROR, location_node, 'port')
        else:
            self._log.error(
                "the node '%s' don't have the required node '%s" %
                (self.__node.tag, 'location'))
            result = False

        if self.__xr.haveSubNode(self.__node, 'date_extraction'):
            date_extraction_node = self.__xr.getSubNode(
                logging.ERROR, self.__node, 'date_extraction')
            if 'plugin' in date_extraction_node.attrib:
                self.__plugin = self.__xr.getAttributeValue(
                    logging.WARNING, date_extraction_node, 'plugin')
            else:
                self._log.error(
                    "the node '%s' don't have the required attribute '%s'" %
                    (date_extraction_node.tag, 'plugin'))
                result = False

            if self.__xr.haveSubNode(date_extraction_node, 'regexp'):
                self.__dateRegexp = self.__xr.getSubNodeValue(
                    logging.ERROR, date_extraction_node, 'regexp')
            else:
                self._log.error(
                    "the node '%s' don't have the required node '%s'" %
                    (date_extraction_node.tag, 'regexp'))
                result = False

            if self.__xr.haveSubNode(date_extraction_node, 'format'):
                self.__dateFormat = self.__xr.getSubNodeValue(
                    logging.ERROR, date_extraction_node, 'format')
            else:
                self._log.error(
                    "the node '%s' don't have the required node '%s'" %
                    (date_extraction_node.tag, 'format'))
                result = False
        else:
            self._log.error("the node '%s' don't have the required node '%s'"
                            % (self.__node.tag, 'date_extraction'))
            result = False

        if self.__xr.haveSubNode(self.__node, 'selection'):
            selection_node = self.__xr.getSubNode(
                logging.ERROR, self.__node, 'selection')

            if self.__xr.haveSubNode(selection_node, 'update'):
                self.__update = self.__xr.getSubNodeValue(
                    logging.ERROR, selection_node, 'update')

            if self.__xr.haveSubNode(selection_node, 'date_folders'):
                date_folders_node = self.__xr.getSubNode(
                    logging.ERROR, selection_node, 'date_folders')

                if self.__xr.haveSubNode(date_folders_node, 'pattern'):
                    self.__date_pattern = self.__xr.getSubNodeValue(
                        logging.ERROR, date_folders_node, 'pattern')
                else:
                    result = False

                self.__date_backlogInDays, readOK = read_integer(
                    self._log, self.__xr, date_folders_node, 'backlog_in_days')
                if not readOK:
                    result = False

                if self.__xr.haveSubNode(date_folders_node, 'max_date'):
                    self.__date_maxDate = self.__xr.getSubNodeValue(
                        logging.ERROR, date_folders_node, 'max_date')
                if self.__xr.haveSubNode(date_folders_node, 'min_date'):
                    self.__date_minDate = self.__xr.getSubNodeValue(
                        logging.ERROR, date_folders_node, 'min_date')

            if self.__xr.haveSubNode(selection_node, 'directories'):
                directories_node = self.__xr.getSubNode(
                    logging.ERROR, selection_node, 'directories')

                self.__directories_ignoreNewerThan, readOK = read_integer(
                    self._log, self.__xr, directories_node, 'ignore_newer_than')
                if not readOK:
                    result = False
                self.__directories_ignoreOlderThan, readOK = read_integer(
                    self._log, self.__xr, directories_node, 'ignore_modify_time_older_than')
                if not readOK:
                    result = False
                if self.__xr.haveSubNode(directories_node, 'ignore_regexp'):
                    self.__directories_ignoreRegexp = self.__xr.getSubNodeValue(
                        logging.ERROR, directories_node, 'ignore_regexp')
                if self.__xr.haveSubNode(directories_node, 'regexp'):
                    self.__directories_regexp = self.__xr.getSubNodeValue(
                        logging.ERROR, directories_node, 'regexp')

            if self.__xr.haveSubNode(selection_node, 'files'):
                files_node = self.__xr.getSubNode(
                    logging.ERROR, selection_node, 'files')

                self.__files_ignoreOlderThan, readOK = read_integer(
                    self._log, self.__xr, files_node, 'ignore_modify_time_older_than')
                if not readOK:
                    result = False
                if self.__xr.haveSubNode(files_node, 'ignore_regexp'):
                    self.__files_ignoreRegexp = self.__xr.getSubNodeValue(
                        logging.ERROR, files_node, 'ignore_regexp')
                self.__files_ignoreSensingtimeOlderThan, readOK = read_integer(
                    self._log,
                    self.__xr,
                    files_node,
                    'ignore_sensing_time_older_than'
                )
                if not readOK:
                    result = False
                if self.__xr.haveSubNode(files_node, 'regexp'):
                    self.__files_regexp = self.__xr.getSubNodeValue(
                        logging.ERROR, files_node, 'regexp')

            if self.__xr.haveSubNode(selection_node, 'opensearch'):
                opensearch_node = self.__xr.getSubNode(
                    logging.ERROR, selection_node, 'opensearch')

                if self.__xr.haveSubNode(opensearch_node, 'area'):
                    self.__opensearch_area = self.__xr.getSubNodeValue(
                        logging.ERROR, opensearch_node, 'area')
                if self.__xr.haveSubNode(opensearch_node, 'dataset'):
                    self.__opensearch_dataset = self.__xr.getSubNodeValue(
                        logging.ERROR, opensearch_node, 'dataset')
                if self.__xr.haveSubNode(opensearch_node, 'request_format'):
                    self.__opensearch_requestFormat = self.__xr.getSubNodeValue(
                        logging.ERROR, opensearch_node, 'request_format')

            if self.__xr.haveSubNode(selection_node, 'geotemporal'):
                geotemporal_node = self.__xr.getSubNode(
                    logging.ERROR, selection_node, 'geotemporal')

                if self.__xr.haveSubNode(geotemporal_node, 'geotemporal_list'):
                    self.__geotemporal_list = self.__xr.getSubNodeValue(
                        logging.ERROR, geotemporal_node, 'geotemporal_list')

        return result

    protocol = property(lambda self: self.__protocol, None,
                        None, 'read only protocol variable')
    protocol_option = property(lambda self: self.__protocol_option, None,
                               None, 'read only protocol variable')
    rootPath = property(lambda self: self.__rootPath, None,
                        None, 'read only rootPath variable')
    server = property(lambda self: self.__server, None,
                      None, 'read only server variable')
    login = property(lambda self: self.__login, None,
                     None, 'read only login variable')
    password = property(lambda self: self.__password, None,
                        None, 'read only password variable')
    port = property(lambda self: self.__port, None,
                    None, 'read only port variable')
    plugin = property(lambda self: self.__plugin, None,
                      None, 'read only pluging variable')
    dateRegexp = property(
        lambda self: self.__dateRegexp,
        None,
        None,
        'read only dateRegexp variable')
    dateFormat = property(
        lambda self: self.__dateFormat,
        None,
        None,
        'read only dateFormat variable')
    update = property(lambda self: self.__update, None,
                      None, 'read only update variable')
    date_pattern = property(
        lambda self: self.__date_pattern,
        None,
        None,
        'read only date_pattern variable')
    date_backlogInDays = property(
        lambda self: self.__date_backlogInDays,
        None,
        None,
        'read only date_backlogInDays variable')
    date_maxDate = property(
        lambda self: self.__date_maxDate,
        None,
        None,
        'read only date_maxDate variable')
    date_minDate = property(
        lambda self: self.__date_minDate,
        None,
        None,
        'read only date_minDate variable')
    directories_ignoreNewerThan = property(
        lambda self: self.__directories_ignoreNewerThan,
        None,
        None,
        'read only directories_ignoreNewerThan variable')
    directories_ignoreOlderThan = property(
        lambda self: self.__directories_ignoreOlderThan,
        None,
        None,
        'read only directories_ignoreOlderThan variable')
    directories_ignoreRegexp = property(
        lambda self: self.__directories_ignoreRegexp,
        None,
        None,
        'read only directories_ignoreRegexp variable')
    directories_regexp = property(
        lambda self: self.__directories_regexp,
        None,
        None,
        'read only directories_regexp variable')
    files_ignoreOlderThan = property(
        lambda self: self.__files_ignoreOlderThan,
        None,
        None,
        'read only files_ignoreOlderThan variable')
    files_ignoreSensingtimeOlderThan = property(
        lambda self: self.__files_ignoreSensingtimeOlderThan,
        None,
        None,
        'read only files_ignoreSensingtimeOlderThan variable')
    files_ignoreRegexp = property(
        lambda self: self.__files_ignoreRegexp,
        None,
        None,
        'read only files_ignoreRegexp variable')
    files_regexp = property(
        lambda self: self.__files_regexp,
        None,
        None,
        'read only files_regexp variable')
    opensearch_area = property(
        lambda self: self.__opensearch_area,
        None,
        None,
        'read only opensearch_area variable')
    opensearch_dataset = property(
        lambda self: self.__opensearch_dataset,
        None,
        None,
        'read only opensearch_dataset variable')
    opensearch_requestFormat = property(
        lambda self: self.__opensearch_requestFormat,
        None,
        None,
        'read only opensearch_requestFormat variable')
    geotemporal_list = property(
        lambda self: self.__geotemporal_list,
        None,
        None,
        'read only geotemporal_list variable')


class ConfigurationDestination(object):

    def __init__(self, log, xml_reader, node):
        """
        ConfigurationDestination instantiation method

        @param log: Name of text logger to use
        @type log: C{Logger}
        @param xml_reader : XMLReader
        @type xml_reader: C{XMLReader}
        @param node: destination node of  configuration file.
        @type node: C{str}
       """
        self._log = log
        self.__xr = xml_reader
        self.__node = node

        self.__location = None
        self.__subpath = None
        self.__type = None
        self.__checksum = None
        self.__compression = None
        self.__compression_type = None
        self.__compression_subdir = None
        self.__archive = None
        self.__archive_subdir = None
        self.__spoolLink = None
        self.__keepParentFolder = None
        self.__files_ignoreRegexp = None
        self.__files_regexp = None
        self.__fileGroupName = None
        self.__optionalSpool = None
        self.__validation = None

    def read(self):

        result = True

        if self.__xr.haveSubNode(self.__node, 'location'):
            self.__location = [self.__xr.getNodeValue(_)
                               for _ in self.__xr.getAllSubNode(self.__node, 'location')]
        else:
            self._log.error(
                "the node '%s' don't have the required node '%s'" %
                (self.__node.tag, 'location'))
            result = False

        if self.__xr.haveSubNode(self.__node, 'optional_spool_location'):
            self.__optionalSpool = [self.__xr.getNodeValue(_)
                                    for _ in self.__xr.getAllSubNode(self.__node, 'optional_spool_location')]
        else:
            self.__optionalSpool = []

        if self.__xr.haveSubNode(self.__node, 'organization'):
            organization_node = self.__xr.getSubNode(
                logging.ERROR, self.__node, 'organization')

            if self.__xr.haveSubNode(organization_node, 'subpath'):
                self.__subpath = self.__xr.getSubNodeValue(
                    logging.ERROR, organization_node, 'subpath')

            if self.__xr.haveSubNode(organization_node, 'type'):
                self.__type = self.__xr.getSubNodeValue(
                    logging.ERROR, organization_node, 'type')

            self.__keepParentFolder = False
            if self.__xr.haveSubNode(organization_node, 'keep_parent_folder'):
                keep_parent_folder = self.__xr.getSubNodeValue(
                    logging.ERROR, organization_node, 'keep_parent_folder', '')
                if keep_parent_folder and keep_parent_folder.lower() == 'true':
                    self.__keepParentFolder = True

        if self.__xr.haveSubNode(self.__node, 'post-processing'):
            post_precessing_node = self.__xr.getSubNode(
                logging.ERROR, self.__node, 'post-processing')

            if self.__xr.haveSubNode(post_precessing_node, 'checksum'):
                self.__checksum = self.__xr.getSubNodeValue(
                    logging.ERROR, post_precessing_node, 'checksum', '')

            if self.__xr.haveSubNode(post_precessing_node, 'compression'):
                self.__compression = self.__xr.getSubNodeValue(
                    logging.ERROR, post_precessing_node, 'compression', '')
                compression_node = self.__xr.getSubNode(
                    logging.ERROR, post_precessing_node, 'compression')
                self.__compression_type = self.__xr.getAttributeValue(
                    logging.ERROR, compression_node, 'type', '')

                self.__compression_subdir = False
                compression_subdir = self.__xr.getAttributeValue(
                    logging.WARNING, compression_node, 'subdir', '')
                if compression_subdir.lower() == 'true':
                    self.__compression_subdir = True

            if self.__xr.haveSubNode(post_precessing_node, 'archive'):
                self.__archive = self.__xr.getSubNodeValue(
                    logging.ERROR, post_precessing_node, 'archive')
                archive_node = self.__xr.getSubNode(
                    logging.ERROR, post_precessing_node, 'archive')
                self.__archive_subdir = False
                archive_subdir = self.__xr.getAttributeValue(
                    logging.ERROR, archive_node, 'subdir', '')
                if archive_subdir.lower() == 'true':
                    self.__archive_subdir = True

            if self.__xr.haveSubNode(post_precessing_node, 'validation'):
                self.__validation = self.__xr.getSubNodeValue(
                    logging.ERROR, post_precessing_node, 'validation')

            if self.__xr.haveSubNode(post_precessing_node, 'file_group_name'):
                self.__fileGroupName = self.__xr.getSubNodeValue(
                    logging.ERROR, post_precessing_node, 'file_group_name')

            if self.__xr.haveSubNode(post_precessing_node, 'spool_link'):
                self.__spoolLink = [self.__xr.getNodeValue(_)
                                    for _ in self.__xr.getAllSubNode(post_precessing_node, 'spool_link')]
            else:
                self.__spoolLink = []

            if self.__xr.haveSubNode(post_precessing_node, 'files_filter'):
                files_filter_node = self.__xr.getSubNode(
                    logging.ERROR, post_precessing_node, 'files_filter')

                if self.__xr.haveSubNode(files_filter_node, 'ignore_regexp'):
                    self.__files_ignoreRegexp = self.__xr.getSubNodeValue(
                        logging.ERROR, files_filter_node, 'ignore_regexp')

                if self.__xr.haveSubNode(files_filter_node, 'regexp'):
                    self.__files_regexp = self.__xr.getSubNodeValue(
                        logging.ERROR, files_filter_node, 'regexp')

        return result

    location = property(lambda self: self.__location, None,
                        None, 'read only location variable')
    subpath = property(lambda self: self.__subpath, None,
                       None, 'read only subpath variable')
    keepParentFolder = property(
        lambda self: self.__keepParentFolder,
        None,
        None,
        'read only subpath variable')
    type = property(lambda self: self.__type, None,
                    None, 'read only type variable')
    checksum = property(lambda self: self.__checksum, None,
                        None, 'read only checksum variable')
    compression = property(
        lambda self: self.__compression,
        None,
        None,
        'read only compression variable')
    compression_type = property(
        lambda self: self.__compression_type,
        None,
        None,
        'read only compression_type variable')
    compression_subdir = property(
        lambda self: self.__compression_subdir,
        None,
        None,
        'read only compression_subdir variable')
    archive = property(lambda self: self.__archive, None,
                       None, 'read only archive variable')
    archive_subdir = property(
        lambda self: self.__archive_subdir,
        None,
        None,
        'read only archive_subdir variable')
    spoolLink = property(
        lambda self: self.__spoolLink,
        None,
        None,
        'read only spoolLink variable')
    files_ignoreRegexp = property(
        lambda self: self.__files_ignoreRegexp,
        None,
        None,
        'read only files_ignoreRegexp variable')
    files_regexp = property(
        lambda self: self.__files_regexp,
        None,
        None,
        'read only files_regexp variable')

    fileGroupName = property(
        lambda self: self.__fileGroupName,
        None,
        None,
        'read only fileGroupName variable')
    optionalSpool = property(
        lambda self: self.__optionalSpool,
        None,
        None,
        'read only optionalSpool variable')
    validation = property(lambda self: self.__validation, None, None, 'read only validation variable')


class ConfigurationSettings(object):

    def __init__(self, log, xml_reader, node):
        """
        ConfigurationSettings instantiation method

        @param log: Name of text logger to use
        @type log: C{Logger}
        @param xml_reader : XMLReader
        @type xml_reader: C{XMLReader}
        @param node: settings node of  configuration file.
        @type node: C{str}
       """
        self._log = log
        self.__xr = xml_reader
        self.__node = node

        self.__database_path = None
        self.__database_purgeScansOlderThan = None
        self.__database_keepLastScan = None
        self.__checkSourceAvailability = None
        self.__delayBetweenDownloads = None
        self.__cycleLength = None
        self.__nbRetries = None
        self.__nbParallelDownloads = None
        self.__maxNbOfFilesDownloadedPerCycle = None
        self.__delayBeforeDownloadStart = None
        self.__delayBetweenScans = None
        self.__maxNbOfConcurrentStreams = None
        self.__maxNbOfLinesInAutoListing = None
        self.__monitoring = None
        self.__projectName = None
        self.__protocolTimeout = None

    def read(self):

        result = True

        if self.__xr.haveSubNode(self.__node, 'database'):
            database_node = self.__xr.getSubNode(
                logging.ERROR, self.__node, 'database')

            if self.__xr.haveSubNode(database_node, 'path'):
                self.__database_path = self.__xr.getSubNodeValue(
                    logging.WARNING, database_node, 'path')

            self.__database_purgeScansOlderThan, readOK = read_integer(
                self._log, self.__xr, database_node, 'purge_scans_older_than')
            if not readOK:
                result = False

            self.__database_keepLastScan = True
            if self.__xr.haveSubNode(database_node, 'keep_last_scan'):
                database_keep_last_scan = self.__xr.getSubNodeValue(
                    logging.ERROR, database_node, 'keep_last_scan', '')
                if database_keep_last_scan.lower() == 'false':
                    self.__database_keepLastScan = False
        else:
            self.__database_keepLastScan = True

        self.__checkSourceAvailability = True
        if self.__xr.haveSubNode(self.__node, 'check_source_availability'):
            check_source_availability = self.__xr.getSubNodeValue(
                logging.WARNING, self.__node, 'check_source_availability', '')
            if check_source_availability.lower() == 'false':
                self.__checkSourceAvailability = False

        self.__delayBetweenDownloads, readOK = read_integer(
            self._log, self.__xr, self.__node, 'delay_between_downloads')
        if not readOK:
            result = False

        self.__cycleLength, readOK = read_integer(
            self._log, self.__xr, self.__node, 'cycle_length')
        if not readOK:
            result = False

        self.__nbRetries, readOK = read_integer(
            self._log, self.__xr, self.__node, 'nb_retries')
        if not readOK:
            result = False

        self.__nbParallelDownloads, readOK = read_integer(
            self._log, self.__xr, self.__node, 'nb_parallel_downloads')
        if not readOK:
            result = False

        self.__maxNbOfFilesDownloadedPerCycle, readOK = read_integer(
            self._log, self.__xr, self.__node, 'max_nb_of_files_downloaded_per_cycle')
        if not readOK:
            result = False

        self.__delayBeforeDownloadStart, readOK = read_integer(
            self._log, self.__xr, self.__node, 'delay_before_download_start')
        if not readOK:
            result = False

        self.__delayBetweenScans, readOK = read_integer(
            self._log, self.__xr, self.__node, 'delay_between_scans')
        if not readOK:
            result = False

        self.__maxNbOfConcurrentStreams, readOK = read_integer(
            self._log, self.__xr, self.__node, 'max_nb_of_concurrent_streams')
        if not readOK:
            result = False

        self.__maxNbOfLinesInAutoListing, readOK = read_integer(
            self._log, self.__xr, self.__node, 'max_nb_of_lines_in_auto_listing')
        if not readOK:
            result = False

        self.__monitoring = True
        if self.__xr.haveSubNode(self.__node, 'monitoring'):
            monitoring = self.__xr.getSubNodeValue(logging.WARNING,
                                                   self.__node, 'monitoring', '')
            if monitoring is not None and monitoring.lower() == 'false':
                self.__monitoring = False

        if self.__xr.haveSubNode(self.__node, 'project_name'):
            self.__projectName = self.__xr.getSubNodeValue(logging.WARNING,
                                                           self.__node, 'project_name')

        self.__protocolTimeout, readOK = read_integer(
            self._log, self.__xr, self.__node, 'protocol_timeout')
        if not readOK:
            result = False

        return result

    database_path = property(
        lambda self: self.__database_path,
        None,
        None,
        'read only database_path variable')
    database_purgeScansOlderThan = property(
        lambda self: self.__database_purgeScansOlderThan,
        None,
        None,
        'read only database_purgeScansOlderThan variable')
    database_keepLastScan = property(
        lambda self: self.__database_keepLastScan,
        None,
        None,
        'read only database_keepLastScan variable')
    checkSourceAvailability = property(
        lambda self: self.__checkSourceAvailability,
        None,
        None,
        'read only checkSourceAvailability variable')
    delayBetweenDownloads = property(
        lambda self: self.__delayBetweenDownloads,
        None,
        None,
        'read only delayBetweenDownloads variable')
    cycleLength = property(
        lambda self: self.__cycleLength,
        None,
        None,
        'read only cycleLength variable')
    nbRetries = property(
        lambda self: self.__nbRetries,
        None,
        None,
        'read only nbRetries variable')
    nbParallelDownloads = property(
        lambda self: self.__nbParallelDownloads,
        None,
        None,
        'read only nbParallelDownloads variable')
    maxNbOfFilesDownloadedPerCycle = property(
        lambda self: self.__maxNbOfFilesDownloadedPerCycle,
        None,
        None,
        'read only maxNbOfFilesDownloadedPerCycle variable')
    delayBeforeDownloadStart = property(
        lambda self: self.__delayBeforeDownloadStart,
        None,
        None,
        'read only delayBeforeDownloadStart variable')
    delayBetweenScans = property(
        lambda self: self.__delayBetweenScans,
        None,
        None,
        'read only delayBetweenScans variable')
    maxNbOfConcurrentStreams = property(
        lambda self: self.__maxNbOfConcurrentStreams,
        None,
        None,
        'read only maxNbOfConcurrentStreams variable')
    maxNbOfLinesInAutoListing = property(
        lambda self: self.__maxNbOfLinesInAutoListing,
        None,
        None,
        'read only maxNbOfLinesInAutoListing variable')
    monitoring = property(
        lambda self: self.__monitoring,
        None,
        None,
        'read only monitoring variable')

    projectName = property(
        lambda self: self.__projectName,
        None,
        None,
        'read only projectName variable')

    protocolTimeout = property(
        lambda self: self.__protocolTimeout,
        None,
        None,
        'read only protocolTimeout variable')
