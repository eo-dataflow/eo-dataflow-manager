#
# -*- coding: UTF-8 -*-
#
"""JobState module contain the L{JobState} class
"""

__docformat__ = 'epytext'
from eo_dataflow_manager.scheduler.com.sys.File import File


class JobState:
    """Class JobState
    """

    def __init__(self, jobFile, mode='r', jobsElastic=None, jobsRabbitMQ=None):
        """JobState instantiation method

        @param jobFile: the L{JobState} to use
        @type jobFile: L{File}
        @param mode: L{JobState} mode (may be 'r' for read or 'w' for write)
        @type mode: C{str}

        @raise TypeError: If jobFile isn't a L{File} instance
        @raise ValueError: If mode isn't 'r' or 'w'
        """
        # Check attribute
        if not isinstance(jobFile, File):
            raise TypeError('jobFile must be a File instance')
        if mode not in ('r', 'w'):
            raise ValueError("bad mode value (must be 'r' or 'w')")
        # Init attribute
        self.__jobFile = jobFile

        self.__mode = mode
        self.__states = {}

        self.__jobsElastic = jobsElastic
        self.__jobsRabbitMQ = jobsRabbitMQ

        # Init the JobState
        self.__jobFile.open(mode)

        # If JobState init in reading mode, do a first update value
        self.__writtenState = {}
        self.__pendingState = {}
        if mode == 'r':
            self.reload()

    def reload(self):
        """ Discard current state and read state from file """
        self.__writtenState = {}
        self.__pendingState = {}

        for line in self.__jobFile.readlines():
            line = line.strip()
            if line and line[0] != '#':
                keyval = line.split('=')
                if len(keyval) > 1:
                    self.__writtenState[keyval[0]] = '='.join(keyval[1:])

    def sync(self):
        """ Update the file and elasticsearch database to add new keys/values, i.e. update cold storage """
        assert self.__mode == 'w', 'Not allowed in read mode'

        # Update file on disk
        self.__jobFile.seek(0)
        for k in self.__pendingState:
            # Write key/value to file
            self.__jobFile.write('%s=%s\n' % (k, self.__pendingState[k]))
            # Copy pending state to written state
            self.__writtenState[k.lower()] = self.__pendingState[k]
        self.__jobFile.write('EOF\n')
        self.__jobFile.truncate()

        # Update elastic database
        if self.__jobsElastic is not None:
            self.__jobsElastic.update(self.__jobFile.path, self.__writtenState)

        # Update RabbitMQ database
        self.__jobsRabbitMQ.publish(self.__jobFile.path, self.__writtenState, 'jobs')

        # once everything is copied, empty pending state
        self.__pendingState = {}

    def __getitem__(self, key):
        """ Retrieve a value stored in the job state """
        if key in self.__pendingState:
            return self.__pendingState[key]
        if key in self.__writtenState:
            return self.__writtenState[key]
        raise ValueError("Key '%s' not found in state" % key)

    def __setitem__(self, key, value):
        """ Sets a value into the job state (Won't be written on disk/db until self.sync() is called) """
        assert self.__mode == 'w', 'Not allowed in read mode'
        self.__pendingState[key] = value

    def close(self, remove=True):
        assert self.__mode == 'w', 'the L{JobState} instance not in write mode'

        # Write end marker to cold storage
        #self.sync()

        # Close file
        self.__jobFile.close()

        # Remove it
        if remove:
            self.__jobFile.remove()

        # Discard file variable (for easier debugging)
        self.__jobFile = None

    def isClosed(self):
        """Check if the JobState file is closed

        @rtype: C{bool}
        """
        return 'EOF' in self or self.__jobFile is None or self.__jobFile.closed
