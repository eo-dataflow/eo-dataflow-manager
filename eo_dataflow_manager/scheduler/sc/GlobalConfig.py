import os


class GlobalConfig(object):
    """docstring for GlobalConfig"""

    def __init__(self, filePath=None, data=None):
        self.filePath = filePath
        self.data = data
        self.administrators = []
        self.yamlContent = None
        self.__legacyPaths = None
        self.reload()

    def reload(self):
        from eo_dataflow_manager.ifr_lib_modules.ifr_yaml import YamlConfig

        # Default config
        configTemplate = """\
            general:
              mainloop_timeout: 30
              max_activated_flow: 25
              max_activated_files_by_loop: 100
              ui_worker: false
            paths:
              workspace: /tmp/eo-dataflow-manager/workspace
              appdata: /srv/eo-dataflow-manager/

            admins: []

            emm:
              project: CERSAT
              message_type: download
              targets: []
              filesystem:
                path: spools/message/
              elasticsearch:
                hosts: [http://localhost:9200]
                user: null
                password: null
                index: dl-emm
                sniff_cluster: False
              rabbitmq:
                host: localhost
                port: 5672
                ssl: false
                user: null
                password: null
                virtual_host: "/"
                queue_name: dl-emm
                routing_key: null
                message_type: download

            jobs:
              targets: []
              elasticsearch:
                hosts: [http://localhost:9200]
                user: null
                password: null
                index: dl-jobs
                sniff_cluster: False
              rabbitmq:
                host: localhost
                port: 5672
                ssl: false
                user: null
                password: null
                virtual_host: "/"
                queue_name: dl-jobs
                routing_key: null
                message_type: jobs

            ui_worker:
              queue: #{queue_name: { queue_arguments: {alias : alias_downloader_name}}}
                name: UI.host.0
                alias: alias_downloader_name
              broker_url: #"amqp://user:password@host:5672/vhost"
                host: localhost
                port: 5672
                user: null
                password: null
                virtual_host: "/"
              broker_heartbeat: 120
              imports: [eo_dataflow_manager.worker.celery_tasks]
              result_backend: "rpc://"
              result_expires: 300
              task_serializer: json
              result_serializer: json
              timezone: Europe/Paris
              enable_utc: True
              result_compression: bzip2
              accept_content: [json]

            metrics:
              targets: [filesystem]
              filesystem:
                path: spools/monitoring/
              elasticsearch:
                hosts: [http://localhost:9200]
                user: null
                password: null
                index: dl-jobs
                sniff_cluster: False
              rabbitmq:
                host: localhost
                port: 5672
                ssl: false
                user: null
                password: null
                virtual_host: "/"
                queue_name: dl-metrics
                routing_key: null
                message_type: metrics

            logs:
              default:
                root:
                  handlers: [stdout]
                  level: INFO
                loggers:
                  elasticsearch: {level: WARNING}
                  pika: {level: WARNING}
                handlers:
                  stdout:
                    class: logging.StreamHandler
                    formatter: stdout_fmt
                    stream: "ext://sys.stdout"
                    filters: []  # info_operator
                formatters:
                  stdout_fmt:
                    (): "eo_dataflow_manager.ifr_lib_modules.ifr_logging.IfrColorFormatter"
                    fmt: "%(log_color)s%(levelname).3s%(reset)s|%(message_log_color)s%(message)s%(reset)s  %(thin)s%(filename)s:%(lineno)d@%(funcName)s()%(reset)s"
                filters:
                  # --- Filter used to suppress INFO messages not containing OPERATOR tag
                  info_operator:
                    (): eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter
              file_log:
                handlers:
                  - file:
                      class: eo_dataflow_manager.ifr_lib_modules.ifr_logging.IfrFileHandler
                      formatter: file_fmt
                      # filename: ""
                      filters: [] # info_operator
                formatters:
                  file_fmt:
                      format: "%(asctime)s|%(levelname)-8s|%(process)-5d|%(message)-100s|"
                filters:
                  # --- Filter used to suppress INFO messages not containing OPERATOR tag
                  info_operator:
                  class: eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter
            """

        # Load default config
        self.yamlContent = YamlConfig(content=configTemplate)

        # Override with user's config
        if self.data is not None:
            self.yamlContent.merge(YamlConfig(content=self.data))
        else:
            self.yamlContent.merge(YamlConfig(file=self.filePath))

        # fill host definitions
        def mergeDicts(a, b):
            """ merge b into a """
            res = a.copy()
            for key in b:
                if key not in a:
                    res[key] = b[key]
                else:
                    assert isinstance(b[key], type(a[key])), 'Type mismatch in %s key: %s instead of %s' % (
                        key, type(b[key]), type(a[key]))

                    if isinstance(a[key], dict) or isinstance(b[key], dict):
                        res[key] = mergeDicts(a[key], b[key])
                    else:
                        res[key] = b[key]
            return res

        self.administrators = []
        for admin in self['admins']:
            self.administrators.append((admin['name'], admin['mail']))

        self.__updateLegacyPaths()

    def __updateLegacyPaths(self):
        # Legacy names for file paths
        self.__legacyPaths = {
            # "std_config": "" # Replaced by config.yaml
            'downloads_config': os.path.join(self['paths.appdata'], 'downloads/'),
            'work': os.path.join(self['paths.workspace'], 'downloads/'),
            'process_log_archive': os.path.join(self['paths.workspace'], 'log/'),
            'jobfile': os.path.join(self['paths.workspace'], 'cache/jobfile/'),
            'providerdir': os.path.join(self['paths.workspace'], 'cache/providerfile/'),
            'providerfile': os.path.abspath(os.path.join(self['paths.workspace'], 'cache/providerfile/sc_providers.xml')),
            'messages_to_log': os.path.join(self['paths.workspace'], 'spools/message/'),
            'exec_path': ':'.split(os.environ['PATH']),
            'static_plugins': os.path.join(self['paths.appdata'], 'plugins/'),
            'dynamic_plugins': os.path.join(self['paths.workspace'], 'cache/plugins/'),
            'monitoring': os.path.join(self['paths.workspace'], 'spools/monitoring'),
            'synchrodir': os.path.join(self['paths.workspace'], 'cache/synchrofile'),
        }

    def __str__(self):
        return str(self.yamlContent)

    def __getitem__(self, path):
        return self.yamlContent[path]

    def getPath(self, name):
        return self.__legacyPaths[name]

    def shortenPath(self, path):
        """
        Visually shorten a file path (relative to either the workspace or appdata folder)
        """
        if path.startswith(self['paths.workspace']):
            return '$WS' + os.path.join('/',
                                        path[len(self['paths.workspace']):])
        elif path.startswith(self['paths.appdata']):
            return '$APP' + os.path.join('/',
                                         path[len(self['paths.appdata']):])
        return path

    def setWorkspacePath(self, path):
        self.yamlContent.as_dict()['paths']['workspace'] = path
        self.__updateLegacyPaths()

    def setAppdataPath(self, path):
        self.yamlContent.as_dict()['paths']['appdata'] = path
        self.__updateLegacyPaths()
