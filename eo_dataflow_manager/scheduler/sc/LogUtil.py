# #
# # -*- coding: UTF-8 -*-
# #

import logging


def setupFileLogger(logger, config, targetFile):

    formattersConfig = config.get("formatters", {})
    filtersConfig = config.get("filters", {})

    handlers = []
    hdlFilters = []

    dict_hdl = {k: v for d in config.get("handlers", []) for k, v in d.items()}
    for hdlCfg in dict_hdl.values():
        finalHdlCfg = hdlCfg.copy()
        hdlClassStr = finalHdlCfg.pop("class")
        hdlFmtName = finalHdlCfg.pop("formatter")
        finalHdlCfg["filename"] = targetFile
        if 'filters' in finalHdlCfg:
            hdlFilters = finalHdlCfg.pop("filters")

        fmtConfig = formattersConfig[hdlFmtName].copy()
        fmtConfig["fmt"] = fmtConfig.pop("format")

        import importlib
        hdlMod = importlib.import_module(".".join(hdlClassStr.split(".")[0:-1]))
        hdlClass = getattr(hdlMod, hdlClassStr.split(".")[-1])
        handler = hdlClass(**finalHdlCfg)
        handler.setFormatter(logging.Formatter(**fmtConfig))

        # add filters to handler
        for filter in hdlFilters:
            filterConfig = filtersConfig[filter].copy()
            filterClassStr = filterConfig.pop("class")
            filterMod = importlib.import_module(".".join(filterClassStr.split(".")[0:-1]))
            filterClass = getattr(filterMod, filterClassStr.split(".")[-1])
            filterObj = filterClass(**filterConfig)
            handler.addFilter(filterObj)

        logger.addHandler(handler)
        handlers.append(handler)

    return handlers
