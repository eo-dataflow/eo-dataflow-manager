#
# -*- coding: UTF-8 -*-
#


class ProviderModel(object):
    def __init__(self, provider_type, server, username, passwd, port=None, option=None):
        self.type = provider_type
        self.server = server
        self.username = username
        self.passwd = passwd
        self.port = port
        self.option = {} if option is None else option
        key = [self.type]
        if self.server is not None:
            key.append(self.server)
        if self.username is not None:
            key.append(self.username)
        if self.passwd is not None:
            key.append(self.passwd)
        if self.port is not None:
            key.append(self.port)
        self.uid = hash(''.join(key))

    def __eq__(self, other):
        return self.uid == other.uid

    def getUid(self):
        return self.uid
