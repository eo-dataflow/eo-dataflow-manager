#
# -*- coding: UTF-8 -*-
#
# !/usr/bin/env python
#
# fcad: adding DirNameBasedDataReader class
#
# Hist : 2018/11/19 Removing explicit path names
#

import datetime
import logging
import os
import re

from eo_dataflow_manager.scheduler.sc.IDataReader import IDataReader


class DirRegexpDataReader(IDataReader):
    ONLY_LOCAL = False
    DIRECTORY = True

    def __init__(
            self,
            loggerName,
            date_regexp_pattern,
            dateFormat,
            storage_regexp,
            dateIndex=0):
        self._log = logging.getLogger(loggerName)
        #        self._log.debug("DirNameBasedDataReader: dir prefix ' %s dateFormat ' %s storageRegexp %s'"%(dirPrefix, dateFormat, storageRegexp))

        self.date_index = dateIndex

        if not date_regexp_pattern or not dateFormat:
            self.date_regexp_pattern = '()'
            self.date_format = ''
        else:
            self.date_regexp_pattern = date_regexp_pattern
            self.date_format = dateFormat

        self.storage_regexp = storage_regexp

        self.__container = False

    def getRelativePath(self, filepath, roothpath=None):
        filename = self.getStorageName(filepath)
        dt = self.getDate(filepath)

        if self.__container:
            subdir = self.getLastDir(filepath)
        else:
            subdir = ''

        if self.storage_regexp is not None:
            filedir = dt.strftime(self.storage_regexp)
            return os.path.join(
                os.path.join(
                    filedir,
                    subdir),
                filename)
        else:
            filepath = filepath.lstrip('/')
            return filepath

    def getStorageName(self, filepath):

        filedir, filename = os.path.split(filepath)

        return filename

    def getDate(self, filepath):

        self.getLastDir(filepath)

        filename = filepath
        datePattern = re.compile(self.date_regexp_pattern)
        dt = None
        try:
            m = datePattern.match(filename)
            filedate = m.groups()[self.date_index]
            dt = datetime.datetime.strptime(filedate, self.date_format)
            self._log.debug(
                "DirRegexpDataReader::getDate for file '%s' : %s" %
                (filename, dt))
        except Exception as e:
            self._log.exception(
                "DirRegexpDataReader::getDate : Unable to get date for file '%s' (regexp=%s) " %
                (filename, self.date_regexp_pattern))
            raise e

        return dt

    def getLastDir(self, filepath):

        filedir, filename = os.path.split(filepath)
        filedirs = filepath.split('/')

        return filedirs[len(filedirs) - 2]

    def setContainer(self, container):
        self.__container = container


if __name__ == '__main__':
    filepath = '/...../S3A_SL_2_WCT____20161120T222134_20161120T222434_20161121T000045_0179_011_143______MAR_O_NR_002.SEN3/D2_SST_io.nc'
    logging.basicConfig()
    date_format = '%Y%m%dT%H%M%S'
    storage_regexp = '%Y/%j'
    date_regexp_pattern = r'.*\/S3A_SL_2_WCT____([0-9]{8}T[0-9]{6})_.*'

    reader = DirRegexpDataReader(
        'root',
        date_regexp_pattern,
        date_format,
        storage_regexp)

    print('filepath = ', filepath)
    print('getLastDir() => ', reader.getLastDir(filepath))
    print('getDate() => ', reader.getDate(filepath))
    print('getStorageName() => ', reader.getStorageName(filepath))
    reader.setContainer(True)
    print('getRelativePath() => ', reader.getRelativePath(filepath))
    reader.setContainer(False)
    print('getRelativePath() => ', reader.getRelativePath(filepath))
