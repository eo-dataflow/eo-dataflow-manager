#
# -*- coding: UTF-8 -*-
#
#!/usr/bin/env python


import datetime
import logging
import os
import re

from eo_dataflow_manager.scheduler.sc.IDataReader import IDataReader


class RegexpDataReader(IDataReader):

    ONLY_LOCAL = False
    DIRECTORY = False

    def __init__(
            self,
            loggerName,
            date_regexp_pattern,
            dateFormat,
            storagePath):
        self._log = logging.getLogger(loggerName)
        self._log.debug(
            "RegexpDataReader: date regexp pattern '%s dateFormat %s storagePath %s'" %
            (date_regexp_pattern, dateFormat, storagePath))

        if not date_regexp_pattern or not dateFormat:
            self.date_regexp_pattern = '()'
            self.date_format = ''
        else:
            self.date_regexp_pattern = date_regexp_pattern
            self.date_format = dateFormat

        self.storage_path = storagePath

    def getRelativePath(self, filepath, roothpath=None):
        filename = self.getStorageName(filepath)
        dt = self.getDate(filepath)
        if self.storage_path is not None:
            filedir = dt.strftime(self.storage_path)
            return os.path.join(filedir, filename)
        else:
            filepath = filepath.lstrip('/')
            return filepath

    def getStorageName(self, filepath):
        filedir, filename = os.path.split(filepath)
        return filename

    def getDate(self, filepath):

        filename = self.getStorageName(filepath)
        datePattern = re.compile(self.date_regexp_pattern)
        dt = None
        try:
            m = datePattern.match(filename)
            filedate = m.groups()[0]
            dt = datetime.datetime.strptime(filedate, self.date_format)
            self._log.debug(
                "RegexpDataReader::getDate for file '%s' : %s" %
                (filename, dt))
        except Exception as e:
            self._log.exception(
                "RegexpDataReader::getDate : Unable to get date for file '%s' (regexp=%s) " %
                (filename, self.date_regexp_pattern))
            raise e

        return dt


if __name__ == '__main__':

    #filepath = 'allData/5/MYD35_L2/2005/007/MYD35_L2.A2005007.0000.005.2010089063151.hdf'
    #    filepath = "S3A_SR_1_SRA____20180409T035608_20180409T040608_20180409T051400_0599_030_018______SVL_O_NR_003 https://scihub.copernicus.eu/dhus/odata/v1/Products('8beb744e-0bd1-4ce7-bf48-701d3a4bd936')/$value"
    filepath = 'S3A_SR_1_SRA____20180409T035608_20180409T040608_20180409T051400_0599_030_018______SVL_O_NR_003'
    logging.basicConfig()
    regexp_date = '.*____([0-9]{8}T[0-9]{6}).*'
    date_format = '%Y%m%dT%H%M%S'
    storage_path = '%Y/%j'

    reader = RegexpDataReader('root', regexp_date, date_format, storage_path)
    print('getDate() => ', reader.getDate(filepath))
    print('getStorageName() => ', reader.getStorageName(filepath))
    print('getRelativePath() => ', reader.getRelativePath(filepath))
