#
# -*- coding: UTF-8 -*-
#
# !/usr/bin/env python
#
# fcad: adding class TreeDataReader(IDataReader): class
#

import datetime
import logging
import os

from eo_dataflow_manager.scheduler.sc.IDataReader import IDataReader


class TreeDataReader(IDataReader):
    ONLY_LOCAL = False
    DIRECTORY = False

    def __init__(
            self,
            loggerName,
            date_regexp_pattern=None,
            dateFormat=None,
            storagePath=None):
        self.storagePath = storagePath
        self.dateFormat = dateFormat
        self.date_regexp_pattern = date_regexp_pattern
        self._log = logging.getLogger(loggerName)

    def getRelativePath(self, filepath, roothpath=None):
        treepath = self.getTreePath(filepath, roothpath)

        return treepath

    def getStorageName(self, filepath):
        filedir, filename = os.path.split(filepath)

        return filename

    def getTreePath(self, filepath, roothpath):
        return filepath.replace(roothpath, '').lstrip('/')

    def getDate(self, filepath):
        return datetime.date.today()
