#
# -*- coding: UTF-8 -*-
#
"""Factory module, to load plugins
"""

__docformat__ = 'epytext'

import logging

know_classes = {}


def my_import(name):
    mod = __import__(name)
    components = name.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def ListingBuilderFactory(classname, loggerName):
    """
    ProcessRunnerFactory instanciate a ProcessRunner class
    in function ot the class name.
    If module or class is unreachable log the error and raise
    a RuntimeError exception
    """

    # If the class is unknow, try to load it
    if classname not in know_classes:
        # Try to import the modules "classname"
        try:
            tmpModule = my_import(
                "eo_dataflow_manager.scheduler.plugins.ListingBuilder." + classname)
        except ModuleNotFoundError as e:
            # Try without appending the "scheduler.sc.ListingBuilder" strings
            logging.exception(e)
            try:
                tmpModule = my_import(classname)
            except ModuleNotFoundError as e:
                # add log information ??
                logging.exception(e)
                raise Exception("Unknown plugin : %s" % (classname))

        if classname.split('.')[-1] not in dir(tmpModule):
            # add log information
            raise RuntimeError(
                "Class %s unreachable in the %s module" %
                (classname, classname))

        # Store the class in the know_class dictionnary
        know_classes[classname] = tmpModule.__dict__[classname.split('.')[-1]]

    # Build a instance of the class know_class[classname]
    return know_classes[classname](loggerName)


def DataReaderFactory(
        classname,
        loggerName=None,
        regexpDate=None,
        dateFormat=None,
        storagePath=None,
        dirPrefix=None,
        dateIndex=0):
    """
    ProcessRunnerFactory instanciate a ProcessRunner class
    in function ot the class name.
    If module or class is unreachable log the error and raise
    a RuntimeError exception
    """

    # If the class is unknow, try to load it
    if classname not in know_classes:
        # Try to import the modules "classname"
        try:
            tmpModule = my_import(
                "eo_dataflow_manager.scheduler.plugins.DataReader." + classname)
        except BaseException:
            # Try without appending the
            # "eo_dataflow_manager.scheduler.plugins.DataReader" strings
            try:
                tmpModule = my_import(classname)
            except BaseException:
                # add log information ??
                raise Exception("Unknown plugin : %s" % (classname))
        #
        if classname.split('.')[-1] not in dir(tmpModule):
            # add log information
            raise RuntimeError(
                "Class %s unreachable in the %s module" %
                (classname, classname))
        #
        # Store the class in the know_class dictionnary
        know_classes[classname] = tmpModule.__dict__[classname.split('.')[-1]]

    # Build a instance of the class know_class[classname]
    try:
        if dirPrefix is not None and dateFormat is not None:
            return know_classes[classname](
                loggerName, dirPrefix, dateFormat, storagePath, dateIndex)
        elif regexpDate is not None:
            return know_classes[classname](
                loggerName, regexpDate, dateFormat, storagePath)
        else:
            return know_classes[classname](
                loggerName, regexpDate, dateFormat, storagePath)
    except Exception as e:
        raise Exception(
            "DataReader [%s] cannot be instanciated [%s,%s,%s,%s,%s]: %s" %
            (classname, loggerName, dirPrefix, dateFormat, storagePath, dateIndex, e))


def PostProcessingFactory(classname, log, id, **kwargs):
    """
    ProcessRunnerFactory instanciate a ProcessRunner class
    in function ot the class name.
    If module or class is unreachable log the error and raise
    a RuntimeError exception
    """

    # If the class is unknow, try to load it
    if classname not in know_classes:
        # Try to import the modules "classname"
        try:
            tmpModule = my_import(
                "eo_dataflow_manager.scheduler.plugins.PostProcessing." + classname)
        except BaseException:
            # Try without appending the
            # "eo_dataflow_manager.scheduler.plugins.DataReader" strings
            try:
                tmpModule = my_import(classname)
            except BaseException:
                # add log information ??
                raise Exception("Unknown plugin : %s" % classname)
        #
        if classname.split('.')[-1] not in dir(tmpModule):
            # add log information
            raise RuntimeError(
                "Class %s unreachable in the %s module" %
                (classname, classname))
        #
        # Store the class in the know_class dictionnary
        know_classes[classname] = tmpModule.__dict__[classname.split('.')[-1]]

    # Build a instance of the class know_class[classname]
    try:
        return know_classes[classname](log, id, **kwargs)
    except Exception as e:
        raise Exception("PostProcessing [%s] cannot be instanciated: %s" % (classname, str(e)))