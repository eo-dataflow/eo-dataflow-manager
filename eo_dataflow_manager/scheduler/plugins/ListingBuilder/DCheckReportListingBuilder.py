#
# -*- coding: UTF-8 -*-
#

import logging
import os
import shutil
from subprocess import PIPE, Popen, TimeoutExpired

from eo_dataflow_manager.scheduler.sc.IListingBuilder import IListingBuilder


class DCheckReportListingBuilder(IListingBuilder):

    def __init__(self, logger_name):
        self.__activated = True
        self.__current_token = None

        self.__base_url = None
        self.__process = None

        self._log = logging.getLogger(logger_name)
        # These attributes are set by the download, before getListing() call
        self.__download = None
        self.__download_configuration = None

        self.__config_path = None

        self.__timestamp = None
        self.__timestamp_tmp = None
        self.__dcheck_database = None
        self.__dreport_output = None

        # initialisation de la conf du dcheck : pas d'utilisation de fichier generique, pour forcer
        # l'utilisateur a configurer convenablement le scan
        self.__dcheck_config = None
        # initialisation de la conf du dreport : utilisation d'un fichier generique s'il n'y a pas
        # de config specifique fournie par l'utilisateur
        self.__dreport_config = None
        self.__smart_crawler = False

        # 28/02/2018 PMT : ajout du mode debug
        self.__debug = False

    def setDownload(self, download, download_conf_file):
        self.__download = download
        self.__download_configuration = download_conf_file
        # self._log.debug(" Download conf file '%s'" % (self.__download_configuration))
        # Le but est ici de trouver la configuration du dcheck / dreport a
        # partir de la conf du download. Cela concerne principalement :
        # url complete (server+base_dir), patterns_filters

    # 28/02/2018 PMT : ajout du mode debug
    def set_mode_debug(self, debug_mode=False):
        self.__debug = debug_mode

    def set_smartcrawler(self, smart_crawler):
        self.__smart_crawler = smart_crawler

    def check_provider(self):
        # on verifie ici que le provider du download est supporte par ce plugin
        provider = self.__download.provider
        provider_type = provider.__class__.__name__
        self._log.debug("  provider_type '%s'" % provider_type)
        if provider_type == 'FtpProvider':
            self.__base_url = 'ftp://'
            if provider.username is not None:
                self.__base_url += provider.username
            if provider.password is not None:
                self.__base_url += ':' + provider.password
            if provider.username is not None or provider.password is not None:
                self.__base_url += '@'
            self.__base_url += '%s' % provider.server
            if provider.port is not None:
                self.__base_url += ':%s' % (str(provider.port))
            self.__base_url += '/' + self.__download.remote_storage_repository
        elif provider_type == 'SftpProvider':
            self.__base_url = 'sftp://'
            if provider.username is not None:
                self.__base_url += provider.username
            if provider.password is not None:
                self.__base_url += ':' + provider.password
            if provider.username is not None or provider.password is not None:
                self.__base_url += '@'
            self.__base_url += '%s' % provider.server
            if provider.port is not None:
                self.__base_url += ':%s' % (str(provider.port))
            self.__base_url += '/' + self.__download.remote_storage_repository
        elif provider_type == 'FtpsProvider':
            self.__base_url = 'ftps://'
            if provider.username is not None:
                self.__base_url += provider.username
            if provider.password is not None:
                self.__base_url += ':' + provider.password
            if provider.username is not None or provider.password is not None:
                self.__base_url += '@'
            self.__base_url += '%s' % provider.server
            if provider.port is not None:
                self.__base_url += ':%s' % str(provider.port)
            self.__base_url += '/' + self.__download.remote_storage_repository
        elif provider_type == 'WebdavProvider':
            self.__base_url = 'webdav://'
            if provider.username is not None:
                self.__base_url += provider.username
            if provider.password is not None:
                self.__base_url += ':' + provider.password
            if provider.username is not None or provider.password is not None:
                self.__base_url += '@'
            self.__base_url += '%s' % provider.server
            if provider.port is not None:
                self.__base_url += ':%s' % (str(provider.port))
            self.__base_url += '/' + self.__download.remote_storage_repository
        elif provider_type in ['HttpProvider', 'Https_filetableProvider']:
            protocol = 'http:'
            if provider_type.startswith('Https'):
                protocol = 'https:'
            self.__base_url = '%s//%s/%s' % (protocol,
                                             provider.server,
                                             self.__download.remote_storage_repository.lstrip('/'))
        elif provider_type in ['HttpsProvider', 'EumdacProvider']:
            # 2018/12/18 PMT#14 protocol type in lowercase
            if self.__download.remoteStorageProviderType == 'Https_OpenSearch' \
                    or self.__download.remoteStorageProviderType == 'Https_opensearch':
                protocol = 'https_opensearch'
            else:
                protocol = 'https'
            if provider.username is not None and provider.password is not None:
                self.__base_url = '%s://%s:%s@%s/%s' % (protocol,
                                                        provider.username,
                                                        provider.password,
                                                        provider.server,
                                                        self.__download.remote_storage_repository.lstrip('/'))
            else:
                self.__base_url = f'{protocol}://{provider.server}/' \
                                      f"{self.__download.remote_storage_repository.lstrip('/')}"
        elif provider_type == 'S3Provider':
            protocol = self.__download.remoteStorageProviderType
            self.__base_url = '%s//%s/%s' % (protocol,
                                             provider.server,
                                             self.__download.remote_storage_repository.lstrip('/'))
        elif provider_type == 'EodmsProvider':
            self.__base_url = self.__download.remote_storage_repository
        # 06/04/2018 PMT add LocalmoveProvider
        elif provider_type in ['LocalpathProvider', 'LocalpointerProvider', 'OnlynotifyProvider', 'LocalmoveProvider']:
            self.__base_url = self.__download.remote_storage_repository
        else:
            self.__activated = False
            raise Exception(
                'DCheckReportListingBuilder : unknown provider : %s. Plugin is disabled !' %
                provider_type)

    def setConfiguration(self, listing_mode):
        self._log.debug('      - set configuration')

        plugins_config_path = self.__download.globalConfig.getPath('static_plugins')
        self.__config_path = os.path.join(plugins_config_path, 'dcheckreport')

        self.__dcheck_config = self.__download_configuration
        self._log.debug(
            "      - dcheck  config file  '%s'" %
            self.__dcheck_config)

        self.__dreport_config = self.set_dreport_configuration(listing_mode)
        self._log.debug(
            "      - dreport config file  '%s'" %
            self.__dreport_config)

    def set_dreport_configuration(self, listing_mode):
        plugins_config_path = self.__download.globalConfig.getPath('dynamic_plugins')
        dreport_config_name = 'dreport_' + listing_mode + '_files_v7'

        plugin_filename = os.path.join(plugins_config_path, dreport_config_name + '.py')

        if not os.path.exists(plugin_filename):
            # normal configuration => The source module is in the same directory as that of the current module

            plugin_source_file = os.path.join(os.path.split(str(__file__))[0],
                                              dreport_config_name + '.py')

            self._log.debug(f"        plugin source file name  '{plugin_source_file}'")

            if not os.path.exists(plugin_source_file):
                raise IOError(
                    f"DCheckReportListingBuilder : unknown dreport configuration file : '{plugin_source_file}'.")

            try:
                shutil.copyfile(plugin_source_file, plugin_filename)
            except Exception as e:
                raise IOError(
                    f'DCheckReportListingBuilder : unknown dreport configuration file :'
                    f" '{plugin_source_file}' ({e}).")

        return plugin_filename

    def __make_string_parameter(self, value):
        return_value = value
        if value:
            return_value = "'" + value + "'"
        return return_value

    def __make_numeric_parameter(self, value):
        return_value = value
        if value:
            return_value = str(value)
        return return_value

    def __make_list_parameter(self, value):
        return_value = value
        if value:
            return_value = "['" + value + "']"
        return return_value

    def __make_dict_parameter(self, value):
        return_value = '{'
        line = ''
        for key in value.keys():
            line += f"'{key}': "
            line += f'{value[key]},' if isinstance(value[key], (int, float)) else f"'{value[key]}',"
        if line != '':
            return_value += line[:-1]
        return_value += '}'
        return return_value

    def __make_boolean_parameter(self, value):
        if value:
            return 'True'
        else:
            return 'False'

    def __write_line_dcheck_file_config(
            self, config_file, value, key, default_value):
        line = [key]
        line.append('=')
        to_write = False
        # ecriture
        if value:
            line.append(value)
            to_write = True
        elif default_value:
            to_write = True
            line.append(default_value)
        if to_write:
            config_file.write('%s\n' % (''.join(line)))

    def __run_dcheck_report(self, command):
        # On verifie que le fichier de conf existe bien, sinon dcheck peut s'executer sans erreur,
        # et sans le fichier de conf...
        if command == 'eo-dataflow-manager-dcheck':
            args_list = [
                self.__base_url,
                '-d',
                self.__dcheck_database,
                '-c',
                self.__dcheck_config,
            ]
            if self.__debug:
                args_list.append('--debug')
            if self.__smart_crawler:
                args_list.append('--smart-crawler')
            if not os.path.exists(self.__dcheck_config):
                raise Exception(
                    'dcheck error : config file does not exists : %s' %
                    self.__dcheck_config)
        elif command == 'eo-dataflow-manager-dreport':
            # TODO : gicler le ignore config differences !? (necessite
            # probablemnet une correctoin du dreport ?)
            args_list = [
                '-d',
                self.__dcheck_database,
                '-o',
                self.__dreport_output,
                '--ignore-config-differences',
                '-c',
                self.__dreport_config,
                '-t',
                self.__timestamp_tmp]
            if self.__debug:
                args_list.append('--debug')
            if not os.path.exists(self.__dreport_config):
                raise Exception(
                    'dreport error : config file does not exists : %s' %
                    self.__dreport_config)
        else:
            raise Exception(
                "__runDCheckReport : invalid command '%s'" %
                command)

        self._log.debug(
            '  running %s with args : %s' %
            (command, str(args_list)))
        # TODO : mettre 'dcheck' en configuration (pour utiliser dcheck-VERSION
        # par exemple)
        cmd = [command] + args_list

        self.__process = Popen(
            cmd, stdout=PIPE, stderr=PIPE, close_fds=False, text=True)
        pid = self.__process.pid

        if self.__debug:
            # Forward output
            def forward_output(out_log, log_level, log_type):
                while not out_log.closed:
                    line = out_log.readline()
                    if not line:
                        break
                    self._log.log(log_level, '>> %s(%s) %s: %s', command, self.__process.pid, log_type, line.rstrip())
                out_log.close()

            import logging
            from threading import Thread
            a = Thread(target=forward_output, args=(self.__process.stdout, logging.INFO, 'stdout'))
            b = Thread(target=forward_output, args=(self.__process.stderr, logging.WARNING, 'stderr'))

            a.start()
            b.start()
            self.__process.wait()
            a.join()
            b.join()
        else:
            output = ''
            while self.__process.returncode is None:
                try:
                    out, err = self.__process.communicate(timeout=1)
                    output += err + out
                except TimeoutExpired:
                    pass

            if self.__process.returncode != 0:
                self._log.error('[%s] DCheck logs:\n%s' % (self.__download.id, output))

        returncode = self.__process.returncode
        self.__process = None
        if returncode != 0:
            raise Exception('>> %s pid=%s returned error code %s' % (command, pid, returncode))

    def stop(self):
        if self.__process is not None:
            self._log.debug('kill process %s' % (str(self.__process.pid)))
            self.__process.kill()

    def getListing(self, token):
        listing = []
        if not self.__activated:
            self._log.error(
                'DCheckReportListingBuilder : plugin disabled... getListing aborted')
            return []

        self.__current_token = token

        # Set dcheck args/options and run it !
        try:
            self.__run_dcheck_report('eo-dataflow-manager-dcheck')
        except Exception as e:
            self._log.exception(e)
            raise Exception(
                'DCheck error => no listing will be generated (%s)' %
                (str(e)))

        try:
            # Gestion du fichier timestamp : pour eviter qu'un fichier timestamp ne soit utilise,
            # alors qu'il y a eu un probleme apres la generation du listing precedent
            # (exemple : ouverture du rapport et renvoie de la liste au eo-dataflow-manager), qui fait que
            #  le eo-dataflow-manager n'aurait pas pris en compte le rapport, le timestamp utilise par le dreport
            #  est un fichier .tmp. Le vrai timestamp est recopie en .tmp avant l'appel du dreport, et est renomme lors
            #  du ackListingStorage. Ainsi, tant que le eo-dataflow-manager n'a pas valide la bonne reception du listing
            # le dreport continuera d'utiliser le meme timestamp

            # fabrication du listing dreport a partir de la base de donnees
            self.__run_dcheck_report('eo-dataflow-manager-dreport')

            # lecture du listing dreport, pour renvoyer au eo-dataflow-manager la liste
            # des fichiers du nouveau listing
            with open(self.__dreport_output) as f:
                lines = f.readlines()
        except Exception as e:
            self._log.exception(e)
            raise Exception(
                'DReport error => no listing to return to eo-dataflow-manager (%s)' %
                (str(e)))

        # Analyse des lignes, pour recupere les erreurs et ignorer les
        # commentaires et lignes vides
        error_lines = []
        for line in lines:
            if line.startswith('#'):  # header, titres, infos diverses...
                continue
            elif line.startswith('!'):  # exec infos comme les erreurs etc...
                error_lines.append(line)
            else:  # cas d'un fichier
                listing.append(line)

        if len(error_lines) > 0:
            raise Exception(
                'DReport : report contains anomalies => do not return listing to eo-dataflow-manager. Errors : %s' %
                (str(error_lines)))

        return listing

    def setWorkingDirectory(self, directory_path):
        self.__timestamp = os.path.join(directory_path, 'dreport.timestamp')
        self.__timestamp_tmp = self.__timestamp + '.tmp'
        self.__dreport_output = os.path.join(directory_path, 'report.txt')
        self.__dcheck_database = os.path.join(
            directory_path, 'dcheck_database.db')

    def ackListingStorage(self, token):
        assert self.__current_token == token, \
            'DCheckReportListingBuilder::ackListingStorage : ' \
            'unattended token (ack [%s] != current [%s])' % (self.__current_token, token)

        self._log.debug(
            'DCheckReportListingBuilder::ackListingStorage : prise en compte du nouveau timestamp')
        if os.path.exists(self.__timestamp_tmp):
            shutil.move(self.__timestamp_tmp, self.__timestamp)
