from eo_dataflow_manager.dchecktools.reports import Report_ListFiles

REPORT = Report_ListFiles.Report_ListFiles()
REPORT.display_titles = True
REPORT.display_unmodified_files = False
REPORT.display_removed_files = False
REPORT.display_created_files = True
REPORT.display_modified_files = True
REPORT.check_infos = ['size', 'mtime', 'sensingtime']  # to get modified files
REPORT.report_infos = ['size', 'mtime']  # to get file size and file modification date

# ignoring all directories informations
REPORT.ignoreUnModifiedDirectories = True
REPORT.ignoreModifiedDirectories = True
REPORT.ignoreCreatedDirectories = True
REPORT.ignoreRemovedDirectories = True
