import os

from eo_dataflow_manager.scheduler.plugins.PostProcessing.IPostProcessing import IPostProcessing


class FileFilter(IPostProcessing):

    def __init__(self, log, id, **kwargs):

        self._log = log
        self._id = id
        self._dp_pattern_filter_destination = kwargs.get('dpPatternFilterDestination', None)
        self._file_ignored = False

    def process(self, downloaded_file):
        if downloaded_file.isFolder:
            try:
                accepted, nb_ignored, nb_files = self.process_folder(str(downloaded_file.localFolder))
                self._log.debug('OPERATOR : [%s] %s / %s files were ignored or not accepted by the regex in the '
                                'tar/zip files : %s.', self._id,
                                nb_ignored, nb_files, downloaded_file.remoteFilename)
                self._file_ignored = not accepted
            except Exception:
                raise Exception(
                    "An error occurred during the name verification with the regex.'%s'" %
                    downloaded_file.remoteFilepath)
        return self._file_ignored

    def process_folder(self, folder_path):

        accepted = True
        nb_files = 0
        nb_ignored = 0

        for root, dirs, files in os.walk(folder_path):
            for file in files:
                nb_files += 1
                file_path = os.path.join(root, file)

                if not self._dp_pattern_filter_destination.isAccepted(file_path):
                    os.remove(file_path)
                    nb_ignored += 1

        self.delete_empty_directories(folder_path)

        # Check if the directory is empty
        if not os.listdir(folder_path):
            accepted = False

        return accepted, nb_ignored, nb_files

    def delete_empty_directories(self, filepath):
        for root, dirs, files in os.walk(filepath, topdown=False):
            for subdir in dirs:
                sub_folder_path = os.path.join(root, subdir)
                if not os.listdir(sub_folder_path):
                    os.rmdir(sub_folder_path)
