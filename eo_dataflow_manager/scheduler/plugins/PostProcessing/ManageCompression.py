import os

from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.com.sys.Folder import Folder
from eo_dataflow_manager.scheduler.plugins.PostProcessing.IPostProcessing import IPostProcessing
from eo_dataflow_manager.scheduler.sc.Download import FOLDER_TO_STORE, FOLDER_WAITING_TO_COMPLETE


class ManageCompression(IPostProcessing):

    def __init__(self, log, id, **kwargs):

        self._log = log
        self._id = id
        self._download_dirs = kwargs.get('downloadDirs', None)
        self._download_folders = kwargs.get('downloadFolders', None)
        self._download_path = kwargs.get('downloadPath', None)
        self._misc_param_integrity_check = kwargs.get('misc_param_integrity_check', None)
        self._zip_add_sub_directory = kwargs.get('zip_add_sub_directory', None)
        self._compression_action = kwargs.get('compression_action', None)
        self._compression_type = kwargs.get('compression_type', None)

    def process(self, downloadFile):
        ca = self._compression_action
        ct = self._compression_type
        self._log.debug('Begin Manage Compression %s ca: %s ct: %s' %
                        (downloadFile.localFile.getName(), ca, ct))
        if ca == 'uncompress':
            # 09/04/2018 PMT#34 ajout de la décompression des ZIP contenant
            # plusieurs fichiers
            if ct == File.ZIP and downloadFile.localFile.isZipFileWithMultipleFiles():
                self.manage_zip_file(downloadFile)
            else:
                if downloadFile.localFile.isCompressed(comptype=ct):
                    # Check if the file have a standard extension
                    if downloadFile.localFile.getName()[-(len(ct) + 1):] == '.' + ct \
                            or downloadFile.localFile.getName()[-(len(ct) + 1):].lower() == '.' + ct:
                        # Create new file, without the compress extension
                        unzip_file = File(os.path.join(downloadFile.localFile.getDirPath(
                        ), downloadFile.localFile.getName(short=True)[:-(len(ct) + 1)]))
                    else:
                        # create a explicite new file name for the uncompressed
                        # file
                        unzip_file = File(
                            os.path.join(
                                downloadFile.localFile.getDirPath(),
                                downloadFile.localFile.getName(
                                    short=True) + '.un' + ct + 'ed'))

                    # Try to uncompress the file
                    try:
                        unzip_file = downloadFile.localFile.uncompress(
                            unzip_file, ct)
                    except Exception as e:
                        self._log.exception(
                            'Uncompression error : %s' % (str(e)))
                        # Clean the bad unzipFile
                        if unzip_file.exist():
                            unzip_file.remove()
                        raise e
                    # la decompression s'est bien passee, on vire le fichier
                    # compresse
                    downloadFile.localFile.remove()
                    downloadFile.localFile = unzip_file
                else:
                    raise Exception(
                        "Try to uncompress a non-compressed '%s' file : %s" %
                        (ct, downloadFile.localFile.getName()))
        elif ca == 'compress':
            if ct == File.ZIP:
                self.manage_zip_file(downloadFile)
            elif ct == File.LZW:
                raise Exception(
                    f"Invalid compression type : '{ct}' [download_id={self._id}]")
            else:
                try:
                    new_file = File(
                        '%s.%s' %
                        (downloadFile.localFile.getName(short=True),
                         ct),
                        downloadFile.localFile.getFolder())
                    zip_file = downloadFile.localFile.compress(
                        dest=new_file, compress=ct)
                    downloadFile.localFile.remove()
                    downloadFile.localFile = zip_file
                except Exception as e:
                    self._log.exception('Invalid compression action: %s"' % (str(e)))
                    raise e
        else:
            raise Exception(
                "Invalid compression action : '%s' [download_id=%s]" %
                (ca, self._id))

    def get_folder(self, folder_id):
        folder = None
        if folder_id in list(self._download_dirs.keys()) and \
                folder_id in list(self._download_folders.keys()):
            if self._download_folders[folder_id] is not None:
                folder = self._download_folders[folder_id]
            else:
                relative_dir = self._download_dirs[folder_id]
                folder = Folder(os.path.join(
                    self._download_path, relative_dir), recursive=True)
                self._download_folders[folder_id] = folder

        return folder

    def manage_zip_file(self, downloaded_file):
        ca = self._compression_action
        ct = self._compression_type
        filepath = downloaded_file.localFile.getName()
        filename = downloaded_file.localFile.getName(short=True)
        self._log.debug('Begin Manage Zip archive %s type: %s, action: %s '
                        % (filepath, ct, ca))
        if ct == 'zip' and ca == 'uncompress':
            if downloaded_file.localFile.isZipFileWithMultipleFiles():
                if self._misc_param_integrity_check == 'SAFE':
                    base_extract_folder = self.get_folder(FOLDER_WAITING_TO_COMPLETE)
                else:
                    base_extract_folder = self.get_folder(FOLDER_TO_STORE)
                relative_path = downloaded_file.getRelativePath(dir_only=True)

                logical_folder_name = os.path.join(
                    base_extract_folder.getPath(),
                    relative_path,
                    'SAFE.' + filename)
                folder_name = logical_folder_name
                if self._zip_add_sub_directory:
                    folder_name = os.path.join(folder_name, filename[:-len(ct) - 1])

                extract_folder = Folder(folder_name, create=True)

                self._log.debug(
                    'download file %s is a Zip archive: extract it in %s' %
                    (filepath, extract_folder.getName()))

                # Try to uncompress the file
                try:
                    downloaded_file.localFile.extractFilesFromZip(
                        extract_folder)
                except Exception as e:
                    self._log.debug(
                        'extract Zip archive error in extractFilesFromZip: %s' %
                        (str(e)))
                    self._log.exception(
                        'extract Zip archive error : %s' % (str(e)))
                    # Clean the bad unzipFile
                    extract_folder.remove()
                    raise e

                # Extraction OK, remove zip file
                # indicates that the local target is a folder
                downloaded_file.localFolder = Folder(logical_folder_name)
                downloaded_file.isFolder = True
                downloaded_file.localFile.remove()
            else:
                self.process(downloaded_file)
        else:
            try:
                new_filename = f'{downloaded_file.localFile.getName()}.{ct}'
                new_file = File(new_filename)
                downloaded_file.localFile.remove()
                downloaded_file.localFile = new_file
            except Exception as e:
                self._log.exception('Compression error : %s"' % (str(e)))
                raise e
