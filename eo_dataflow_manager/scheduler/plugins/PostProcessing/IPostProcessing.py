#
# -*- coding: UTF-8 -*-
#


class IPostProcessing(object):
    """
    Interface class for L{IPostProcessing}
    """

    def process(self, downloadFile):
        """Implement a postProcessing method
        """
        raise RuntimeError(
            "process is an abstract method in IPostProcessing")

