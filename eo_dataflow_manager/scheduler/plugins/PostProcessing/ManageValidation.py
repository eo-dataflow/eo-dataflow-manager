import os

from eo_dataflow_manager.ifr_lib_modules.ifr_files import is_file_empty
from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.com.sys.Folder import Folder
from eo_dataflow_manager.scheduler.plugins.DataReader.NetCDFDataReader import NetCDFDataReader
from eo_dataflow_manager.scheduler.plugins.PostProcessing.IPostProcessing import IPostProcessing


class ManageValidation(IPostProcessing):

    def __init__(self, log, id, **kwargs):

        self._log = log
        self._id = id
        self._validation = kwargs.get('validation', None)

    def process(self, downloadFile):
        try:
            if downloadFile.isFolder is False:
                self.check_downloaded_file(downloadFile.localFile.getName(),
                                           True if self._validation == 'netcdf' else False)
            else:
                # If a folder is downloaded, check its files
                local_folder = Folder(downloadFile.localFolder.getPath(), recursive=True)
                for file in local_folder.values(restr=File):
                    self.check_downloaded_file(file.getName(),
                                               True if self._validation == 'netcdf' else False)
        except Exception as e:
            raise Exception(f'[{self._id}]: validity check error for file '
                            f"'{downloadFile.localFile.getName()}'. Error : {str(e)}")

    def check_downloaded_file(self, filepath: str, check_netcdf: bool = False):
        """
        Check if the downloaded file is empty and/or NetCDF validity.
        Args:
            filepath: the downloaded file path.
            check_netcdf: do NetCDF validity check if file extension is nc.
        Raises:
            Exception if empty file and/or not a valid NetCDF file.
        """
        if is_file_empty(filepath):
            raise Exception('Downloaded file {} is empty'.format(filepath))
        file, ext = os.path.splitext(filepath)
        if check_netcdf is True and ext == '.nc' and NetCDFDataReader.check_file(filepath) is False:
            raise Exception('Downloaded file {} is not a valid netcdf file'.format(filepath))
