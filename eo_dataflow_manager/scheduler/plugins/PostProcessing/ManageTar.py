import os

from eo_dataflow_manager.scheduler.com.sys.Folder import Folder
from eo_dataflow_manager.scheduler.plugins.PostProcessing.IPostProcessing import IPostProcessing
from eo_dataflow_manager.scheduler.sc.Download import FOLDER_TO_STORE, FOLDER_WAITING_TO_COMPLETE


class ManageTar(IPostProcessing):

    def __init__(self, log, id, **kwargs):

        self._log = log
        self._id = id
        self._download_dirs = kwargs.get('downloadDirs', None)
        self._download_folders = kwargs.get('downloadFolders', None)
        self._download_path = kwargs.get('downloadPath', None)
        self._tar_action = kwargs.get('tar_action', None)
        self._misc_param_integrity_check = kwargs.get('misc_param_integrity_check', None)
        self._tar_add_sub_directory = kwargs.get('tar_add_sub_directory', None)

    def process(self, downloadFile):
        filepath = downloadFile.localFile.getName()
        filename = downloadFile.localFile.getName(short=True)
        self._log.debug(f'[{self._id}]: Begin Manage Tar archive '
                        f"{downloadFile.localFile.getName()} action: '{self._tar_action}' ")
        if self._tar_action == 'extract':
            if downloadFile.localFile.isTarFile():
                if self._misc_param_integrity_check == 'SAFE':
                    base_extract_folder = self.get_folder(FOLDER_WAITING_TO_COMPLETE)
                else:
                    base_extract_folder = self.get_folder(FOLDER_TO_STORE)
                relative_path = downloadFile.getRelativePath(dir_only=True)

                logical_folder_name = os.path.join(
                    base_extract_folder.getPath(),
                    relative_path,
                    'SAFE.' + filename)
                folder_name = logical_folder_name
                if self._tar_add_sub_directory:
                    folder_name = os.path.join(folder_name, filename[:-len('.tar') - 1])

                # Add  'SAFE.'
                extract_folder = Folder(folder_name, create=True)

                self._log.debug(f'[{self._id}]: download file {filepath}'
                                f' is a tar file: extract it in {extract_folder.getName()}')

                # Try to untar the file
                try:
                    downloadFile.localFile.extractFilesFromTar(
                        extract_folder)
                except Exception as e:
                    self._log.debug(f'[{self._id}]: extract tarfile error in extractFilesFromTar: {str(e)}')
                    self._log.exception(f'[{self._id}]: extract tarfile error : {str(e)}')
                    # Clean the bad unzipFile
                    extract_folder.remove()
                    raise e

                # Extraction OK, remove tar file
                os.remove(filepath)
                # indicates that the local target is a folder
                downloadFile.localFolder = Folder(logical_folder_name)
                downloadFile.isFolder = True

            else:
                self._log.warning(f"[{self._id}]: manageTar : file '{filepath}'"
                                  f" ins't a tarfile. it will be treated as a normal file")

    def get_folder(self, folder_id):
        folder = None
        if folder_id in list(self._download_dirs.keys()) and \
                folder_id in list(self._download_folders.keys()):
            if self._download_folders[folder_id] is not None:
                folder = self._download_folders[folder_id]
            else:
                relative_dir = self._download_dirs[folder_id]
                folder = Folder(os.path.join(
                    self._download_path, relative_dir), recursive=True)
                self._download_folders[folder_id] = folder

        return folder
