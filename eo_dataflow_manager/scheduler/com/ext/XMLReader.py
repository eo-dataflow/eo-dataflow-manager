#
# -*- coding: UTF-8 -*-
#
"""XML File reader tools encapsulation
"""

__docformat__ = 'epytext'

import io
import logging
from xml.etree import ElementTree


class XMLReader(object):
    """
    Class XMLReader
    """

    def __init__(self, loggerName):
        """
        XMLReader initialization
        """

        self._log = logging.getLogger(loggerName)
        self._namespaces = None

    def open(self, file):
        try:
            self._namespaces = dict([node for _, node in ElementTree.iterparse(file, events=['start-ns'])])
            return ElementTree.parse(file)
        except Exception:
            self._log.exception(
                'Error during load/parse in xml (file : %s)' %
                file)
            raise

    def read_from_string(self, xml_data):
        try:
            self._namespaces = dict([node for _, node in ElementTree.iterparse(io.BytesIO(xml_data), events=['start-ns'])])
            return ElementTree.parse(io.BytesIO(xml_data))
        except Exception:
            self._log.exception(
                'Error during load/parse in xml data')
            raise

    def getNodeTagName(self, node):
        return node.tag

    def getNodeValue(self, node):
        return node.text

    def haveSubNode(self, node, subnodeName):
        # Other method than find ????
        tmpNode = node.find(subnodeName, namespaces=self._namespaces)
        if tmpNode is None:
            return False
        return True

    def getSubNode(self, level, node, subnodeName):
        tmpNode = node.find(subnodeName, namespaces=self._namespaces)
        if tmpNode is None:
            self._log.log(
                level, "the node '%s' don't have the required node '%s'" %
                (node.tag, subnodeName))
            raise IOError(
                "Invalid XML file, can't find '%s' in node %s" %
                (subnodeName, node.tag))

        return tmpNode

    def getSubNodeValue(self, level, node, subnodeName, default=None):
        tmpNode = node.find(subnodeName, namespaces=self._namespaces)
        if tmpNode is None:
            if default is None:
                self._log.log(
                    level, "the node '%s' don't have the required node '%s'" %
                    (node.tag, subnodeName))
                raise IOError(
                    "Invalid XML file, can't find '%s' in node %s" %
                    (subnodeName, node.tag))
            return default

        return tmpNode.text

    def getAllSubNode(self, node, subnodeName=None):
        if subnodeName is None:
            return node.getchildren()
        else:
            return node.findall(subnodeName, namespaces=self._namespaces)

    def getAttributeValue(self, level, node, attribName, default=None):
        if attribName not in node.attrib:
            if default is None:
                self._log.log(
                    level, "the node '%s' don't have the required attribute '%s'" %
                    (node.tag, attribName))
                raise IOError(
                    "Invalide XML, can't find '%s' attribute in node %s" %
                    (attribName, node.tag))
            return default

        return node.attrib[attribName]

    def getRootNode(self, level, tree, rootName=None):
        """
        Return the root node of a tree

        """
        tmpNode = tree.getroot()
        if rootName is not None and tmpNode.tag != rootName:
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! change KKKKK
            self._log.log(
                level, "the root node of the file %s isn't '%s'" %
                ('KKKK', rootName))
            raise IOError(
                "Invalid XML, found '%s' as root node instead of '%s' searched" %
                (tmpNode.tag, rootName))
        return tmpNode
