#
# -*- coding: UTF-8 -*-
#
"""XML File writer tools encapsulation

@author: Romain de Joux
@contact: U{Romain de Joux<mailto:rdejoux@ifremer.fr>}
@version: $Revision: 1.1.1.1 $
@status: Last modification $Date: 2005/01/29 16:09:47 $
@copyright: IFREMER
"""

__docformat__ = 'epytext'


try:
    from xml.etree import ElementTree
except ImportError:
    from elementtree import ElementTree


class XMLWriter(object):
    """
    Class XMLWriter
    """

    def __init__(self):
        """
        XMLWriter initialization
        """
        pass

    def write(self, xmlTree, file):
        try:
            root = xmlTree.getroot()
            self.__indent(root, 0)
            # ElementTree.dump(root)
            xmlString = ElementTree.tostring(root, encoding='unicode')
            if isinstance(file, str):
                with open(file, 'w') as fd:
                    fd.write(xmlString)
            else:
                file.write(xmlString)
        except Exception:
            #raise "Error during xml writing (file : %s)" % file
            raise

    def __indent(self, elem, level):
        lll = ["\n"]
        for index in range(level):
            lll.append(" ")

        i = ''.join(lll)

        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + " "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.__indent(elem, level + 1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def newXmlTree(self):
        return ElementTree.ElementTree()

    def xmlTreeSetRoot(self, tree, root):
        tree._setroot(root)

    def newNode(self, tag, text, tail="", attribute={}):
        e = ElementTree.Element(tag, attrib=attribute)
        e.text = text
        e.tail = tail
        return e

    def newSubNode(self, node, tag, text, tail="", attribute={}):
        e = ElementTree.SubElement(node, tag, attrib=attribute)
        e.text = text
        e.tail = tail
        return e

#    def nodeAddChild(self, node, subnode):
#        node.

    def nodeSetText(self, node, text):
        node.text = text

    def nodeAddAttrib(self, node, key, value):
        node.set(key, value)
