#
# -*- coding: UTF-8 -*-
#
import logging
import os
import re

from eo_dataflow_manager.scheduler.sc import Controller


class PatternFilter(object):
    def __init__(self):

        self.__filters_string = []
        self.__filters = []
        self._log = logging.getLogger(Controller.SCHD_LOGGER_NAME)

        self.__activated = False
        self.__accept_by_default = True

    def add(self, apply_to, regexp_force, regexp_ignore):

        self._log.debug(
            '     pattern filters configuration: add %s, accept : %s, refuse:%s' %
            (apply_to, regexp_force, regexp_ignore))

        if regexp_force:
            action = 'accept'
            self.__filters_string.append((apply_to, action, regexp_force))
            self.__filters.append((apply_to, action, re.compile(regexp_force)))
            self.__activated = True
            self.__accept_by_default = False

        if regexp_ignore:
            action = 'refuse'
            self.__filters_string.append((apply_to, action, regexp_ignore))
            self.__filters.append(
                (apply_to, action, re.compile(regexp_ignore)))
            self.__activated = True

        for afilter in self.__filters_string:
            apply_to, action, regexp = afilter
            self._log.debug(
                '  apply: %s, action: %s, regexp: %s' %
                (apply_to, action, regexp))

    def isAccepted(self, filepath):
        is_accepted = None
        if not self.__activated:
            return True

        filename = os.path.basename(filepath)
        dirpath = os.path.dirname(filepath)

        self._log.debug(
            "  file  '%s' : path '%s', filename '%s'" %
            (filepath, dirpath, filename))

        for afilter in self.__filters:
            apply_to, action, regexp = afilter
            self._log.debug('  apply: %s, action: %s' % (apply_to, action))
            if apply_to == 'filepath':
                base_string = filepath
            elif apply_to == 'filename':
                base_string = filename
            elif apply_to == 'path':
                base_string = dirpath

            if regexp.match(base_string) is not None:
                if action == 'accept':
                    is_accepted = True
                elif action == 'refuse':
                    is_accepted = False
                break
            self._log.debug(
                '     %s is_accepted:%s, default:%s' %
                (base_string, str(is_accepted), str(
                    self.accept_by_default)))

        if is_accepted is not None:
            return is_accepted
        else:
            return self.accept_by_default

    activated = property(
        lambda self: self.__activated,
        None,
        None,
        'is pattern filter activated ?')
    accept_by_default = property(
        lambda self: self.__accept_by_default,
        None,
        None,
        'default mode is accept ?')
    filters = property(
        lambda self: self.__filters,
        None,
        None,
        'filters regexp list')
    filters_string = property(
        lambda self: self.__filters_string,
        None,
        None,
        'filters regexp list (readable version)')
