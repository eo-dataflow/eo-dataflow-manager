
import json
import logging
import queue
import threading
import time
from datetime import datetime
from ssl import create_default_context

import pika

connectionPool = {}

MAX_RETRY = 100


class PublishRabbitMQ(threading.Thread):

    queues = {}

    def __init__(self,
                 host="localhost",
                 port=pika.connection.Parameters.DEFAULT_PORT,
                 ssl=pika.connection.Parameters.DEFAULT_SSL,
                 user=None,
                 password=None,
                 virtual_host=pika.connection.Parameters.DEFAULT_VIRTUAL_HOST,
                 queue_name="dl-****",
                 routing_key=None,
                 message_type='****'
                 ):

        threading.Thread.__init__(self, name=f'RabbitMQ {queue_name}')

        self.__log = logging.getLogger()

        sslContext = create_default_context() if ssl is True else None
        self.__connectionParams = {
            "host": host,
            "credentials": pika.credentials.PlainCredentials(user, password) if password is not None else
            pika.connection.Parameters.DEFAULT_CREDENTIALS,
            "ssl_options": pika.SSLOptions(sslContext) if sslContext is not None else None,
            "port": port,
            "virtual_host": virtual_host,
            "blocked_connection_timeout": 0,
            "heartbeat": 60
            }

        # Internal queue of messages to publish (asynchronous processing)
        self.__messages = queue.SimpleQueue()

        self.__queue_name = queue_name
        self.__routing_key = routing_key
        self.__running = False
        self.__message_type = message_type

    def run(self):

        def custom_serialize(value):
            if isinstance(value, datetime):
                return value.isoformat()
            assert 0, "Unknown type %s for %s" % (type(value), value)
            return ""

        connection = None
        channel = None

        self.__running = True

        self.__log.info(f"RabbitMQ publisher started : "
                        f"message type '{self.__message_type}', "
                        f"queue '{self.__queue_name}'")

        PublishRabbitMQ.queues[self.__message_type] = self

        retry = False
        retry_counter = 0

        while self.__running or not self.__messages.empty():
            # Ensures RabbitMQ connection is established and valid
            if not connection or connection.is_closed or channel.is_closed:
                # Establish new connection
                try:
                    connection = pika.BlockingConnection(
                        pika.ConnectionParameters(**self.__connectionParams)
                    )
                    channel = connection.channel()
                    # Declare queue
                    channel.queue_declare(
                        queue=self.__queue_name,
                        durable=True
                    )
                except (pika.exceptions.AMQPError, ConnectionRefusedError) as err:
                    retry_counter += 1
                    if retry_counter < 10:
                        self.__log.warning(
                            f"RabbitMQ connection failed for queue '{self.__queue_name}' parameters :\n'{self.__connectionParams}' ")
                        self.__log.warning(f"Trying again in {retry_counter * 2} seconds... ({retry_counter}/{MAX_RETRY})")
                        time.sleep(retry_counter * 2)
                        continue
                    break

                retry_counter = 0
                self.__log.debug(f"RabbitMQ connection for queue '{self.__queue_name}' established :\n'{self.__connectionParams}' ")

            # Check heartbeats
            connection.process_data_events()

            # Send pending message
            try:
                # Retrieve next pending message
                (identifier, message, content_type) = self.__messages.get(block=True, timeout=5)
                try:
                    # Send message to RabbitMQ broker
                    channel.basic_publish(
                        exchange="",
                        routing_key=self.__routing_key if self.__routing_key is not None else self.__queue_name,
                        mandatory=True,
                        # Messages properties : https://www.rabbitmq.com/publishers.html#message-properties
                        properties=pika.spec.BasicProperties(
                            message_id=identifier,
                            content_type=content_type,
                            # delivery_mode=2 ensures persistent messages (1 = transient)
                            delivery_mode=2
                        ),
                        body=json.dumps(message, default=custom_serialize)
                    )
                    # Message published = reset retry flag anyway
                    retry = False
                except (pika.exceptions.StreamLostError,
                        pika.exceptions.AMQPConnectionError,
                        pika.exceptions.ChannelWrongStateError) as e:
                    if retry:
                        retry = False
                        self.__log.error(f"The message type '{content_type}' "
                            f"cannot be written (error {e}), "
                            f"message :\n{message}"
                        )
                    else:
                        # Reset connection
                        connection = None
                        retry = True
                        self.__log.warning(f"Retry to publish message due to error {e}")
            except queue.Empty:
                pass
                # Next loop iteration

        connection.close()
        self.__log.info(f"RabbitMQ publisher stopped : "
                        f"message type '{self.__message_type}, "
                        f"queue '{self.__queue_name}'"
                        )

    def join(self, timeout=None):
        """
        Stop the thread
        """
        if self.__running:
            self.__running = False
            self.__log.debug(f"RabbitMQ publisher stopping '{self.getName()}' : "
                             f"message type '{self.__message_type}, "
                             f"queue '{self.__queue_name}', "
                             f"routing_key '{self.__routing_key}' "
                             )
            threading.Thread.join(self, timeout)
            PublishRabbitMQ.queues[self.__message_type] = None

    def __publish(self, identifier, message, content_type):
        assert isinstance(message, dict), "message must be a dict"
        self.__messages.put((identifier, message, content_type))

    @classmethod
    def publish(cls, identifier, message, content_type):
        if content_type in cls.queues and cls.queues[content_type] is not None:
            cls.queues[content_type].__publish(identifier, message, content_type)

    @classmethod
    def get_publisher(cls, queue_name, host, port, ssl, user, password, virtual_host, routing_key):
        if queue_name  in cls.queues:
            return cls.queues[queue_name]
        return None

