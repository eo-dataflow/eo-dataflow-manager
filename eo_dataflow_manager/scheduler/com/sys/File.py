#
# -*- coding: UTF-8 -*-
#
"""File is a facility layer for file management

@author: Romain de Joux
@contact: U{Romain de Joux<mailto:rdejoux@ifremer.fr>}
@version: 1.3
@date: 2005-10-06
@copyright: IFREMER
"""

__docformat__ = 'epytext'

import datetime

# unused : import os
import os.path
import shutil
import sys
import tarfile
import time  # for sleep in getLockFile
import zipfile
from bz2 import BZ2File

# Precondition : file extension matches file type
# - magic module is not needed anymore
# from magic import file as magicFile
from gzip import GzipFile
from hashlib import md5, sha1, sha256

from unlzw3 import unlzw

from . import Folder

LOCK_SLEEP_TIME = 0.2
READ_BUFFER = 1024 * 1024  # 1Mo


class File(object):

    """
    File is a facility layer for file management:
      - transparently open I{file} with C{file}, C{BZ2File}, C{GzipFile}
      - capability on file to:
         - L{copy}, L{move}, L{remove};
         - L{compress} and L{uncompress};
         - get and check validity with L{getCheckSum} and L{checkIntegrity}
         - Mapping common file operation (L{read}, L{write}, etc.)
      - easy interaction with L{Folder<Folder.Folder>} with L{copyTo} and L{moveTo}


    @group Opening: open, isOpen, fileDesc
    @group Compressing: compress, uncompress, isCompressed, getCompressionType
    @group Validity: checkIntegrity, getCheckSum
    @group LockManagement: getLock, isLock, unLock, isLockExpired
    @group FileManipulation: copy*, move*, remove, exist
    @group NativeFileMapping: __iter__, read*, write*, flush, close, isatty, seek, tell
    """

    # Define of class constant
    # Compression method
    GZIP = 'gz'
    BZIP2 = 'bz2'
    ZIP = 'zip'
    LZW = 'z'
    TGZ = 'tgz'
    LIST_COMPRESS = [ZIP, GZIP, BZIP2, LZW, TGZ]

    # Digest algorithm
    MD5 = 'md5'
    SHA = 'sha'
    SHA256 = 'sha256'
    LIST_DISGEST = [MD5, SHA, SHA256]

    def __init__(self, name, folder=None):
        """
        L{File} instantiation method

        @param name: filename on the disk
        @type name: C{str}
        """

        if isinstance(folder, (Folder.Folder, File)):
            self.__name = name
            self.__path = folder.path
        else:
            self.__path, self.__name = os.path.split(os.path.abspath(name))

        self.__mode = None
        self.__fd = False
        self.__lockFile = None

    def open(self, mode='r', compress=None, compressLevel=9):
        """Do the opening of the file

        @param mode: file opening mode
        @type mode: C{str}
        @param compress: compressing mode
        @type compress: C{str} contain value must be in L{File.LIST_COMPRESS} or C{None}
        @param compressLevel: compress level indicator
        @type compressLevel: C{int} must be between 0 and 9

        @raise OSError: if the instance of L{File} doesn't exist and open in read mode.
        @raise IOError: if the instance of L{File} is actually open.
        @raise TypeError: if compressLevel isn't a C{int}
        @raise ValueError: In three case:
                         1. if compress isn't in L{File.LIST_COMPRESS};
                         2. if compressLevel isn't between 0 and 9;
                         3. if the mode are invalid or incompatible
                         with the compressing mode.
        """

        # Check if the File is already open
        if self.isOpen():
            raise IOError('File (%s) already opened' % self.getName())

        # if open in read/append mode, overwrite the compress parameter
        if mode[0] in ('r', 'a'):
            if not self.exist():
                # If File doesn't exist, raise an OSError exception
                raise OSError("File (%s) doesn't exist," % self.getName() +
                              'can try to open it only in write mode')
            # Overwrite the compression type argument
            assert compress is None
            compress = self.getCompressionType()

        if not compress:
            opener = open
            validMode = ('r', 'a', 'w', 'rb', 'ab', 'wb',
                         'r+', 'a+', 'w+', 'r+b', 'a+b', 'w+b', 'x')
            args = {'mode': mode}
            if sys.version_info[0] == 2:
                args['name'] = self.getName()
            else:
                args['file'] = self.getName()
        else:
            if compress in File.LIST_COMPRESS:
                if not isinstance(compressLevel, int):
                    raise TypeError('compressLevel must be an integer')
                if compressLevel < 1 or compressLevel > 9:
                    raise ValueError('compressLevel must be between 0 and 9')
            else:
                raise ValueError(
                    'bad `compress`, value must be in File.LIST_COMPRESS')

        if compress in (File.GZIP, File.TGZ):
            opener = GzipFile
            validMode = ('r', 'a', 'w', 'rb', 'ab', 'wb')
            args = {'filename': self.getName(), 'mode': mode, 'compresslevel': compressLevel}
        elif compress == File.BZIP2:
            opener = BZ2File
            validMode = ('r', 'a', 'w', 'rb', 'ab', 'wb')
            args = {'filename': self.getName(), 'mode': mode, 'compresslevel': compressLevel}
        elif compress == File.ZIP:
            opener = zipfile.ZipFile
            validMode = ('r', 'a', 'w', 'rb', 'ab', 'wb')
            args = {'file': self.getName(), 'mode': mode, 'compression': zipfile.ZIP_DEFLATED}
        elif compress == File.LZW:
            opener = open
            validMode = ('rb')
            if sys.version_info[0] == 2:
                args = {'name': self.getName(), 'mode': mode}
            else:
                args = {'file': self.getName(), 'mode': mode}
        else:
            assert compress is None, 'Unknown compress mode: %s' % compress

        # Use opener, validMode and moreArgs to valid and do the opening
        if mode not in validMode:
            raise ValueError(
                'Invalid mode value (in relationship of the compress type ?)')
        self.__fd = opener(*[], **args)
        # save the mode in __mode
        self.__mode = mode

    def isOpen(self):
        """
        Return True if the File is open
        @rtype: C{bool}
        """
        return bool(self.__fd)

    def fileDesc(self):
        """Return the file descriptor of the L{File}"""
        return self.__fd

    def getName(self, short=False, real=False):
        """Return the filename of the L{File}

        @param short: flag use to get only the filename without path
        @type short: C{bool}
        """
        if short:
            return self.__name
        if real and self.isLink():
            readFile = os.readlink(os.path.join(self.__path, self.__name))
            if not os.path.isabs(readFile):
                readFile = os.path.join(self.__path, readFile)
            return os.path.abspath(readFile)
        return os.path.join(self.__path, self.__name)

    name = property(lambda self: self.getName(short=True), None, None,
                    'name of the file (short name)')

    path = property(lambda self: self.getName(short=False), None, None,
                    'File path')

    def getDirPath(self):
        """Return the directory path containing the L{File}"""
        return self.__path

    dirPath = property(getDirPath, None, None,
                       'path to the folder containing the file')

    def getFolder(self):
        """Return instance of L{Folder<Folder.Folder>} pointing the L{File} path

        @return: C{Folder(self.getDirPath())}
        @rtype: L{Folder<Folder.Folder>}"""
        return Folder.Folder(self.getDirPath())

    folder = property(getFolder, None, None,
                      'instance of L{Folder<Folder.Folder>} pointing the L{File} path')

    def exist(self):
        """Return True if the file exist
        @rtype: C{bool}
        """
        return os.path.isfile(self.getName())

    def isLink(self):
        """Return True if the file is a link
        @rtype: C{bool}
        """
        return os.path.islink(self.getName())

    def getRealFile(self):
        """Return the file pointed self (if self.isLink() == True)
        @rtype: C{File}
        """
        if not self.isLink():
            raise "File %s isn't a symbolic link, can't return real file" % self.getName()
        return File(self.getName(real=True))

    def getCompressionType(self):
        """Return the compression type of the L{File}"""
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())

        extension = self.__name.split('.')[-1].lower()
        if extension in File.LIST_COMPRESS:
            return extension
        else:
            if zipfile.is_zipfile(self.getName()):
                return 'zip'
            return None

    def isCompressed(self, comptype='any'):
        """Return True if the L{File} is compressed

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise ValueError: if comptype not in L{File.LIST_COMPRESS} and not equal 'any'.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if comptype not in File.LIST_COMPRESS and comptype != 'any':
            raise ValueError('File.isCompressed: bad `comptype` argument value')
        ct = self.getCompressionType()
        if ct is None:
            return False
        if comptype != 'any':
            return comptype == ct
        return True

    def isTarFile(self):
        """Return True if the L{File} is a tar file

        @raise OSError: if the instance of L{File} doesn't exist.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        return tarfile.is_tarfile(self.getName())

    def compress(self, dest=None, compress=GZIP, compressLevel=9):
        """Do the compression of the L{File}

        @param dest: destination of the compress data
        @type dest: L{File}
        @param compress: compressing mode.
        @type compress: C{str} contain value must be in L{File.LIST_COMPRESS} or C{None}
        @param compressLevel: compress level indicator.
        @type compressLevel: C{int} must be between 0 and 9

        @return: the compressed L{File}
        @rtype: L{File}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} is actually open.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('I/O operation on open file')
        # compress and compressLevel argument will be test in File open method
        if dest:
            if not isinstance(dest, File):
                raise TypeError('dest must be a instance of File')
            zipFile = dest
        else:
            # Create a File with current name and compress type in extension for name
            zipFile = File(self.getName() + '.' + compress)
        # Open the new file in write mode and in compression mode,
        # in the specified compress level
        try:
            zipFile.open('w', compress, compressLevel)
            # open of the current file in read mode
            self.open('rb')
            # do the compression copy (with bufferization)
            buff = True
            while buff:
                buff = self.read(READ_BUFFER)
                if buff:
                    zipFile.write(buff)
            # Close the two files
        finally:
            self.close()
            zipFile.close()

        return zipFile

    def uncompress(self, dest=None, comptype='any'):
        """Do the uncompression of the L{File}

        @param dest: destination of the uncompress data
        @type dest: L{File}

        @return: the uncompressed L{File}
        @rtype: L{File}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: In two case:
                         1. if the instance of L{File} is actually open;
                         2. if the instance of L{File} isn't compressed.
        """

        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('I/O operation on open file')
        ct = self.getCompressionType()
        if not ct:
            raise IOError("File '%s' isn't compressed" % self.getName())

        if dest:
            if not isinstance(dest, File):
                raise TypeError('dest must be a instance of File')
            unzipFile = dest
        else:
            # Check if the file have a standard extension
            if self.getName()[-(len(ct) + 1):] == '.' + ct:
                # Create new file, without the compress extension
                unzipFile = File(self.getName()[:-(len(ct) + 1)])
            else:
                # create a explicite new file name for the uncompressed file
                unzipFile = File(self.getName() + '.un' + ct + 'ed')

        if comptype == File.ZIP:
            # Open the current compressed file
            self.open('r')
            filenames = self.__fd.namelist()
            if len(filenames) > 1:
                raise Exception('ZipFile must only contains one file')

            # Open the new uncompressed file
            _filepath, name = os.path.split(filenames[0])
            unzipFile = File(os.path.join(self.__path, name))
            unzipFile.open('wb')
            for filename in filenames:
                try:
                    data = self.__fd.read(filename)
                    unzipFile.write(data)
                except:
                    self.close()
                    unzipFile.close()
                    raise
        else:
            # Open the current compressed file
            self.open('rb')
            if comptype == File.LZW:
                data_uncompress = True
                # Open the new uncompressed file
                unzipFile.open('wb')
                try:
                    data = self.__fd.read()
                    data_uncompress = unlzw(data)
                    unzipFile.write(data_uncompress)
                except:
                    self.close()
                    unzipFile.close()
                    raise
            else:
                # Open the new uncompressed file
                unzipFile.open('wb')
                # do the uncompression copy (with bufferization)
                buff = True
                try:
                    while buff:
                        buff = self.read(READ_BUFFER)
                        if buff:
                            unzipFile.write(buff)
                except:
                    # Close the two files
                    self.close()
                    unzipFile.close()
                    raise

        # Close the two files
        self.close()
        unzipFile.close()

        return unzipFile

    def extractFilesFromTar(self, dest=None):
        """Do the extraction of the tar L{File}

        @param dest: destination directory
        @type dest: L{Folder}
        @param dest: add extractFilesFromTar in a directory
        @type dest: L{Boolean}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: In two case:
                         1. if the instance of L{File} is actually open;
                         2. if the instance of L{File} isn't tar file.
        """

        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('I/O operation on open file')
        if not tarfile.is_tarfile(self.getName()):
            raise IOError("File '%s' isn't a tar file" % self.getName())
        if dest is not None:
            if not isinstance(dest, Folder.Folder):
                raise TypeError('dest must be a instance of Folder')
        else:
            raise IOError("Destination isn't defined")

        # Open the current compressed file
        tarFile = tarfile.open(self.getName())

        # Open the new uncompressed file
        tarFile.extractall(path=dest.getPath())

        tarFile.close()

    def extractFilesFromZip(self, dest=None):
        """Do the extraction of the zip L{File}

        @param dest: destination directory
        @type dest: L{Folder}
        @param dest: add extractFilesFromZip in a directory
        @type dest: L{Boolean}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: In two case:
                         1. if the instance of L{File} is actually open;
                         2. if the instance of L{File} isn't tar file.
        """

        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('I/O operation on open file')
        if not zipfile.is_zipfile(self.getName()):
            raise IOError("File '%s' isn't a zip file" % self.getName())
        if dest is not None:
            if not isinstance(dest, Folder.Folder):
                raise TypeError('dest must be a instance of Folder')
        else:
            raise IOError("Destination isn't defined")

        # Open the current compressed file
        zipFile = zipfile.ZipFile(self.getName())

        # Open the new uncompressed file
        zipFile.extractall(path=dest.getPath())

        zipFile.close()

    def compressFileToZip(self, dest=None, replace=False):
        """Do the zip compression of L{File}

        @param dest: destination file
        @type dest: L{str}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: In two case:
                         1. if the instance of L{File} is actually open;
                         2. if the instance of L{File} isn't tar file.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('I/O operation on open file')
        if dest is not None:
            if os.path.exists(dest):
                if replace:
                    os.remove(dest)
                else:
                    raise IOError('Destination file must not exist')
        else:
            raise IOError("Destination isn't defined")

        # Open the destination compressed file
        with zipfile.ZipFile(dest, mode='x') as zipFile:
            # Add the new file in zip file
            zipFile.write(self.getName(), self.getName(short=True))

    def isZipFileWithMultipleFiles(self):
        """Return True if the L{File} is a ZIP file with multiple files

        @raise OSError: if the instance of L{File} doesn't exist.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if not zipfile.is_zipfile(self.getName()):
            return False
        self.open()
        filenames = self.__fd.namelist()
        self.close()
        return len(filenames) > 1

    def getUniqueDirectoryTar(self):
        """ Check the contains of the L{File} and return name of unique root folder or ''

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} isn't tar file.
        """

        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if not self.isTarFile():
            raise IOError("File '%s' isn't a tar file" % self.getName())

        tar_file = tarfile.TarFile(self.getName())

        file_names = tar_file.getnames()
        tar_file.close()

        folder_name = file_names[0]
        check_name = folder_name + '/'

        for file_name in file_names[1:]:
            if file_name[:len(check_name)] != check_name:
                folder_name = ''

        return folder_name

    def getUniqueDirectoryZip(self):
        """ Check the contains of the L{File} and return name of unique root folder or ''

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} isn't tar file.
        """

        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if not zipfile.is_zipfile(self.getName()):
            raise IOError("File '%s' isn't a zip file" % self.getName())

        zip_file = zipfile.ZipFile(self.getName())
        file_names = zip_file.namelist()
        zip_file.close()

        folder_name = file_names[0]

        for file_name in file_names[1:]:
            if file_name[:len(folder_name)] != folder_name:
                folder_name = ''

        return folder_name

    def checkCompressIntegrity(self):
        """Check the compression integrity of the L{File}

        @return: True if the L{File} is valid
        @rtype: C{bool}
        """

        try:
            self.uncompress(File('/dev/null'))
        except:
            return False

        return True

    def getCheckSum(self, cksumtype=MD5, hexdigest=True):
        """Compute the cryptographic hashes of the L{File}

        @param cksumtype: cksumtype of hashes use to compute the checksum
        @type cksumtype: L{str} that must be in L{File.LIST_DISGEST}
        @param hexdigest: ask for hexdigest instaid of bindigest
        @type hexdigest: C{bool}

        @return: the computed hash value
        @rtype:C{str}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise ValueError: if cksumtype not in L{File.LIST_DISGEST}.
        """

        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if cksumtype not in File.LIST_DISGEST:
            raise ValueError('bad `cksumtype`, value must be in File.LIST_DISGEST')

        chk = {
            File.MD5: md5(),
            File.SHA: sha1(),
            File.SHA256: sha256(),
        }[cksumtype]

        with open(self.getName(), 'rb') as fd:
            # do checksum computation (with bufferization)
            buff = True
            while buff:
                buff = fd.read(READ_BUFFER)
                if buff:
                    chk.update(buff)

        if hexdigest:
            return chk.hexdigest()
        return chk.digest()

    def checkIntegrity(self, cksum, cksumtype=MD5, hexdigest=True):
        """Check the cryptographic hashes of the L{File}

        @param cksum: the checksum to compare
        @type cksum: C{str}
        @param cksumtype: cksumtype of hashes use to compute the checksum
        @type cksumtype: L{str} that must be in L{File.LIST_DISGEST}
        @param hexdigest: ask for hexdigest instead of bindigest
        @type hexdigest: C{bool}

        @rtype: C{bool}
        """
        return cksum == self.getCheckSum(cksumtype, hexdigest)

    # Filesystem operation methods
    def move(self, newName):
        """Move the L{File} to a new place

        @param newName: the new L{File} name
        @type newName: C{str}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} is actually open.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('FileSystem operation on open file')
        shutil.move(self.getName(), newName)
        self.__path, self.__name = os.path.split(os.path.abspath(newName))

    def moveTo(self, folder):
        """Move the L{File} to the L{Folder<Folder.Folder>}

        @param folder: the destination folder
        @type folder: L{Folder<Folder.Folder>}

        @raise TypeError: if the C{folder} parameter isn't a L{Folder<Folder.Folder>}
        """

        if not isinstance(folder, Folder.Folder):
            raise TypeError('Invalid argument type, need a Folder object')

        self.move(os.path.join(folder.path, self.name))

    def link(self, linkName, symbolic=False, keepExist=False):
        """Link the L{File} to a new place

        @param linkName: the link L{File} name
        @type linkName: C{str}
        @param symbolic: create a symbolic link instead of a hard link (default C{False})
        @type symbolic: C{bool}
        @param keepExist: If true, do not overwrite L{linkName} if exist (default C{False})
        @type keepExist: C{bool}
        @return: a L{File} instance of the link file
        @rtype: L{File}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} is actually open.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('FileSystem operation on open file')

        if not keepExist and (os.path.isfile(linkName) or
                              os.path.islink(linkName)):
            os.remove(linkName)
        if symbolic:
            os.symlink(self.getName(), linkName)
        else:
            os.link(self.getName(), linkName)
        return File(linkName)

    def copy(self, copyName):
        """Copy the L{File} to a new place

        @param copyName: the new L{File} name
        @type copyName: C{str}
        @return: a L{File} instance of the new file
        @rtype: L{File}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} is actually open.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('FileSystem operation on open file')
        if self.isLink():
            real = self.getRealFile()
            return real.link(copyName, symbolic=True)
        shutil.copy(self.getName(), copyName)
        return File(copyName)

    def copyTo(self, folder):
        """Copy the L{File} to the L{Folder<Folder.Folder>}

        @param folder: the destination folder
        @type folder: L{Folder<Folder.Folder>}
        @return: a L{File} instance of the new file
        @rtype: L{File}

        @raise TypeError: if the C{folder} parameter isn't a L{Folder<Folder.Folder>}
        """
        if not isinstance(folder, Folder.Folder):
            raise TypeError('Invalid argument type, need a Folder object')
        return self.copy(os.path.join(folder.path, self.name))

    def remove(self):
        """Remove the L{File} from the disk

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} is actually open.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError('FileSystem operation on open file')
        os.remove(self.getName())

    def getSize(self):
        """Return the size of the L{File}

        @return: the size of the L{File}
        @rtype: C{int}

        @raise OSError: if the instance of L{File} doesn't exist.
        """
        try:
            return os.path.getsize(self.getName())
        except:
            raise OSError("File '%s' doesn't exist" % self.getName())

    def getTime(self):
        """Return the time of last modification of the L{File}

        @return: the time of last modification of the L{File}
        @rtype: C{int}

        @raise OSError: if the instance of L{File} doesn't exist.
        """
        try:
            return os.path.getmtime(self.getName())
        except:
            raise OSError("File '%s' doesn't exist" % self.getName())

    # Lock file management
    def isLock(self):
        return self.__lockFile is not None and self.__lockFile.exist()

    def isLockExpired(self, delay, lockExtension='.lock'):
        lf = File(self.getName() + lockExtension)
        expired = True
        if lf.exist():
            expired = datetime.datetime.now() - datetime.datetime.fromtimestamp(lf.getTime()
                                                                                ) > datetime.timedelta(seconds=delay)
        return expired

    def acquireLock(self, lockvalid, lockExtension='.lock'):
        if self.__lockFile is not None:
            raise RuntimeError('File is already locked with %s' %
                               str(self.__lockFile))

        lf = File(self.getName() + lockExtension)
        if lf.exist():
            return False
        # Try to get the lock
        try:
            lf.open('w')
            lf.write(lockvalid)
            lf.close()
            time.sleep(LOCK_SLEEP_TIME)
            lf.open()
            line = lf.readline()
            lf.close()
        except IOError as e:
            print('cannot open', self.getName() + lockExtension)
            print(e)
            return False
        lockvalid_l1 = lockvalid.split('\n')[0].strip()
        if line.strip() != lockvalid_l1:
            return False

        self.__lockFile = lf
        return True

    def getLock(self):
        if self.__lockFile is None:
            raise RuntimeError(
                "File hasn't lock file (use acquireLock to get one)")
        return self.__lockFile

    def unLock(self):
        if self.__lockFile is None:
            raise RuntimeError("File isn't locked, can't unLock the File")
        # Check if already exist
        if self.__lockFile.exist():
            # Close to can remove
            self.__lockFile.close()
            self.__lockFile.remove()
        # Unlock the file
        self.__lockFile = None

    def resetLock(self):
        """unLock file in memory only (don't remove lock file on disk). To use carefully !
        Mainly use by Scheduler process, to free lock on files which are sent to processing, because
        AbstractprocessRunner removes the lock file (on disk) put by the scheduler when starting processing
        """
        if self.__lockFile is None:
            raise RuntimeError("File isn't locked, can't unLock the File")
        self.__lockFile = None

    # define __repr__ use to get a textual presentation of the object with print
    def __repr__(self):
        """use to get a textual presentation of the L{File} with print"""
        return self.path

    # define __str__ use to get a textual representation of the object with str()
    def __str__(self):
        """use to get a textual representation of the L{File} with str()"""
        return self.__repr__()

    # define __eq__ to set the equality of to File that have same file for target
    def __eq__(self, other_file):
        """set the equality of to File that have same file for target"""
        if isinstance(other_file, File):
            return self.getName() == other_file.getName()
        return False

    # define __hash__ to get the unique hash for an unique filename (with path)
    def __hash__(self):
        """get the unique hash for an unique filename (with path)"""
        return hash('%d%s' % (hash(File), self.getName()))

    # define the iterable capability of the File object
    def __iter__(self):
        """x.__iter__() <==> iter(x)"""
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.__iter__()

    # Mapping/define properties of File object
    size = property(getSize, None, None,
                    'size of the L{File}')

    name = property(lambda self: self.__name, None, None,
                    'name of the L{File}')

    mode = property(lambda self: self.__mode, None, None,
                    "opening's mode of the L{File}")

    closed = property(lambda self: not self.isOpen(), None, None,
                      'equal C{True} if the L{File} is close, C{False} if open')

    # mapping common file, GzipFile and BZipFile methods
    def close(self):
        """close() -> None or (perhaps) an integer.  Close the file.

        Sets data attribute .closed to True.  A closed file cannot be used for
        further I/O operations.  close() may be called more than once without
        error.  Some kinds of file objects (for example, opened by popen())
        may return an exit status upon closing.
        """
        if self.__fd:
            self.__fd.close()
            self.__fd = None
            self.__mode = None

    def flush(self, *args):
        """flush() -> None.  Flush the internal I/O buffer."""
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.flush(*args)

    def isatty(self, *args):
        """isatty() -> true or false.  True if the file is connected to a tty device."""
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.isatty(*args)

    def read(self, *args):
        """read([size]) -> read at most size bytes, returned as a string.

        If the size argument is negative or omitted, read until EOF is reached.
        Notice that when in non-blocking mode, less data than what was requested
        may be returned, even if no size parameter was given.
        """
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.read(*args)

    def readline(self, *args):
        """readline([size]) -> next line from the file, as a string.

        Retain newline.  A non-negative size argument limits the maximum
        number of bytes to return (an incomplete line may be returned then).
        Return an empty string at EOF.
        """
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.readline(*args)

    def readlines(self, *args):
        """
        readlines([size]) -> list of strings, each a line from the file.

        Call readline() repeatedly and return a list of the lines so read.
        The optional size argument, if given, is an approximate bound on the
        total number of bytes in the lines returned.
        """
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.readlines(*args)

    def seek(self, *args):
        """
        seek(offset[, whence]) -> None.  Move to new file position.

        Argument offset is a byte count.  Optional argument whence defaults to
        0 (offset from start of file, offset should be >= 0); other values are 1
        (move relative to current position, positive or negative), and 2 (move
        relative to end of file, usually negative, although many platforms allow
        seeking beyond the end of a file).  If the file is opened in text mode,
        only offsets returned by tell() are legal.  Use of other offsets causes
        undefined behavior.
        Note that not all file objects are seekable.
        """
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.seek(*args)

    def truncate(self, *args):
        """
        truncate([size]) -> None.  truncate the file to the given size.
        """
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.truncate(*args)

    def tell(self, *args):
        """tell() -> current file position, an integer (may be a long integer)."""
        if not self.isOpen():
            raise IOError('I/O operation onclosed file')
        return self.__fd.tell(*args)

    def write(self, *args):
        """
        write(str) -> None.  Write string str to file.

        Note that due to buffering, flush() or close() may be needed before
        the file on disk reflects the data written.
        """
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.write(*args)

    def writelines(self, *args):
        """
        writelines(sequence_of_strings) -> None.  Write the strings to the file.

        Note that newlines are not added.  The sequence can be any iterable object
        producing strings. This is equivalent to calling write() for each string.
        """
        if not self.isOpen():
            raise IOError('I/O operation on closed file')
        return self.__fd.writelines(*args)
