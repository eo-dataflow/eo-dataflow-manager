#
# -*- coding: UTF-8 -*-
#
"""Folder is a facility layer for folder management

@author: Romain de Joux
@contact: U{Romain de Joux<mailto:rdejoux@ifremer.fr>}
@version: $Revision: 1.1.1.1 $
@status: Last modification $Date: 2005/01/29 16:09:47 $
@copyright: IFREMER
"""

__docformat__ = 'epytext'

import os.path
import shutil
import sys

from eo_dataflow_manager.ifr_lib_modules.ifr_os import is_io_locked

if sys.version_info[0] == 2:
    # Python2 import fix
    import File  # pylint: disable=import-error
else:
    from . import File

# unused : from sets import Set
# Gestion getTar + option compress=None || gzip || bz2


def nfs_check(folderpath, timeout=2.0):
    try:
        if is_io_locked(folderpath, timeout):
            raise IOError('Folder::nfs_check: timeout')
    except Exception as e:
        raise IOError(str(e))


class Folder(object):
    """
    Folder is a facility layer for folder management

    @group Accessors: get*, set*
    @group FolderManipulation: copy*, move*, remove
    @group Updating: scan, _walkInFolder
    """

    def __init__(self, path, recursive=False, create=False, scan=True, probing=True):
        """
        L{Folder} instantiation method

        @param path: folder path on the disk
        @type path: C{str}
        @param recursive: define the scan mode of the L{Folder}
        @type recursive: C{bool}
        @param create: flag to ask L{Folder} to create C{path} on the disk
        @type create: C{bool}

        @raise ValueError: if C{path} is in invalid folder path
        """

        # Check the validity of the path
        if not os.path.isdir(path):
            if not os.path.exists(path) and create:
                try:
                    os.makedirs(path)
                except:
                    raise ValueError("Can't create folder '%s'" % path)
            else:
                raise ValueError("Folder does not exist: %s" % (path))

        # Save the valid path
        self.__path = os.path.abspath(path)

        # Initialize dictionaries of folder entry (one for files, one for folders)!
        self.__filesContent = {}
        self.__foldersContent = {}

        # Save the recursive mode
        self.__recursive = recursive

        # Set probing option
        self.__probing = probing

        # Set scanned state
        self.__scanned = False

        # little hack (should be in a specialized class 'OutputFolder'...), to
        # manage the localcache option : when set to 'true', each file of
        # the output folder is saved to the cache, and a symbolic link is created in the
        # output folder to the cached file. Only available with output folders !
        self.use_local_cache = False

        # another little hack (should be in a specialized class 'InputFolder'...), to
        # manage the "preferred host" option for the Folder. Mainly used by DiskSpool "input_spool",
        # to force the use of hosts by input_spool instead of by node
        self.possible_host = []

        self.nfs_check_error = False

        # Update the content of the folder
        if scan is True:
            try:
                self.scan()
            except Exception:
                # not really important here, since we will do it after, when needed (using self.__scanned flag)
                pass

    def getParent(self):
        """Return the L{Folder} instance of the parent of self

        @return: L{Folder} parent of self
        @rtype: L{Folder}
        """
        return Folder(os.path.split(self.__path)[0], scan=False)

    def getName(self):
        """Return the name of the L{Folder}

        @return: name of the L{Folder}
        @rtype: C{str}
        """
        return os.path.split(self.__path)[1]

    name = property(getName, None, None,
                    "get the name of the L{Folder}")

    def getPath(self):
        """Return the path of the L{Folder}

        @return: path of the L{Folder}
        @rtype: C{str}
        """
        return self.__path

    path = property(getPath, None, None,
                    "get the path of the L{Folder}")

    def remove(self, recursively=False):
        """Remove the L{Folder} from the disk

        @param recursively: flag to remove folder recursively
        @type recursively: C{bool}

        @raise IOError: if the L{Folder} isn't empty and C{recursively=False}
        """
        if recursively:
            shutil.rmtree(self.getPath(), ignore_errors=True)
        else:
            if len(self) != 0:
                raise IOError("Folder is not empty")
            else:
                os.rmdir(self.getPath())

    def move(self, dstPath):
        """Move the L{Folder} to a new place

        @param dstPath: the new L{Folder} path
        @type dstPath: C{str}

        @raise TypeError: if C{otherPath} isn't a string.
        @raise ValueError: if C{otherPath} isn't a valid path
        """
        if type(dstPath) != str:
            raise TypeError("dstPath must be a string")
        # if not os.path.isdir(otherPath):
        #    raise ValueError,"Invalid folder path"
        # if dstName == None:
        #    dstName = self.getName()
        shutil.move(self.getPath(), dstPath)

    def moveIn(self, f):
        """Move the L{File<File.File>} f to the L{Folder}

        @param f: A L{File<File.File>} to move in C{self}
        @type f: L{File<File.File>}

        @raise TypeError: if f isn't a L{File<File.File>}
        """
        if not isinstance(f, File.File):
            raise TypeError("Invalid argument type, need a File object")

        f.move(os.path.join(self.path, f.name))

    def copy(self, newPath):
        """Copy the L{Folder} to a new place

        @param newPath: the new L{Folder} path
        @type newPath: C{str}

        @raise TypeError: if C{newPath} isn't a string.
        @raise ValueError: if C{newPath} isn't a valid path
        """
        if type(newPath) != str:
            raise TypeError("newPath must be a string")
        if not os.path.isdir(newPath):
            raise ValueError("Invalid folder path")
        shutil.copytree(self.getPath(), newPath + self.getName())

    def copyIn(self, f):
        """Copy the L{File<File.File>} f to the L{Folder}

        @param f: A L{File<File.File>} to copy in C{self}
        @type f: L{File<File.File>}
        @return: a L{File} instance of the new file
        @rtype: L{File}

        @raise TypeError: if f isn't a L{File<File.File>}
        """
        if not isinstance(f, File.File):
            raise TypeError("Invalid argument type, need a File object")
        return f.copy(os.path.join(self.path, f.name))

    # Private methods use by the dictionary mapping
    def __makeAllValues(self, restr=None):
        """Private methods use by the dictionary mapping
        """
        # do not scan, because it must have been done before the __makeAllValues() call
        # if self.__scanned == False:
        #    self.scan()

        if restr is None or restr == File.File:
            for f in list(self.__filesContent.keys()):
                self.__makeFile(f)
        if restr is None or restr == Folder:
            for F in list(self.__foldersContent.keys()):
                self.__makeFolder(F)

    # Create a File object for the file 'key'
    def __makeFile(self, key):
        """Private methods
        """
        # do not scan, because it must have been done before the __makeFile() call
        # if self.__scanned == False:
        #    self.scan()

        entry = self.__filesContent[key]
        if not entry:
            self.__filesContent[key] = File.File(
                os.path.join(self.__path, key))

    # Create a Folder object for the folder 'key'
    def __makeFolder(self, key):
        """Private methods
        """
        # do not scan, because it must have been done before the __makeFolder() call
        # if self.__scanned == False:
        #    self.scan()

        entry = self.__foldersContent[key]
        if not entry:
            self.__foldersContent[key] = Folder(
                os.path.join(self.__path, key), self.__recursive)

    # Properties about the recursive mode
    def getRecursive(self):
        """Get the recursive flag of the L{Folder}

        @return: recursive flag status
        @rtype: C{bool}
        """
        return self.__recursive

    def setRecursive(self, flag):
        """Set the recursive flag of the L{Folder}

        @param flag: recursive flag status
        @type flag: C{bool}

        @raise TypeError: if C{flag} isn't a bool.
        """
        if type(flag) != bool:
            raise TypeError("flag must be a bool (True/False)")
        self.__recursive = flag

    recursive = property(getRecursive, setRecursive, None,
                         "get/set the recursive flag of the Folder")

    def _sortMethod(self, folder, filename):
        return os.stat(os.path.join(folder, filename)).st_mtime

    def _walkInFolder(self):
        """Method use by the L{scan} to get the disk content of the L{Folder}

        @return: a C{yield} of C{tuple} contain filename and C{isfile} bool information
        @rtype: C{yield} of C{tuple}
        """
        ##
        # @return: a yield of tuple contain filename and isfile bool information

        if not self.__recursive:
            # OLD : first = os.walk(self.__path).next()
            next(os.walk(self.__path))
            # Take only the first one
            iterator = [next(os.walk(self.__path)), ]
        else:
            iterator = os.walk(self.__path)  # Store Iterator in iterator

        # Loop on iterator
        for c, lF, lf in iterator:
            pathFromCurrent = c[len(self.__path) + 1:]
            for f in lf:
                yield os.path.join(pathFromCurrent, f), True  # => is a file
            for folder in lF:
                # => not a file
                yield os.path.join(pathFromCurrent, folder), False

    # define __repr__ use to get a textual presentation of the object with print
    def __repr__(self):
        return self.__path

    # define __str__ use to get a textual representation of the object with str()
    def __str__(self):
        return self.__repr__()

    # Check if the network folder is accessible (in case of NFS mounts)
    def __nfsCheck(self, timeout=0.5):
        from eo_dataflow_manager.ifr_lib_modules.ifr_os import is_io_locked
        self.nfs_check_error = False
        try:
            if is_io_locked(self.path, timeout):
                self.nfs_check_error = True
                raise IOError('Folder::nfs_check: server not responding')
        except Exception as e:
            raise IOError(str(e))


    # Update Folder content from FileSystem
    def scan(self):
        """Scan the disk to update the L{Folder} content
        """

        tmpFilesContent = {}
        tmpFoldersContent = {}

        # Reset the last scan, in case of raise (to avoid the return of obsolete content lists)
        self.__filesContent = tmpFilesContent
        self.__foldersContent = tmpFoldersContent
        self.__scanned = False

        # check that the directory is available to avoid NFS Freezes for example. The probeDir program
        # always exits shortly (and do not wait for the folder to become available again when nfs problems occurs)
        if self.__probing:
            nfs_check(self.path)

        for key, isFile in self._walkInFolder():
            if isFile:
                if key in self.__filesContent:
                    tmpFilesContent[key] = self.__filesContent[key]
                else:  # File not present at the previous scan
                    tmpFilesContent[key] = None
            else:  # It's a folder
                if key in self.__foldersContent:
                    tmpFoldersContent[key] = self.__foldersContent[key]
                else:  # Folder not present at the previous scan
                    tmpFoldersContent[key] = None

        self.__filesContent = tmpFilesContent
        self.__foldersContent = tmpFoldersContent
        self.__scanned = True

    # Get information, Dictionary mapping
    def items(self, restr=None):
        """Return L{Folder} items

        @param restr: Restriction on return items type
        @type restr: L{Folder} or L{File<File.File>} class

        @rtype: C{list} of C{tuple} contain (names,
                L{File<File.File>} or L{Folder})
        """
        if self.__scanned is False:
            try:
                self.scan()
            except Exception:
                pass

        self.__makeAllValues(restr)
        if restr == File.File:
            return list(self.__filesContent.items())
        if restr == Folder:
            return list(self.__foldersContent.items())
        # Any other case, return all
        return list(self.__foldersContent.items()) + list(self.__filesContent.items())

    def keys(self, restr=None):
        """Return L{Folder} keys (ie file or folder name)

        @param restr: Restriction on return keys type
        @type restr: L{Folder} or L{File<File.File>} class

        @rtype: C{list} of C{str}
        """
        if self.__scanned is False:
            try:
                self.scan()
            except Exception:
                pass

        if restr == File.File:
            return list(self.__filesContent.keys())
        if restr == Folder:
            return list(self.__foldersContent.keys())
        # Any other case, return all
        return list(self.__foldersContent.keys()) + list(self.__filesContent.keys())

    def values(self, restr=None):
        """Return L{Folder} values

        @param restr: Restriction on return values type
        @type restr: L{Folder} or L{File<File.File>} class

        @rtype: C{list} of L{File<File.File>} and/or L{Folder}
        """
        if self.__scanned is False:
            try:
                self.scan()
            except Exception:
                pass

        self.__makeAllValues(restr)

        if restr == File.File:
            return list(self.__filesContent.values())
        if restr == Folder:
            return list(self.__foldersContent.values())
        # Any other case, return all
        return list(self.__foldersContent.values()) + list(self.__filesContent.values())

    def has_key(self, key, restr=None):
        """Check if C{key} exist in the folder

        @param key: Key (ie file or folder name) to check
        @type key: C{str}
        @param restr: Restriction on key validation
        @type restr: L{Folder} or L{File<File.File>} class

        @rtype: C{bool}
        """
        if self.__scanned is False:
            try:
                self.scan()
            except Exception:
                pass

        if restr == File.File:
            return key in self.__filesContent
        if restr == Folder:
            return key in self.__foldersContent
        return key in self.__foldersContent or key in self.__filesContent

    def __getitem__(self, key):
        """Get the C{key} element of the L{Folder} (x.__getitem__(y) <==> x[y])

        @param key: Key (ie file or folder name) to check
        @type key: C{str}

        @rtype: L{Folder} or L{File<File.File>}

        @raise KeyError: if C{key} isn't an element of the L{Folder}
        """
        if self.__scanned is False:
            try:
                self.scan()
            except Exception:
                pass

        if key in self.__filesContent:
            self.__makeFile(key)
            return self.__filesContent[key]
        if key in self.__foldersContent:
            self.__makeFolder(key)
            return self.__foldersContent[key]
        raise KeyError("%s invalid key" % key)

    def __contains__(self, key):
        """Check if C{key} exist in the folder (x.__contains__(y) <==> y in x)

        @param key: Key (i.e. file or folder name) to check
        @type key: C{str}

        @rtype: C{bool}
        """
        return key in self

    def __len__(self):
        """Return how many element the L{Folder} contain

        @return: value change in function of the state of the recursive flag
        @rtype: C{int}
        """
        if self.__scanned is False:
            try:
                self.scan()
            except Exception:
                pass

        return len(self.__foldersContent) + len(self.__filesContent)

    # Iterable capability
    def __iter__(self):
        """Get iterator through the L{Folder}

        @rtype: C{yield} of L{Folder} and L{File<File.File>}
        """
        if self.__scanned is False:
            try:
                self.scan()
            except Exception:
                pass

        for F in self.__foldersContent:
            yield F
        for f in self.__filesContent:
            yield f
