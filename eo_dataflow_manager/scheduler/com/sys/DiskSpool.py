#
# -*- coding: UTF-8 -*-
#
"""DiskSpool extend the L{Folder} class with pattern restriction on L{scan<Folder.scan>}
"""

__docformat__ = 'epytext'

import os.path
import re

from .File import File
from .Folder import Folder


class DiskSpool(Folder):
    """
    DiskSpool extend the L{Folder} class with pattern restriction on L{scan<Folder.scan>}

    @group Special: __*__
    @group Validation: validatedFile
    @group Updating: _walkInFolder
    """

    def __init__(self, path, patterns,
                 recursive=False,
                 compression=None,
                 integrity=None,
                 lockFileExt=None,
                 create=False):
        """
        L{DiskSpool} instantiation method (call the L{Folder.__init__})

        @param path: folder path on the disk
        @type path: C{str}
        @param recursive: define the scan mode of the L{Folder}
        @type recursive: C{bool}
        @param compression: possible compressing mode.
        @type compression: C{list} of C{str} take in the L{File.LIST_COMPRESS}
        @param integrity: possible type of hashes use to compute the checksum
        @type integrity: C{list} of C{str} take in the L{File.LIST_DISGEST}
        @param lockFileExt: Lock file extension, use to ignore file in spool
        @type lockFileExt: C{str}
        """
        # Compile and save the regular expression (with Ignore case flag)
        # Compile BEFORE start Folder.__init__
        # because Folder.__init__ call self.scan
        # that call self._walfInFolder that use self.__re
        if not isinstance(patterns, list):
            patterns = [patterns, ]

        self.__re = []

        for p in patterns:
            self.__re.append(re.compile(p + '$', re.I))

        # Lock file extention
        self.__lockFileExt = lockFileExt

        # Save the full text pattern list
        self.__patterns = patterns

        # Compression flag
        self.__compression = compression

        # Integrity Flag
        self.__integrity = integrity

        # !!!! self.__provider ?????
        Folder.__init__(self, path, recursive, create)

    def validatedFile(self, f):
        """Check if the L{File} C{f} validate the spool patterns

        @param f: A L{File} to copy in C{self}
        @type f: L{File}

        @rtype: C{bool}

        @raise TypeError: if f isn't a L{File}
        """
        if not isinstance(f, File):
            raise TypeError('Invalid argument type, need a File object')

        # Check with only file name
        for rexp in self.__re:
            if rexp.match(f.name):
                return True
        # Check with full path
        for rexp in self.__re:
            if rexp.match(f.getName()):
                return True

        return False

    def _walkInFolder(self):
        """DiskSpool _walkInFolder version :
        filter each folder entry with the C{patterns} attribute
        and check if file with the C{lockFileExt} extension doesn't exist

        @return: a C{yield} of C{tuple} contain filename and C{isfile} bool information
        @rtype: C{yield} of C{tuple}
        """
        for key, isFile in Folder._walkInFolder(self):
            valid = False
            for rexp in self.__re:
                if rexp.match(key):
                    valid = True
                    break
            if valid:
                # yield each file which is not a directory or a lock file or a
                # file which has a lock
                if not isFile or self.__lockFileExt is None or not os.path.exists(
                        os.path.join(self.path, key + self.__lockFileExt)):
                    yield key, isFile
