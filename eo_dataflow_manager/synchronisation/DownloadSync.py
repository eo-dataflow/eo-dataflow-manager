
import argparse
import datetime
import logging
import os
import sys
from socket import gethostname

from eo_dataflow_manager.scheduler.com.ext.PublishRabbitMQ import PublishRabbitMQ
from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.com.sys.Folder import Folder
from eo_dataflow_manager.scheduler.sc import LogUtil
from eo_dataflow_manager.scheduler.sc.GlobalConfig import GlobalConfig
from eo_dataflow_manager.scheduler.sc.Scheduler import EXIT_BAD_ARGS
from eo_dataflow_manager.scheduler.sc.job_targets import JobsElastic
from eo_dataflow_manager.synchronisation.SynchronisationLocalRemote import SynchronisationLocalRemote

VERSION = '0.1'

FOLDER_LISTINGS_AUTO = 'orders/listings/auto'
LOGGER_DEFAULT_FILE_NAME = 'synchro_'
SYNCHRO_LOGGER_LEVEL = logging.INFO
LOGGER_NAME = 'downloadsync'


class DownloadSync(object):

    def __init__(self, logger_name=LOGGER_NAME):
        self.__download = None
        self.__verbose = False
        self.__path = {}
        self.__logger_name = logger_name

    def getOptions(self):
        # Get a parser
        progName = os.path.basename(sys.argv[0])
        usage = 'usage: %s [options] ()' % progName
        parser = argparse.ArgumentParser(usage=usage, prog=progName + ' ' + VERSION)

        parser.add_argument('-c', '--config',
                            action='store',
                            type=str,
                            dest='config',
                            metavar='FILE [REQUIRED]',
                            required=True,
                            help='Path to the eo-dataflow-manager YAML config file.')

        parser.add_argument('-d', '--download',
                            action='store',
                            type=str,
                            dest='download',
                            metavar='FILE [REQUIRED]',
                            required=True,
                            help='path of a download config file')

        parser.add_argument('-w', '--workspace_dir',
                            action='store',
                            type=str,
                            dest='workspace_dir',
                            metavar='PATH',
                            help='Set PATH as the workspace directory of the System Controller')

        parser.add_argument('-a', '--appdata_dir',
                            action='store',
                            type=str,
                            dest='appdata_dir',
                            metavar='PATH',
                            help='Set PATH as the application data directory of the System Controller')

        parser.add_argument('-o', '--output',
                            action='store',
                            type=str,
                            dest='output',
                            metavar='FILE',
                            help='path of an output file')

        parser.add_argument('-l', '--local',
                            action='store',
                            type=str,
                            dest='local_db',
                            metavar='FILE',
                            help='path of the local sqlite database (will be created if not exists)')

        parser.add_argument('-r', '--remote',
                            action='store',
                            type=str,
                            dest='remote_db',
                            metavar='FILE',
                            help='path of the remote sqlite database (will be created if not exists)')

        parser.add_argument('-j', '--job-path',
                            action='store',
                            type=str,
                            dest='job_path',
                            metavar='FILE',
                            help='path of job path (will be created if not exists)')

        parser.add_argument('--history-path',
                            action='store',
                            type=str,
                            dest='history_path',
                            metavar='FILE',
                            help='history path (will be created if not exists)')

        parser.add_argument('--debug',
                            action='store_true',
                            dest='debug_mode',
                            help='Debug mode')

        parser.add_argument('--with-download',
                            action='store_true',
                            dest='download_mode',
                            help='run in download mode (File download)')

        parser.add_argument('--redownload',
                            action='store_true',
                            dest='redownload_mode',
                            help='run in redownload mode (File redownload)')

        parser.add_argument('--min-date',
                            action='store',
                            type=str,
                            dest='min_date',
                            help='Starting date')

        parser.add_argument('--max-date',
                            action='store',
                            type=str,
                            dest='max_date',
                            help='Ending date')

        parser.add_argument('--modified',
                            action='store_true',
                            dest='modified',
                            help='Reload modified files')

        # local
        parser.add_argument('-u', '--local-path',
                            action='store',
                            type=str,
                            dest='local_path',
                            metavar='',
                            help='local path')

        parser.add_argument('-v', '--verbose',
                            action='store_true',
                            dest='verbose',
                            help='Activate verbose output (show log in STDOUT)')

        options = parser.parse_args()
        return options

    def run(self, options):
        # if options.debug_mode:
        #     logLevel = logging.DEBUG

        sync = SynchronisationLocalRemote(
            logger=self.log,
            globalConfig=self.__globalConfig
        )

        if options.download is not None:
            download_config_file = options.download
            sync.readConfig(download_config_file)

            if not self.log.handlers:
                # Setup file logging target
                handlers = LogUtil.setupFileLogger(
                    self.log,
                    self.__globalConfig['logs.file_log'],
                    os.path.join(self.__globalConfig.getPath('process_log_archive'),
                                 LOGGER_DEFAULT_FILE_NAME + sync.id + '.log')
                )

            tmpName = ''
            for i in sync.id:
                if i.isalnum() or i.isspace():
                    tmpName = tmpName + i
                else:
                    tmpName = tmpName + ' '

            downloadPath = os.path.join(
                self.__globalConfig.getPath('work'), '_'.join(
                    tmpName.split()))
            internal_path = os.path.join(downloadPath, 'internal')

        try:
            # Setup Elasticsearch job upload
            if 'elasticsearch' in self.__globalConfig['jobs.targets']:
                jobs_elasticsearch = JobsElastic(
                    hosts=self.__globalConfig['jobs.elasticsearch.hosts'],
                    scheme=self.__globalConfig['jobs.elasticsearch.scheme'],
                    user=self.__globalConfig['jobs.elasticsearch.user'],
                    password=self.__globalConfig['jobs.elasticsearch.password'],
                    indexNameTemplate=self.__globalConfig['jobs.elasticsearch.index'],
                    sniffCluster=self.__globalConfig['jobs.elasticsearch.sniff_cluster'],
                )
                self.__log.debug('Jobs will be pushed to ElasticSearch')

                sync.set_job_elasticsearch(jobs_elasticsearch)

            # Setup RabbitMQ job upload
            if 'rabbitmq' in self.__globalConfig['jobs.targets']:
                jobs_rabbitmq = PublishRabbitMQ(
                    host=self.__globalConfig['jobs.rabbitmq.host'],
                    port=self.__globalConfig['jobs.rabbitmq.port'],
                    ssl=self.__globalConfig['jobs.rabbitmq.ssl'],
                    user=self.__globalConfig['jobs.rabbitmq.user'],
                    password=self.__globalConfig['jobs.rabbitmq.password'],
                    virtual_host=self.__globalConfig['jobs.rabbitmq.virtual_host'],
                    queue_name=self.__globalConfig['jobs.rabbitmq.queue_name'],
                    routing_key=self.__globalConfig['jobs.rabbitmq.routing_key'],
                    message_type=self.__globalConfig['jobs.rabbitmq.message_type'],
                )
                jobs_rabbitmq.start()
            sync.set_job_rabbitmq(PublishRabbitMQ)

            if options.job_path:
                jobFile = self.getStateFile(options.job_path)
                sync.setJobFile(jobFile)
                jobFile.acquireLock('%s%d' % (gethostname(), os.getpid()))
                sync.setJobState()
                path, _jobname = os.path.split(os.path.abspath(options.job_path))
                Folder(path, create=True)

            if options.history_path:
                sync.setHistoryPath(options.history_path)
                Folder(options.history_path, create=True)

            sync.modified = options.modified
            if options.output is None and (
                    options.download_mode or options.redownload_mode):
                self.log.error(
                    'Output file must be defined (option -o) in download mode')
                sync.unLock()
                sys.exit(1)

            if options.redownload_mode is not None:
                sync.redownload_mode = options.redownload_mode

            try:
                date_format = '%Y%m%d'
                if options.max_date is not None:
                    dt = datetime.datetime.strptime(options.max_date, date_format)
                    sync.maxDate = dt
            except ValueError:
                self.log.error('Option --max : Date format must be yyyymmdd')
                sync.unLock()
                sys.exit(1)

            try:
                if options.min_date is not None:
                    dt = datetime.datetime.strptime(options.min_date, date_format)
                    sync.minDate = dt
            except ValueError:
                self.log.error('Option --min : Date format must be yyyymmdd')
                sync.unLock()
                sys.exit(1)

            if options.local_path is not None:
                sync.local_url = options.local_path

            if options.local_db is not None:
                local_database_path = options.local_db
            else:
                #local_database_path = workingdir.getDatabasePathFromUrl(sync.local_url, prefix='local_')
                local_name = 'synchro_local_database.db'
                local_database_path = os.path.join(internal_path, local_name)

            self.log.debug('Local Dcheck database : %s', local_database_path)
            sync.setLocalDatabase(local_database_path)
            if options.remote_db is not None:
                remote_database_path = options.remote_db
            else:
                #remote_database_path = workingdir.getDatabasePathFromUrl(sync.remote_url, prefix='remote_')
                remote_name = 'synchro_remote_database.db'
                remote_database_path = os.path.join(internal_path, remote_name)

            self.log.debug('Remote Dcheck database : %s', remote_database_path)
            sync.setRemoteDatabase(remote_database_path)

            if options.output is not None:
                output_filepath = options.output
                sync.setOutputFilepath(output_filepath)

            self.log.info('Starting synchronisation...')
            sync.run()

            if options.output is not None and (
                    options.download_mode or options.redownload_mode or options.modified):
                fileOutput = File(output_filepath)
                try:
                    fileOutput.open()
                    nblines = fileOutput.readlines()
                finally:
                    fileOutput.close()
                workPath = self.__globalConfig.getPath('work')
                tmpName = ''
                for i in sync.id:
                    if i.isalnum() or i.isspace():
                        tmpName = tmpName + i
                    else:
                        tmpName = tmpName + ' '

                # make the download workspace path
                downloadPath = os.path.join(workPath, '_'.join(tmpName.split()))
                folder = Folder(
                    os.path.join(
                        downloadPath,
                        FOLDER_LISTINGS_AUTO),
                    recursive=True)
                fileOutput.move(
                    fileOutput.path +
                    '/' +
                    fileOutput.name +
                    '.list.sync')
                self.log.info('Move output file %s to folder %s ',
                              fileOutput.name,
                              folder.path)
                fileOutput.moveTo(folder)
                self.log.info(
                    '%s files found during synchronisation ',
                    len(nblines))
                sync.updateJobState('Output', os.path.abspath(fileOutput.getDirPath()))

            else:
                sync.updateJobState('Output', None if sync.getOutputFilepath() is None else os.path.abspath(sync.getOutputFilepath()))

        finally:
            # teminate the rabbitmq publisher threads
            for rabbitmq_publisher in PublishRabbitMQ.queues:
                PublishRabbitMQ.queues[rabbitmq_publisher].join()
                self.log.debug(f'RabbitMQ publisher {rabbitmq_publisher} stopped.')
            # unlock and close job files
            sync.unLock()
            self.log.info('End !')

    def main(self):
        options = self.getOptions()
        self.__verbose = options.verbose

        if options.config is not None:
            self.__globalConfig = GlobalConfig(options.config)
        else:
            config = {'paths': {}}
            if options.workspace_dir is not None:
                config.workspace = options.workspace_dir
            if options.appdata_dir is not None:
                config.appdata = options.appdata_dir

            self.__globalConfig = GlobalConfig(data=config)

        from eo_dataflow_manager.ifr_lib_modules.ifr_logging import setup_logging
        setup_logging(
            config=self.__globalConfig['logs.default'],
            root_level=logging.DEBUG if self.__verbose else SYNCHRO_LOGGER_LEVEL,
        )
        self.log = logging.getLogger(self.__logger_name)

        # Check if exist workspace_dir and appdata_dir
        if not os.path.isdir(self.__globalConfig['paths.workspace']):
            self.log.error(
                "the specified workspace directory doesn't exist (path: %s )",
                self.__globalConfig['paths.workspace'])
            sys.exit(EXIT_BAD_ARGS)
        if not os.path.isdir(self.__globalConfig['paths.appdata']):
            self.log.error(
                "the specified application data directory doesn't exist (path: %s )",
                self.__globalConfig['paths.appdata'])
            sys.exit(EXIT_BAD_ARGS)

        self.log.info('Use %s for workspace directory',
                      self.__globalConfig['paths.workspace'])
        self.log.info('Use %s for application data directory',
                      self.__globalConfig['paths.appdata'])

        self.run(options)

    def getStateFile(self, job_path):
        jobFile = File(job_path)
        return jobFile


def downloadsync_main():
    ds = DownloadSync()
    ds.main()
