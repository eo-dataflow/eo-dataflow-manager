
import datetime
import logging
import os
import shutil
import sys
from socket import gethostname
from string import capwords
from subprocess import PIPE, Popen
from xml.parsers.expat import ExpatError

from eo_dataflow_manager.dchecktools.common.basefileinfos import SqliteBaseFilesInfos
from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError
from eo_dataflow_manager.emm.api import messages
from eo_dataflow_manager.scheduler.com.ext.PatternFilter import PatternFilter
from eo_dataflow_manager.scheduler.com.ext.XMLReader import XMLReader
from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.plugins import Factory
from eo_dataflow_manager.scheduler.sc.ConfigurationFileUtil import ConfigurationFileUtil
from eo_dataflow_manager.scheduler.sc.job_state import JobState

STATE_INIT = 'Initializing'
STATE_RUNNING = 'Running'
STATE_STOPPED = 'Stopped'
STATE_FAIL = 'Fail'
STATE_SUCCESS = 'Success'


class Config:

    def __init__(self):
        self.database_path = None
        self.check_infos = None
        self.logLevel = logging.INFO


class SynchronisationLocalRemote(object):
    """
    """

    def __init__(self, logger, globalConfig):
        self.__id = None
        self.__log = logger

        self.__download_config_file = None
        self.__globalConfig = globalConfig

        self.__remote_storage_repository = None
        self.__remote_storage_type = None
        self.__remote_storage_server = None
        self.__remote_storage_username = None
        self.__remote_storage_passwd = None
        self.__remote_storage_port = None
        self.__remote_database_path = None
        self.__remote_url = None

        self.__local_storage_repository = None
        self.__local_database_path = None

        self.__history_path = None

        self.__output = None
        self.__output_filepath = None

        self.__dp_pattern_filter = None

        self.__state = None
        self.__localState = None
        self.__jobFile = None

        self.__compression_action = None
        self.__compression_type = None
        self.__zip_add_sub_directory = None
        self.redownload_mode = False
        self.__data_reader = None
        self.minDate = None
        self.maxDate = None
        self.modified = False

        self.remote_dreport_config = None
        self.report_infos = None
        self.remote_dcheck_config = None
        self.__data_reader_node = None
        self.__data_reader_name = None
        self.__base_url = None

        self.__jobsElastic = None
        self.__jobsRabbitMQ = None

        self.__configuration = None

    def setJobState(self, state=[STATE_INIT, '']):
        self.__log.debug('setJobState %s', state[0])
        if self.__jobFile is not None:

            try:
                if self.__state is None:
                    try:
                        self.__state = JobState(
                            self.__jobFile,
                            mode='w',
                            jobsElastic=self.__jobsElastic,
                            jobsRabbitMQ=self.__jobsRabbitMQ
                        )

                    except BaseException:
                        err_msg = 'Impossible to create JobState instance'
                        self.__log.error('%s: %s', err_msg, self.__jobFile)
                        self.__log.exception(err_msg)
                        self.__state = None
                        self.__jobFile = None
                else:
                    self.__localState = {}
                    self.__localState['Download'] = self.__id
                    self.__localState['Host'] = gethostname()
                    self.__localState['Pid'] = os.getpid()
                    self.__localState['State'] = state[0]
                    self.__localState['Details'] = state[1]
                    self.__localState['Date'] = datetime.datetime.utcnow().strftime(
                        messages.MSG_DATE_FORMAT)
                    self.__localState['redownload_mode'] = self.redownload_mode
                    #self.__state['Output'] = self.__output_filepath
                    self.syncJobState()
                    self.__state.sync()
            except Exception:
                self.__log.exception('Cannot update jobstate file (state=%s). Skipping update', state)

    def updateJobState(self, key, value):
        try:
            if self.__localState is not None:
                self.__localState[key] = value
                self.syncJobState()
                self.__state.sync()
        except Exception:
            self.__log.exception('Cannot update jobstate file (key/value=%s/%s). Skipping update', key, value)

    def syncJobState(self):
        #self.__log.info(f"====================> sync localState = {self.__localState}")
        for k in self.__localState:
            self.__state[k] = self.__localState[k]

    def setLocalUrl(self, local_url):
        self.__local_storage_repository = local_url

    def setJobFile(self, job_file):
        self.__jobFile = job_file

    def getJobFile(self):
        return self.__jobFile

    def set_job_elasticsearch(self, job_elastic):
        self.__jobsElastic = job_elastic

    def set_job_rabbitmq(self, job_rabbitmq):
        self.__jobsRabbitMQ = job_rabbitmq

    def setHistoryPath(self, history_path):
        self.__history_path = history_path

    def unLock(self):
        if self.__jobFile is not None:
            self.__jobFile.unLock()
            self.syncJobState()
            self.__state.close(False)
            if self.__history_path is not None:
                self.__jobFile.copy(self.__history_path)
                shutil.copy(self.__output_filepath, self.__history_path)


    id = property(lambda self: self.__id, None, None, '')
    remote_url = property(lambda self: self.__remote_url, None, None, '')
    local_url = property(
        lambda self: self.__local_storage_repository, setLocalUrl, None, '')

    def setLocalDatabase(self, local_database_path):
        self.__local_database_path = local_database_path

    def setRemoteDatabase(self, remote_database_path):
        self.__remote_database_path = remote_database_path

    def setOutputFilepath(self, output_filepath):
        self.__output_filepath = output_filepath

    def getOutputFilepath(self, ):
        return self.__output_filepath

    def readConfig(self, download_config_file):
        """Read and build the L{Download} tree from
        the XML configuration subtree (recursive construction)
        """
        self.__download_config_file = download_config_file

        xr = XMLReader(self.__log.name)
        self.__log.info('  BEGIN: reading of the download configuration: %s',
                        self.__download_config_file)
        # Open the xml tree
        try:
            xmltree = xr.open(self.__download_config_file)
        except ExpatError:
            raise IOError('invalid download_config file %s ' %
                          self.__download_config_file)
        except IOError:
            self.__log.warning("Download_config file doesn't exist %s",
                               self.__download_config_file)
            raise IOError("Download_config file doesn't exist %s " %
                          self.__download_config_file)
        # Get the root node

        configuration = ConfigurationFileUtil(self.__log.name, xr, xmltree)
        configuration.Read()
        self.__configuration = configuration

        self.__id = configuration.id

        # Download Policies
        self.__dp_pattern_filter = PatternFilter()
        self.__dp_pattern_filter.add('filename',
                                     configuration.source.files_regexp,
                                     configuration.source.files_ignoreRegexp)
        self.__dp_pattern_filter.add(
            'path',
            configuration.source.directories_regexp,
            configuration.source.directories_ignoreRegexp)

        # Listing Builder plugin dreport
        plugins_config_path = self.__globalConfig.getPath('dynamic_plugins')
        if configuration.source.update is None:
            dreport_config_name = 'dreport_modified_files_v7.py'
        else:
            dreport_config_name = 'dreport_' + configuration.source.update + '_files_v7.py'
        self.remote_dreport_config = os.path.join(plugins_config_path, dreport_config_name)
        if not os.path.isfile(self.remote_dreport_config):
            raise IOError(f" Unknown dreport configuration file : '{self.remote_dreport_config}'.")

        modulepath = self.remote_dreport_config[:-3]
        (moddir, modname) = os.path.split(modulepath)
        sys.path.append(moddir)
        try:
            modulepath = modulepath[:-3]
            module = __import__(modname)
        except ImportError as msg:
            error = DC_ConfigError('Cannot import config file : %s (%s)' % (modulepath + '.py', msg))
            raise Exception(error)
        module_attr = module.__dict__.keys()
        # default workspace
        if 'REPORT' in module_attr:
            module_attr = module.REPORT.__dict__.keys()
            if 'report_infos' in module_attr:
                self.report_infos = module.REPORT.report_infos

        # Listing Builder plugin for dcheck
        self.remote_dcheck_config = download_config_file

        if not os.path.isfile(self.remote_dcheck_config):
            raise IOError(" Unknown dcheck configuration file : '%s'." %
                          (self.remote_dcheck_config))

        self.__local_storage_repository = configuration.destination.location

        self.__remote_storage_type = capwords(configuration.source.protocol)
        self.__remote_storage_repository = configuration.source.rootPath
        if self.__remote_storage_repository is None:
            self.__remote_storage_repository = '/'
        self.__remote_storage_server = configuration.source.server
        self.__remote_storage_username = configuration.source.login
        if self.__remote_storage_username is None:
            self.__remote_storage_username = 'anonymous'
        self.__remote_storage_passwd = configuration.source.password
        if self.__remote_storage_passwd is None:
            self.__remote_storage_passwd = 'anonymous'
        self.__remote_storage_port = configuration.source.port
        # remote_max_activated_flow = configuration.settings.maxNbOfConcurrentStreams

        if self.__remote_storage_type == 'Onlynotify':
            raise IOError(
                ' No synchronisation available for provider type %s ' %
                self.__remote_storage_type)

        self.getRemoteUrl()

        if configuration.destination.compression is not None:
            self.__compression_action = configuration.destination.compression
            if self.__compression_action == 'none':
                self.__compression_action = None
            else:
                self.__compression_type = configuration.destination.compression_type
                if self.__compression_type == 'none':
                    self.__compression_type = None
                self.__zip_add_sub_directory = configuration.destination.compression_subdir

        # Data Reader Plugin
        self.__data_reader_node = configuration.id
        self.__data_reader_name = configuration.source.plugin
        regexpDate = configuration.source.dateRegexp
        dateFormat = configuration.source.dateFormat
        storagePath = configuration.destination.subpath
        self.__data_reader = Factory.DataReaderFactory(self.__data_reader_name,
                                                       self.__log.name,
                                                       regexpDate, dateFormat,
                                                       storagePath,
                                                       None,
                                                       0)

        if self.__data_reader.DIRECTORY:
            self.__data_reader.setContainer(configuration.destination.keepParentFolder)

        return configuration

    def getRemoteUrl(self):
        self.__remote_url = ''
        if self.__remote_storage_type in ['Ftp', 'Ftps', 'Sftp', 'Webdav']:
            if self.__remote_storage_type == 'Ftp':
                self.__remote_url += 'ftp://'
            elif self.__remote_storage_type == 'Ftps':
                self.__remote_url += 'ftps://'
            elif self.__remote_storage_type == 'Sftp':
                self.__remote_url += 'sftp://'
            elif self.__remote_storage_type == 'Webdav':
                self.__remote_url += 'webdav://'
            else:
                raise Exception('Unknown provider : %s.  !' % self.__remote_storage_type)

            if self.__remote_storage_username is not None:
                self.__remote_url += self.__remote_storage_username
            if self.__remote_storage_passwd is not None:
                self.__remote_url += ':' + self.__remote_storage_passwd
            if self.__remote_storage_username is not None or self.__remote_storage_passwd is not None:
                self.__remote_url += '@'
            self.__remote_url += '%s' % (self.__remote_storage_server)
            if self.__remote_storage_port is not None:
                self.__remote_url += ':%s' % (str(self.__remote_storage_port))
            self.__remote_url += '/' + self.__remote_storage_repository

        elif self.__remote_storage_type in ['Http', 'Https_filetable']:
            protocol = 'http:'
            if self.__remote_storage_type.startswith('Https'):
                protocol = 'https:'
            self.__base_url = '%s//%s/%s' % (
                protocol,
                self.__remote_storage_server,
                self.__remote_storage_repository.lstrip('/')
            )

        elif self.__remote_storage_type.startswith('Https'):
            if self.__remote_storage_type == 'Https_OpenSearch' or self.__remote_storage_type == 'Https_opensearch':
                self.__remote_url = 'https_opensearch://'
            elif self.__remote_storage_type == 'Https_Listing_File' or self.__remote_storage_type == 'Https_listing_file':
                self.__remote_url = 'https_listing_file://'
            elif self.__remote_storage_type in ['Https_Po_Daac','Https_po_daac', 'Https_xx_daac']:
                self.__remote_url = 'https_xx_daac://'
            elif str(self.__remote_storage_type).lower() == 'https_eop_os':
                self.__remote_url = 'https_eop_os://'
            else:
                self.__remote_url = 'https://'

            if self.__remote_storage_username is not None:
                self.__remote_url += self.__remote_storage_username
            if self.__remote_storage_passwd is not None:
                self.__remote_url += ':' + self.__remote_storage_passwd
            if self.__remote_storage_username is not None or self.__remote_storage_passwd is not None:
                self.__remote_url += '@'
            self.__remote_url += '%s' % self.__remote_storage_server
            if self.__remote_storage_port is not None:
                self.__remote_url += ':%s' % (str(self.__remote_storage_port))
            self.__remote_url += '/' + self.__remote_storage_repository.lstrip('/')


        elif self.__remote_storage_type in ['Localpath', 'Localmove', 'Localpointer', 'Onlynotify']:
            self.__remote_url = self.__remote_storage_repository
        else:
            raise Exception('Unknown provider : %s.  !' %
                            (self.__remote_storage_type))

    def run(self):
        orig_output_path = None
        tmp_output_path = None
        if self.__output_filepath is not None:
            orig_output_path = self.__output_filepath
            tmp_output_path = orig_output_path + '.tmp'
            try:
                with open(tmp_output_path, 'w') as self.__output:
                    self.doSynchronisation()
            except IOError as msg:
                self.__log.error(msg)
                self.unLock()
                sys.exit(1)

            with open(tmp_output_path, 'r') as fileOutput:
                nblines = fileOutput.readlines()

            os.rename(tmp_output_path, orig_output_path)
            self.__log.info('%s files found during synchronisation', len(nblines))
            self.__log.info('listing filename :%s', orig_output_path)
            self.updateJobState('Nb_file', len(nblines))

    def doSynchronisation(self):
        self.setJobState([STATE_RUNNING, 'runDcheckLocal'])
        self.__log.info('Begin check file Local')
        listFileLocal = self.runDcheckLocal()
        self.__log.info('End check file Local')
        listFilenameLocal = self.reportFile(listFileLocal)
        self.__log.info('Begin check file Remote')
        self.setJobState([STATE_RUNNING, 'runDcheckRemote'])
        listFileRemote = self.runDcheckRemote()
        self.__log.info('End check file Remote')

        self.__log.info('Listing in progress')
        for fileInfo in listFileRemote:
            filepath = fileInfo.filename
            accepted = self.__dp_pattern_filter.isAccepted(filepath)
            if accepted:
                mtime = fileInfo.mtime
                size = fileInfo.size
                sensingtime = fileInfo.sensingtime
                # TODO https download ==> filepath : filename_id --|-- URL (filename_id is not necessarily the real filename)
                _basedir, filename = os.path.split(filepath)
                if not fileInfo.isDirectory:
                    try:
                        if (sensingtime is None and (
                                self.minDate is not None or self.maxDate is not None)):
                            self.__log.warning(
                                ' !!! Option date ignored. Datareader plugin name can not determinate date). ')

                        if not ((self.minDate is not None and sensingtime is not None
                                 and sensingtime < self.minDate)
                                or (self.maxDate is not None and sensingtime is not None
                                    and sensingtime > self.maxDate)):
                            if self.__compression_action == 'uncompress':
                                splitFileName = filename.split('.')
                                extension = splitFileName[-1]
                                # TODO ZIP multi files + TAR file + TGZ file
                                if extension in [File.BZIP2, File.ZIP, File.GZIP, File.LZW]:
                                    filename = '.'.join(splitFileName[:-1])

                            if self.__configuration.destination.keepParentFolder:
                                index = os.path.join(self.get_relative_path(filepath), filename)
                            else:
                                index = filename

                            if index not in listFilenameLocal:
                                self.__log.debug('%s not in local list', filename)
                                self.reportString(filepath, info=fileInfo)
                            elif self.modified:
                                self.__log.debug('Filename %s exist in local list', filename)
                                _basedir, _filename, localmtime, localsize = listFilenameLocal[filename]
                                if mtime > localmtime:
                                    self.__log.debug('mtime:  %s > local mtime %s ',
                                                     mtime, localmtime)
                                    self.reportString(filepath, info=fileInfo)
                                elif size > localsize:
                                    self.__log.debug('size:  %s > local mtime %s ',
                                                     size, localsize)
                                    self.reportString(filepath, info=fileInfo)

                    except AttributeError:
                        self.__log.warning('Cannot determine date of filename %s',
                                           filename)

        self.setJobState([STATE_SUCCESS, ''])


    def runDcheckLocal(self):
        if (not self.redownload_mode and os.path.exists(
                self.__local_storage_repository[0])):
            self.runDCheckReport(True)
            config = Config()
            config.database_path = self.__local_database_path
            return self.getListFile(config)
        return []

    def runDcheckRemote(self):
        self.runDCheckReport(False)
        # Objets initialization
        config = Config()
        config.database_path = self.__remote_database_path
        return self.getListFile(config)

    def runDCheckReport(self, isLocal=False):
        if isLocal:
            dcheck_database = self.__local_database_path
            base_url = self.__local_storage_repository[0]
            argsList = [base_url, '-d', dcheck_database]
        else:
            dcheck_database = self.__remote_database_path
            base_url = self.__remote_url
            dcheck_config = self.remote_dcheck_config
            argsList = [base_url, '-d', dcheck_database, '-c', dcheck_config]

        cmd = ['eo-dataflow-manager-dcheck'] + argsList
        self.__log.info('Cmd %s', cmd)
        process = Popen(cmd, stdout=PIPE, stderr=PIPE,
                        close_fds=True, text=True)

        stdout_lines, stderr_lines = process.communicate()

        self.__log.debug('%s > bits in buffer stdout: %d bits, and in buffer stderr: %d bits',
                         cmd,
                         len(''.join(stdout_lines)),
                         len(''.join(stderr_lines)))

#        process.wait()
        returncode = process.returncode
        if returncode != 0:
            # pour eviter d'avoir un tableau de caracteres illisible en
            # retour...
            stdout_string = ''.join(stdout_lines)
            stderr_string = ''.join(stderr_lines)
            raise Exception(
                'dcheck error : return code = %s [stdout=%s] [stderr=%s]' %
                (returncode, stdout_string, stderr_string))

    def getListFile(self, config):
        db = SqliteBaseFilesInfos()
        try:
            db.setConfig(config)
        except DC_ConfigError as msg:
            self.__log.error(msg)
            self.unLock()
            sys.exit(1)
        db.createEngine()

        last_execution_id = db.req_LastTerminatedExecutions()[0].id
        listFile = db.req_ListFilesInfo(last_execution_id)
        self.__log.debug('Last execution %s', last_execution_id)
        return listFile

    def reportString(self, string, info=None, suffix='\n', prefix=''):
        if info:
            string = string + ', size=' + str(info.size) + ', mtime=' + info.mtime.strftime('%Y-%m-%d %H:%M:%S')

        string = prefix + string + suffix
        if self.__output:
            self.__output.write(string)
        else:
            sys.stdout.write(string)

    def reportFile(self, listFile):
        listFilename = {}
        for fileInfo in listFile:
            filepath = fileInfo.filename
            mtime = fileInfo.mtime
            size = fileInfo.size
            sensingtime = fileInfo.sensingtime
            basedir, filename = os.path.split(filepath)
            self.__log.debug('filepath %s ', filepath)
            if not fileInfo.isDirectory:
                try:
                    if not ((self.minDate is not None and sensingtime < self.minDate) or (
                            self.maxDate is not None and sensingtime > self.maxDate)):
                        if self.__configuration.destination.keepParentFolder:
                            relative_file_path = os.path.join(self.get_relative_path(filepath), filename)
                            listFilename[relative_file_path] = basedir, filename, mtime, size
                        else:
                            listFilename[filename] = basedir, filename, mtime, size
                except AttributeError:
                    # no date matched (attribute group pattern)
                    pass
                except ValueError as msg:
                    self.__log.warning('Cannot determine date of filepath  %s(%s) ',
                                       filepath, msg)
        return listFilename

    def get_relative_path(self, filepath):
        #  TODO : si modification de la classe DirRegexpDataReader revoir les différents cas
        relative_path = ''
        parent_path, _ = os.path.split(filepath)
        _, parent_folder = os.path.split(parent_path)
        if self.__configuration.destination.type == 'datetree':
            if self.__data_reader.DIRECTORY:  # cas DirRegexpDataReader
                relative_path = os.path.split(self.__data_reader.getRelativePath(filepath))[0]
            else:
                relative_path = self.__data_reader.getRelativePath(filepath)

        if self.__configuration.destination.keepParentFolder:
            if not self.__data_reader.DIRECTORY:  # cas pas DirRegexpDataReader
                relative_path = os.path.join(relative_path, parent_folder)
            elif self.__configuration.destination.type == 'spool':
                relative_path = parent_folder

        return relative_path
