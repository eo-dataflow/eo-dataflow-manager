#!/usr/bin/env python

"""Start Download reloader """
import logging
import os
import sys
from optparse import OptionParser
from xml.etree.ElementTree import ParseError
from xml.parsers.expat import ExpatError

from eo_dataflow_manager.dchecktools.common.basefileinfos import FILE_URL_SEPARATOR
from eo_dataflow_manager.ifr_lib_modules.ifr_logging import setup_logging
from eo_dataflow_manager.scheduler.com.ext.XMLReader import XMLReader
from eo_dataflow_manager.scheduler.com.sys.File import File
from eo_dataflow_manager.scheduler.sc import LogUtil
from eo_dataflow_manager.scheduler.sc.ConfigurationFileUtil import ConfigurationFileUtil
from eo_dataflow_manager.scheduler.sc.Download import DownloaderException
from eo_dataflow_manager.scheduler.sc.DownloaderDatabase import DownloaderDatabase
from eo_dataflow_manager.scheduler.sc.GlobalConfig import GlobalConfig
from eo_dataflow_manager.scheduler.sc.Scheduler import EXIT_BAD_ARGS

VERSION = '1.1'

LOGGER_DEFAULT_FILE_NAME = 'reload_'
RELOAD_LOGGER_LEVEL = logging.INFO
LOGGER_NAME = 'downloadreload'
FOLDER_LISTINGS_HISTORY='listings/history'

class ReLoader(object):

    def __init__(self):
        self.__globalConfig = None

        self.__database = None

        self.__verbose = False
        self.__lockfileExtension = '.lock'
        self.__logFile = None
        self.internal_path = None
        self.__listfilepaths = []
        self.count = 0

    @staticmethod
    def getOptions():
        # Get a parser
        usage = 'usage: %prog [options] ()'
        parser = OptionParser(usage=usage, version='%prog ' + VERSION)

        # Set need argument
        parser.add_option('-c', '--config',
                          action='store', type='string',
                          dest='config', metavar='FILE [REQUIRED]',
                          help='Path to the eo-dataflow-manager YAML config file.')

        parser.add_option('-d', '--download',
                          action='store', type='string',
                          dest='download', metavar='FILE [REQUIRED]',
                          help='path of a download config file')

        parser.add_option('-i', '--input',
                          action='store', type='string',
                          dest='filepath', metavar='FILE  [REQUIRED]',
                          help='Path of the listing file')

        parser.add_option('-w', '--workspace_dir',
                          action='store', type='string',
                          dest='workspace_dir', metavar='PATH',
                          help='Set PATH as the workspace directory of the System Controller')

        parser.add_option('-a', '--appdata_dir',
                          action='store', type='string',
                          dest='appdata_dir', metavar='PATH',
                          help='Set PATH as the application data directory of the System Controller')

        parser.add_option('', '--debug', action='store_true',
                          dest='debug_mode', metavar='',
                          help='run in debug mode (more verbose)',
                          default=False)

        parser.add_option('-v', '--verbose',
                          action='store_true', dest='verbose', default=False,
                          help='Activate verbose output (show log in STDOUT)\n')

        (options, args) = parser.parse_args()
        # Check if required argument are here:
        if not options.download:
            print('Missing path of a download config file !!!')
            parser.print_help()
            sys.exit(1)

        if not options.filepath:
            print('Missing path of a input file !!!')
            parser.print_help()
            sys.exit(1)

        return options, args

    def read_configuration(self, xml_file):

        xr = XMLReader(LOGGER_NAME)

        self.log.debug('  --> read of the download configuration : %s', xml_file)

        try:
            xmltree = xr.open(xml_file)
        except ExpatError:
            raise IOError('invalid download_config file %s ' % xml_file)
        except IOError as e:
            self.log.warning("Download_config file doesn't exist %s ", xml_file)
            raise DownloaderException(e)
        except ParseError:
            self.log.warning('Download_config file parse error %s ', xml_file)
            raise IOError('invalid download_config file %s ' % xml_file)
        except Exception as e:
            self.log.warning('Read download_config file error %s (%s)', xml_file, e)
            raise IOError('invalid download_config file %s ' % xml_file)

        configuration = ConfigurationFileUtil(LOGGER_NAME, xr, xmltree)
        configuration.Read()

        return configuration

    def run(self, options):

        configuration = self.read_configuration(options.download)

        if not self.log.handlers:
            # Setup file logging target
            LogUtil.setupFileLogger(
                self.log,
                self.__globalConfig['logs.file_log'],
                os.path.join(self.__globalConfig.getPath('process_log_archive'),
                             LOGGER_DEFAULT_FILE_NAME + configuration.id + '.log')
            )

        if options.debug_mode:
            self.log.setLevel(logging.DEBUG)

        self.log.info('Using download config file : %s', options.download)
        self.log.info('Input reload file name : %s', options.filepath)

        tmpName = ''
        for i in configuration.id:
            if i.isalnum() or i.isspace():
                tmpName = tmpName + i
            else:
                tmpName = tmpName + ' '

        downloadPath = os.path.join(self.__globalConfig.getPath('work'), '_'.join(tmpName.split()))
        self.internal_path = os.path.join(downloadPath, 'internal')

        self.initDownloaderDatabase(self.internal_path, configuration.id)

        self.log.info('Read file...')
        self.markReDownload(options.filepath,
                            configuration.destination.keepParentFolder is not False,
                            configuration.source.protocol.lower() == 'https_opensearch')

        self.log.info('End !')

    def main(self):
        options, _ = ReLoader.getOptions()
        self.__verbose = options.verbose

        self.__globalConfig = GlobalConfig(options.config)

        setup_logging(
            config=self.__globalConfig['logs.default'],
            root_level=logging.DEBUG if self.__verbose else RELOAD_LOGGER_LEVEL,
        )
        self.log = logging.getLogger(LOGGER_NAME)

        # Overwrite globalConfig workspace & appdata dirs
        if options.workspace_dir:
            self.__globalConfig.setWorkspacePath(options.workspace_dir)
        if options.appdata_dir:
            self.__globalConfig.setAppdataPath(options.appdata_dir)

        # Check if exist workspace_dir and appdata_dir
        if not os.path.isdir(self.__globalConfig['paths.workspace']):
            self.log.error("the specified workspace directory doesn't exist (path: %s )", self.__globalConfig['paths.workspace'])
            sys.exit(EXIT_BAD_ARGS)
        if not os.path.isdir(self.__globalConfig['paths.appdata']):
            self.log.error("the specified application data directory doesn't exist (path: %s )", self.__globalConfig['paths.appdata'])
            sys.exit(EXIT_BAD_ARGS)

        self.log.info('Use %s for workspace directory', self.__globalConfig['paths.workspace'])
        self.log.info('Use %s for application data directory', self.__globalConfig['paths.appdata'])

        self.run(options)

    def initDownloaderDatabase(self, internal_path, download_id):
        db_rel_path = 'downloader_%s.db' % (download_id)
        db_prefix = 'sqlite:///'

        dp = os.path.join(internal_path, db_rel_path)
        db_url = db_prefix + dp

        self.log.debug('path of the database %s', db_url)

        self.__database = DownloaderDatabase(download_id, db_url)
        self.__database.createEngine(db_must_exist=True)

    def markReDownload(self, listing_path, isSafe, hasSeparator):
        """ read listing and reset state flag """

        self.__database.getSession()
        self.count = 0

        self.log.debug("Start listing storage for '%s'", listing_path)
        # verifie que les fichiers a telecharger correspondent bien aux patterns autorises, sinon on
        # lock le listing et on envoie un message operateur
        listingFile = File(listing_path)
        try:
            listingFile.open('r')
            lines = listingFile.readlines()
        finally:
            listingFile.close()

        for line in lines:
            filename = line.strip()
            # jump if it's a commented line
            if filename.startswith('#') or len(filename) == 0:
                continue

            self.log.debug("Input '%s'...", filename)

            if isSafe:
                if filename[0] in ('/', '*', '%'):
                    filename_template = filename + '%'
                else:
                    filename_template = '%/' + filename + '%'
            elif hasSeparator:
                filename_template = filename + FILE_URL_SEPARATOR + '%'
            else:
                if filename[0] in ('/', '*', '%'):
                    filename_template = filename
                else:
                    filename_template = '%/' + filename

            try:
                list_Filepaths = self.__database.resetDownload(filename_template)

                count = len(list_Filepaths)
                self.__listfilepaths += list_Filepaths
                self.count += count
            except Exception as e:     # error
                self.log.info("Error search '%s' in database (%s)", filename, str(e))
                self.__database.closeSession()
                raise Exception(e)

            self.log.info("number of files to redownload for '%s' : %d", filename, count)

        self.__database.closeSession()

        self.log.info('total number of files to redownload : %d', self.count)

        # archive of fictif listing
        listingDstName = listingFile.getName(short=True) + '.list'
        listingDstPath = os.path.join(self.internal_path, FOLDER_LISTINGS_HISTORY)
        DstPath = os.path.join(listingDstPath, listingDstName)

        if os.path.exists(DstPath):
            # si le meme nom de fichier existe deja, on utilise un
            # numero d'increment
            i = 0
            exist_in_history = True
            while exist_in_history:
                i += 1
                DstPath = os.path.join(
                    listingDstPath,
                    '%s_' %(i) + listingDstName)
                exist_in_history = os.path.exists(DstPath)

        self.log.info(" Listing archived: '%s' " % (DstPath))

        listing = File(DstPath)
        try:
            listing.open('w+')
            for filePaths in self.__listfilepaths:
                listing.write(filePaths + '\n')
                self.log.debug(" file to reload: '%s' " % (filePaths))
        finally:
            listing.close()

    def getCountFileReloaded(self,):
        return self.count

def reloader_main():
    rld = ReLoader()
    rld.main()
