
import logging
import os

from eo_dataflow_manager.scheduler.com.ext.PublishRabbitMQ import PublishRabbitMQ
from .api.messages import EnhancedFileWriter, IMessageWriter
from .targets.EMMElastic import EMMElastic


class EMMWriter(IMessageWriter):

    def __init__(self, globalConfig):
        IMessageWriter.__init__(self)

        self.projet_name = globalConfig["emm.project"]
        self.message_type = globalConfig["emm.message_type"]

        self.__log = logging.getLogger()

        self.__file_writer = None
        if "filesystem" in globalConfig["emm.targets"]:
            path = globalConfig["emm.filesystem.path"]
            if not os.path.isabs(path):
                path = os.path.join(globalConfig["paths.workspace"], path)

            self.__file_writer = EnhancedFileWriter(
                filename='eo-dataflow-manager',
                directory=path,
                extension='.xml'
            )

        # Elasticsearch
        self.__emm_elastic = None
        if "elasticsearch" in globalConfig["emm.targets"]:
            self.__emm_elastic = EMMElastic(
                hosts=globalConfig["emm.elasticsearch.hosts"],
                scheme=globalConfig["emm.elasticsearch.scheme"],
                user=globalConfig["emm.elasticsearch.user"],
                password=globalConfig["emm.elasticsearch.password"],
                indexNameTemplate=globalConfig["emm.elasticsearch.index"],
                sniffCluster=globalConfig["emm.elasticsearch.sniff_cluster"],
            )

        # RabbitMQ
        self.__emm_rabbitmq = PublishRabbitMQ

    fileWriter = property(lambda self: self.__file_writer, None, None, None)

    def getFilesystemDir(self):
        return self.__file_writer.directory

    def writeMessage(
            self,
            message,
            dataset_id,
            filename=None,
            create_missing_dirs=None,
            pretty_print=True,
            encoding='UTF-8',
            xml_declaration=True):

        # overwrite project and type
        message.project = self.projet_name
        message.type = self.message_type

        # Filesystem
        if self.__file_writer is not None:
            try:
                self.__file_writer.writeMessage(
                    message,
                    dataset_id,
                    filename,
                    create_missing_dirs,
                    pretty_print,
                    encoding,
                    xml_declaration
                )
            except Exception as e:
                self.__log.exception('EMMWriter::writeMessage: error while writing EMM Message: %s', e)

        # Elastic search
        if self.__emm_elastic is not None:
            self.__emm_elastic.writeMessage(dataset_id, message)

        # RabbitMQ
        self.__emm_rabbitmq.publish(dataset_id, message.toDict(), self.message_type)

    def setProjectName(self, project_name):
        self.projet_name = project_name
