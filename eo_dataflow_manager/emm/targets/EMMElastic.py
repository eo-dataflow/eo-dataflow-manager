
from datetime import datetime

try:
    import elasticsearch
except  ImportError:
    elasticsearch = None

from ..api.messages import IMessageWriter


class EMMElastic(IMessageWriter):

    def __init__(self, hosts, scheme="http", user=None, password=None, indexNameTemplate="emm", sniffCluster=False):
        """
        hosts: list of elasticsearch hosts (`hostname:port` format)
        """
        import logging
        self.__log = logging.getLogger("cs")

        IMessageWriter.__init__(self)

        self._es = elasticsearch.Elasticsearch(
            # scheme=scheme,
            hosts=hosts,
            http_auth=None if user is None else (user, password),
            # Sniffing detects additional nodes from the same cluster
            sniff_on_start=sniffCluster,
            sniff_on_connection_fail=sniffCluster,
            verify_certs=True,
        )

        info = self._es.info(ignore=403)  # pylint: disable=unexpected-keyword-arg
        health = self._es.cluster.health(ignore=403)  # pylint: disable=unexpected-keyword-arg

        if "status" not in info:
            # status key is set if there is an error, i.e. 403
            self.__log.info("Elasticsearch cluster: %s, version %s, health=%s nodes=%s",
                            info["cluster_name"],
                            info["version"]["number"],
                            health["status"] if health["status"] == 200 else "Unknown",
                            [n["host"] for n in self._es.nodes.stats()["nodes"].values()])
            self.__log.debug("            full info: %s", info)
            if health["status"] == 200:
                self.__log.debug("          full health: %s", health)
        else:
            self.__log.debug("Elasticsearch cluster hosts: %s", hosts)
        self.__log.debug("             Client version: %s", ".".join([str(i) for i in elasticsearch.VERSION]))

        self.__indexNameTemplate = indexNameTemplate

    def writeMessage(self, identifier, message):
        """
        Write a full EMM message into the database (the message must not exist)
        """
        self._es.index(
            index=self.__getIndexName(),
            doc_type=message.type,
            id=identifier,
            body=message.toDict(),
        )

    def updateMessagePart(self, identifier, messagePart):
        """
        Updates a message stored in elasticsearch database (with the document ID and content)
        """
        assert isinstance(messagePart, dict)
        self._es.update(
            index=self.__getIndexName(),
            doc_type=messagePart.type,
            id=identifier,
            body={"doc": messagePart},
        )

    def __getIndexName(self):
        return datetime.now().strftime(self.__indexNameTemplate)
