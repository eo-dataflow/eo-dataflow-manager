#
# -*- coding: UTF-8 -*-
#

""" Messages API module contains the L{MessageFactory} class and the main
objects to help building EMM_Messages.

@contact: U{Frederic PAUL<mailto:frederic.paul@ifremer.fr>}
@copyright: IFREMER
"""

import datetime
import json
import os
import shutil  # for move() file
import sys  # for sys.stderr.write
import threading
from collections import defaultdict
from socket import gethostname  # for the EnhancedFileWriter

LXML_VERSION = None
try:
    from lxml import etree
    LXML_VERSION = 'lxml'
    # print("running with lxml.etree")
except ImportError:
    try:
        # Python 2.5
        import xml.etree.cElementTree as etree
        LXML_VERSION = 'xml.etree.cElementTree'
        # print("running with cElementTree on Python 2.5+")
    except ImportError:
        try:
            # Python 2.5
            import xml.etree.ElementTree as etree
            LXML_VERSION = 'xml.etree.ElementTree'
            # print("running with ElementTree on Python 2.5+")
        except ImportError:
            try:
                # normal cElementTree install
                import cElementTree as etree
                LXML_VERSION = 'cElementTree'
                # print("running with cElementTree")
            except ImportError:
                try:
                    # normal ElementTree install
                    import elementtree.ElementTree as etree
                    LXML_VERSION = 'elementree.ElementTree'
                    # print("running with ElementTree")
                except ImportError as e:
                    # print("Failed to import ElementTree from any known place")
                    raise e


__version__ = '1.0.0'

# The internal version attribute should be incremented very often, as a
# revision version (to help information retrieving when this file is
# embedded in some others projects)
__revision_version__ = '0100'

# Version of the XML Schema this API is compatible with
DEFAULT_SCHEMA_VERSION = '1.0'


##############################################################################
# MessageFactory                                                             #
##############################################################################
# This class could be in a separate module, but it is better to let it here
# to simplify the use of the messages API : messages generation is usable
# by copying the present file in your project

class MessageFactory(object):
    def __init__(self, template=None):
        self.__template = template

    def create(self, msg_dict, template=None):
        assert 'type' in msg_dict
        t = self.__template
        if template is not None:
            t = template

        md = msg_dict
        m = None
        if md['type'] == 'message':
            m = Message(template=t)
        if md['type'] == 'processingMessage':
            m = ProcessingMessage(template=t)
        if md['type'] == 'downloadMessage':
            m = DownloadMessage(template=t)

        if m is None:
            raise Exception("Message type '%s' not managed" % (md['type']))

        sender_name = None
        sender_type = None
        recipient_name = None
        recipient_type = None
        reference_name = None
        reference_type = None
        processing_logfile = None
        processing_logfile_misc_infos = None
        for key, value in list(msg_dict.items()):
            # Message
            if key == 'date':
                m.date = value
            elif key == 'level':
                m.level = StringLevel[md[key].upper()]
            elif key == 'project':
                m.project = md[key]
            elif key == 'type':
                m.type = md[key]
            elif key == 'sender_name':
                sender_name = md[key]
            elif key == 'sender_type':
                sender_type = md[key]
            elif key == 'recipient_name':
                recipient_name = md[key]
            elif key == 'recipient_type':
                recipient_type = md[key]
            elif key == 'summary':
                m.summary = md[key]
            elif key == 'reference_name':
                reference_name = md[key]
            elif key == 'reference_type':
                reference_type = md[key]
            elif key == 'details':
                m.details = md[key]
            elif key == 'misc_infos':
                m.misc_infos = MiscInfos(pairs=md[key])
            elif key == 'uid':
                m.uid = md[key]
            elif key == 'parent_uid':
                m.parent_uid = md[key]
            elif key == 'schemaVersion':
                m.schemaVersion = md[key]
            # Processing
            elif key == 'processing_process_id':
                m.processing.process_id = md[key]
            elif key == 'processing_start_date':
                m.processing.start_date = md[key]
            elif key == 'processing_stop_date':
                m.processing.stop_date = md[key]
            elif key == 'processing_exit_code':
                m.processing.exit_code = md[key]
            elif key == 'processing_hostname':
                m.processing.hostname = md[key]
            elif key == 'processing_username':
                m.processing.username = md[key]
            elif key == 'processing_cmd_line':
                m.processing.cmd_line = md[key]
            elif key == 'processing_pid':
                m.processing.pid = md[key]
            elif key == 'processing_logfile':
                processing_logfile = md[key]
            elif key == 'processing_logfile_misc_infos':
                processing_logfile_misc_infos = md[key]
            elif key == 'processing_input_files':
                raise Exception(
                    'Processing input files not managed in the factory. Use returned message to add input files')
            elif key == 'processing_output_files':
                raise Exception(
                    'Processing output files not managed in the factory. Use returned message to add output files')
            # Download
            elif key == 'download_name':
                m.download.name = md[key]
            elif key == 'download_provider':
                m.download.provider = md[key]
            elif key == 'download_files':
                raise Exception(
                    'download_files is not managed in the factory. Use returned message to add download files')
            # ChainContext
            elif key == 'chain_context_chain_controller':
                if m.chain_context is None:
                    m.chain_context = ChainContext()
                m.chain_context.chain_controller = md[key]
            elif key == 'chain_context_chain_name':
                if m.chain_context is None:
                    m.chain_context = ChainContext()
                m.chain_context.chain_name = md[key]
            elif key == 'chain_context_node_name':
                if m.chain_context is None:
                    m.chain_context = ChainContext()
                m.chain_context.node_name = md[key]
            elif key == 'chain_context_node_process':
                if m.chain_context is None:
                    m.chain_context = ChainContext()
                m.chain_context.node_process = md[key]
            else:
                sys.stderr.write(
                    "WARNING : MessageFactory::create : '%s' key is not managed\n" %
                    (key))

        if sender_name is not None or sender_type is not None:
            m.sender = Sender(name=sender_name, type=sender_type)
        if recipient_name is not None or recipient_type is not None:
            m.recipient = Recipient(name=recipient_name, type=recipient_type)
        if reference_name is not None or reference_type is not None:
            m.reference = Reference(name=reference_name, type=reference_type)
        if processing_logfile is not None:
            logfile_name = os.path.basename(processing_logfile)
            logfile_path = os.path.dirname(processing_logfile)
            m.processing.logfile = File(name=logfile_name, path=logfile_path)
        if processing_logfile_misc_infos is not None:
            m.processing.logfile.misc_infos = processing_logfile_misc_infos

        return m


##############################################################################
# Message API                                                                #
##############################################################################
INFO = 0
WARNING = 1
ERROR = 2
CRITICAL = 3

LevelString = {
    INFO: 'INFO',
    WARNING: 'WARNING',
    ERROR: 'ERROR',
    CRITICAL: 'CRITICAL'
}

StringLevel = {
    'INFO': INFO,
    'WARNING': WARNING,
    'ERROR': ERROR,
    'CRITICAL': CRITICAL
}

MSG_DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'


class BaseElement(object):
    def __init__(self):
        self.blocList = {}
        # etree.ElementBase.__init__(self)

    def getElement(self):
        raise 'Must be specialized ! (class=%s)' % (
            self.__class__.__name__)  # TODO : meilleur raise


###
# Writers : manage the Message serialization
###
class IMessageWriter(object):
    """
    Interface class for L{OperationMessage} writer capable
    """

    def write(self, message):
        """Write message capatibility

        @param message: Message to be write
        @type message: L{OperationMessage}
        """
        raise RuntimeError(
            'writeMessage is an abstract method in IMessageWriter')


class EnhancedFileWriter(IMessageWriter):
    """ Class used to serialize Messages in files (using backup directories if needed)
    """

    def __init__(
            self,
            filename=None,
            directory=None,
            create_missing_dirs=True,
            add_uniq_prefix=True,
            extension='.xml',
            enable=False):
        self.setFilename(filename)
        self.setDirectory(directory)
        self.setCreateMissingDirs(create_missing_dirs)
        self.setAddUniqPrefix(add_uniq_prefix)
        self.setExtension(extension)
        self.enable = enable

    def setExtension(self, value):
        self.__extension = value

    def setFilename(self, value):
        self.__filename = value

    def setDirectory(self, value):
        self.__directory = value

    def setCreateMissingDirs(self, value):
        self.__create_missing_dirs = value

    def setAddUniqPrefix(self, value):
        self.__add_uniq_prefix = value

    def writeMessage(
            self,
            message,
            dataset_id,
            filename=None,
            create_missing_dirs=None,
            pretty_print=True,
            encoding='UTF-8',
            xml_declaration=True):
        assert message is not None, 'Cannot write message (None)'

        f = None
        if filename is not None:
            f = filename
        else:
            f = self.filename

        if f is None:
            raise Exception('Cannot write file without a filename...')

        if self.add_uniq_prefix:
            dt = datetime.datetime.utcnow()
            timestamp_string = dt.strftime(MSG_DATE_FORMAT)
            uniq_prefix = '%s-%s-%s-%s_' % (timestamp_string, gethostname(),
                os.getpid(), threading.current_thread().name.split('-')[1][-7:].zfill(7))
            suffix = '_%.6d' % (dt.microsecond)
            f_path, f_name = os.path.split(f)
            f = os.path.join(f_path, uniq_prefix + f_name.rstrip(self.__extension) + suffix)

        if not f.endswith(self.__extension):
            f = f + self.__extension

        write_ok = False
        try:
            rel_filedir = os.path.dirname(f)
            abs_filedir = os.path.join(self.directory, rel_filedir)
            if not os.path.exists(
                    abs_filedir) and self.create_missing_dirs:
                os.makedirs(abs_filedir)

            abs_filepath = os.path.join(self.directory, f)

            # check if file already exists, rename it if needed
            i = 0
            while os.path.exists(abs_filepath):
                i += 1
                new_fname = '%s_%.5d%s' % (
                    f.rstrip(self.__extension), i, self.__extension)
                abs_filepath = os.path.join(self.directory, new_fname)

            tmp_abs_filepath = abs_filepath + '.tmp'

            if self.__extension == '.xml':
                elementtree = etree.ElementTree(message.getElement())
                if LXML_VERSION == 'lxml':
                    elementtree.write(
                        tmp_abs_filepath,
                        pretty_print=pretty_print,
                        encoding=encoding,
                        xml_declaration=xml_declaration)
                else:  # only tested with elementtree.ElementTree
                    elementtree.write(tmp_abs_filepath, encoding=encoding)
            else:
                with open(tmp_abs_filepath, 'wb') as file:
                    dict_data = message.parseToJson(message.getElement())
                    file.write(json.dumps(dict_data, sort_keys=True, indent=4))

            shutil.move(tmp_abs_filepath, abs_filepath)
        except Exception as e:
            sys.stderr.write(
                "WARNING : EnhancedFileWriter : cannot write file '%s' in directory '%s'. Error = %s\n" %
                (f, self.directory, e))
        else:
            write_ok = True

        if not write_ok:
            sys.stderr.write(
                'ERROR : EnhancedFileWriter : Cannot write message anywhere ! Message will be lost...\n')
            sys.stderr.write(message.toString(
                method=self.__extension[1:]) + '\n')

    filename = property(
        lambda self: self.__filename,
        setFilename,
        None,
        'Description...')
    directory = property(
        lambda self: self.__directory,
        setDirectory,
        None,
        'Description...')
    create_missing_dirs = property(
        lambda self: self.__create_missing_dirs,
        setCreateMissingDirs,
        None,
        'Description...')
    add_uniq_prefix = property(
        lambda self: self.__add_uniq_prefix,
        setAddUniqPrefix,
        None,
        'Description...')


###
# Validators : mainly the XMLSchemaValidator, to validate Messages objects with XML Schema
###
class IMessageValidator(object):
    def validate(self, message, error_details=False):
        assert 0, 'Abstract method'


class XMLSchemaValidator(IMessageValidator):

    def __init__(self, xml_schema=None):
        self.setXmlSchema(xml_schema)

    def setXmlSchema(self, xml_schema_path):
        assert xml_schema_path is None or isinstance(xml_schema_path, str)

        self.__xml_schema = None
        if isinstance(xml_schema_path, str):
            if not os.path.exists(xml_schema_path):
                raise Exception(
                    'Invalid path for schema : %s' %
                    (xml_schema_path))
            xml_schema_doc = etree.parse(xml_schema_path)
            try:
                xml_schema = etree.XMLSchema(xml_schema_doc)
            except etree.XMLSchemaParseError as e:
                # print "Error : XMLSchemaValidator::setXmlSchema : invalid schema [path=%s]"%(xml_schema_path)
                #log = e.error_log.filter_from_level(etree.ErrorLevels.FATAL)
                # print e.error_log
                raise e
            except Exception as e:
                raise e
                #self._log.exception("Error while loading schema : %s [details=%s]"%(xml_schema_path), str(e))

            self.__xml_schema = xml_schema

    def validate(self, message, error_details=False):
        if self.xml_schema is None:
            raise Exception('Schema is not set, cannot validate !')
        if isinstance(message, Message):
            res = self.xml_schema.validate(message.getElement())
        else:
            res = self.xml_schema.validate(message)
        if res is False and error_details is True:
            #print((self.xml_schema.error_log))
            pass
        return res

    xml_schema = property(
        lambda self: self.__xml_schema,
        setXmlSchema,
        None,
        'Description...')

###
# Readers : manage the Message unserialization
###


class MessageReader(object):

    def fromFile(xml_filepath, validator=None):
        tree = etree.parse(xml_filepath)
        root = tree.getroot()
        return MessageReader.fromElement(root, validator)

    def fromString(xml_string, validator=None):
        root = etree.fromstring(xml_string)
        return MessageReader.fromElement(root, validator)

    def fromElement(element, validator=None):
        # check if message is correct before unserialization, when possible
        if validator is not None:
            res = validator.validate(element)
            if not res:
                #print(("Error : MessageReader::fromElement : invalid message : %s" % (
                #    validator.xml_schema.error_log)))
                return None

        message = None
        messagetag = element.tag
        if messagetag == 'message':
            message = Message()
        elif messagetag == 'processingMessage':
            message = ProcessingMessage()
        elif messagetag == 'downloadMessage':
            message = DownloadMessage()

        if message is None:
            raise Exception('Unknown message tag : %s' % (messagetag))
        else:
            message.fromElement(element)

        return message

    fromFile = staticmethod(fromFile)
    fromString = staticmethod(fromString)
    fromElement = staticmethod(fromElement)


class Message(BaseElement):

    def __init__(self, level=None,
                 date='utc_now',
                 project=None,
                 type=None,
                 sender=None,
                 recipient=None,
                 summary=None,
                 reference=None,
                 details=None,
                 misc_infos=None,
                 uid=None,
                 parent_uid=None,
                 schemaVersion=DEFAULT_SCHEMA_VERSION,
                 template=None):

        BaseElement.__init__(self)

        self.setLevel(level)
        self.setDate(date)
        self.setProject(project)
        self.setType(type)
        self.setSender(sender)
        self.setRecipient(recipient)
        self.setSummary(summary)
        self.setReference(reference)
        self.setDetails(details)
        self.setMiscInfos(misc_infos)
        self.setUid(uid)
        self.setParentUid(parent_uid)
        self.setSchemaVersion(schemaVersion)

        Message.setTemplate(self, template)

    def toDict(self, identifier=None):
        def convToString(obj):
            from eo_dataflow_manager.scheduler.com.sys.File import File

            if isinstance(obj, dict):
                for key in obj:
                    obj[key] = convToString(obj[key])
            if isinstance(obj, list):
                for index in range(len(obj)):
                    obj[index] = convToString(obj[index])
            elif isinstance(obj, File):
                obj = obj.path
            elif isinstance(obj, Reference):
                obj = obj.toDict()

            return obj

        from copy import deepcopy
        return convToString(deepcopy({
            'schemaVersion': self.schemaVersion,
            'date': self.date,
            'level': self.level,
            'date': self.date,
            'project': self.project,
            'type': self.type,
            'sender': {'name': self.sender.name, 'type': self.sender.type},
            'recipient': {'name': self.recipient.name, 'type': self.recipient.type},
            'summary': self.summary,
            'reference': self.reference,
            'details': self.details,
            'misc_infos': self.misc_infos.pairs if self.misc_infos is not None else None,
            'uid': self.uid,
            'parent_uid': self.parent_uid,
        }))


    def toString(
            self,
            encoding='UTF-8',
            method='xml',
            xml_declaration=None,
            pretty_print=True):
        if method == 'xml':
            if LXML_VERSION == 'lxml':
                return etree.tostring(
                    self.getElement(),
                    encoding=encoding,
                    xml_declaration=xml_declaration,
                    pretty_print=pretty_print)
            else:  # only tested with elementtree.ElementTree
                return etree.tostring(self.getElement(), encoding=encoding)
        elif method == 'json':
            return str(self.parseToJson(self.getElement()))

        else:
            raise "Message.toString() : method '%s' not implemented !" % (method)

    def write(self, messageWriter=None, file=None, validator=None,
              pretty_print=True, encoding='UTF-8', xml_declaration=True):
        """
            Call a message writer to serialize this message. The "file" parameter should not be used
            in an production environment : useful for testing, but writers would be more secure
        """
        if messageWriter is not None and isinstance(
                messageWriter, IMessageWriter):
            messageWriter.write(self)
        else:
            if file is not None:
                elementtree = etree.ElementTree(self.getElement())
                elementtree.write(
                    file,
                    pretty_print=pretty_print,
                    encoding=encoding,
                    xml_declaration=xml_declaration)

    def isValid(self, messageValidator, error_details=False):
        assert messageValidator is not None and isinstance(
            messageValidator, IMessageValidator)

        return messageValidator.validate(self, error_details=error_details)

    def getElement(self):
        message = etree.Element('message')
        message.set('schemaVersion', self.schemaVersion)

        date = etree.SubElement(message, 'date')
        date.text = str(self.date)

        level = etree.SubElement(message, 'level')
        if self.level in list(LevelString.keys()):
            level.text = LevelString[self.level]
        else:
            level.text = str(self.level)

        project = etree.SubElement(message, 'project')
        project.text = self.project

        if self.type is not None:
            msgtype = etree.SubElement(message, 'type')
            msgtype.text = self.type

        if self.sender is not None:
            sender = self.sender.getElement()
            message.append(sender)

        if self.recipient is not None:
            recipient = self.recipient.getElement()
            message.append(recipient)

        if self.summary is not None:
            summary = etree.SubElement(message, 'summary')
            summary.text = self.summary

        if self.reference is not None:
            reference = self.reference.getElement()
            message.append(reference)

        if self.details is not None:
            details = etree.SubElement(message, 'details')
            details.text = self.details

        if self.misc_infos is not None:
            misc_infos = self.misc_infos.getElement()
            message.append(misc_infos)

        if self.uid is not None:
            uid = etree.SubElement(message, 'uid')
            uid.text = self.uid

        if self.parent_uid is not None:
            parent_uid = etree.SubElement(message, 'parent_uid')
            parent_uid.text = self.parent_uid
        return message

    @staticmethod
    def parseToJson(elt):

        dict_json = {elt.tag: {} if elt.attrib else None}
        children = list(elt)
        if children:
            ddict = defaultdict(list)
            for dictchild in map(Message.parseToJson, children):
                for k, v in list(dictchild.items()):
                    ddict[k].append(v)
            dict_json = {
                elt.tag: {
                    k: v[0] if len(v) == 1 else v for k,
                    v in list(
                        ddict.items())}}
        if elt.attrib:
            dict_json[elt.tag].update(('@' + k, v)
                                      for k, v in list(elt.attrib.items()))

        if elt.text:
            text = elt.text.strip()
            if children or elt.attrib:
                if text:
                    dict_json[elt.tag]['#text'] = text
            else:
                try:
                    dict_json[elt.tag] = int(text)
                except ValueError:
                    dict_json[elt.tag] = text
        return dict_json

    def fromElement(self, element, validator=None):
        dateElt = element.find('date')
        if dateElt is not None:
            self.date = dateElt.text

        levelElt = element.find('level')
        if levelElt is not None and levelElt.text in list(StringLevel.keys()):
            self.level = StringLevel[levelElt.text]

        projectElt = element.find('project')
        if projectElt is not None:
            self.project = projectElt.text

        typeElt = element.find('type')
        if typeElt is not None:
            self.type = typeElt.text

        senderElt = element.find('sender')
        if senderElt is not None:
            self.sender = Sender()
            self.sender.fromElement(senderElt)

        recipientElt = element.find('recipient')
        if recipientElt is not None:
            self.recipient = Recipient()
            self.recipient.fromElement(recipientElt)

        summaryElt = element.find('summary')
        if summaryElt is not None:
            self.summary = summaryElt.text

        referenceElt = element.find('reference')
        if referenceElt is not None:
            self.reference = Reference()
            self.reference.fromElement(referenceElt)

        detailsElt = element.find('details')
        if detailsElt is not None:
            self.details = detailsElt.text

        miscInfosElt = element.find('misc_infos')
        if miscInfosElt is not None:
            self.misc_infos = MiscInfos()
            self.misc_infos.fromElement(miscInfosElt)

        uidElt = element.find('uid')
        if uidElt is not None:
            self.uid = uidElt.text

        parentUidElt = element.find('parent_uid')
        if parentUidElt is not None:
            self.parent_uid = parentUidElt.text

    def setLevel(self, value):
        assert value is None or isinstance(value, int)
        if value is not None:
            assert value in (INFO, WARNING, ERROR, CRITICAL)
        self.__level = value

    def setDate(self, value='utc_now'):
        assert isinstance(value, str)
        if value == 'utc_now':
            dt = datetime.datetime.utcnow()
            value = dt.strftime(MSG_DATE_FORMAT)
        self.__date = value

    def setProject(self, value):
        assert value is None or isinstance(value, str)
        self.__project = value

    def setType(self, value):
        assert value is None or isinstance(value, str)
        self.__type = value

    def setSender(self, value):
        assert value is None or isinstance(value, Sender)
        self.__sender = value

    def setRecipient(self, value):
        assert value is None or isinstance(value, Recipient)
        self.__recipient = value

    def setSummary(self, value):
        self.__summary = value

    def setReference(self, value):
        assert value is None or isinstance(value, Reference)
        self.__reference = value

    def setDetails(self, value):
        self.__details = value

    def setMiscInfos(self, value):
        assert value is None or isinstance(value, MiscInfos)
        self.__misc_infos = value

    def setUid(self, value):
        self.__uid = value

    def setParentUid(self, value):
        self.__parent_uid = value

    def setSchemaVersion(self, value):
        self.__schemaVersion = value

    def setTemplate(self, template):
        assert template is None or isinstance(template, Message)
        if template is not None:
            if self.project is None:
                self.project = template.project
            if self.type is None:
                self.type = template.type
            if self.sender is None:
                self.sender = template.sender
            if self.recipient is None:
                self.recipient = template.recipient

    date = property(lambda self: self.__date, setDate, None, 'Description...')
    level = property(
        lambda self: self.__level,
        setLevel,
        None,
        'Description...')
    project = property(
        lambda self: self.__project,
        setProject,
        None,
        'Description...')
    type = property(lambda self: self.__type, setType, None, 'Description...')
    sender = property(
        lambda self: self.__sender,
        setSender,
        None,
        'Description...')
    recipient = property(
        lambda self: self.__recipient,
        setRecipient,
        None,
        'Description...')
    summary = property(
        lambda self: self.__summary,
        setSummary,
        None,
        'Description...')
    reference = property(
        lambda self: self.__reference,
        setReference,
        None,
        'Description...')
    details = property(
        lambda self: self.__details,
        setDetails,
        None,
        'Description...')
    misc_infos = property(
        lambda self: self.__misc_infos,
        setMiscInfos,
        None,
        'Description...')
    uid = property(lambda self: self.__uid, setUid, None, 'Description...')
    parent_uid = property(
        lambda self: self.__parent_uid,
        setParentUid,
        None,
        'Description...')
    schemaVersion = property(
        lambda self: self.__schemaVersion,
        setSchemaVersion,
        None,
        'Description...')


class Processing(BaseElement):

    def __init__(self, process_id=None,
                 start_date=None,
                 stop_date=None,
                 exit_code=None,
                 hostname=None,
                 username=None,
                 cmd_line=None,
                 pid=None,
                 logfile=None,
                 input_files=None,
                 output_files=None):
        BaseElement.__init__(self)

        self.setProcessId(process_id)
        self.setStartDate(start_date)
        self.setStopDate(stop_date)
        self.setExitCode(exit_code)
        self.setHostname(hostname)
        self.setUsername(username)
        self.setCmdLine(cmd_line)
        self.setPid(pid)
        self.setLogfile(logfile)
        self.setInputFiles(input_files)
        self.setOutputFiles(output_files)

    def getElement(self):
        processing = etree.Element('processing')

        process_id = etree.SubElement(processing, 'process_id')
        process_id.text = self.process_id

        if self.start_date is not None:
            start_date = etree.SubElement(processing, 'start_date')
            start_date.text = self.start_date

        if self.stop_date is not None:
            stop_date = etree.SubElement(processing, 'stop_date')
            stop_date.text = self.stop_date

        if self.exit_code is not None:
            exit_code = etree.SubElement(processing, 'exit_code')
            exit_code.text = str(self.exit_code)

        if self.hostname is not None:
            hostname = etree.SubElement(processing, 'hostname')
            hostname.text = self.hostname

        if self.username is not None:
            username = etree.SubElement(processing, 'username')
            username.text = self.username

        if self.cmd_line is not None:
            cmd_line = etree.SubElement(processing, 'cmd_line')
            cmd_line.text = self.cmd_line

        if self.pid is not None:
            pid = etree.SubElement(processing, 'pid')
            pid.text = str(self.pid)

        if self.logfile is not None:
            logfile = self.logfile.getElement()
            logfile.tag = 'logfile'
            processing.append(logfile)

        if self.input_files is not None:
            input_files = self.input_files.getElement()
            processing.append(input_files)

        if self.output_files is not None:
            output_files = self.output_files.getElement()
            processing.append(output_files)

        return processing

    def fromElement(self, element):
        processIdElt = element.find('process_id')
        if processIdElt is not None and processIdElt.text is not None:
            self.process_id = processIdElt.text

        startDateElt = element.find('start_date')
        if startDateElt is not None and startDateElt.text is not None:
            self.start_date = startDateElt.text

        stopDateElt = element.find('stop_date')
        if stopDateElt is not None and stopDateElt.text is not None:
            self.stop_date = stopDateElt.text

        exitCodeElt = element.find('exit_code')
        if exitCodeElt is not None and exitCodeElt.text is not None:
            self.exit_code = exitCodeElt.text

        hostnameElt = element.find('hostname')
        if hostnameElt is not None and hostnameElt.text is not None:
            self.hostname = hostnameElt.text

        usernameElt = element.find('username')
        if usernameElt is not None and usernameElt.text is not None:
            self.username = usernameElt.text

        cmdLineElt = element.find('cmd_line')
        if cmdLineElt is not None and cmdLineElt.text is not None:
            self.cmd_line = cmdLineElt.text

        pidElt = element.find('pid')
        if pidElt is not None and pidElt.text is not None:
            self.pid = pidElt.text

        logfileElt = element.find('logfile')
        if logfileElt is not None:
            self.logfile = File()
            self.logfile.fromElement(logfileElt)

        inputFilesElt = element.find('input_files')
        if inputFilesElt is not None:
            self.input_files = InputFileList()
            self.input_files.fromElement(inputFilesElt)

        outputFilesElt = element.find('output_files')
        if outputFilesElt is not None:
            self.output_files = OutputFileList()
            self.output_files.fromElement(outputFilesElt)

    def setProcessId(self, value):
        assert value is None or isinstance(value, str)
        self.__process_id = value

    def setStartDate(self, value):
        assert value is None or isinstance(value, str)
        self.__start_date = value

    def setStopDate(self, value):
        assert value is None or isinstance(value, str)
        self.__stop_date = value

    def setExitCode(self, value):
        assert value is None or isinstance(
            value, str) or isinstance(
            value, int)
        if value is not None:
            value = int(value)
        self.__exit_code = value

    def setHostname(self, value):
        assert value is None or isinstance(value, str)
        self.__hostname = value

    def setUsername(self, value):
        assert value is None or isinstance(value, str)
        self.__username = value

    def setCmdLine(self, value):
        assert value is None or isinstance(value, str)
        self.__cmd_line = value

    def setPid(self, value):
        assert value is None or isinstance(
            value, str) or isinstance(
            value, int)
        if value is not None:
            value = int(value)
        self.__pid = value

    def setLogfile(self, value):
        assert value is None or isinstance(value, File)
        self.__logfile = value

    def setInputFiles(self, value):
        assert value is None or isinstance(value, InputFileList)
        self.__input_files = value

    def setOutputFiles(self, value):
        assert value is None or isinstance(value, OutputFileList)
        self.__output_files = value

    def setTemplate(self, template):
        assert template is None or isinstance(template, Processing)
        if template is not None:
            if self.process_id is None:
                self.process_id = template.process_id
            if self.hostname is None:
                self.hostname = template.hostname
            if self.username is None:
                self.username = template.username

    process_id = property(
        lambda self: self.__process_id,
        setProcessId,
        None,
        'Description...')
    start_date = property(
        lambda self: self.__start_date,
        setStartDate,
        None,
        'Description...')
    stop_date = property(
        lambda self: self.__stop_date,
        setStopDate,
        None,
        'Description...')
    exit_code = property(
        lambda self: self.__exit_code,
        setExitCode,
        None,
        'Description...')
    hostname = property(
        lambda self: self.__hostname,
        setHostname,
        None,
        'Description...')
    username = property(
        lambda self: self.__username,
        setUsername,
        None,
        'Description...')
    cmd_line = property(
        lambda self: self.__cmd_line,
        setCmdLine,
        None,
        'Description...')
    pid = property(lambda self: self.__pid, setPid, None, 'Description...')
    logfile = property(
        lambda self: self.__logfile,
        setLogfile,
        None,
        'Description...')
    input_files = property(
        lambda self: self.__input_files,
        setInputFiles,
        None,
        'Description...')
    output_files = property(
        lambda self: self.__output_files,
        setOutputFiles,
        None,
        'Description...')


class Download(BaseElement):
    def __init__(self, name=None,
                 provider=None,
                 download_files=None):
        self.setName(name)
        self.setProvider(provider)
        self.setDownloadFiles(download_files)

    def setName(self, value):
        self.__name = value

    def setProvider(self, value):
        self.__provider = value

    def setDownloadFiles(self, value):
        assert value is None or isinstance(value, DownloadFileList)
        self.__download_files = value

    def getElement(self):
        downloadElt = etree.Element('download')
        nameElt = etree.SubElement(downloadElt, 'name')
        nameElt.text = self.name
        providerElt = etree.SubElement(downloadElt, 'provider')
        providerElt.text = self.provider
        if self.download_files is not None:
            download_files = self.download_files.getElement()
            downloadElt.append(download_files)
        return downloadElt

    def fromElement(self, element):
        nameElt = element.find('name')
        if nameElt is not None:
            self.name = nameElt.text
        providerElt = element.find('provider')
        if providerElt is not None:
            self.provider = providerElt.text
        downloadFilesElt = element.find('download_files')
        if downloadFilesElt is not None:
            self.download_files = DownloadFileList()
            self.download_files.fromElement(downloadFilesElt)

    def setTemplate(self, template):
        assert template is None or isinstance(template, Download)
        if template is not None:
            if self.name is None:
                self.name = template.name
            if self.provider is None:
                self.provider = template.provider

    name = property(lambda self: self.__name, setName, None, 'Description...')
    provider = property(
        lambda self: self.__provider,
        setProvider,
        None,
        'Description...')
    download_files = property(
        lambda self: self.__download_files,
        setDownloadFiles,
        None,
        'Description...')


class ChainContext(BaseElement):
    def __init__(self, chain_controller=None,
                 chain_name=None,
                 node_name=None,
                 node_process=None):
        self.setChainController(chain_controller)
        self.setChainName(chain_name)
        self.setNodeName(node_name)
        self.setNodeProcess(node_process)

    def getElement(self):
        chainContextElt = etree.Element('chain_context')

        if self.chain_controller is not None:
            chainControllerElt = etree.SubElement(
                chainContextElt, 'chain_controller')
            chainControllerElt.text = self.chain_controller

        chainNameElt = etree.SubElement(chainContextElt, 'chain_name')
        chainNameElt.text = self.chain_name

        nodeNameElt = etree.SubElement(chainContextElt, 'node_name')
        nodeNameElt.text = self.node_name

        if self.node_process is not None:
            nodeProcessElt = etree.SubElement(chainContextElt, 'node_process')
            nodeProcessElt.text = self.node_process

        return chainContextElt

    def fromElement(self, element):
        chainControllerElt = element.find('chain_controller')
        if chainControllerElt is not None:
            self.chain_controller = chainControllerElt.text

        chainNameElt = element.find('chain_name')
        if chainNameElt is not None:
            self.chain_name = chainNameElt.text

        nodeNameElt = element.find('node_name')
        if nodeNameElt is not None:
            self.node_name = nodeNameElt.text

        nodeProcessElt = element.find('node_process')
        if nodeProcessElt is not None:
            self.node_process = nodeProcessElt.text

    def setChainController(self, value):
        assert value is None or isinstance(value, str)
        self.__chain_controller = value

    def setChainName(self, value):
        assert value is None or isinstance(value, str)
        self.__chain_name = value

    def setNodeName(self, value):
        assert value is None or isinstance(value, str)
        self.__node_name = value

    def setNodeProcess(self, value):
        assert value is None or isinstance(value, str)
        self.__node_process = value

    def setTemplate(self, template):
        assert template is None or isinstance(template, ChainContext)

        if template is not None:
            if self.chain_controller is None:
                self.chain_controller = template.chain_controller
            if self.chain_name is None:
                self.chain_name = template.chain_name
            if self.node_name is None:
                self.node_name = template.node_name
            if self.node_process is None:
                self.node_process = template.node_process

    def toDict(self):
        return {
            'chain_controller': self.chain_controller,
            'chain_name': self.chain_name,
            'node_name': self.node_name,
            'node_process': self.node_process,
        }

    chain_controller = property(
        lambda self: self.__chain_controller,
        setChainController,
        None,
        'Description...')
    chain_name = property(
        lambda self: self.__chain_name,
        setChainName,
        None,
        'Description...')
    node_name = property(
        lambda self: self.__node_name,
        setNodeName,
        None,
        'Description...')
    node_process = property(
        lambda self: self.__node_process,
        setNodeProcess,
        None,
        'Description...')


class ContactCenter(BaseElement):
    def __init__(self, name=None, type=None):
        self.setName(name)
        self.setType(type)

    def getElement(self):
        contact_center = etree.Element('contact_center')
        contact_center.set('type', self.type)
        contact_center.text = self.name
        return contact_center

    def fromElement(self, element):
        attType = element.get('type')
        if attType is not None:
            self.type = attType
        if element.text is not None:
            self.name = element.text

    def setName(self, name):
        self.__name = name

    def setType(self, type):
        self.__type = type

    name = property(lambda self: self.__name, setName, None, 'Description...')
    type = property(lambda self: self.__type, setType, None, 'Description...')


class Sender(ContactCenter):
    def getElement(self):
        sender = ContactCenter.getElement(self)
        sender.tag = 'sender'
        return sender

    def fromElement(self, element):
        return ContactCenter.fromElement(self, element)


class Recipient(ContactCenter):
    def getElement(self):
        recipient = ContactCenter.getElement(self)
        recipient.tag = 'recipient'
        return recipient

    def fromElement(self, element):
        return ContactCenter.fromElement(self, element)


class FileList(BaseElement):
    def __init__(self, file_list=None):
        self.setFileList(file_list)

    def setFileList(self, value):
        assert value is None or isinstance(value, list)
        if value is None:
            value = []

        for i in value:
            # TODO : utiliser des isinstance partout ou il peut y avoir de
            # l'heritage !!!
            assert isinstance(i, File)

        self.__file_list = value

    def getElement(self):
        file_list = etree.Element('file_list')
        for f in self.file_list:
            file_list.append(f.getElement())
        return file_list

    def fromElement(self, element):
        fileListElt = element.findall('file')
        for fileElt in fileListElt:
            f = File()
            f.fromElement(fileElt)
            self.file_list.append(f)

    file_list = property(
        lambda self: self.__file_list,
        setFileList,
        None,
        'Description...')


class InputFileList(FileList):
    def getElement(self):
        input_files = FileList.getElement(self)
        input_files.tag = 'input_files'
        return input_files

    def fromElement(self, element):
        return FileList.fromElement(self, element)


class OutputFileList(FileList):
    def getElement(self):
        output_files = FileList.getElement(self)
        output_files.tag = 'output_files'
        return output_files

    def fromElement(self, element):
        return FileList.fromElement(self, element)


class DownloadFileList(FileList):
    def getElement(self):
        download_files = FileList.getElement(self)
        download_files.tag = 'download_files'
        return download_files

    def fromElement(self, element):
        return FileList.fromElement(self, element)


class File(BaseElement):
    def __init__(self, filepath=None, name=None, path=None, misc_infos=None):
        if filepath is not None:
            name = os.path.basename(filepath)
            path = os.path.dirname(filepath)
        self.setName(name)
        self.setPath(path)
        self.setMiscInfos(misc_infos)

    def getElement(self):
        file = etree.Element('file')
        if self.name is not None:
            file.set('name', self.name)
        if self.path is not None:
            file.set('path', self.path)

        if self.misc_infos is not None:
            misc_infos = self.misc_infos.getElement()
            file.append(misc_infos)

        return file

    def fromElement(self, element):
        attName = element.get('name')
        attPath = element.get('path')

        # check on load from element if name is not filepath...
        if attPath is None:
            attName, attPath = os.path.split(attName)

        if attName is not None:
            self.name = attName
        if attPath is not None:
            self.path = attPath

        miscInfosElt = element.find('misc_infos')
        if miscInfosElt is not None:
            self.misc_infos = MiscInfos()
            self.misc_infos.fromElement(miscInfosElt)

    def setName(self, value):
        self.__name = value

    def setPath(self, value):
        self.__path = value

    def setMiscInfos(self, value):
        assert value is None or isinstance(value, MiscInfos)
        self.__misc_infos = value

    name = property(lambda self: self.__name, setName, None, 'Description...')
    path = property(lambda self: self.__path, setPath, None, 'Description...')
    misc_infos = property(
        lambda self: self.__misc_infos,
        setMiscInfos,
        None,
        'Description...')


class MiscInfos(BaseElement):
    def __init__(self, pairs=None):
        self.setPairs(pairs)

    def getElement(self):
        misc_infos = etree.Element('misc_infos')
        for key in list(self.pairs.keys()):
            info = etree.SubElement(misc_infos, key)
            info.text = self.pairs[key]
        return misc_infos

    def fromElement(self, element):
        pairs = {}
        for elt in element.getchildren():
            key = elt.tag
            value = elt.text
            if value is not None:
                pairs[key] = value
        self.setPairs(pairs)

    def setPairs(self, value):
        assert value is None or isinstance(value, dict)
        self.__pairs = value

    pairs = property(
        lambda self: self.__pairs,
        setPairs,
        None,
        'Description...')


class Reference(BaseElement):
    def __init__(self, name=None, type=None):
        self.setName(name)
        self.setType(type)

    def getElement(self):
        reference = etree.Element('reference')
        if self.type is not None:
            reference.set('type', self.type)
        reference.text = self.name

        return reference

    def fromElement(self, element):
        attType = element.get('type')
        if attType is not None:
            self.type = attType
        if element.text is not None:
            self.name = element.text

    def setName(self, name):
        self.__name = name

    def setType(self, type):
        self.__type = type

    def toDict(self):
        return {
            'name': self.name,
            'type': self.type,
        }

    name = property(lambda self: self.__name, setName, None, 'Description...')
    type = property(lambda self: self.__type, setType, None, 'Description...')


class ProcessingMessage(Message):
    def __init__(
            self,
            level=None,
            processing=None,
            chain_context=None,
            template=None,
            *args,
            **kargs):
        Message.__init__(self, level=level, *args, **kargs)

        self.setProcessing(processing)
        self.setChainContext(chain_context)

        self.setTemplate(template)

    def getElement(self):
        message = Message.getElement(self)
        message.tag = 'processingMessage'
        message.append(self.processing.getElement())
        if self.chain_context is not None:
            message.append(self.chain_context.getElement())
        return message

    def fromElement(self, element, validator=None):
        Message.fromElement(self, element, validator)

        processingElt = element.find('processing')
        if processingElt is not None:
            self.processing.fromElement(processingElt)

        chainContextElt = element.find('chain_context')
        if chainContextElt is not None:
            self.chain_context = ChainContext()
            self.chain_context.fromElement(chainContextElt)

    def setProcessing(self, value):
        assert value is None or isinstance(value, Processing)
        if value is not None:
            self.blocList['processing'] = value
        else:
            self.blocList['processing'] = Processing()

    def setChainContext(self, value):
        assert value is None or isinstance(value, ChainContext)
        if value is not None:
            self.blocList['chain_context'] = value
        else:
            self.blocList['chain_context'] = None

    def setTemplate(self, template):
        assert template is None or isinstance(template, Message)
        if template is not None:
            Message.setTemplate(self, template)
            if isinstance(template, ProcessingMessage):
                self.processing.setTemplate(template.processing)
                if template.chain_context is not None:
                    if self.chain_context is None:
                        self.chain_context = ChainContext()
                    self.chain_context.setTemplate(template.chain_context)

    def toDict(self):
        res = Message.toDict(self)
        if 'processing' in self.blocList:
            res['processing_process_id'] = self.blocList['processing'].process_id
            res['processing_start_date'] = self.blocList['processing'].start_date
            res['processing_stop_date'] = self.blocList['processing'].stop_date
            res['processing_exit_code'] = self.blocList['processing'].exit_code
            res['processing_hostname'] = self.blocList['processing'].hostname
            res['processing_username'] = self.blocList['processing'].username
            res['processing_cmd_line'] = self.blocList['processing'].cmd_line
            res['processing_pid'] = self.blocList['processing'].pid
            res['processing_logfile'] = self.blocList['processing'].logfile
            res['processing_logfile_misc_infos'] = self.blocList['processing'].logfile.misc_infos
        if 'chain_context' in self.blocList:
            res['chain_context'] = self.blocList['chain_context'].toDict()
        return res

    processing = property(
        lambda self: self.blocList['processing'],
        setProcessing,
        None)
    chain_context = property(
        lambda self: self.blocList['chain_context'],
        setChainContext,
        None)


class DownloadMessage(Message):
    def __init__(
            self,
            level=None,
            processing=None,
            download=None,
            template=None,
            *args,
            **kargs):
        Message.__init__(self, level=level, *args, **kargs)

        self.setProcessing(processing)
        self.setDownload(download)
        self.setTemplate(template)

    def getElement(self):
        message = Message.getElement(self)
        message.tag = 'downloadMessage'
        if self.processing is not None:
            message.append(self.processing.getElement())

        message.append(self.download.getElement())
        return message

    def fromElement(self, element, validator=None):
        Message.fromElement(self, element, validator)

        processingElt = element.find('processing')
        if processingElt is not None:
            self.processing.fromElement(processingElt)

        downloadElt = element.find('download')
        if downloadElt is not None:
            self.download.fromElement(downloadElt)

    def setTemplate(self, template):
        assert template is None or isinstance(template, Message)
        if template is not None:
            Message.setTemplate(self, template)
            if isinstance(template, DownloadMessage):
                self.processing.setTemplate(template.processing)
                if template.download is not None:
                    self.download.setTemplate(template.download)

    def setProcessing(self, value):
        assert value is None or isinstance(value, Processing)
        if value is not None:
            self.blocList['processing'] = value
        else:
            self.blocList['processing'] = Processing()

    def setDownload(self, value):
        assert value is None or isinstance(value, Download)
        if value is not None:
            self.blocList['download'] = value
        else:
            self.blocList['download'] = Download()

    def toDict(self):
        res = Message.toDict(self)
        res['download_name'] = self.download.name
        res['download_provider'] = self.download.provider
        return res

    processing = property(
        lambda self: self.blocList['processing'],
        setProcessing)
    download = property(lambda self: self.blocList['download'], setDownload)


class OperatorLogbookMessage(Message):
    def __init__(self, level=None, *args, **kargs):
        pass


class ControlMessage(Message):
    def __init__(self, level=None, *args, **kargs):
        pass
