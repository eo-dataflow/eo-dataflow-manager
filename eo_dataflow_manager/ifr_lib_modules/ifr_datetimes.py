#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_datetimes
    ~~~~~~~~~~~~~~~~~~~~~

    Provides some utilities for managing date and time.
"""
import datetime
import sys
import time
from calendar import monthrange

import dateparser

from eo_dataflow_manager.ifr_lib_modules import ifr_numbers
from eo_dataflow_manager.ifr_lib_modules.ifr_exception import BadParameterException, ConversionException

if sys.version_info < (3, 2):
    DEFAULT_ISO_FORMAT = '%Y-%m-%dT%H:%M:%S'
else:
    DEFAULT_ISO_FORMAT = '%Y-%m-%dT%H:%M:%S%z'


def now():
    """Retrieves the current datetime.

    Returns:
        datetime.datetime: The current datetime object
    """
    return datetime.datetime.now()


def elapsed_time(date1, date2):
    """Calculates the elapsed time between the two dates

    Args:
        date1 (datetime.datetime): a date
        date2 (datetime.datetime): a date

    Returns:
        datetime.timedelta: The elapsed time between date2 and date1

    Examples:
            >>> date1 = str_to_datetime('2018-02-01T14:28:42+0200')
            >>> date2 = str_to_datetime('2018-08-31T07:48:12+0200')
            >>> print(elapsed_time(date1, date2))
            210 days, 17:19:30
    """
    return date2 - date1


def datetime_to_str(date=now(), pattern='%d/%m/%Y'):
    """Converts a datetime to a str.

    Args:
        date (datetime.datetime): The datetime object to convert. Defaults to now()
        pattern: The string representation pattern. Defaults to '%d/%m/%Y'.

    Returns:
        str: The datetime object converted to string

    Raises:
        ConversionException: if conversion failed (bad pattern, ...)

    Examples:
            >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
            >>> print(datetime_to_str(date))
            01/02/2018
    """
    try:
        return date.strftime(pattern)
    except ValueError as err:
        raise ConversionException('Cannot convert {} to string with the format {}. '
                                  'The following error occurred : {}'
                                  .format(date, pattern, err))


def datetime_to_iso(date=now()):
    """Retrieves a date formatted to ISO format

    Args:
        date (datetime.datetime): The datetime object to convert. Defaults to now()

    Returns:
        str: The datetime object converted to string

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(datetime_to_iso(date))
        2018-02-01T14:28:42+02:00
    """
    return date.isoformat()


def parse_date(date_string, date_formats=None, languages=None, locales=None, region=None, settings=None):
    """Parse date and time from given date string.

    @see dateparser.parse for more information
    """
    def format_list_opt(opt):
        return opt if opt is None or isinstance(opt, list) else [opt]
    try:
        res = dateparser.parse(
            date_string,
            date_formats=format_list_opt(date_formats),
            languages=format_list_opt(languages),
            locales=format_list_opt(locales),
            region=region,
            settings=settings)
        if res is not None:
            return res

        raise ConversionException(
            'Cannot convert {} to datetime'.format(date_string))
    except ValueError as err:
        raise ConversionException(
            'Cannot convert {} to datetime. The following error occurs : {}'.format(date_string, err))


def str_to_datetime(value, pattern=DEFAULT_ISO_FORMAT):
    """Converts a string to a datetime from a pattern.

    Args:
        value (str): A string representing the date to convert
        pattern (str): The format of the date to convert. Defaults to '%Y-%m-%dT%H:%M:%S%z' or '%Y-%m-%dT%H:%M:%S',
        depending on python version.
        See https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior

    Returns:
        datetime.datetime: The converted datetime

    Raises:
        ConversionException: if conversion failed (bad pattern, ...)

    Examples:
            >>> print(str_to_datetime('20180201','%Y%m%d'))
            2018-02-01 00:00:00

            >>> print(str_to_datetime('2018-02-01T14:28:42+0200'))
            2018-02-01 14:28:42+02:00
    """
    try:
        return datetime.datetime.strptime(value, pattern)
    except ValueError as err:
        raise ConversionException('Cannot convert {} to datetime with the format {}. '
                                  'The following error occurs : {}'
                                  .format(value, pattern, err))


def day(date=now()):
    """Retrieves the number of day in the month of a date.

    Args:
        date (datetime.date): a date object. Defaults to now()

    Returns:
        str: the number of the day in the month (2 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(day(date))
        01
    """
    return date.strftime('%d')


def previous_day(date=now()):
    """Retrieves the day before a date.

    Args:
        date (datetime.datetime): a datetime object. Defaults to now()

    Returns:
        datetime.datetime: the day before a date

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(previous_day(date))
        2018-01-31 14:28:42+02:00
    """
    return date - datetime.timedelta(days=1)


def day_in_year(date=now()):
    """Retrieves the number of the day in the year of a date.

    Args:
        date (datetime.date): a datetime object. Defaults to now()

    Returns:
        str: the number of the day in the year (3 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(day_in_year(date))
        032
    """
    return date.strftime('%j')


def month(date=now()):
    """Retrieves the number of the month in the year of a date.

    Args:
        date (datetime.date): a datetime object. Defaults to now()

    Returns:
        str: the number of the month in the year (2 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(month(date))
        02
    """
    return date.strftime('%m')


def previous_month(date=now()):
    """Retrieves the previous month of a date.

    Args:
        date (datetime.date): a date object. Defaults to now()

    Returns:
        str: the number of the month in the year (2 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(previous_month(date))
        01
    """
    return str(date.month-1 if date.month > 1 else 12).zfill(2)


def year_month(date=now(), separator=''):
    """Retrieves a date formatted to '<year>[separator]<month>'

    Args:
        date (datetime.date): a date object. Defaults to now()
        separator (str) : a separator between year and month. Defaults to ''

    Returns:
        str: the current month with the pattern '<year>[separator]<month>'

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(year_month(date))
        201802
    """
    return '{}{}{}'.format(year(date), separator, month(date))


def previous_year_month(date=now(), separator=''):
    """Retrieves the previous month of a date formatted to '<year>[separator]<month>'.

    Args:
        date (datetime.date): a date object. Defaults to now()
        separator (str) : a separator between year and month. Defaults to ''

    Returns:
        str: the previous month with the pattern '<year>[separator]<month>'

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(previous_year_month(date))
        201801
    """
    return year_month(date.replace(day=1) - datetime.timedelta(days=1), separator)


def month_range(date=now()):
    """Retrieves the number of the day in the year of a date.

    Args:
        date (datetime.date): a date object. Defaults to now()

    Returns:
        datetime: the first day of the month
        datetime: the last day of the month

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(month_range(date))
        (datetime.date(2018, 2, 1), datetime.date(2018, 2, 28))
    """
    year_int = ifr_numbers.to_int(date.strftime('%Y'))
    month_int = ifr_numbers.to_int(date.strftime('%m'))
    ndays = monthrange(year_int, month_int)[1]
    return (
        datetime.datetime(year_int, month_int, 1),
        datetime.datetime(year_int, month_int, ndays)
    )


def year(date=now()):
    """Retrieves the year of a date.

    Args:
        date (datetime.date): a date object. Defaults to now()

    Returns:
        str: the year (4 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(year(date))
        2018
    """
    return date.strftime('%Y')


def short_year(date=now()):
    """Retrieves the year of a date.

    Args:
        date (datetime.date): a date object. Defaults to now()

    Returns:
        str: the year (2 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(short_year(date))
        18
    """
    return date.strftime('%y')


def previous_year(date=now()):
    """Retrieves the previous year of a date.

    Args:
        date (datetime.date): a date object. Defaults to now()

    Returns:
        str: the previous year (4 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(previous_year(date))
        2017
    """
    return str(date.year - 1).zfill(4)


def add_years(date=now(), years=1):
    """Adds years to a date

    Args:
        date (datetime.date): a date object. Defaults to now()
        years (int): numbers of years to add

    Returns:
        date: the new date

    Examples:
        >>> date = str_to_datetime('2020-02-29T14:28:42+0200')
        >>> print(add_years(date, 1))
        2021-02-28 14:28:42+02:00
    """
    try:
        return date.replace(year=date.year + years)
    except ValueError:
        return date.replace(year=date.year + years, day=date.day - 1)


def sub_years(date=now(), years=1):
    """Subtracts years to a date

    Args:
        date (datetime.date): a date object. Defaults to now()
        years (int): numbers of years to subtract

    Returns:
        date: the new date

    Examples:
        >>> date = str_to_datetime('2020-02-29T14:28:42+0200')
        >>> print(sub_years(date, 1))
        2019-02-28 14:28:42+02:00
    """
    return add_years(date, -years)


def days_between(start_date, end_date):
    """Retrieve list of days in a period

    Args:
        start_date (datetime.datetime): start date
        end_date (datetime.datetime): end date

    Returns:
        List[datetime.datetime]: list oy days
    """
    assert isinstance(start_date, datetime.date)
    assert isinstance(end_date, datetime.date)
    assert start_date <= end_date

    days = list()
    days.append(start_date)
    for i in range(1, (end_date - start_date).days + 1):
        days.append(start_date + datetime.timedelta(days=i))
    return days


def is_time_older_or_younger_than(ref_time: time.time, delta_value: int = 1, delta_unit: str = 'minutes') -> bool:
    """Compare if a time is older or younger than X units

    Args:
        ref_time (time.time): time to be compared
        delta_value (int): positive or negative value
        delta_unit (str): unit in seconds, minutes, hours, days

    Returns:
        bool: True if condition is validated
    """
    conversion_dict = {'seconds': 1, 'minutes': 60, 'hours': 3600, 'days': 86400}

    # check delta unit
    if delta_unit not in conversion_dict:
        raise BadParameterException(f'"{delta_unit}" is not a valid time unit. '
                                    f'Time units list : {", ".join(conversion_dict.keys())}')

    # build time_value
    time_value = (time.time() - ref_time) / conversion_dict[delta_unit]

    # manage positive or negative delta value
    if delta_value < 0:
        return abs(delta_value) > time_value
    return time_value >= delta_value
