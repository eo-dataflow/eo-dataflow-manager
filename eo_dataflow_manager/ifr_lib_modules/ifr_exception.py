#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_exception
    ~~~~~~~~~~~~~~~~~~~~~

    Provides generic exception classes.

    Examples :
        >>> raise GenericException('my message')
"""


class GenericException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


class FileNotFoundException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class ItemNotFoundException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class BadTypeException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class BadValueException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class MissingParameterException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class BadParameterException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class SyntaxException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class PatternException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class ConversionException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class ConfigurationException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)
