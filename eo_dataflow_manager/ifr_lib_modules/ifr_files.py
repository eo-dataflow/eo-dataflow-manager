#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_files
    ~~~~~~~~~~~~~~~~~

    Provides some utilities for managing files and directories
"""

import gzip
import hashlib
import os
import re
import sys
import time
from collections import OrderedDict
from datetime import datetime

from eo_dataflow_manager.ifr_lib_modules import ifr_os
from eo_dataflow_manager.ifr_lib_modules.ifr_exception import (
    ConversionException,
    FileNotFoundException,
    ItemNotFoundException,
)


def script_info(script_file):
    """Retrieves executed script information

    Returns:
        dict: script information
    """
    return OrderedDict([
        ('full_path', os.path.abspath(script_file)),
        ('dir_name', parent_basename(script_file)),
        ('name', strip_extension(os.path.basename(script_file))),
        ('mtime', mtime(script_file))
    ])


def parent_dir(file_path):
    """Retrieves the absolute path to parent directory of a file.

    Args:
        file_path (str): path to file.

    Returns:
        str: The directory of the file

    Examples:
        >>> print(dirname(iifr_files.py.py))
        ifr_lb
    """
    return os.path.dirname(os.path.abspath(file_path))


def parent_basename(file_path):
    """Retrieves the parent directory of a file. Resolves

    Args:
        file_path (str): path to file.

    Returns:
        str: The directory of the file

    Examples:
        >>> print(dirname(iifr_files.py.py))
        ifr_lb
    """
    return os.path.basename(parent_dir(file_path))


def strip_extension(file_path):
    """Removes the extension from a file path

    Args:
        file_path (str): path to file.

    Returns:
        str: The file path without its extension

    Examples:
        >>> print(basename_splitext("ifr_lb/file.py"))
        file
    """
    return os.path.splitext(file_path)[0]


def basename_splitext_files(files):
    """return a list of filename without extension

    Args:
        files (list): a list o files

    Returns:
        list: normalized file list

    Examples:
        >>> print(basename_splitext_files(["ifr_lb/file.py", "ifr_lb/file2.py"]))
        [file, file2]
    """
    return [strip_extension(os.path.basename(f)) for f in files]


def mtime(file_path):
    """Retrieves the last modification datetime of a file

    Args:
        file_path (str): path to file. Defaults to sys.argv[0] (current called python script)

    Returns:
        time.struct_time: The last modification datetime of the file

    Examples:
        >>> print(mtime())
        time.struct_time(
            tm_year=2018, tm_mon=11, tm_mday=8, tm_hour=7,
            tm_min=49, tm_sec=48, tm_wday=3, tm_yday=312, tm_isdst=0
        )
    """
    return time.gmtime(os.stat(file_path).st_mtime)


def mtime_str(file_path, pattern='%Y-%m-%d %H:%M:%S%z'):
    """Retrieves the last modification datetime of a file.

    Args:
        file_path (str): path to file.
        pattern (str): The format of the date to convert. Defaults to '%Y-%m-%dT%H:%M:%S%z'

    Returns:
        str: The last modification datetime of the file, formatted according to pattern

    Examples:
        >>> print(mtime())
        2018-11-08 07:49:48+0100
    """
    return time.strftime(pattern, mtime(file_path))


def is_mtime_older_than(file_path, delta_value=10, delta_unit='minutes'):
    """Checks if file mtime is older than a delta time.

    Args:
        file_path (str): path to file.
        delta_value (int):
        delta_unit (str): unit amongst [seconds, minutes, hours, days]

    Returns:
        bool: True if file mtime is older than a delta time, otherwise False

    Raises:
        ConversionException : invalid delta unit

    Examples:
        >>> print(is_mtime_older_than())
        True
    """
    conversion_dict = {'seconds': 1, 'minutes': 60, 'hours': 3600, 'days': 86400}
    try:
        factor = conversion_dict[delta_unit]
        return ((time.time() - os.path.getmtime(file_path)) / factor) >= delta_value
    except KeyError:
        raise ConversionException('"{}" is not a valid time unit. Time units list : {}'
                                  .format(delta_unit, ', '.join(conversion_dict.keys())))


def is_file_empty(file_path):
    """Checks if file is empty (size == 0).

    Args:
        file_path (str): path to file.

    Returns:
        bool: True if file is empty, otherwise False

    Examples:
        >>> print(is_file_empty())
        True
    """
    return os.stat(file_path).st_size == 0


def zip_file(src_path, dest_path=None, delete_source=False, zip_file_extension='gz'):
    """Zips a file with or without deleting the _source.

    Args:
        src_path (str): The _source file path.
        dest_path (str): The destination file path. Same as the _source path + the passed extension by default.
        delete_source (bool): Indicates whether the _source file should be deleted, False by default.
        zip_file_extension (str): The zip file extension, 'gz' by default
    """

    # define and create destination path
    if dest_path is None:
        dest_path = src_path
    else:
        parent_dir_path = os.path.dirname(os.path.abspath(dest_path))
        if not os.path.isdir(parent_dir_path):
            ifr_os.mkdirs(parent_dir_path)

    # zip file
    with open(src_path, 'rb') as src, gzip.open(dest_path + '.' + zip_file_extension, 'wb') as zipped_file:
        zipped_file.writelines(src)

    # delete _source
    if delete_source:
        os.remove(src_path)


def __find_in_syspath(pathname, match_fct=os.path.isfile):
    """Finds an item in syspath

    Args:
        pathname (str): path of the item
        match_fct (fct): Match function. Defaults to 'os.path.isfile'

    Returns:
        str: absolute path of the item

    Raises:
        ItemNotFoundException: if the item is not found
    """
    for dirname in sys.path:
        candidate = os.path.join(dirname, pathname)
        if match_fct(candidate):
            return candidate
    raise ItemNotFoundException("Can't find pathname %s" % pathname)


def find_file_in_syspath(pathname):
    """Finds a file in syspath

    Args:
        pathname (str): path of the file

    Returns:
        str: absolute path of the file

    Raises:
        ItemNotFoundException: if the file is not found
    """
    return __find_in_syspath(pathname)


def find_dir_in_syspath(pathname):
    """Finds a directory in syspath

    Args:
        pathname (str): path of the directory

    Returns:
        str: absolute path of the directory

    Raises:
        ItemNotFoundException: if the directory is not found
    """
    return __find_in_syspath(pathname, os.path.isdir)


def get_hash(file_path):
    """Get the file hash

    Args:
        file_path (str): path to file.

    Returns:
        str: The file hash, a string of hexadecimal digits

    """
    if not os.path.isfile(file_path):
        raise FileNotFoundException('File does not exist : {}'.format(file_path))
    sha256_hash = hashlib.sha256()
    with open(file_path, 'rb') as file:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: file.read(4096), b''):
            sha256_hash.update(byte_block)

    return sha256_hash.hexdigest()


def to_unix_path(path):
    """Converts path to unix path (slash instead of backslash)

    Args:
        path: path to convert

    Returns:
        str: path converted

    """
    # espace multiple backslash
    escape_many = re.compile(r'(?P<slash>\\[\\]+)')
    for elt in escape_many.findall(path):
        path = path.replace(elt, '?' * len(elt))

    # replace single backslash
    path = path.replace('\\', '/')

    # unescaped backslash
    return path.replace('?', '\\')


def touch(path, new_mtime=None):
    """Create or modify update time of a file

    Args:
        path (str): path to file
        new_mtime (datetime): new modification time
    """
    assert new_mtime is None or isinstance(new_mtime, datetime)

    times = None
    if new_mtime is not None:
        utime = time.mktime(new_mtime.timetuple())

        if os.path.exists(path):
            st = os.stat(path)
            times = (st.st_atime, utime)
        else:
            times = (utime, utime)

    if os.path.isdir(path):
        os.utime(path, times=times)
    elif os.path.isfile(path):
        with open(path, 'a'):
            os.utime(path, times=times)
