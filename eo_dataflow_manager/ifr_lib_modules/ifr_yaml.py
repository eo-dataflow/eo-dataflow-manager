#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_yaml
    ~~~~~~~~~~~~~~~~

    Provides a class YamlConfig, to manage configuration from YAML syntax.
"""

import functools
import logging
import os
from abc import ABCMeta
from collections import OrderedDict
from typing import Any, Iterable, Type

import ruamel.yaml as yaml
import six

from eo_dataflow_manager.ifr_lib_modules import ifr_collections, ifr_tokens
from eo_dataflow_manager.ifr_lib_modules.ifr_exception import (
    BadParameterException,
    BadTypeException,
    ConfigurationException,
    FileNotFoundException,
    ItemNotFoundException,
    SyntaxException,
)

"""
ALL_PYYAML_LOADERS = (yaml.Loader, yaml.SafeLoader, yaml.RoundTripDumper)
ALL_PYYAML_DUMPERS = (yaml.Dumper, yaml.SafeDumper, yaml.RoundTripDumper)
"""
ALL_PYYAML_LOADERS = (yaml.Loader, yaml.SafeLoader)
ALL_PYYAML_DUMPERS = (yaml.Dumper, yaml.SafeDumper)


def merge(content1, content2):
    config = YamlConfig(content1)
    if content2 is not None:
        return config.merge(YamlConfig(content2))
    return config


class YamlConfig(object):
    """Loads a yaml content from a file or a stream.

    Can retrieve all leaf keys.
    Can access key in different ways.

    Can replace tokens (default ${key}) from a key / value pairs of items.
    See args : replacements, token_prefix, token_suffix, warn_if_orphaned_keys

    Can merge with another YamlConfig object.

    Can export content to dict, ini format.

    Attributes:
        content: YAML content as OrderedDict
        leaf_keys: list of all leaf keys
        orphan_keys: dictionary of orphaned keys (key/counter)

    Examples:

        - load YAML content :

            >>> yaml_content=\"""\\
                # example
                name:
                  # details
                  family: Smith   # very common
                  given: Alice    # one of the siblings
                \"""
            >>> config = YamlConfig(stream=yaml_content)

        - print content :

            >>> print(config)
            name:
              family: Smith
              given: Alice

        - export content :

            >>> print(dict_to_str(config.to_dict()))
            - name.family: Smith
            - name.given: Alice
            >>> print(config.to_ini())
            [name]
            family = Smith
            given = Alice

        - retrieve property via array syntax :

            >>> print(config['name'])
            {'family': 'Smith', 'given': 'Alice'}
            >>> print(config['name.family'])
            Smith
            >>> print(config['name']['family'])
            Smith

        - retrieve property via box :

            >>> print(config.box.name)
            {'family': 'Smith', 'given': 'Alice'}
            >>> print(config.box.name.family)
            Smith

        - replacements :

            >>> yaml_content2=\"""\\
                my_project:
                  id: 3
                  group: ${name.family}
                  other: ${fake.key.one}
                \"""
            >>> config2 = YamlConfig(stream=yaml_content2, replacements=config.to_dict())
            >>> print(config2)
            my_project:
              group: Smith
              id: 3
              other: ${fake.key.one}
            >>> print(config2.orphan_keys)
            Counter({'fake.key.one': 1})

        - merge :

            >>> yaml_content3=\"""\\
                my_project:
                  other: test
                \"""
            >>> config3 = YamlConfig(stream=yaml_content3, replacements=config.to_dict())
            >>> config2.merge(config3)
            >>> print(config2)
            my_project:
              group: Smith
              id: 3
              other: test
    """

    def __init__(self,
                 content=None,
                 file=None,
                 replacements=None,
                 token_prefix='${',
                 token_suffix='}',
                 envvar_separator='env:',
                 warn_if_orphaned_keys=False,
                 encoding=None):
        """Initializes attributes and load yaml content from file or from stream.

        Also :
        - replaces tokens if requested
        - retrieves orphaned keys
        - transforms yaml content to box
        - load all leaf keys

        Args:
            content (str | stream | binary | dict, optional) : YAML content
            file (str, optional) : YAML file
            replacements (dict, optional): key / value pairs of items to replace
            token_prefix (str, optional): token suffix. Defaults to '${')
            token_suffix (str, optional): token prefix. Defaults to '}'
            envvar_separator (str, optional): the environment variable token marker. Defaults to 'env:'
            warn_if_orphaned_keys (bool) : issue a warning if the yaml stream contains orphaned keys

        Raises:
            FileNotFoundException: YAML file is not found
            SyntaxException: YAML content is invalid
        """
        # init logger
        self._log = logging.getLogger(__name__)

        # init attributes
        self.content = None
        self.orphan_keys = None
        self.leaf_keys = None
        self.encoding = 'utf-8' if encoding is None else encoding

        # retrieve options
        self.replacements = replacements
        self.token_prefix = token_prefix
        self.token_suffix = token_suffix
        self.envvar_separator = envvar_separator
        self.warn_if_orphaned_keys = warn_if_orphaned_keys

        # load yaml content
        if content is not None:
            self.__load_yaml(content, None)
        elif file is not None:
            if not os.path.isfile(file):
                raise FileNotFoundException("Yaml file doesn't exists : {}".format(file))
            self.__load_yaml(None, file)

        # load orphaned keys
        self.__update_orphan_keys()

        # load all keys
        self.__load_leaf_keys()

    @staticmethod
    def dump(data, dumper=yaml.RoundTripDumper):
        """Serialize a Python object into a YAML stream.
        If stream is None, return the produced string instead.
        """
        return yaml.safe_dump(data, Dumper=dumper)

    def __load_yaml(self, content, file_path):
        """Loads a YAML document.

        Replace tokens if requested.

        Raises:
            SyntaxException: YAML content is invalid
        """
        try:

            if file_path is not None:
                with open(file_path, 'rt', encoding=self.encoding) as f:
                    content = yaml.load(f, Loader=yaml.RoundTripLoader)
            elif not isinstance(content, dict):
                # Parse yaml content and make it a dict
                content = yaml.load(content, Loader=yaml.RoundTripLoader)

            if content:
                content = ifr_tokens.replace_envvar_tokens(
                    content,
                    self.token_prefix,
                    self.token_suffix,
                    self.envvar_separator
                )

                if self.replacements:
                    content = ifr_tokens.replace_tokens(
                        content,
                        self.replacements,
                        self.token_prefix,
                        self.token_suffix
                    )

            self.content = content

            if hasattr(content, 'close'):
                content.close()

        except Exception as err:
            if hasattr(content, 'close'):
                content.close()

            raise SyntaxException('Yaml syntax problem (ScannerError) {}'.format(str(err)))

    def __update_orphan_keys(self):
        """Retrieves orphaned keys.

        Issues a warning if requested.
        """
        self.orphan_keys = None
        self.orphan_keys = ifr_collections.counter(ifr_tokens.get_tokens(
            yaml.dump(self.content, Dumper=yaml.RoundTripDumper),
            self.token_prefix,
            self.token_suffix)
        )
        if self.warn_if_orphaned_keys and self.orphan_keys:
            self._log.warning('Orphan tokens found : {}'.format(len(self.orphan_keys)))
            self._log.warning(ifr_collections.dict_to_str(self.orphan_keys, title='ORPHANED KEYS'))

    def __load_leaf_keys(self):
        """Retrieves all leaf keys from Box object."""
        def iterate_dict(d, parents=None):
            if parents is None:
                parents = []
            result = []
            if d is not None and hasattr(d, 'items'):
                for k, v in d.items():
                    if isinstance(v, dict):
                        result.extend(iterate_dict(v, parents + [k]))
                    else:
                        if parents:
                            result.append('.'.join(parents) + '.' + k)
                        else:
                            result.append(k)

            return result
        self.leaf_keys = None
        self.leaf_keys = iterate_dict(self.content)

    def merge(self, obj):
        """Merges the content with that of another YamlConfig instance.

        Also : updates leaf keys, orphaned keys.

        Args:
            obj (YamlConfig|dict): a YamlConfig instance

        Raises:
            BadTypeException: yaml_config_instance is not an instance of YamlConfig

        """
        # skip None value
        if obj is None:
            return

        if not isinstance(obj, (YamlConfig, dict)) :
            raise BadTypeException('Object to merge is not an instance of YamlConfig or dict : {}', type(obj))

        if isinstance(obj, YamlConfig):
            dict_to_merge = obj.content
        else:
            dict_to_merge = obj

        if self.content is None:
            self.content = dict_to_merge
        else:
            self.content = ifr_collections.merge_dict(self.content, dict_to_merge)

        # load orphaned keys
        self.__update_orphan_keys()

        # load all keys
        self.__load_leaf_keys()

    def is_leaf(self, item):
        return item in self.leaf_keys

    def contains(self, item):
        """Checks if key exists.

        Args:
            item (str): a key

        Returns:
            bool: True if key exists, otherwise False

        """
        # try to retrieve item
        try:
            self[item]
            return True
        except ItemNotFoundException:
            return False

    def __getitem__(self, item):
        """Retrieves item by key using array convention.

        Args:
            item (str): a key

        Returns:
            bool: True if key exists, otherwise False

        Raises:
            ItemNotFoundException: key does not exist
        """
        assert isinstance(item, six.string_types), 'key must be a string'

        if self.content is None:
            raise ConfigurationException('Config is empty !')

        try:
            if '.' not in item:
                return self.content[item]
            return functools.reduce(
                (lambda level, k: level.get(k)), item.split('.'), self.content
            )
        except (KeyError, AttributeError):
            raise ItemNotFoundException('Item {} does not exist.'.format(item))

    def __setitem__(self, key, value):
        self.merge(ifr_collections.to_dict(key, value))

    def __contains__(self, item):
        return self.contains(item)

    def __str__(self):
        """Prints YAML document.

        Returns:
            str: YAML content
        """
        return yaml.dump(self.content, Dumper=yaml.RoundTripDumper)

    def keys(self):
        """Retrieves first level keys

        Returns:
            list: first level keys
        """
        return self.content.keys()

    def leafs(self, start_with=None):
        """Exports YAML leaf nodes to dictionary (leaf key / value).

        Returns:
            dict: YAML document as dictionary
        """
        return OrderedDict((k, self[k]) for k in self.leaf_keys if (start_with is None or k.startswith(start_with)))

    def as_dict(self):
        """Retrieves configuration as dictionnary."""
        return self.content

    def __get_item(self, item=None, check_is_not_leaf=False):
        """Retrieves all or a fragment of the configuration.

        Args:
            item (str, optional): item to export. Defaults to None (=all)
            check_is_not_leaf (bool): should we raise an exception if item is a leaf. Defaults to False

        Returns:
            any: item value

        Raises:
            BadParameterException: if item is a leaf and check_is_not_leaf is True
        """
        if item:
            if check_is_not_leaf and self.is_leaf(item):
                raise BadParameterException('Item {} is a leaf item'.format(item))
            return self[item]
        return self.content

    def to_yaml(self, item=None):
        """Exports configuration or a fragment to YAML format.

        Args:
            item (str, optional): item to export. Defaults to None (=all)

        Returns:
            str: a configuration to YAML format

        Example:
            >>> yaml_content=\"""\\
            name:
              family: Smith
              given: Alice
            test:
              sub:
                a: b
                c: d
            \"""
            >>> config = YamlConfig(stream=yaml_content)
            >>> print(config.to_yaml("test"))
        """
        return yaml.dump(self.__get_item(item), Dumper=yaml.RoundTripDumper)

    def to_json(self, item=None):
        """Exports configuration or a fragment to JSON format.

        Args:
            item (str, optional): item to export. Defaults to None (=all)

        Returns:
            str: a configuration to JSON format

        Example:
            >>> yaml_content=\"""\\
            name:
              family: Smith
              given: Alice
            test:
              sub:
                a: b
                c: d
            \"""
            >>> config = YamlConfig(stream=yaml_content)
            >>> print(config.to_json("test"))
            {"name": {"family": "Smith", "given": "Alice"}, "test": {"sub": {"a": "b", "c": "d"}}}
        """
        from eo_dataflow_manager.ifr_lib_modules.ifr_json import json_dumps
        return json_dumps(self.__get_item(item))

    def to_ini(self, item=None):
        """Exports configuration or a fragment to INI format.

        Args:
            item (str, optional): item to export. Defaults to None (=all)

        Returns:
            str: a configuration to INI format

        Example:
            >>> yaml_content=\"""\\
            name:
              family: Smith
              given: Alice
            test:
              sub:
                a: b
                c: d
            \"""
            >>> config = YamlConfig(stream=yaml_content)
            >>> print(config.to_ini("test"))
            [sub]
            a = b
            c = d
            >>> print(config.to_ini("test.sub"))
            ...
            eo_dataflow_manager.ifr_lib_modules.exception.ItemNotFoundException: Item test.sub is a leaf item
        """
        return ifr_collections.dict_to_ini(self.__get_item(item))


class IfrYamlCodec(six.with_metaclass(ABCMeta, object)):
    _yaml_prefix = 'ifr_yaml'
    _elligible_classes = []

    @classmethod
    def types_to_yaml_tags(cls):
        """return a dict<Class, classname>"""
        return {c: c.__name__ for c in cls._elligible_classes}

    @classmethod
    def yaml_tags_to_types(cls):
        """return a dict<classname, Class>"""
        return {c.__name__: c for c in cls._elligible_classes}

    @classmethod
    def get_codec_yaml_prefix(cls):
        """return codec prefix"""
        return f'!{cls._yaml_prefix}/'

    @classmethod
    def get_known_types(cls) -> Iterable[Type[Any]]:
        """return the list of types that we know how to encode"""
        return cls.types_to_yaml_tags().keys()

    @classmethod
    def is_yaml_tag_supported(cls, yaml_tag_suffix: str) -> bool:
        """return True if the given yaml tag suffix is supported"""
        return yaml_tag_suffix in cls.yaml_tags_to_types().keys()

    @classmethod
    def from_yaml_dict(cls, yaml_tag_suffix: str, dct, **kwargs):
        """Create an object corresponding to the given tag, from the decoded dict"""
        typ = cls.yaml_tags_to_types()[yaml_tag_suffix]
        return typ(**dct)

    @classmethod
    def to_yaml_dict(cls, obj) -> Any:
        """Encode the given object and also return the tag that it should have"""
        return vars(obj)

    @classmethod
    def get_obj_yaml_prefix(cls, obj):
        return cls.types_to_yaml_tags()[type(obj)]

    @classmethod
    def get_yaml_tag(cls, obj):
        yaml_tag_suffix = cls.get_codec_yaml_prefix()
        if len(yaml_tag_suffix) == 0 or yaml_tag_suffix[-1] != '/':
            yaml_tag_suffix = yaml_tag_suffix + '/'

        return yaml_tag_suffix + cls.get_obj_yaml_prefix(obj)

    @classmethod
    def encode(cls, dumper: yaml.Dumper, obj: Any) -> Any:
        """Encode object instances

        Args:
            - dumper: Dumper instance
            - obj: any object to encode

        Returns:

        """
        # case of List instance
        if isinstance(obj, list):
            return dumper.represent_sequence(cls.get_yaml_tag(obj), obj, flow_style=None)

        # other cases
        return dumper.represent_mapping(cls.get_yaml_tag(obj), cls.to_yaml_dict(obj), flow_style=None)

    @classmethod
    def decode(cls, loader, yaml_tag_suffix,  node,  **kwargs):
        if not cls.is_yaml_tag_supported(yaml_tag_suffix):
            return None

        # case of List object
        if issubclass(cls.yaml_tags_to_types()[yaml_tag_suffix], list):
            typ = cls.yaml_tags_to_types()[yaml_tag_suffix]
            return typ(*loader.construct_sequence(node, deep=True))

        # other cases
        loader.flatten_mapping(node)
        pairs = loader.construct_pairs(node, deep=True)  # 'deep' allows the construction to be complete (inner seq...)
        constructor_args = OrderedDict(pairs)
        return cls.from_yaml_dict(yaml_tag_suffix, constructor_args, **kwargs)

    @classmethod
    def register(cls, loaders=ALL_PYYAML_LOADERS, dumpers=ALL_PYYAML_DUMPERS) -> None:
        """
        Registers this codec with PyYaml, on the provided loaders and dumpers (default: all PyYaml loaders and dumpers).
         - The encoding part is registered for the object types listed in cls.get_known_types(), in order
         - The decoding part is registered for the yaml prefix in cls.get_yaml_prefix()

        :param loaders: the PyYaml loaders to register this codec with. By default all pyyaml loaders are considered
            (Loader, SafeLoader...)
        :param dumpers: the PyYaml dumpers to register this codec with. By default all pyyaml loaders are considered
            (Dumper, SafeDumper...)
        :return:
        """
        for loader in loaders:
            loader.add_multi_constructor(cls.get_codec_yaml_prefix(), cls.decode)

        for dumper in dumpers:
            for t in cls.get_known_types():
                dumper.add_multi_representer(t, cls.encode)


class YamlSerializable(object):
    """Represents an abstract yaml serializable object."""
    def to_yaml(self, **options):
        return yaml.safe_dump(self, **options)

    @classmethod
    def from_yaml(cls, value):
        return yaml.load(value, Loader=yaml.Loader)
