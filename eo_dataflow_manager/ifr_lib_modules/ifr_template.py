#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_template
    ~~~~~~~~~~~~~~~~~~~~

    Provides a class Template, to wrap Jinja2 Template class and add some features.
"""
import logging
from email.mime.text import MIMEText

from jinja2 import Environment
from jinja2 import Template as Jinja2Template
from jinja2.meta import find_undeclared_variables


class Template:
    """Wraps Jinja2 Template class.

    Add a feature to check expected and provided variables.

    Add shortcuts to generate mail MIME text.
    """

    def __init__(self, template_file):
        """Template class constructor.

        Args:
            template_file (str): The path to the Jinja2 template file.
        """
        self._log = logging.getLogger(__name__)
        self._template_file = template_file

    def list_template_variables(self):
        """Lists the template file variables.

        Returns:
            list: The template's variables list
        """
        env = Environment()
        with open(self._template_file) as f:
            stream = f.read()
        return list(find_undeclared_variables(env.parse(stream)))

    def render(self, replacements=None, check_variables=True):
        """Generates an output from a replacements dictionary.

        Use Jinja2 Template class.

        Warns in case of unexpected and/or missing variables.

        Args:
            replacements (dict<str, obj>, optional): The replacements dictionary
            check_variables (bool): indicate whether variables must be checked. Defaults to 'True'

        Returns:
            str: The output report
        """

        # check expected and input variables if requested
        if check_variables:
            template_var = frozenset(self.list_template_variables())
            input_var = frozenset(replacements.keys())

            unexpected_variables = input_var - template_var
            missing_variables = template_var - input_var

            if unexpected_variables:
                self._log.warning(
                    "The following variables are not expected by the template '{}' : {}"
                    .format(self._template_file, ','.join(unexpected_variables))
                )
            if missing_variables:
                self._log.warning(
                    "The following variables expected by the template '{}' are  missing : {}"
                    .format(self._template_file, ','.join(missing_variables))
                )

        # load jinja2 template from file content
        with open(self._template_file) as template_content:
            jinja_template = Jinja2Template(template_content.read())

        # generate report from template
        return jinja_template.render(replacements)

    def to_mimetext(self, replacements, output='plain', charset='utf-8'):
        """Generates a MIME text object.

        Warns in case of unexpected and/or missing variables.

        Args:
            replacements (dict<str, obj>, optional): The replacements dictionary
            output (str): The output type (ex : plain, html...). Defaults to 'plain'
            charset (str): The character set to use. Defaults to 'utf-8'

        Returns:
            MIMEText: a MIME text
        """
        return MIMEText(self.render(replacements), output, _charset=charset)

    def to_plain_mimetext(self, replacements, charset='utf-8'):
        """Generates a plain MIME text.

        Args:
            replacements (dict<str, obj>, optional): The replacements dictionary
            charset (str): The character set to use. Defaults to 'utf-8'

        Returns:
            MIMEText: a plain MIME text
        """
        return self.to_mimetext(replacements, 'plain', charset)

    def to_html_mimetext(self, replacements, charset='utf-8'):
        """Generates an HTML MIME text.

        Args:
            replacements (dict<str, obj>, optional): The replacements dictionary
            charset (str): The character set to use. Defaults to 'utf-8'

        Returns:
            MIMEText: an HTML MIME text
        """
        return self.to_mimetext(replacements, 'html', charset)
