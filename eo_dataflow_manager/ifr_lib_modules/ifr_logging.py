#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_logging
    ~~~~~~~~~~~~~~~~~~~~

    Provides utilities for managing logging.
"""

try:
    pass
except ImportError:
    pass

import logging
import logging.config as logging_conf
import os
import re
import sys
import time
from logging.handlers import TimedRotatingFileHandler

import six
from colorlog import LevelFormatter
from colorlog.escape_codes import escape_codes, parse_colors

from eo_dataflow_manager.ifr_lib_modules import ifr_collections, ifr_misc
from eo_dataflow_manager.ifr_lib_modules.ifr_datetimes import str_to_datetime
from eo_dataflow_manager.ifr_lib_modules.ifr_exception import ConfigurationException
from eo_dataflow_manager.ifr_lib_modules.ifr_files import zip_file
from eo_dataflow_manager.ifr_lib_modules.ifr_yaml import YamlConfig


def level_code(level):

    if isinstance(level, six.string_types):
        name_to_level = {
            'CRITICAL': logging.CRITICAL,
            'FATAL': logging.FATAL,
            'ERROR': logging.ERROR,
            'WARN': logging.WARNING,
            'WARNING': logging.WARNING,
            'INFO': logging.INFO,
            'DEBUG': logging.DEBUG,
            'NOTSET': logging.NOTSET
        }

        if level not in name_to_level:
            return logging.NOTSET
        return name_to_level[level]

    if isinstance(level, int):
        level_int = [0, 10, 20, 30, 40, 50]
        if level not in level_int:
            return logging.NOTSET
        return level

    return logging.NOTSET


def level_name(level):
    """Retrieves log level name from str or numerical value.

    This function exists because ``logging.getLevelName()`` returns a numerical value in some cases.

    Args:
        level (str or int): log level

    Returns:
        str: log level name

    Examples:
        >>> print(level_name("WARN"))
        WARNING
        >>> print(level_name("WARNING"))
        WARNING
        >>> print(level_name(logging.WARNING))
        WARNING
        >>> print(level_name(30))
        WARNING
    """
    from eo_dataflow_manager.ifr_lib_modules.ifr_numbers import is_number

    if isinstance(level, six.string_types):
        level = level.upper()

    name = logging.getLevelName(level)
    if is_number(name):
        return logging.getLevelName(name)
    return name


class InfoOperatorFilter(logging.Filter):
    """ Filter the INFO level events.
    Only events which messages containing OPERATOR tags will be propagated.
    """
    def filter(self, record):
        if record.levelname != 'INFO':
            return True
        # For level INFO, keep only messages containing OPERATOR tag.
        if 'OPERATOR' in record.msg:
            return True
        else:
            return False


class IfrFileHandler(TimedRotatingFileHandler):
    """Handler for logging to a file, rotating the log file at certain timed
    intervals and compress the logs if requested.
    When parameter fixed to MIDNIGHT by default and backupCount to 10.
    Inherits from TimedRotatingFileHandler. One additional argument : compress, True by default.

    Example of configuration :
      info_file_handler:
        '()': 'eo_dataflow_manager.ifr_lib_modules.ifr_logging.IfrFileHandler'
        level: DEBUG
        formatter: colored
        filename: ${LOG_DIR}/info.log
        encoding: utf8
        when: D
        backupCount: 20
        compress: True
        atTime: '15:30:45'
    """
    COMPRESSED_FILE_EXTENSION = 'gz'

    def __init__(self, filename, when='MIDNIGHT', interval=1, backupCount=10, encoding=None, delay=False, utc=False,
                 atTime=None, compress=True):

        super(IfrFileHandler, self).__init__(filename=filename, when=when, interval=interval, backupCount=backupCount,
                                             encoding=encoding, delay=delay, utc=utc)

        self.compress = compress

        # in python < 3.4, atTime does not exist in TimedRotatingFileHandler
        if atTime:
            atTime = str_to_datetime(atTime, '%H:%M:%S').time()
        self.atTime = atTime

        # retrieve compression option and recompile extMatch with zip extension if needed (used by getFilesToDelete)
        # if compress is True, modify extension pattern (used to delete old files)
        # see TimedRotatingFileHandler.__init__()
        if self.compress:
            pattern = self.extMatch.pattern
            self.extMatch = re.compile(pattern[:-1] + r'\.' + self.COMPRESSED_FILE_EXTENSION + pattern[-1:])

    def doRollover(self):
        if self.stream:
            self.stream.close()
            self.stream = None
        # get the time that this sequence started at and make it a TimeTuple
        current_time = int(time.time())
        dst_now = time.localtime(current_time)[-1]
        t = self.rolloverAt - self.interval
        if self.utc:
            time_tuple = time.gmtime(t)
        else:
            time_tuple = time.localtime(t)
            dst_then = time_tuple[-1]
            if dst_now != dst_then:
                if dst_now:
                    addend = 3600
                else:
                    addend = -3600
                time_tuple = time.localtime(t + addend)
        dfn = self.baseFilename + '.' + time.strftime(self.suffix, time_tuple)
        if os.path.exists(dfn):
            os.remove(dfn)
        # Issue 18940: A file may not have been created if delay is True.
        if os.path.exists(self.baseFilename):
            os.rename(self.baseFilename, dfn)
            if self.compress:
                zip_file(dfn, delete_source=True, zip_file_extension=self.COMPRESSED_FILE_EXTENSION)
        if self.backupCount > 0:
            for s in self.getFilesToDelete():
                os.remove(s)
        if not self.delay:
            self.stream = self._open()
        new_rollover_at = self.computeRollover(current_time)
        while new_rollover_at <= current_time:
            new_rollover_at = new_rollover_at + self.interval
        # If DST changes and midnight or weekly rollover, adjust for this.
        if (self.when == 'MIDNIGHT' or self.when.startswith('W')) and not self.utc:
            dst_at_rollover = time.localtime(new_rollover_at)[-1]
            if dst_now != dst_at_rollover:
                if not dst_now:  # DST kicks in before next rollover, so we need to deduct an hour
                    addend = -3600
                else:  # DST bows out before next rollover, so we need to add an hour
                    addend = 3600
                new_rollover_at += addend
        self.rolloverAt = new_rollover_at


class IfrColorFormatter(LevelFormatter):

    __default_log_colors = {
        'DEBUG':    'cyan',
        'INFO':     'green',
        'WARNING':  'yellow',
        'ERROR':    'red',
        'CRITICAL': 'bold_red'
    }
    _default_secondary_colors = {
        'message': {
            'WARNING': 'yellow',
            'ERROR':    'red',
            'CRITICAL': 'bold_red'
        }
    }

    _default_msg_format = ('%(purple)s%(asctime)s%(reset)s | '
                           '%(log_color)s%(levelname).3s%(reset)s | '
                           '%(message_log_color)s%(message)s%(reset)s')

    _default_date_format = '%d/%m/%y %H:%M:%S'
    _default_multine_color = 'bold_white'

    def __init__(self,
                 fmt=None,
                 datefmt=None,
                 style='%',
                 log_colors=None,
                 reset=True,
                 secondary_log_colors=None,
                 multilines_color=None
                 ):
        if fmt is None:
            fmt = self._default_msg_format

        if datefmt is None:
            datefmt = self._default_date_format

        if log_colors is None:
            _log_colors = self.__default_log_colors
        else:
            _log_colors = ifr_collections.merge_dict(self.__default_log_colors, log_colors)

        if secondary_log_colors is None:
            _secondary_colors = self._default_secondary_colors
        else:
            _secondary_colors = ifr_collections.merge_dict(self._default_secondary_colors, secondary_log_colors)

        super(IfrColorFormatter, self).__init__(fmt, datefmt, style, _log_colors, reset, _secondary_colors)

        if multilines_color is None:
            self.multilines_color = self._default_multine_color
        else:
            self.multilines_color = multilines_color

    def format(self, record):
        message = super(IfrColorFormatter, self).format(record)

        _msg_fragments = message.split('\n')
        if len(_msg_fragments) <= 1:
            return message

        multiline_msg = list()
        multiline_msg.append(_msg_fragments.pop(0))
        multiline_msg_color = parse_colors(self.multilines_color) + '\n'.join(_msg_fragments)
        if self.reset and not multiline_msg_color.endswith(escape_codes['reset']):
            multiline_msg_color += escape_codes['reset']
        multiline_msg.append(multiline_msg_color)
        return '\n'.join(multiline_msg)


class IfrNoColorFormatter(logging.Formatter):

    _default_msg_format = '%(asctime)s | %(levelname).3s | %(message)s'
    _default_date_format = '%d/%m/%y %H:%M:%S'

    def __init__(self, fmt=None, datefmt=None, style='%'):
        if fmt is None:
            fmt = self._default_msg_format

        if datefmt is None:
            datefmt = self._default_date_format

        if sys.version_info > (3, 2):
            super(IfrNoColorFormatter, self).__init__(fmt, datefmt, style)
        elif sys.version_info > (2, 7):
            super(IfrNoColorFormatter, self).__init__(fmt, datefmt)
        else:
            logging.Formatter.__init__(self, fmt, datefmt)

    def format(self, record):
        message = super(IfrNoColorFormatter, self).format(record)
        return ifr_misc.strip_color(message)


class ColorLogger(logging.Logger):
    """Adds features to standard logger
    - add 'color' option to log functions, to format_message message
    - add 'repeat' option to log functions, to format_message repeat a term to a defined message length
    - print dictionary

    Examples:
        # initialize Logger
        >>> logging.setLoggerClass(ColorLogger)
        >>> log = logging.getLogger(__name__)
        >>> log.message_length = 150

        # standard message
        >>> log.info('my message')

        # standard message with color
        >>> log.info('my message', color='thin_blue')

        # repeat message with log.message_length
        >>> log.info('=', color='thin_blue', repeat=True)

        # log each items of dictionary
        >>> log.log_dict(logging.INFO, {'key1':{'key2': 'value'}}, title='Configuration :', header='-', footer='-', color='thin_white')
    """

    def __init__(self, name, level=logging.NOTSET):
        self.message_length = None
        super(ColorLogger, self).__init__(name, level)

    def __repeat_msg(self, msg, repeat=False):
        """Repeats a term to message length if repeat is True

        Args:
            msg (str): message to log
            repeat (bool): repeat the message if True. Defaults to False

        Returns:
            str: formatted message
        """
        if repeat and self.message_length is not None and msg is not None:
            return msg * self.message_length
        return msg

    def format_message(self, msg, color=None, repeat=False):
        """Formats a message

        Args:
            msg (str): message to log
            color (str, optional): color name
            repeat (bool): repeat the message if True. Defaults to False

        Returns:
            str: formatted message
        """
        return ifr_misc.ljust(ifr_misc.colorize(self.__repeat_msg(msg, repeat), color), self.message_length)

    def _log(self, level, msg, args, exc_info=None, extra=None, stack_info=False, color=None, repeat=False):
        """Provides a formatted message to logging.Logger._log()

        Args:
            msg (str): message to log
            color (str, optional): color name
            repeat (bool): repeat the message if True. Defaults to False

        Returns:
            str: formatted message
        """
        if sys.version_info < (3, 2):
            super(ColorLogger, self)._log(
                level,
                self.format_message(msg, color, repeat),
                args,
                exc_info=exc_info,
                extra=extra
            )
        else:
            super(ColorLogger, self)._log(
                level,
                self.format_message(msg, color, repeat),
                args,
                exc_info=exc_info,
                extra=extra,
                stack_info=stack_info
            )

    def log_dict(self, level, dictionary, exc_info=None, extra=None, stack_info=False,
                 title=None, header=None, footer=None, title_footer=None, item_sorted=False, color=None, *args):

        """Logs each items of a dictionary with possible decorators.

        Args:
            level(str): logging level
            dictionary(dict): the dictionary to log
            title (str, optional): a title to display before the list of elements
            header (str, optional): a separator before the title.
            footer (str, optional): a separator before the title.
            title_footer (str, optional): a separator after the title.
            item_sorted (bool, optional): if true, sort the items by the key, otherwise keep the default sort
            color (str, optional): color options (ex: 'blue', 'bold_blue,bg_white', ...)
        """
        if not self.isEnabledFor(level):
            return

        if dictionary is None or (isinstance(dictionary, dict) and len(dictionary) == 0):
            return

        # retrieve each lines formatted
        lines = ifr_collections.prettify_dict(
            dictionary=dictionary,
            title=title,
            header=self.__repeat_msg(header, True),
            footer=self.__repeat_msg(footer, True),
            title_footer=self.__repeat_msg(title_footer, True),
            item_sorted=item_sorted,
            color_options=color
        )

        # log each lines
        for line in lines:
            # adjust message length if we use ansi colors
            if color is not None and self.message_length is not None and len(line) <= self.message_length:
                line = line.ljust(self.message_length + (len(line) - ifr_misc.ansilen(line)))

            # log line
            self._log(level, line, args=args, exc_info=exc_info, extra=extra, stack_info=stack_info)


def setup_logging(config=None, root_level=logging.INFO):
    """Configures python logging framework.

    Use default logging configuration and override it if config is set.

    Root logging level is set with root_level

    Args:
        config (dict | str | stream | binary, optional): logging configuration dictionary or YAML content
        root_level (str | int): logging level. Defaults to INFO

    Raises:
        FileNotFoundException: logging configuration file is not found
        SyntaxException: logging configuration is not a valid yaml file
        ConfigurationException: logging configuration content is not a valid logging configuration

    Examples:
        >>> print(setup_logging())
        # will configure logging with the default configuration
        # root level will be set to INFO
        >>> print(setup_logging(config={'root': {'handlers': ['stdout', 'stderr']}}, root_level='DEBUG')
        # will override default configuration with content of config
        # root level will be set to DEBUG
    """

    default_logging_config = """\
        version: 1
        disable_existing_loggers: False
        root:
          handlers: [console]
        handlers:
          console:
            class: logging.StreamHandler
            level: DEBUG
            formatter: console_fmt
            stream: ext://sys.stdout
        formatters:
          console_fmt:
            '()': "eo_dataflow_manager.ifr_lib_modules.ifr_logging.IfrColorFormatter"
            datefmt: '%d/%m/%Y %H:%M:%S'
        """

    logging_config = YamlConfig(content=default_logging_config)

    # merge with config
    if config is not None:
        logging_config.merge(YamlConfig(content=config))

    # configure logging
    try:
        logging_conf.dictConfig(logging_config.content)
    except ValueError as err:
        raise ConfigurationException('Error in logging configuration : {}'.format(str(err)))

    # define root logger level
    if root_level is not None:
        logging.getLogger().setLevel(level_name(root_level))

    # If the is no log level defined, set the info log level
    if logging.getLogger().getEffectiveLevel() == logging.NOTSET:
        logging.getLogger().setLevel(logging.INFO)

def basic_config(root_level=logging.INFO):
    setup_logging(
        config="""
        version: 1
        disable_existing_loggers: False
        root:
          handlers: [console]
        handlers:
          console:
            class: logging.StreamHandler
            level: DEBUG
            formatter: console_fmt
            stream: ext://sys.stdout
        formatters:
          console_fmt:
            '()': "eo_dataflow_manager.ifr_lib_modules.ifr_logging.IfrColorFormatter"
            datefmt: '%d/%m/%Y %H:%M:%S'
        """,
        root_level=root_level
    )
