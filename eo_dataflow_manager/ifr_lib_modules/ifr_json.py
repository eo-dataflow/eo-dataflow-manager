#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_json
    ~~~~~~~~~~~~~~~~

    Provides some utilities to manage json
"""

import datetime
import json
import re
from collections import OrderedDict

import six

from eo_dataflow_manager.ifr_lib_modules import ifr_datetimes
from eo_dataflow_manager.ifr_lib_modules.ifr_exception import ConversionException


class JsonSerializable(object):
    """Represents an abstract json serializable object."""

    def to_json(self, **options):
        """Exports attributes to JSON object
        Returns:
            str: The object's json formatted representation
        """
        return json_dumps(self.__dict__, **options)

    @classmethod
    def from_json(cls, value):
        objects = json_loads(value)
        return cls(**objects)


def json_dumps(dictionary, **options):
    only_public_members = options.pop('only_public_members', True)

    def json_encoder(o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()
        if isinstance(o, datetime.timedelta):
            return o.seconds

        if only_public_members is True:
            return {k: v for k, v in o.__dict__.items() if not k.startswith('_')}

        return o.__dict__

    return json.dumps(
        dictionary,
        default=json_encoder,
        ensure_ascii=False,
        **options
    )


def json_loads(value, **options):
    def json_decoder(pairs):
        d = OrderedDict()
        for k, v in pairs:
            d[k] = v
            if isinstance(v, six.string_types):
                try:
                    if re.match(r'\d{4}-\d{2}-\d{2}', v):
                        d[k] = ifr_datetimes.parse_date(v)
                except ConversionException:
                    continue
        return d

    return json.loads(value, object_pairs_hook=json_decoder, **options)


def json_load(fp, **options):
    return json_loads(fp.read(), **options)
