#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_thead
    ~~~~~~~~~~~~~~~~~

    Provides utilities for managing threads.

"""
import ctypes
import inspect
import threading


def call_timeout(fct, timeout=0.5):
    """Checks if a function is executed in the deadline.

    Args:
        fct (function): fonction to be called in a thread
        timeout (float): delay to wait for a response from te function

    Returns:
        bool: True if the timeout is not exceeded. Otherwise False

    """
    t = Thread(target=fct)
    t.start()
    t.join(timeout)
    try:
        t.terminate()
    except threading.ThreadError:
        return True
    except Exception:
        return False
    else:
        return False


def _async_raise(tid, exctype):
    """raises the exception, performs cleanup if needed"""
    if not inspect.isclass(exctype):
        raise TypeError('Only types can be raised (not instances)')
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, ctypes.py_object(exctype))
    if res == 0:
        raise ValueError('invalid thread id')
    elif res != 1:
        # """if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"""
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, 0)
        raise SystemError('PyThreadState_SetAsyncExc failed')


class Thread(threading.Thread):
    """Killable Thread"""

    def _get_my_tid(self):
        """determines this (self's) thread id"""
        if not self.is_alive():
            raise threading.ThreadError('the thread is not active')

        # do we have it cached?
        if hasattr(self, '_thread_id'):
            return self._thread_id  # pylint: disable=E0203

        # no, look for it in the _active dict
        for tid, tobj in threading._active.items():
            if tobj is self:
                self._thread_id = tid
                return tid

        raise AssertionError("could not determine the thread's id")

    def raise_exc(self, exctype):
        """raises the given exception type in the context of this thread"""
        _async_raise(self._get_my_tid(), exctype)

    def terminate(self):
        """raises SystemExit in the context of the given thread, which should
        cause the thread to exit silently (unless caught)"""
        self.raise_exc(SystemExit)
