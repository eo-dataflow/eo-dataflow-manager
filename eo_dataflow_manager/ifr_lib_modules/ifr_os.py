#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_os
    ~~~~~~~~~~~~~~

    Provides some utilities to retrieves os information
"""
import os
import sys
from collections import OrderedDict
from getpass import getuser
from multiprocessing import cpu_count
from platform import system
from socket import gethostname

import six

from eo_dataflow_manager.ifr_lib_modules.ifr_exception import ItemNotFoundException
from eo_dataflow_manager.ifr_lib_modules.ifr_thread import call_timeout

try:
    from pwd import getpwnam
except ImportError:
    getpwnam = None

try:
    from grp import getgrgid, getgrnam
except ImportError:
    getgrnam = None
    getgrgid = None


def execution_info():
    """Retrieves execution information

    Returns:
        dict: execution information
    """
    info = OrderedDict([
        ('command', ' '.join(sys.argv)),
        ('pid', os.getpid()),
        ('hostname', gethostname()),
        ('system', system()),
        ('nb_proc', cpu_count()),
        ('user', getuser())
    ])
    info.update(user_info())
    return info


def user_info(user_name=None):
    """Retrieves user information

    Args:
        user_name (str): user name. If empty retrieve current user

    Returns:
        dict: user information
    """
    if not user_name:
        user_name = getuser()

    if getpwnam is None:
        return {'user': user_name}

    try:
        pwd = getpwnam(user_name)

        return OrderedDict([
            ('user', user_name),
            ('user_uid', pwd.pw_uid),
            ('user_gid', pwd.pw_gid),
            ('user_gname', group_name(pwd.pw_gid))
        ])
    except KeyError:
        raise ItemNotFoundException('User not found : {}'.format(user_name))


def group_id(group_name):
    """Retrieves unix group id

    Args:
        group_name (str): unix group name.

    Returns:
        int: unix group id
    """
    if getgrnam is None or group_name is None or not isinstance(group_name, six.string_types):
        return None

    try:
        result = getgrnam(group_name)
    except KeyError:
        result = None

    if result is not None:
        return result[2]
    return None


def group_name(group_id):
    """Retrieves unix group id

    Args:
        group_id (int): unix group name.

    Returns:
        int: unix group id
    """
    if getgrgid is None or group_id is None or not isinstance(group_id, int):
        return None

    try:
        result = getgrgid(group_id)
    except KeyError:
        result = None

    if result is not None:
        return result[0]
    return None


def user_id(user_name):
    """Retrieves unix user id

    Args:
        user_name (str): unix user name.

    Returns:
        int: unix group id
    """
    if getpwnam is None or user_name is None:
        return None

    try:
        result = getpwnam(user_name)
    except KeyError:
        result = None

    if result is not None:
        return result[2]
    return None


if os.name == 'nt':
    def chown(path, owner=None, group=None):
        pass
else:
    def chown(path, owner=None, group=None):
        """Changes file/directory owner/group

        Args:
             path (str): path to directory
             owner (str|int, optional): owner id or name. Defaults to None
             group (str|int, optional): group id or name. Defaults to None

        Returns:
            bool: True if file is empty, otherwise False

        Examples:
            >>> print(chown("/tmp/mydir/mysubdir/my_file.ext", group="toto"))
        """

        if not owner and not group:
            return

        _owner = owner
        _group = group

        # -1 means don't change it
        if owner is None:
            _owner = -1
        # user can either be an int (the uid) or a string (the system username)
        elif isinstance(owner, six.string_types):
            _owner = user_id(owner)
            if _owner is None:
                raise ItemNotFoundException('No such user: {!r}'.format(owner))

        # -1 means don't change it
        if group is None:
            _group = -1
        elif not isinstance(group, int):
            _group = group_id(group)
            if _group is None:
                raise ItemNotFoundException('No such group: {!r}'.format(group))

        os.chown(path, _owner, _group)


def mkdirs(dirpath, mode=0o777, exist_ok=True, group=None, owner=None):
    """Creates tree directories

    If group or owner is set, changes owner/group.

    Args:
         dirpath (str): path to directory
         mode (octal): directories permissions
         exist_ok (bool) : If the target directory already exists, raise an OSError if exist_ok is False.
           Otherwise no exception is raised
         group (str, optional): group name. Defaults to None
         owner (str, optional): owner name. Defaults to None

    Returns:
        bool: True if file is empty, otherwise False

    Raises:


    Examples:
        >>> print(mkdirs("/tmp/mydir/mysubdir"))
        True
    """

    # retrieve head and tail from path
    head, tail = os.path.split(dirpath)

    # if dir path ends with "/", split return an empty tail
    # we retry with head if tail is empty
    if not tail:
        head, tail = os.path.split(head)

    # if parent does not exists and is different from "/"
    if head and tail and not os.path.exists(head):
        try:
            mkdirs(head, mode, group, owner, exist_ok)
        except OSError:
            # Defeats race condition when another thread created the path
            pass

        cdir = os.path.curdir
        if isinstance(tail, bytes):
            if sys.version_info <= (2, 7):
                cdir = bytes(os.path.curdir, 'ASCII')
            else:
                cdir = bytes(os.path.curdir)
        if tail == cdir:    # xxx/newdir/. exists if xxx/newdir exists
            return False

    # create current dir
    try:
        os.mkdir(dirpath)
    except OSError:
        # Cannot rely on checking for EEXIST, since the operating system
        # could give priority to other errors like EACCES or EROFS
        if not exist_ok or not os.path.isdir(dirpath):
            raise
        return False

    # change permissions
    chown(dirpath, owner=owner, group=group)

    return True


def rmdirs(dirpath, stop_dirname=None, stop_dirpath=None):
    """Same as os.removedirs, with a safety addition concerning root_dir removal

    Super-rmdir; remove a leaf directory and all empty intermediate
    ones.  Works like rmdir except that, if the leaf directory is
    successfully removed, directories corresponding to rightmost path
    segments will be pruned away until either the whole path is
    consumed or an error occurs.  Errors during this latter phase are
    ignored -- they generally mean that a directory was not empty.

    Args:
        dirpath (str): path to directory.
        stop_dirname (str, optional): name of directory where to stop
        stop_dirpath (str, optional): directory path where to stop

    Examples:
        >>> rmdirs('/export/home/mypath/subdir1/subdir2', stop_dirname='mypath')
        >>> rmdirs(
        >>>   '/export/home/mypath/subdir1/subdir2',
        >>>   stop_dirpath='/export/home/mypath'
        >>> )
        # will remove subdir2 and subdir1 if they are empty
    """

    # remove current dir
    os.rmdir(dirpath)

    # retrieve head and tail from path
    head, tail = os.path.split(dirpath)

    # if dir path ends with "/", split return an empty tail
    # we retry with head if tail is empty
    if not tail:
        head, tail = os.path.split(head)

    # if tail is empty, we are in "/" directory
    while head and tail:
        # conditions to stop browsing in parent tree
        if stop_dirname and tail == stop_dirname:
            break
        if stop_dirpath and head == stop_dirpath:
            break

        # try to remove dir
        try:
            os.rmdir(head)
        except OSError:
            pass

        # goes up the tree
        head, tail = os.path.split(head)


def rm_empty_dirs(dirpath, remove_root_dir=False):
    """Remove empty sub directories

    Args:
        dirpath (str): root path
        remove_root_dir (bool): remove also root_path

    Returns:
        int: number of directories deleted
    """
    assert isinstance(dirpath, str)
    assert isinstance(remove_root_dir, bool)

    nb_dir_removed = 0

    for root, dirnames, filenames in os.walk(dirpath, topdown=False):
        # non empty folder
        if filenames:
            continue

        # root folder and remove_root_dir==False
        if root == dirpath and remove_root_dir is False:
            continue

        # remove directory
        # raise OSError if directory is not empty
        try:
            os.rmdir(root)
            nb_dir_removed += 1
        except OSError:
            pass

    return nb_dir_removed


def is_io_locked(dirpath, timeout=0.5):
    """Check if the path is accessible

    Args:
        dirpath (str): path to directory.
        timeout (float): name of directory where to stop. Defaults to 0.5s

    Returns:
        bool: False if path is accessible, otherwise True

    Examples:
        >>> is_io_locked('/export/home')
        True
    """
    return not call_timeout(lambda: os.stat(dirpath), timeout)


def make_executable(filepath):
    """Chmod +x on file

    Args:
        filepath (str): path to file
    """
    mode = os.stat(filepath).st_mode
    mode |= (mode & 0o444) >> 2
    os.chmod(filepath, mode)
