#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_tokens
    ~~~~~~~~~~~~~~~~~~

    Provides token utilities.
"""
import os
import re
import string

import six

from eo_dataflow_manager.ifr_lib_modules import ifr_collections, ifr_datetimes
from eo_dataflow_manager.ifr_lib_modules.ifr_exception import ConfigurationException, PatternException

try:
    from collections.abc import Mapping
except:
    from collections import Mapping


def build_token(token_key, token_prefix='${', token_suffix='}'):
    """Builds a token key.

    Args:
        token_key (str): a token key.
        token_prefix (str, optional): token prefix in content. Defaults to '${'
        token_suffix (str, optional): token suffix in content. Defaults to '}'

    Returns:
        str: the built token

    Examples:
        >>> print(build_token('object.property'))
        ${object.property}
        >>> print(build_token('object.property', '{{', '}}'))
        {{object.property}}
    """
    token = token_key
    if token_prefix:
        token = token_prefix + token
    if token_suffix:
        token = token + token_suffix
    return token


def replace_tokens_in_string(
        content,
        replacements,
        token_prefix='${', token_suffix='}',
        replacements_is_map=False,
        escape_token=True):

    assert isinstance(content, six.string_types), 'Content should be a string.'
    assert isinstance(replacements, (dict, Mapping)), 'Replacements parameter should be a dictionary.'

    def replace(key, value):
        re_key = re.escape(key)

        if escape_token is True:
            re_prefix = re.escape(token_prefix)
            re_suffix = re.escape(token_suffix)
        else:
            re_prefix = token_prefix
            re_suffix = token_suffix

        re_replace = re.compile(re_prefix + r'\s*(' + re_key + r')\s*' + re_suffix)
        return re_replace.sub(value, content)

    # replacements should be a dict<str>
    replacements_as_map = replacements
    if not replacements_is_map:
        replacements_as_map = ifr_collections.dict_to_map(replacements)

    # skip if no replacements set
    if len(replacements_as_map) == 0:
        return content

    # retrieve tokens in content
    for token_key in get_tokens(content, token_prefix, token_suffix, escape_token=escape_token, remove_duplicates=True):
        if token_key in replacements_as_map:
            try:
                content = replace(token_key, str(replacements_as_map[token_key]))
            except Exception:
                continue

    return content


def replace_tokens(content, replacements, token_prefix='${', token_suffix='}', escape_token=True):
    """Replaces tokens in a stream.

    Args:
        content (str/dict): a str or dict to replace tokens
        replacements (dict): a key/value dictionary
        token_prefix (str, optional): token prefix in content. Defaults to '${'
        token_suffix (str, optional): token suffix in content. Defaults to '}'
        escape_token (bool): boolean indicating whether the token prefix and suffix should be escaped

    Returns:
        str: the input content with replaced tokens

    Raises:
        MissingParameterException: replacements must not be None
        BadTypeException: replacements must be a dictionary

    Examples:
        >>> dictionary = {'lorem.ipsum.key1': 'abcdef', 'lorem.ipsum.key2': 'ghijkl'}
        >>> print(replace_tokens(
        >>>   'Lorem ipsum dolor sit amet, ${lorem.ipsum.key1}, consectetur adipiscing elit.'
        >>>   '${ lorem.ipsum.key1 } ${lorem.ipsum.key2 }',
        >>>    dictionary
        >>> ))
        Lorem ipsum dolor sit amet, abcdef, consectetur adipiscing elit.abcdef ghijkl
    """

    # replacements should be a dict<str>
    replacements_as_map = ifr_collections.dict_to_map(replacements)

    if isinstance(content, six.string_types):
        return replace_tokens_in_string(
            content,
            replacements_as_map,
            token_prefix,
            token_suffix,
            escape_token=escape_token,
            replacements_is_map=True
        )

    if isinstance(content, dict):

        def replace_node(node):
            if isinstance(node, dict):
                for key, value in node.items():
                    # Recursively replace token in key
                    newKey = replace_node(key)
                    if newKey != key:
                        # Change key
                        node[newKey] = node.pop(key)
                        key = newKey

                    # Recursively replace token in value
                    node[key] = replace_node(value)
            elif isinstance(node, list):
                # Recursively replace token in each element
                for i in range(0, len(node)):
                    node[i] = replace_node(node[i])
            elif isinstance(node, six.string_types):
                node = replace_tokens_in_string(
                    node,
                    replacements_as_map,
                    token_prefix,
                    token_suffix,
                    escape_token=escape_token,
                    replacements_is_map=True
                )
            return node
        return replace_node(content)

    return content


def replace_envvar_tokens(content, token_prefix='${', token_suffix='}', envvar_separator='env:'):
    """Replaces environment variable tokens in a stream.
    Example: tmpdir: ${env:TMPDIR} >>> tmpdir: 'C:/users/<userlogin>'

    Args:
        content (dict): dict to replace tokens
        token_prefix (str, optional): token prefix in content. Defaults to '${'
        token_suffix (str, optional): token suffix in content. Defaults to '}'
        envvar_separator (str, optional): the environment variable token marker. Defaults to 'env:'

    Returns:
        str: the input content with replaced envvar tokens

    """
    return replace_tokens(
        content=content,
        replacements=os.environ,
        token_prefix=re.escape(token_prefix) + r'\s*' + re.escape(envvar_separator),
        token_suffix=re.escape(token_suffix),
        escape_token=False
    )


def get_tokens(content, token_prefix='${', token_suffix='}', remove_duplicates=False, escape_token=True):
    """Searches for unreplaced tokens in a content.

    Args:
        content (str): a string to find unreplaced tokens
        token_prefix (str, optional): token prefix in content. Defaults to '${'
        token_suffix (str, optional): token suffix in content. Defaults to '}'
        remove_duplicates (bool): boolean indicating whether the duplicates should be removed
        escape_token (bool): boolean indicating whether the token prefix and suffix should be escaped

    Returns:
        list: The list of orphaned tokens

    Examples:
        >>> print(get_tokens(
        >>>   'Lorem ipsum dolor sit amet, ${lorem.ipsum.key1}, consectetur adipiscing elit.' \
        >>>   '${lorem.ipsum.key1} ${lorem.ipsum.key2}'
        >>> ))
        ['lorem.ipsum.key1', 'lorem.ipsum.key1', 'lorem.ipsum.key2']
    """

    if isinstance(content, dict):
        from ruamel import yaml
        content_to_analyse = yaml.dump(content, Dumper=yaml.RoundTripDumper)
    elif isinstance(content, list):
        content_to_analyse = '\n'.join(content)
    else:
        content_to_analyse = content

    if escape_token is True:
        re_prefix = re.escape(token_prefix)
        re_suffix = re.escape(token_suffix)
    else:
        re_prefix = token_prefix
        re_suffix = token_suffix

    compiled_combined = re.compile(re_prefix + r'\s*(.*?)\s*' + re_suffix)
    if remove_duplicates:
        return set(compiled_combined.findall(content_to_analyse))
    return compiled_combined.findall(content_to_analyse)


def get_tokens_counter(content, token_prefix='${', token_suffix='}'):
    """Searches for unreplaced tokens in a content.

    Args:
        content (str): a string to find unreplaced tokens
        token_prefix (str, optional): token prefix in content. Defaults to '${'
        token_suffix (str, optional): token suffix in content. Defaults to '}'

    Returns:
        list: The list of orphaned tokens

    Examples:
        >>> print(get_tokens_counter(
        >>>   'Lorem ipsum dolor sit amet, ${lorem.ipsum.key1}, consectetur adipiscing elit.' \
        >>>   '${lorem.ipsum.key1} ${lorem.ipsum.key2}'
        >>> ))
        Counter({'lorem.ipsum.key1': 2, 'lorem.ipsum.key2': 1})
    """
    return ifr_collections.counter(get_tokens(content, token_prefix, token_suffix))


def str_format(value, **tokens):
    class SafeDict(dict):
        def __missing__(self, key):
            return '{' + key + '}'

    return string.Formatter().vformat(value, (), SafeDict(**tokens))


def date_to_replacements_dict(date):
    return {
        'short_year': ifr_datetimes.short_year(date),
        'year': ifr_datetimes.year(date),
        'month': ifr_datetimes.month(date),
        'day': ifr_datetimes.day(date),
        'day_in_year': ifr_datetimes.day_in_year(date),
        'date': ifr_datetimes.datetime_to_str(date, '%Y%m%d'),
        'datetime': ifr_datetimes.datetime_to_str(date, '%Y-%m-%dT%H:%M:%S'),
        'time': ifr_datetimes.datetime_to_str(date, '%H%M%S')
    }


def str_format_datetime(value, date):
    """Replaces date tokens in a string.

    Tokens available::
        {year}: 4 digits year
        {short_year}: 2 digits year
        {month}: 2 digits month
        {day}: 2 digits day number in the month
        {day_in_year}: 3 digits day number in the year

    Args:
        value (str): a string to replace date tokens
        date (datetime.datetime): a datetime object

    Returns:
        str: the input value with replaced tokens

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(str_format_datetime(
        >>>   'filenamesuffix_{year}{month}{day}_filenameprefix.extension',
        >>>   date
        >>> ))
        filenamesuffix_20180201_filenameprefix.extension
    """
    return str_format(value, **date_to_replacements_dict(date))


def get_datetime(value, regexp_pattern=r'([0-9]{8})', datetime_pattern='%Y%m%d', replace=None):
    """Extracts datetime from a string with a regular expression, using group named 'date'

    Args:
        value (str): string to extract date
        regexp_pattern (str): a regular expression to extract date.
            Must contains a group named 'date'.
            Defaults to r".*([0-9]{8}).*"
        datetime_pattern (str) : a datetime format pattern. Defaults to '%Y%m%d'

    Returns:
        datetime.datetime: the datetime object extracted from input value

    Raises:
        PatternException: if pattern not found in filename

    Examples:
        >>> print(get_datetime(
        >>>   'filenamesuffix_20180201_filenameprefix.extension',
        >>>   r".*([0-9]{8}).*"
        >>> ))
        2018-02-01 00:00:00
    """
    pattern = re.compile(regexp_pattern)
    if replace is None:
        res = pattern.findall(value)
        if not res:
            raise PatternException(f'Invalid pattern {regexp_pattern} for {value}.')
        date_str = res[0]
    else:
        if not pattern.match(value):
            raise PatternException(f'Invalid pattern {regexp_pattern} for {value}.')
        date_str = pattern.sub(replace, value)

    return ifr_datetimes.str_to_datetime(date_str, datetime_pattern)


def get_date(value, regexp_pattern=r'([0-9]{8})', datetime_pattern='%Y%m%d'):
    """Extracts date from a string with a regular expression, using group named 'date'

    Args:
        value (str): string to extract date
        regexp_pattern (str): a regular expression to extract date.
            Must contains a group named 'date'.
            Defaults to r".*([0-9]{8}).*"
        datetime_pattern (str) : a datetime format pattern. Defaults to '%Y%m%d'

    Returns:
        date: the date object extracted from input value

    Raises:
        PatternException: if pattern not found in filename

    Examples:
        >>> print(get_date(
        >>>   'filenamesuffix_20180201_filenameprefix.extension',
        >>>   r"(?:^|[^\d])(?P<date>\d{8})(?:$|[^\d])"
        >>> ))
        2018-02-01
    """
    return get_datetime(value, regexp_pattern, datetime_pattern).date()


def replace_date_tokens(content, token_prefix='${', token_suffix='}', date_separator='date:'):
    """Injects dynamic date tokens in a string using the date and/or format values passed in token.

    Args:
        content (str): the string to extract the date and format from
        token_prefix (str, optional): The token prefix in content. Defaults to '${'
        token_suffix (str, optional): The token prefix in content. Defaults to '}'
        date_separator (str, optional): The date separator. Defaults to 'date:'

    Returns:
        str: The input content with replaced tokens

    Examples:
        >>> print(replace_date_tokens(
        >>>   "${date: [value='4 days ago', format='%Y%m%d']}",
        >>>   token_prefix='${',
        >>>   token_suffix='}',
        >>>   date_separator='date:'))
        >>> ))
        20190711 if today is 2019/07/15

        >>> print(replace_date_tokens(
        >>>   "${date: [format='%Y/%j']}",
        >>>   token_prefix='${',
        >>>   token_suffix='}',
        >>>   date_separator='date:'))
        >>> ))
        2019/196 if today is 2019/07/15

        >>> print(replace_date_tokens(
        >>>   "${date: [value='4 days ago', format='%Y/%j']}",
        >>>   token_prefix='${',
        >>>   token_suffix='}',
        >>>   date_separator='date:'))
        >>> ))
        2019/192 if today is 2019/07/15

        >>> print(replace_date_tokens(
        >>>   "${date: []}",
        >>>   token_prefix='${',
        >>>   token_suffix='}',
        >>>   date_separator='date:'))
        >>> ))
        2019-07-15T14:51:35 if current datetime is 2019/07/15 14h51:35

    """

    # ------------------------------------------
    # 1. Retrieve date tokens
    # ------------------------------------------
    token_prefix = re.escape(token_prefix) + r'\s*' + re.escape(date_separator)
    token_suffix = re.escape(token_suffix)

    date_tokens = get_tokens(content, token_prefix, token_suffix, remove_duplicates=True, escape_token=False)

    # skip if not tokens found
    if not date_tokens:
        return content

    # ------------------------------------------
    # 2. build token replacements
    # ------------------------------------------
    replacements = dict()

    default_format = ifr_datetimes.DEFAULT_ISO_FORMAT
    default_date = ifr_datetimes.now()

    for date_token in date_tokens:

        # check date options syntax
        if not re.search(r"\[(?:,?\s*\w+\s*=\s*['\"]?\s*[^'\"\]]+\s*[\"']?\s*)*\]", date_token):
            raise ConfigurationException('Invalid date options syntax : {}'.format(date_token))

        # default value
        ref_datetime = default_date
        date_format = default_format

        # search options
        for match in re.findall(r"(?P<opt_key>\w+)\s*=\s*[\]'\"]?(?P<opt_value>[^\]'\",]*)[\]'\"]?", date_token):
            if match[0] not in ['format', 'value']:
                raise ConfigurationException("Unknown option '{}' for date token".format(match[0]))

            if not match[1]:
                raise ConfigurationException('Value unset for option {}'.format(match[0]))

            if match[0] == 'format':
                date_format = match[1]
            elif match[0] == 'value':
                ref_datetime = ifr_datetimes.parse_date(match[1])

        # generate date for token
        replacements[date_token] = ifr_datetimes.datetime_to_str(ref_datetime, date_format)

    # ------------------------------------------
    # 3. Replace tokens by date instance
    # ------------------------------------------
    return replace_tokens(
        content=content,
        replacements=replacements,
        token_prefix=token_prefix,
        token_suffix=token_suffix,
        escape_token=False
    )


def replace_regex_tokens(content, replacements, token_prefix='${', token_suffix='}', regexp_separator='regex:', silent=False):
    """Injects dynamic date tokens in a string using the date and/or format values passed in token.

    Args:
        content (str): the string to extract the date and format from
        replacements (dict): list of key/value
        token_prefix (str, optional): The token prefix in content. Defaults to '${'
        token_suffix (str, optional): The token prefix in content. Defaults to '}'
        regexp_separator (str, optional): The regex separator. Defaults to 'regex:'
        silent (bool): do not raise ConfigurationException if input field is unknown

    Returns:
        str: The input content with replaced tokens

    Examples:
        >>> print(replace_regex_tokens(
        >>>   "test_${regex: [input='input_basename', search='L2A_(.*)', replace='L2B_/1']}",
        >>>   replacements={"input_basename" : 'L2A_25km_20191015T162826_20191015T181403.nc'},
        >>>   token_prefix='${',
        >>>   token_suffix='}',
        >>>   regexp_separator='regex:'))
        >>> ))
    """

    # ------------------------------------------
    # 1. Retrieve date tokens
    # ------------------------------------------
    token_prefix = re.escape(token_prefix) + r'\s*' + re.escape(regexp_separator)
    token_suffix = re.escape(token_suffix)

    tokens = get_tokens(content, token_prefix, token_suffix, remove_duplicates=True, escape_token=False)

    # skip if not tokens found
    if not tokens:
        return content

    # ------------------------------------------
    # 2. build token replacements
    # ------------------------------------------
    replacements_final = dict()

    for token in tokens:

        # check date options syntax
        if not re.search(r"\[(?:,?\s*\w+\s*=\s*['\"]?\s*[^'\"\]]+\s*[\"']?\s*)*\]", token):
            raise ConfigurationException('Invalid regex options syntax : {}'.format(token))

        # default value
        input = None
        pattern = None
        replace = ''

        # search options
        for match in re.findall(r"(?P<opt_key>\w+)\s*=\s*[\]'\"]?(?P<opt_value>[^\]'\",]*)[\]'\"]?", token):
            if match[0] not in ['input', 'pattern', 'replace']:
                raise ConfigurationException("Unknown option '{}' for regex token".format(match[0]))

            if not match[1]:
                raise ConfigurationException('Value unset for option {}'.format(match[0]))

            if match[0] == 'input':
                input = match[1]
            elif match[0] == 'pattern':
                pattern = match[1]
            elif match[0] == 'replace':
                replace = match[1]

        if input is None:
            raise ConfigurationException("Missing option 'input'")

        if pattern is None:
            raise ConfigurationException("Missing option 'pattern'")

        if input not in replacements:
            if silent is True:
                continue
            else:
                raise ConfigurationException("Input field '{}' does not exists ! ".format(input))

        # generate date for token
        replacements_final[token] = re.sub(pattern, replace, replacements[input])

    # ------------------------------------------
    # 3. Replace tokens by date instance
    # ------------------------------------------
    return replace_tokens(
        content=content,
        replacements=replacements_final,
        token_prefix=token_prefix,
        token_suffix=token_suffix,
        escape_token=False
    )
