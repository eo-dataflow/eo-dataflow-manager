#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Ifremer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    eo_dataflow_manager.ifr_lib_modules.ifr_numbers
    ~~~~~~~~~~~~~~~~~~~~

    Provides some utilities for managing numbers.

"""


from eo_dataflow_manager.ifr_lib_modules.ifr_exception import ConversionException


def is_number(value):
    """Checks if the input value is a number.

    Args:
        value (str, number): a value

    Returns:
        bool: True if value is a number

    Examples:
        >>> print(is_number('026'))
        True
        >>> print(is_number('abc'))
        False
        >>> print(is_number(26))
        True
        >>> print(is_number('26.05'))
        True
    """
    try:
        float(value)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(value)
        return True
    except (TypeError, ValueError):
        pass

    return False


def to_int(value):
    """Converts a string to int.

    Args:
        value (str): a number as string

    Returns:
        int: a number as int

    Raises:
        ConversionException: if value is not a number

    Examples:
        >>> print(to_int("026"))
        26
        >>> print(to_int("026.05"))
        26
    """
    try:
        return int(to_float(value))
    except ValueError as err:
        raise ConversionException('Cannot convert {} to int. '
                                  'The following error occurs : {}'
                                  .format(value, err))


def to_float(value):
    """Converts a string to int.

    Args:
        value (str): a number as string

    Returns:
        int: a number as int

    Raises:
        ConversionException: if value is not a number

    Examples:
        >>> print(to_float("026"))
        26.0
        >>> print(to_float("026.05"))
        26.05
    """
    try:
        return float(value)
    except ValueError as err:
        raise ConversionException('Cannot convert {} to int. '
                                  'The following error occurs : {}'
                                  .format(value, err))
