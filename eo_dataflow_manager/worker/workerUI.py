#
# -*- coding: UTF-8 -*-
#

import logging
import os
import threading
from subprocess import PIPE, Popen, TimeoutExpired

LOGGER_DEFAULT_FILE_NAME = 'WorkerUI'
WORKERUI_LEVEL = logging.INFO

# Name of plugin celery worker configuration
CONFIG_WORKER = 'celeryconfig'


class WorkerUI(threading.Thread):

    def __init__(self, logger_name, global_config, verbose):

        threading.Thread.__init__(self, name='UI Worker')

        # default queue name
        self.__queueName = 'UI.' + os.uname()[1]
        self.__logFile = os.path.join(global_config.getPath('process_log_archive'), LOGGER_DEFAULT_FILE_NAME + '.log')
        self.__verbose = verbose
        self.__globalConfig = global_config

        self.__process = None

        self._log = logging.getLogger(logger_name)
        self._log.debug('Init Log file for workerUI: ')

        self.__workerStart = False

        if self.__verbose:
            self.__debug = 'DEBUG'
        else:
            self.__debug = 'WARNING'

        self._stopevent = threading.Event()

        plugin_file = os.path.join(self.__globalConfig.getPath('dynamic_plugins'), CONFIG_WORKER + '.py')
        with open(plugin_file, 'w') as plugin:

            # Add reference of configuration file for setting up celery tasks
            line = "downloader_configfile='" + os.path.abspath(self.__globalConfig.filePath) + "'\n"
            plugin.write(line + '\n')

            # Add queue
            for key in self.__globalConfig['ui_worker'].keys():
                if key == 'queue':
                    line = 'task_queues={' + \
                        "'" + self.__globalConfig['ui_worker.queue.name'] + "': " +\
                        "{'queue_arguments': " +\
                        "{'alias': '" + self.__globalConfig['ui_worker.queue.alias'] + "'}," + \
                        "'vhost': '" + self.__globalConfig['ui_worker.broker_url.virtual_host'] + "'}" +\
                        '}'
                    self.__queueName = self.__globalConfig['ui_worker.queue.name']
                elif key == 'broker_url':
                    line = 'broker_url='
                    line += "'amqp://"
                    line += str(self.__globalConfig['ui_worker.broker_url.user']) + ':'
                    line += str(self.__globalConfig['ui_worker.broker_url.password']) + '@'
                    line += str(self.__globalConfig['ui_worker.broker_url.host']) + ':'
                    line += str(self.__globalConfig['ui_worker.broker_url.port']) + '/'
                    line += str(self.__globalConfig['ui_worker.broker_url.virtual_host']) + "'"
                else:
                    entry = 'ui_worker.' + key
                    if isinstance(self.__globalConfig[entry], (str, bool)):
                        value = "'" + str(self.__globalConfig[entry]) + "'"
                    else:
                        value = str(self.__globalConfig[entry])
                    line = key + '=' + value
                plugin.write(line + '\n')

    def run(self):
        # self._log.info("workerUI is attached to the '%s' queue." % self.__queueName)

        command = 'celery'
        args_list = [
            # celery --config=(...).celery_config worker -l DEBUG -Q UI.hostname
            'worker',
            '--config',
            CONFIG_WORKER,
            '--workdir',
            self.__globalConfig.getPath('dynamic_plugins'),
            '-l',
            self.__debug,
            '-Q',
            self.__queueName,
            '-n',
            self.__queueName,
            '--purge'
        ]

        cmd = [command] + args_list

        self.__process = Popen(cmd, stdout=PIPE, stderr=PIPE, close_fds=False, text=True)
        pid = self.__process.pid

        if self.__verbose:
            self._log.info('[worker %s] : launch celery worker (%s)' % (self.__queueName, str(cmd)))

            # Forward output
            def forward_output(output_stream, log_level, log_type):
                while not output_stream.closed and not self._stopevent.is_set():
                    line = output_stream.readline()
                    if not line:
                        break
                    self._log.log(log_level, '>> %s(%s) %s: %s', command, self.__process.pid, log_type, line.rstrip())
                output_stream.close()

            import logging
            from threading import Thread
            a = Thread(target=forward_output, name='UI Worker stdout',
                       args=(self.__process.stdout, logging.INFO, 'stdout'))
            b = Thread(target=forward_output, name='UI Worker stderr',
                       args=(self.__process.stderr, logging.WARNING, 'stderr'))

            a.start()
            b.start()
            self.__workerStart = True
            self.__process.wait()
            self.__workerStart = False
            a.join()
            b.join()
        else:
            self._log.info('[worker %s] : launch celery worker'.format(self.__queueName))
            output = ''
            self.__workerStart = True
            while self.__process.returncode is None and not self._stopevent.is_set():
                try:
                    out, err = self.__process.communicate(timeout=1)
                    output += err + out
                except TimeoutExpired:
                    pass

            if self.__process.returncode is not None and self.__process.returncode != 0:
                self._log.error('WorkerUI logs:\n%s', output)

            self.__workerStart = False

        returncode = self.__process.returncode
        self._log.info('Terminate worker %s... (returned code %s)' % (self.__queueName, returncode))
        if returncode != 0 and not self.__verbose:
            raise Exception('>> %s pid=%s returned error code %s' % (command, pid, returncode))

        self.__process = None

    def join(self, timeout=None):
        '''
        Stop the thread
        '''
        # self._stopevent.set()
        self.__process.terminate()

        while self.__workerStart:
            self._log.info("  waiting for end of worker '%s'".format(self.__queueName))
            threading.Thread.join(self, timeout)

        self._log.info(' Not alive worker %s'.format(self.__queueName))
        # removeHandler(self._log)
