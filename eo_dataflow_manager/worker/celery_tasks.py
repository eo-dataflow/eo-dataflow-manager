import os
import shutil
import sys
from datetime import datetime
from typing import Dict, List, Tuple, Union

from celery import Celery
from lxml import etree, objectify

from eo_dataflow_manager.reloader.ErrorFilesReloader import ErrorFilesReLoader
from eo_dataflow_manager.reloader.ReLoader import ReLoader
from eo_dataflow_manager.scheduler.com.sys import DiskSpool
from eo_dataflow_manager.scheduler.sc.DownloaderDatabase import (
    STATE_DOWNLOAD_ERROR,
    STATE_TO_REDOWNLOAD,
    DownloaderDatabase,
)
from eo_dataflow_manager.scheduler.sc.GlobalConfig import GlobalConfig
from eo_dataflow_manager.synchronisation.DownloadSync import DownloadSync

TASK_READ_BUFFER_LENGTH = 50000


# TODO: check that the file resides in a non-critical directory
# Prevent overwriting on system file, etc...

app = Celery('eo_dataflow_manager.worker.celery_tasks')

# ------------------------------------------------------------------------------------------
# get eo-dataflow-manager global configuration
#    - downloader_configfile is define in worker configuration file (--config option)
globalConfig = GlobalConfig(app.conf.downloader_configfile)

DOWNLOADS_CONFIGURATION_PATH = globalConfig.getPath('downloads_config')
WORKSPACE_PATH = globalConfig['paths.workspace']


# ------------------------------------------------------------------------------------------
# Read last n bytes (TASK_READ_BUFFER_LENGTH) of the file
# ------------------------------------------------------------------------------------------
def read_all_bytes(filepath: str) -> str:
    """"""
    buffer = ''
    if os.path.isfile(filepath):
        filesize = os.lstat(filepath).st_size
        with open(filepath, 'r+') as f:
            if filesize > TASK_READ_BUFFER_LENGTH:
                f.seek(filesize - TASK_READ_BUFFER_LENGTH)
                buffer = '... '
            buffer += f.read()
    return buffer


# ------------------------------------------------------------------------------------------
@app.task
def ping() -> Tuple[bool, bool]:
    """"""
    return True, True
# ------------------------------------------------------------------------------------------


@app.task
def create_file(full_filename: str, content: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        with open(full_filename, 'w') as f:
            f.write(content)
    except Exception as error:
        return False, str(error)

    return True, ''


@app.task
def copy_file(full_filename: str, full_filepath: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        shutil.copyfile(full_filename, full_filepath)
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def rename_file(full_filename: str, full_filepath: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        os.rename(full_filename, full_filepath)
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def remove_file(full_filename: str) -> Tuple[bool, Union[bool, str]]:
    """"""

    try:
        os.remove(full_filename)
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def get_file(full_filename: str) -> Tuple[bool, str]:
    """"""
    try:
        buffer = read_all_bytes(full_filename)
    except Exception as error:
        return False, str(error)

    return True, buffer


# --------------------------------------------------------------------------------------------------------------
@app.task
def read_configuration_file(filename) -> Tuple[bool, str]:
    """"""
    full_filename = os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename)

    try:
        buffer = read_all_bytes(full_filename)
    except Exception as error:
        return False, str(error)

    return True, buffer


@app.task
def read_configuration_files_summary() -> Tuple[bool, Union[Dict[str, str], str]]:
    """"""
    summaries = {}
    try:
        download_files = DiskSpool.DiskSpool(
            DOWNLOADS_CONFIGURATION_PATH,
            r'^[^.].*\.download\.(xml|test\.xml|xml\.BAD|xml\.OFF)$'
        )
        for filename in download_files:
            full_filename = download_files[filename].getName()
            if os.path.isfile(full_filename):
                try:
                    buffer = read_all_bytes(full_filename)
                    file_data = objectify.fromstring(buffer)
                    del file_data.download_settings
                    if file_data.data_source.find('date_extraction') is not None:
                        del file_data.data_source.date_extraction
                    if file_data.data_source.find('selection') is not None:
                        del file_data.data_source.selection
                    if file_data.data_destination.find('organization') is not None:
                        del file_data.data_destination.organization
                    if len(file_data.data_destination.findall('post-processing')) > 0:
                        file_data.data_destination.findall('post-processing')[0].tag = 'post_processing'
                        del file_data.data_destination.post_processing
                    file_summary = etree.tostring(file_data, pretty_print=True)
                    summaries[filename] = (file_data.attrib['id'], file_summary)
                except Exception:
                    continue

        summaries = {name: values[1] for name, values in sorted(summaries.items(), key=lambda i: i[1][0].lower())}
    except Exception as error:
        return False, str(error)

    return True, summaries


@app.task
def get_configuration_files() -> Tuple[bool, Union[List[str], str]]:
    """"""
    filenames = []
    try:
        for filename in os.listdir(DOWNLOADS_CONFIGURATION_PATH):
            if os.path.isfile(os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename)):
                filenames.append(filename)
    except Exception as error:
        return False, str(error)

    return True, filenames


@app.task
def write_configuration_file(filename: str, content: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        with open(os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename), 'w') as f:
            f.write(content)
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def create_configuration_file(filename: str, content: str, **kwargs: dict) -> Tuple[bool, Union[bool, str]]:
    """"""
    return write_configuration_file(filename, content, **kwargs)


@app.task
def copy_configuration_file(filename: str, newfile: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        shutil.copyfile(os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename),
                        os.path.join(DOWNLOADS_CONFIGURATION_PATH, newfile))
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def rename_configuration_file(filename: str, new_filename: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        os.rename(os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename),
                  os.path.join(DOWNLOADS_CONFIGURATION_PATH, new_filename))
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def remove_configuration_file(filename: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        os.remove(os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename))
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def exists_configuration_file(filename: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        exists = os.path.isfile(os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename))
    except Exception as error:
        return False, str(error)

    return True, exists


@app.task
def exists_configuration_files(filenames: Tuple[str]) -> Tuple[bool, Union[bool, str]]:
    """"""
    exists = False
    try:
        for filename in filenames:
            exists |= os.path.isfile(os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename))
    except Exception as error:
        return False, str(error)

    return True, exists


@app.task
def exists_configuration_id(download_id: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    exists = False
    try:
        for filename in os.listdir(DOWNLOADS_CONFIGURATION_PATH):
            if filename[0:1] == '.':
                continue
            full_filename = os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename)
            if os.path.isfile(full_filename):
                buffer = read_all_bytes(full_filename)
                try:
                    file_data = objectify.fromstring(buffer)
                    if file_data.attrib['id'] == download_id:
                        exists = True
                        break
                except KeyError:
                    pass
    except Exception as error:
        return False, str(error)

    return True, exists


@app.task
def archive_download(filename: str, work_id: str, suffix: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        work_path = os.path.join(globalConfig.getPath('work'), work_id)
        if os.path.isdir(work_path):
            # move configuration file into
            shutil.move(os.path.join(DOWNLOADS_CONFIGURATION_PATH, filename), work_path)
            # rename working directory of the download
            os.rename(work_path, work_path + suffix)
        else:
            # download has never been executed, delete configuration file
            remove_configuration_file(filename)
    except Exception as error:
        return False, str(error)

    return True, True


# --------------------------------------------------------------------------------------------------------------
@app.task
def run_reload(configuration_filename: str, data: str, list_filename: str) -> Tuple[bool, str]:
    """"""
    cache_synchro_path = os.path.join(WORKSPACE_PATH, 'cache/synchrofile')
    full_filename = os.path.join(cache_synchro_path, list_filename)

    with open(full_filename, 'w') as f:
        f.write(data)

    try:
        sys.argv = ['',
                    '-c', app.conf.downloader_configfile,
                    '-d', os.path.join(DOWNLOADS_CONFIGURATION_PATH, configuration_filename),
                    '-i', full_filename]

        rl = ReLoader()
        rl.main()
        count = rl.getCountFileReloaded()
    except Exception as error:
        return False, str(error)

    return True, str(count)


# --------------------------------------------------------------------------------------------------------------
@app.task
def run_redownload(configuration_filename: str) -> Tuple[bool, str]:
    """"""
    try:
        sys.argv = ['',
                    '-c', app.conf.downloader_configfile,
                    '-d', os.path.join(DOWNLOADS_CONFIGURATION_PATH, configuration_filename)]

        rl = ErrorFilesReLoader()
        rl.main()
        count = rl.getCountFile()
    except Exception as error:
        return False, str(error)

    return True, str(count)


@app.task
def count_redownload(configuration_filename: str) -> Tuple[bool, str]:
    """"""
    try:
        sys.argv = ['',
                    '-c', app.conf.downloader_configfile,
                    '-d', os.path.join(DOWNLOADS_CONFIGURATION_PATH, configuration_filename)]

        rl = ErrorFilesReLoader()
        rl.count_files_in_error()
        count = rl.getCountFile()
    except Exception as error:
        return False, str(error)

    return True, str(count)

# --------------------------------------------------------------------------------------------------------------


@app.task
def check_status_synchro(filename: str) -> Tuple[bool, str]:
    """"""
    try:
        cache_synchro_path = os.path.join(WORKSPACE_PATH, 'cache/synchrofile')
        synchro_lock_file = os.path.join(cache_synchro_path, filename) + '.lock'

        if os.path.isfile(synchro_lock_file):
            state = 'start'
        else:
            state = 'stop'
    except Exception as error:
        return False, str(error)

    return True, state


@app.task
def run_synchro(download_id: str, configuration_filename: str, listing_filename: str,
                synchro_job_file: str, synchro_subpath: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    logger_name = 'downloadsync_' + download_id

    try:
        cache_synchro_path = os.path.join(WORKSPACE_PATH, 'cache/synchrofile')
        jobfile = os.path.join(cache_synchro_path, download_id + '.synchro.job')
        if os.path.exists(jobfile + '.lock'):
            return False, 'Synchronization is already running'
        elif os.path.exists(jobfile):
            os.remove(jobfile)
        history_path = os.path.join(globalConfig.getPath('work'), synchro_subpath)

        argv = ['',
                '-c', app.conf.downloader_configfile,
                '-d', os.path.join(DOWNLOADS_CONFIGURATION_PATH, configuration_filename),
                '-o', os.path.join(cache_synchro_path, listing_filename),
                '-j', os.path.join(cache_synchro_path, synchro_job_file),
                '--history-path', history_path]

        sys.argv = argv

        ds = DownloadSync(logger_name)
        ds.main()
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def read_synchro_state_file(filename: str) -> Tuple[bool, str]:
    """"""
    cache_synchro_path = os.path.join(WORKSPACE_PATH, 'cache/synchrofile')
    full_filename = os.path.join(cache_synchro_path, filename)
    try:
        buffer = read_all_bytes(full_filename)
    except Exception as error:
        return False, str(error)

    return True, buffer


# --------------------------------------------------------------------------------------------------------------
@app.task
def read_log_file(filename: str) -> Tuple[bool, str]:
    """"""
    full_filename = os.path.join(globalConfig.getPath('process_log_archive'), filename)
    try:
        buffer = read_all_bytes(full_filename)
    except Exception as error:
        return False, str(error)

    return True, buffer


# --------------------------------------------------------------------------------------------------------------
@app.task
def read_test_state_file(filename: str) -> Tuple[bool, str]:
    """"""

    cache_test_path = os.path.join(WORKSPACE_PATH, 'cache/jobfile')
    full_filename = os.path.join(cache_test_path, filename)
    try:
        buffer = read_all_bytes(full_filename)
    except Exception as error:
        return False, str(error)

    return True, buffer


# --------------------------------------------------------------------------------------------------------------
@app.task
def read_listing_file(full_filename: str) -> Tuple[bool, str]:
    """"""
    try:
        buffer = read_all_bytes(full_filename)
    except Exception as error:
        return False, str(error)

    return True, buffer


@app.task
def copy_listing_file(filepath: str, work_id: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    try:
        path, filename = os.path.split(filepath)
        name, file_extension = os.path.splitext(filename)
        if file_extension in ('.sync', '.synchro'):
            filename = name + '.list.sync'

            path = os.path.join(globalConfig.getPath('work'),
                                os.path.join(work_id, 'orders/listings/manual'))
            shutil.copyfile(filepath, os.path.join(path, filename))
        else:
            return False, 'The source file is not a listing'
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def write_manual_listing(filename: str, work_id: str,
                         content: str) -> Tuple[bool, Union[bool, str]]:
    """"""
    full_filename = os.path.join(globalConfig.getPath('work'),
                                 os.path.join(work_id,
                                              os.path.join('orders/listings/manual',
                                                           filename)))
    try:
        with open(full_filename, 'w') as f:
            f.write(content)

        if os.path.isfile(full_filename + '.lock'):
            os.remove(full_filename + '.lock')
    except Exception as error:
        return False, str(error)

    return True, True


@app.task
def read_manual_listing(filename: str, work_id: str) -> Tuple[bool, str]:
    """"""
    full_filename = os.path.join(globalConfig.getPath('work'),
                                 os.path.join(work_id,
                                              os.path.join('orders/listings/manual',
                                                           filename)))
    try:
        buffer = read_all_bytes(full_filename)
    except Exception as error:
        return False, str(error)

    return True, buffer


# --------------------------------------------------------------------------------------------------------------
@app.task
def reset_errors(download_id: str) -> Tuple[bool, Union[int, str]]:
    """"""
    work_id = ''.join(i if i.isalnum() else '_' for i in download_id)
    db_path = os.path.join(globalConfig.getPath('work'),
                           os.path.join(work_id, 'internal'),
                           f'downloader_{download_id}.db')
    database = None
    try:
        database = DownloaderDatabase(download_id, 'sqlite:///' + db_path)
        database.createEngine(db_must_exist=True)
        database.getSession()

        dls_error = database.getDownloads(state=STATE_DOWNLOAD_ERROR)
        date_now = datetime.utcnow()
        for dl in dls_error:
            dl.state = STATE_TO_REDOWNLOAD
            dl.last_update = date_now

        nb_errors = len(dls_error)

        if nb_errors != 0:
            database.commit()
        return True, nb_errors

    except Exception as error:
        return False, str(error)

    finally:
        if database is not None:
            database.closeSession()


# --------------------------------------------------------------------------------------------------------------
