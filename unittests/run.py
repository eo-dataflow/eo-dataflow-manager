import os
from pathlib import Path

from eo_dataflow_manager.scheduler.sc.Scheduler import Scheduler

if __name__ == "__main__":
    options = Scheduler.get_options()
    options.config = Path(os.path.dirname(__file__), "_build", "appdata", "eo-dataflow-manager.yaml")
    options.force = True
    options.verbose = True
    Scheduler().main(options)
