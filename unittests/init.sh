#!/bin/bash
set -e

# goto init.sh directory
cd "$(dirname "$0")"

#Shut off all downloads available in dataset
for FILE in dataset/downloads/*; do
  fullfilename=$(basename "$FILE")
  extension="${fullfilename##*.}"
  filename="${fullfilename%.*}"

  if [ $extension = "xml" ]
    then $(mv "$FILE" "$FILE".OFF)  && echo $filename OFF
  fi
done

# Reset _build directory
rm -rf _build
mkdir -p _build/{output,input,appdata,workspace}
mkdir _build/appdata/downloads

# Reset workspace
rm -rf workspace
mkdir workspace

# setup yaml config
cp eo-dataflow-manager.yaml.tpl _build/appdata/eo-dataflow-manager.yaml
sed -i "s:{{WORKSPACE}}:$PWD/_build/workspace/:g" _build/appdata/eo-dataflow-manager.yaml
sed -i "s:{{APPDATA}}:$PWD/_build/appdata/:g" _build/appdata/eo-dataflow-manager.yaml

echo "Initialization succeeded -> see $PWD/_build"

cd dataset/downloads/
options=($(ls *.download.xml.OFF))
cd ../..

menu() {
  echo "Available downloads:"
  # shellcheck disable=SC2068
  for i in ${!options[@]}; do
      printf "%3d%s) %s\n" $((i+1)) "${choices[i]:- }" "${options[i]}"
  done
  if [[ "$msg" ]];
  then echo "$msg"; fi
}

prompt="Check an option (again to uncheck, ENTER when done): "
menu
while read -rp "$prompt" num && [[ "$num" ]]; do
  [[ "$num" != *[![:digit:]]* ]] &&
  (( num > 0 && num <= ${#options[@]} )) ||
  { msg="Invalid option: $num"; continue; }
  ((num--)); msg="${options[num]} was ${choices[num]:+un}checked"
  [[ "${choices[num]}" ]] && choices[num]="" || choices[num]="+"
done

chosen_downloads=()
printf "You activated"; msg=" nothing"
# shellcheck disable=SC2068
for i in ${!options[@]}; do
  [[ "${choices[i]}" ]] && { printf " %s" "${options[i]}"; msg=""; chosen_downloads+=(${options[i]}); }
done
echo "$msg"
# shellcheck disable=SC2068
for FILE in ${chosen_downloads[@]}; do
  fullfilename=$(basename "$FILE")
  extension="${fullfilename##*.}"
  filename="${fullfilename%.*}"

  if [ $extension = "OFF" ]
      then cp dataset/downloads/"$FILE" _build/appdata/downloads/"$filename" && echo "download successfully Activated=> $FILE"

      INPUTPATH="$PWD/_build/input/$(basename "$FILE" .download.xml.OFF)"
      OUTPUTPATH="$PWD/_build/output/$(basename "$FILE" .download.xml.OFF)"

      mkdir "$INPUTPATH"
      mkdir "$OUTPUTPATH"
      if [ -e "dataset/output_tree/$(basename "$FILE" .download.xml.OFF)" ]
        then cp -r "dataset/output_tree/$(basename "$FILE" .download.xml.OFF)/." $OUTPUTPATH
      fi

      #Replacing {{INPUTPATH}} and {{OUTPUTPATH}} in doxnload.xml file by the real paths
      sed -i "s:{{INPUTPATH}}:$INPUTPATH:g" _build/appdata/downloads/"$filename"
      sed -i "s:{{OUTPUTPATH}}:$OUTPUTPATH:g" _build/appdata/downloads/"$filename"
  fi
done
