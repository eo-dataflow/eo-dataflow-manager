#!/bin/bash
set -ev
cd "$(dirname "$0")"

# => Test_bz2
(( $(ls -l input/Test_bz2/*.txt.bz2 | wc -l) == 4 ))
(( $(ls -l output/Test_bz2/2018/*.txt | wc -l) == 4 ))

# => Test_move
(( $(find input/Test_move/ -type f | wc -l) == 0 ))
(( $(ls -l output/Test_move/*.txt | wc -l) == 4 ))

# => Test_netcdf
(( $(find input/Test_netcdf/ -type f | wc -l) == 4 ))
(( $(ls -l output/Test_netcdf/2018*/19?/*.nc | wc -l) == 4 ))

# => Test_notify
(( $(ls -l input/Test_notify/*.txt | wc -l) == 4 ))
(( $(find output/Test_notify/ -type f | wc -l) == 0 ))

# => Test_pointer
(( $(ls -l input/Test_pointer/*.txt | wc -l) == 4 ))
(( $(ls -l output/Test_pointer/*.txt | wc -l) == 4 ))
for F in output/Test_pointer/*.txt; do
	[ -f $(readlink $F) ] # Test if symlink is valid
done

# => Test_safe TODO
(( $(find input/Test_safe/ -type f | wc -l) == 9 ))
(( $(ls -l output/Test_safe/2018/253/data-20180910/example?.txt | wc -l) == 2 ))
(( $(ls -l output/Test_safe/2018/253/data-20180910/xfdumanifest.xml | wc -l) == 1 ))
(( $(ls -l output/Test_safe/2018/254/data-20180911/* | wc -l) == 2 ))
(( $(ls -l output/Test_safe/2018/255/data-20180912/* | wc -l) == 2 ))
(( $(ls -l output/Test_safe/2018/256/data-20180913/* | wc -l) == 2 ))

# => Test_sha256
(( $(ls -l input/Test_sha256/*.txt | wc -l) == 4 ))
(( $(ls -l input/Test_sha256/*.txt.sha256 | wc -l) == 4 ))
(( $(ls -l output/Test_sha256/*.txt | wc -l) == 4 ))

# => Test_tar
(( $(ls -l input/Test_tar/*.txt.tar | wc -l) == 4 ))
(( $(ls -l output/Test_tar/2018/data-*-example.txt/*.txt | wc -l) == 4 ))

# => Test_Z
(( $(ls -l input/Test_Z/*.txt.Z | wc -l) == 4 ))
(( $(ls -l output/Test_Z/2018/*.txt | wc -l) == 4 ))


# Success !
