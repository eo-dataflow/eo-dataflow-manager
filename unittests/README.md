# Testing process:

The downloader_daemon must be installed (system-wide, in a conda environment, ...) and its executables must be available (i.e. `downloader`, `downloader-init-conf`, ...)

1. Run `./init.sh` and wait to choose downloads to be tested
2. Run downloader using `./run.sh` or `./run.py` (more useful when debugging with IDE)
3. Wait few seconds and Run `./copy-data.sh`

You can run `./downloads_on.sh` to activate all the initialized downloads, or you can run `./downloads_off.sh` to deactivate them