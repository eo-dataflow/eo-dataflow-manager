#!/bin/bash
set -euo pipefail

cd "$(dirname "$0")"

if [ -e _build/appdata/downloads/ ] &&  [ $(ls _build/appdata/downloads/ | wc -l) -ne '0' ]
then
  for FILE in _build/appdata/downloads/*; do
    fullfilename=$(basename "$FILE")
    extension="${fullfilename##*.}"
    filename="${fullfilename%.*}"

    if [ $extension = "OFF" ]
      then $(mv "$FILE" $PWD/_build/appdata/downloads/"$filename")  && echo $filename ON
    fi
  done
else
  echo "Downloads are not initialized yet. Use init.sh to initialize downloads"
fi
