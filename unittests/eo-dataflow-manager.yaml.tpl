# --- Misc config options
general:
  # --
  mainloop_timeout: 10
  # --
  max_activated_flow: 25
  # --
  max_activated_files_by_loop: 100
  # --
  # ui_worker: true

# --- Folder paths
paths:
  # --- Workspace folder path
  workspace: {{WORKSPACE}}
  # --- App data and config folder
  appdata: {{APPDATA}}

metrics:
   targets: []
   rabbitmq:
     host: localhost
     port: 5672
     ssl: false
     user: guest
     password: guest
     queue_name: dl-metrics
     virtual_host: /
# --- Administrator contact list
# admins: []

# # --- EMM Messages
emm:
  # --- List of output target for writing EMM logs. Available targets:
  # --- filesystem: Store emm log files to a directory
  # --- archive: Store emm log files inside workspace/log/messages_archive/<YEAR>/<DAY>/
  # --- elasticsearch: Push emm messages to an elasticsearch database
  # --- rabbitmq: Push emm messages to a RabbitMQ queue
   targets: []
  # --- Shared EMM logs
  # filesystem:
  #   # --- Path, either absolute or relative to the workspace
  #   path: spools/message/
  # --- Elasticsearch configuration
  # elasticsearch:
  #   hosts: [ localhost ]
  #   scheme: http
  #   user: null
  #   password: null
  #   # --- 'index' can contain date information, that is formatted according to
  #   # --- https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior
  #   index: dl-emm
  # --- RabbitMQ configuration
   rabbitmq:
     host: localhost
     port: 5672
     ssl: false
     user: guest
     password: guest
     queue_name: dl-emm
     virtual_host: /

# # --- job files config
jobs:
  # --- List of output target for writing job files. Available targets:
  # --- elasticsearch: Push emm jobs to an elasticsearch database
  # --- rabbitmq: Push emm jobs to a RabbitMQ queue
  targets: []
  # --- Elasticsearch configuration
  elasticsearch:
     hosts: []
  #   scheme: http
  #   user: null
  #   password: null
  #   --- 'index' can contain date information, that is formatted according to
  #   --- https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior
  #   index: dl-jobs
  # --- RabbitMQ configuration
  rabbitmq:
     host: localhost
     port: 5672
     ssl: false
     user: guest
     password: guest
     queue_name: dl-jobs

# #--- User interface configuration
# ui_worker:

  # --- communication with UI
  # queue name: must be unique
  # queue alias: name visible in the user interface
  # queue:
  #   name: eo-downloader-manager1
  #   alias: eo-downloader-manager1
  # broker_url:
  #   host: localhost
  #   port: 5672
  #   user: guest
  #   password: guest
  #   virtual_host: /
  # result_backend: rpc://
   # queue: #{queue_name: { queue_arguments: {alias : alias_downloader_name}}}
   #   name: UI.host.0
   #   alias: alias_downloader_name
   # broker_url: #"amqp://user:password@host:5672/vhost"
   #   host: localhost
   #   port: 5672
   #   user: null
   #   password: null
   #   virtual_host: "/"
   # broker_heartbeat: 30
   # imports: [eo-dataflow-manager.worker.celery_tasks]
   # result_backend: amqp
   # result_expires: 300
   # task_serializer: json
   # result_serializer: json
   # timezone: Europe/Paris
   # enable_utc: True
   # result_compression: bzip2
   # worker_log_color: True
   # accept_content: [json]
   # auto_delete: True
   # expires: 15.0
   # exclusive: True

# # --- Python logging configuration
# logs:
#   # --- Base logger config handling all the logs
#   # --- See https://docs.python.org/3/library/logging.config.html
#   default:
#     root:
#       handlers: [stdout]
#       level: INFO
#     handlers:
#       stdout:
#         class: logging.StreamHandler
#         formatter: stdout_fmt
#         stream: "ext://sys.stdout"
#         filters: [info_operator]
#     formatters:
#       stdout_fmt:
#         (): "ifr_lib.ifr_logging.IfrColorFormatter"
#         fmt: "%(log_color)s%(levelname).3s%(reset)s|%(message_log_color)s%(message)s%(reset)s  %(thin)s%(filename)s:%(lineno)d@%(funcName)s()%(reset)s"
#     filters:
#       # --- Filter used to suppress INFO messages not containing OPERATOR tag
#       info_operator:
#         (): eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter
#   # --- Specific config for file logs
#   # --- All handlers in this config are provided at run-time a `filename` argument, with the destination file path
#   # --- Should be compatible with python logging config
#   file_log:
#     handlers:
#       - file:
#         class: eo_dataflow_manager.ifr_lib_modules.ifr_logging.IfrFileHandler
#         formatter: file_fmt
#         # filename: ""
#         filters: [info_operator]
#     formatters:
#       file_fmt:
#           format: "%(asctime)s|%(levelname)-8s|%(process)-5d|%(message)-100s|"
#     filters:
#       # --- Filter used to suppress INFO messages not containing OPERATOR tag
#       info_operator:
#         class: eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter