#!/bin/bash
set -e

cd "$(dirname "$0")"

if [ -z "$(ls -A _build/appdata/downloads/*.download.xml)" ]; then
  echo "No activated downloads found"
else
  for FILE in _build/appdata/downloads/*.download.xml; do
    echo "Activated download detected => $FILE"

    INPUTPATH="$PWD/_build/input/$(basename "$FILE" .download.xml)"
    OUTPUTPATH="$PWD/_build/output/$(basename "$FILE" .download.xml)"

    if [ -e "$INPUTPATH" ]
      then rm -rf "$INPUTPATH"
    fi

    if [ -e "$OUTPUTPATH" ]
      then rm -rf "$OUTPUTPATH"
    fi

    echo "Copying input data in: $INPUTPATH"
    if [ -e "dataset/data/$(basename "$FILE" .download.xml)" ]
      then mkdir "$INPUTPATH" && cp -r "dataset/data/$(basename "$FILE" .download.xml)/." $INPUTPATH
    else
      echo "No input data needed"
    fi

    echo "Creating output folder: $OUTPUTPATH"
    mkdir "$OUTPUTPATH"
    if [ -e "dataset/output_tree/$(basename "$FILE" .download.xml)" ]
      then cp -r "dataset/output_tree/$(basename "$FILE" .download.xml)/." $OUTPUTPATH
    fi

    echo "Download ready"
  done
  echo "Copy of data succeeded !"
fi


