#!/bin/bash
set -e

cd "$(dirname "$0")"

# Start eo-dataflow-manager
eo-dataflow-manager -c _build/appdata/eo-dataflow-manager.yaml -f -v "$@"
