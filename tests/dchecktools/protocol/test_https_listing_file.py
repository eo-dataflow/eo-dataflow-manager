import os

import pytest

from eo_dataflow_manager.dchecktools.protocols.https_listing_file import Protocol_https_listing_file


def read_list_file(filepath):
    data = ""
    file_path = os.path.join(os.path.dirname(__file__), filepath)
    with open(file_path, 'r') as f:
        for line in f.readlines():
            data += line

    return data


@pytest.mark.parametrize("filepath, files_extracted", [
    (
            "../../data/fileextractor/tracks_2021.list",
            "[('bsh992021.210315.dat  -|-  /bsh992021.210315.dat', '1320', 'Mar 15  2021'), ('bwp012021.210315.dat  -|-  /bwp012021.210315.dat', '5679', 'Mar 15  2021'), ('bsh242021.210315.dat  -|-  /bsh242021.210315.dat', '21849', 'Mar 15  2021'), ('bsh242021.210316.dat  -|-  /bsh242021.210316.dat', '22509', 'Mar 16  2021'), ('bsh242021.210317.dat  -|-  /bsh242021.210317.dat', '22509', 'Mar 17  2021'), ('bsh942021.210317.dat  -|-  /bsh942021.210317.dat', '165', 'Mar 17  2021'), ('bsh012021.210318.dat  -|-  /bsh012021.210318.dat', '7989', 'Mar 18 18:25'), ('bsh022021.210318.dat  -|-  /bsh022021.210318.dat', '11454', 'Mar 18 18:25'), ('bsh032021.210318.dat  -|-  /bsh032021.210318.dat', '4524', 'Mar 18 18:25'), ('bsh042021.210318.dat  -|-  /bsh042021.210318.dat', '2379', 'Mar 18 18:25'), ('bsh052021.210318.dat  -|-  /bsh052021.210318.dat', '15744', 'Mar 18 18:25'), ('bsh062021.210318.dat  -|-  /bsh062021.210318.dat', '4524', 'Mar 18 18:25'), ('bsh072021.210318.dat  -|-  /bsh072021.210318.dat', '7164', 'Mar 18 18:25')]"
    )
])
def test_https_get_files_from_listing_file(filepath, files_extracted) -> None:
    protocol = Protocol_https_listing_file()
    files = protocol.extract_files_from_listing_file(read_list_file(filepath))

    assert str(files) == files_extracted
