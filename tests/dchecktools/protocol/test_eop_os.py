import json
import os

import pytest

from eo_dataflow_manager.dchecktools.protocols.https_eop_os import Protocol_https_eop_os


@pytest.fixture(scope='module')
def output_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')

@pytest.fixture(scope='module')
def workspace_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('workspace')

def read_data(filepath):
    data = ''
    file_path = os.path.join(os.path.dirname(__file__), filepath)
    with open(file_path, 'r') as f:
        data = json.loads(f.read())
    data = data['features']
    return data


@pytest.mark.parametrize('filepath, files_extracted', [
    (
            '../../data/fileextractor/eo_pos.json',
            "[('S3A_SL_2_WST____20220512T095954_20220512T100254_20220512T115518_0179_085_150_4500_MAR_O_NR_003.SEN3  -|-  https://api.eumetsat.int/data/download/collections/EO%3AEUM%3ADAT%3A0412/products/S3A_SL_2_WST____20220512T095954_20220512T100254_20220512T115518_0179_085_150_4500_MAR_O_NR_003.SEN3', 16451, '2022-05-12T12:03:10.78Z'), ('S3B_SL_2_WST____20220512T095832_20220512T100132_20220512T122851_0179_066_008_0720_MAR_O_NR_003.SEN3  -|-  https://api.eumetsat.int/data/download/collections/EO%3AEUM%3ADAT%3A0412/products/S3B_SL_2_WST____20220512T095832_20220512T100132_20220512T122851_0179_066_008_0720_MAR_O_NR_003.SEN3', 20258, '2022-05-12T12:39:58.974Z')]"
    )
])
def test_https_extract_files_from_json(filepath, files_extracted) -> None:
    protocol = Protocol_https_eop_os()
    files = protocol.extract_files_from_json(read_data(filepath))

    print(str(files))
    assert str(files) == files_extracted


@pytest.mark.parametrize('url, files_extracted, full_compare', [
    (
        # With start and end date filters
        'https://api.eumetsat.int/data/search-products/os?pi=EO:EUM:DAT:0412&dtstart=2022-05-12T10:00:00Z&dtend=2022-05-12T10:30:00Z&c=100&format=json',
        25,
        'false'
    ),
    (
        # Return no results
        'https://api.eumetsat.int/data/search-products/os?pi=EO:EUM:DAT:0412&dtstart=2000-05-12T10:00:00Z&dtend=2000-05-12T10:30:00Z&c=100&format=json',
        0,
        'false'
    ),
    (
        # With sat filter
        'https://api.eumetsat.int/data/search-products/os?pi=EO:EUM:DAT:0412&dtstart=2022-05-12T10:00:00Z&dtend=2022-05-12T10:30:00Z&sat=Sentinel-3B&c=100&format=json',
        12,
        'false'
    ),
    (
        # With type filter
        'https://api.eumetsat.int/data/search-products/os?pi=EO:EUM:DAT:0412&dtstart=2022-05-12T10:00:00Z&dtend=2022-05-12T10:30:00Z&type=SL_2_WST___&c=100&format=json',
        25,
        'false'
    ),
    (
        # With timeliness filter
        'https://api.eumetsat.int/data/search-products/os?pi=EO:EUM:DAT:0412&dtstart=2022-05-12T12:00:00Z&dtend=2022-05-12T12:30:00Z&timeliness=NR&c=100&format=json',
        22,
        'false'
    ),
    (
        # With geo filter
        'https://api.eumetsat.int/data/search-products/os?pi=EO:EUM:DAT:0412&dtstart=2022-05-12T13:30:00Z&dtend=2022-05-12T14:30:00Z&c=100&geo=POLYGON((110%2040,%20110%2045,%20116%2045,%20116%2040,%20110%2040))&format=json',
        4,
        'false'
    ),
    (
        # Compare with expected files
        'https://api.eumetsat.int/data/search-products/os?pi=EO:EUM:DAT:0412&dtstart=2022-05-12T10:00:00Z&dtend=2022-05-12T10:01:00Z&timeliness=NR&c=100&format=json',
        "[('S3A_SL_2_WST____20220512T095954_20220512T100254_20220512T115518_0179_085_150_4500_MAR_O_NR_003.SEN3  -|-  https://api.eumetsat.int/data/download/collections/EO%3AEUM%3ADAT%3A0412/products/S3A_SL_2_WST____20220512T095954_20220512T100254_20220512T115518_0179_085_150_4500_MAR_O_NR_003.SEN3', 16451, '2022-05-12T12:03:10.78Z'), ('S3B_SL_2_WST____20220512T095832_20220512T100132_20220512T122851_0179_066_008_0720_MAR_O_NR_003.SEN3  -|-  https://api.eumetsat.int/data/download/collections/EO%3AEUM%3ADAT%3A0412/products/S3B_SL_2_WST____20220512T095832_20220512T100132_20220512T122851_0179_066_008_0720_MAR_O_NR_003.SEN3', 20258, '2022-05-12T12:39:58.974Z')]",
        'true'
    )
])
def test_https_get_files_from_json(url, files_extracted, full_compare) -> None:
    protocol = Protocol_https_eop_os()
    files = protocol.https_get_files_from_eop_os_json(url)

    print(str(files))

    if full_compare == 'true':
        assert str(files) == files_extracted
    else:
        assert(len(files) == files_extracted)


# Credentials to initialize running unit tests test_get_file and test_get_file_error
# Following tests are commented but are useful for testing a single file download. They need valid credentials
# CREDENTIALS = ('******','******')
#
# @pytest.fixture(scope='module')
# def credentials():
#     return CREDENTIALS
#
#
# @pytest.fixture(scope='module')
# def get_api_key(credentials):
#
#     # API endpoint
#     apis_endpoint = "https://api.eumetsat.int"
#     access_token = AccessToken(credentials, apis_endpoint)
#     return access_token
#
#
# @pytest.mark.parametrize('root_products,product_url,expected_size', [
#     (
#         'https://api.eumetsat.int/data/download/collections/EO%3AEUM%3ADAT%3A0412/products/',
#         'S3A_SL_2_WST____20220505T114811_20220505T115111_20220505T125854_0179_085_052_0000_MAR_O_NR_003.SEN3',
#         24329526
#     )
# ])
# def test_get_file(output_dir, workspace_dir, get_api_key, root_products, product_url, expected_size):
#
#     api_token = get_api_key
#     global_config = GlobalConfig()
#     job_path = workspace_dir / Path('cache/providerfile/')
#
#     provider = EumdacProvider(id='https_eop_os_-8273745509849206239', type='Https_eop_os',
#                               job_path=str(job_path),
#                               globalConfig=global_config)
#     localpath = Path(output_dir) / product_url
#     res = provider.getFile(session=api_token, remotepath=root_products+product_url, localpath=str(localpath))
#
#     assert res.size == expected_size
#
#
# @pytest.mark.parametrize('root_products,product_url', [
#     (
#             'https://api.eumetsat.int/data/download/collections/EO%3AEUM%3ADAT%3A0412/products/',
#             'S3A_SL_2_WST____20220505T114811_20220505T115111_20220505T125854_0179_085_052_0000_MAR_O_NR_003_.SEN3',
#     )
# ])
# def test_get_file_error(output_dir, get_api_key, workspace_dir, root_products, product_url):
#     api_token = get_api_key
#     global_config = GlobalConfig()
#     job_path = workspace_dir / Path('cache/providerfile/')
#     provider = EumdacProvider(id='https_eop_os_-8273745509849206239', type='Https_eop_os',
#                               job_path=job_path,
#                               globalConfig=global_config)
#     localpath = Path(output_dir) / product_url
#     res = provider.getFile(session=api_token, remotepath=root_products + product_url, localpath=str(localpath))
#     assert res is None
