import json
import os

import pytest

from eo_dataflow_manager.dchecktools.protocols.https_xx_daac import Protocol_https_xx_daac


def read_data(filepath):
    data = ""
    file_path = os.path.join(os.path.dirname(__file__), filepath)
    with open(file_path, 'r') as f:
        data = json.loads(f.read())

    return data


@pytest.mark.parametrize("filepath, files_extracted", [
    (
            "../../data/fileextractor/Pocloud.json",
            "[('Asia_Ganges1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/Asia_Ganges1kmdaily.nc', 454477448, '2021-08-18T21:14:36.638Z'), ('South_America_Amazon1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/South_America_Amazon1kmdaily.nc', 1505425598, '2021-08-18T21:15:09.738Z'), ('North_America_Mississippi1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/North_America_Mississippi1kmdaily.nc', 1253847611, '2021-08-18T21:16:09.900Z'), ('Asia_Yangtze1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/Asia_Yangtze1kmdaily.nc', 1074253159, '2021-08-18T21:14:24.594Z'), ('Asia_Ayeyarwada1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/Asia_Ayeyarwada1kmdaily.nc', 486445974, '2021-08-18T21:13:59.323Z'), ('North_America_StLawrence1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/North_America_StLawrence1kmdaily.nc', 201372772, '2021-08-18T21:13:42.975Z'), ('South_America_Orinoco1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/South_America_Orinoco1kmdaily.nc', 587572047, '2021-08-18T21:14:09.049Z'), ('South_America_Parana1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/South_America_Parana1kmdaily.nc', 1030460068, '2021-08-18T21:15:02.952Z'), ('Asia_Brahmaputra1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/Asia_Brahmaputra1kmdaily.nc', 379027485, '2021-08-18T21:14:33.451Z'), ('Asia_Indus1kmdaily.nc  -|-  https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-protected/PRESWOT_HYDRO_GRRATS_L2_DAILY_VIRTUAL_STATION_HEIGHTS_V2/Asia_Indus1kmdaily.nc', 116136023, '2021-08-18T21:14:26.958Z')]"
    )
])
def test_https_get_files_from_json(filepath, files_extracted) -> None:
    protocol = Protocol_https_xx_daac()
    files = protocol.extract_files_from_json(read_data(filepath)['items'])

    assert str(files) == files_extracted
