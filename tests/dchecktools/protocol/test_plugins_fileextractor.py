import os

import pytest

from eo_dataflow_manager.dchecktools.protocols.file_extractor import (
    FileExtractor_Nomads,
    FileExtractor_UniBremen,
    FileExtractor_UniHamburg,
)
from eo_dataflow_manager.dchecktools.protocols.https_directorylist import FileExtractor


def read_html_file(filepath):
    html = ''
    file_path = os.path.join(os.path.dirname(__file__), filepath)
    with open(file_path, 'r') as f:
        for line in f.readlines():
            html += line.rstrip('\n')

    return html


def assert_extractor(extractor, filepath, files_extracted) -> None:
    extractor.feed(read_html_file(filepath))
    files = extractor.get_files()
    assert str(files) == files_extracted


@pytest.mark.parametrize('extractor, filepath, files_extracted', [
    (
            FileExtractor(),
            '../../data/fileextractor/fileextractor1.html',
            "{'folder_1': (1, '29-Mar-2020 23:00', '0'), 'file_1.dat': (0, '29-Mar-2020 23:50', '12.4M'), 'file_2.dat': (0, '30-Mar-2020 04:52', '2.4K'), 'file_3.dat': (0, '30-Mar-2020 04:52', '253')}"
    )
])
def test_FileExtractor(extractor, filepath, files_extracted) -> None:
    assert_extractor(extractor, filepath, files_extracted)


@pytest.mark.parametrize('extractor, filepath, files_extracted', [
    (
            FileExtractor_UniBremen(),
            '../../data/fileextractor/unibremen1.html',
            "{'folder_1': (1, '2020-03-30 05:35', '0'), 'file_1.hdf': (0, '2020-03-02 04:51', '1.0M'), 'file_2.tif': (0, '2020-03-02 05:32', '242K')}"
    )
])
def test_UniBremen(extractor, filepath, files_extracted) -> None:
    assert_extractor(extractor, filepath, files_extracted)


@pytest.mark.parametrize('extractor, filepath, files_extracted', [
    (
            FileExtractor_UniHamburg(),
            '../../data/fileextractor/unihamburg1.html',
            "{'folder_1/': (1, '', '0'), 'file_1.nc': (0, '2020-02-01T18:39:22Z', '22.34 Mbytes'), 'file_2.nc': (0, '2020-02-01T00:04:28Z', '349.4 Kbytes')}"
    )
])
def test_UniHamburg(extractor, filepath, files_extracted) -> None:
    assert_extractor(extractor, filepath, files_extracted)


@pytest.mark.parametrize('extractor, filepath, files_extracted', [
    (
            FileExtractor_Nomads(),
            '../../data/fileextractor/nomads1.html',
            "{'folder_1': (1, '08-Apr-2019 19:33', '0'), 'file_1.hdf': (0, '05-Mar-2020 18:43', '1.5G'), 'file_2.hdf.gz': (0, '05-Mar-2020 18:43', '65M')}"
    )
])
def test_Nomads(extractor, filepath, files_extracted) -> None:
    assert_extractor(extractor, filepath, files_extracted)
