import os
from pathlib import Path
from typing import Dict, List

import pytest

from eo_dataflow_manager.scheduler.plugins.DataReader.DirRegexpDataReader import DirRegexpDataReader
from eo_dataflow_manager.scheduler.plugins.DataReader.GribDataReader import GribDataReader
from eo_dataflow_manager.scheduler.plugins.DataReader.NetCDFDataReader import NetCDFDataReader
from eo_dataflow_manager.scheduler.plugins.DataReader.RegexpDataReader import RegexpDataReader

FIXTURE_DIR = Path(__file__).parent.resolve()


def assert_reader(setup_reader_n_data: List[Dict]) -> None:
    for i in range(len(setup_reader_n_data)):
        set_of_test = setup_reader_n_data[i]
        reader = set_of_test['reader']
        filepath = os.path.join(FIXTURE_DIR, set_of_test['filepath'])
        assert str(reader.getDate(filepath)) == set_of_test['getDate']
        assert reader.getStorageName(filepath) == set_of_test['getStorageName']
        assert reader.getRelativePath(filepath) == set_of_test['getRelativePath']


@pytest.mark.parametrize('setup_reader_n_data', [
    [
        {
            'filepath': 'data/regexp_20190625T183259_x.txt',
            'reader': RegexpDataReader('RegexpDataReader', 'regexp_([0-9]{8}T[0-9]{6}).*', '%Y%m%dT%H%M%S', '%Y/%j'),
            'getDate': '2019-06-25 18:32:59',
            'getStorageName': 'regexp_20190625T183259_x.txt',
            'getRelativePath': '2019/176/regexp_20190625T183259_x.txt',
        }
    ]
])
def test_regexp(setup_reader_n_data) -> None:
    assert_reader(setup_reader_n_data)


@pytest.mark.parametrize('setup_reader_n_data', [
    [
        {
            'filepath': 'data/dirregexp_20190625T183259_x/file.txt',
            'reader': DirRegexpDataReader('DirRegexpDataReader', '.*/dirregexp_([0-9]{8}T[0-9]{6}).*',
                                          '%Y%m%dT%H%M%S', '%Y/%j'),
            'getDate': '2019-06-25 18:32:59',
            'getStorageName': 'file.txt',
            'getRelativePath': '2019/176/file.txt',
        },
        {
            'filepath': 'data/dirregexp_20190625T183259_x/file.txt',
            'reader': DirRegexpDataReader('DirRegexpDataReader', '.*/dirregexp_([0-9]{8}T[0-9]{6}).*',
                                          '%Y%m%dT%H%M%S', '%Y/%j'),
            'getDate': '2019-06-25 18:32:59',
            'getStorageName': 'file.txt',
            'getRelativePath': '2019/176/file.txt',
        }
    ]
])
def test_dir_regexp(setup_reader_n_data) -> None:
    assert_reader(setup_reader_n_data)


@pytest.mark.parametrize('setup_reader_n_data', [
    [
        {
            'filepath': 'data/datareader/netcdf_data1.nc',
            'reader': NetCDFDataReader('NetCDFDataReader', 'first_meas_time', '%Y-%m-%d %H:%M:%S.%f', '%Y/%j'),
            'getDate': '2018-07-09 23:51:39.721869',
            'getStorageName': 'netcdf_data1.nc',
            'getRelativePath': '2018/190/netcdf_data1.nc',
        },
        {
            'filepath': 'data/datareader/netcdf_data2.nc',
            'reader': NetCDFDataReader('NetCDFDataReader', 'first_meas_time', '%Y-%m-%d %H:%M:%S.%f', '%Y/%j'),
            'getDate': 'None',
            'getStorageName': 'netcdf_data2.nc',
            'getRelativePath': os.path.join(FIXTURE_DIR, 'data/datareader/netcdf_data2.nc').lstrip('/'),
        }
    ]
])
def test_netcdf(setup_reader_n_data) -> None:
    assert_reader(setup_reader_n_data)


@pytest.mark.parametrize('setup_reader_n_data', [
    [
        {
            'filepath': 'data/datareader/grib_data1',
            'reader': GribDataReader('GribDataReader', 'first_meas_time', '%Y-%m-%d %H:%M:%S.%f', '%Y/%j'),
            'getDate': '2020-02-26 12:00:00',
            'getStorageName': 'grib_data1',
            'getRelativePath': '2020/057/grib_data1',
        },
        {
            'filepath': 'data/datareader/grib_data2',
            'reader': GribDataReader('GribDataReader', 'first_meas_time', '%Y-%m-%d %H:%M:%S.%f', '%Y/%j'),
            'getDate': '2016-04-09 21:00:00',
            'getStorageName': 'grib_data2',
            'getRelativePath': '2016/100/grib_data2',
        },
        {
            'filepath': 'data/datareader/grib_data3',
            'reader': GribDataReader('GribDataReader', '', '', '%Y/%j'),
            'getDate': 'None',
            'getStorageName': 'grib_data3',
            'getRelativePath': os.path.join(FIXTURE_DIR, 'data/datareader/grib_data3').lstrip('/'),
        }
    ]
])
def test_grib(setup_reader_n_data) -> None:
    assert_reader(setup_reader_n_data)
