import os
import shutil
import tempfile
from pathlib import Path

from eo_dataflow_manager.scheduler.sc.Scheduler import Scheduler

DEFAULT_TMP_DIR = Path(tempfile.gettempdir(), "eo-dataflow-manager")


def initialize_environment(
    app_data_path: Path = Path(DEFAULT_TMP_DIR, "appdata"),
    workspace_path: Path = Path(DEFAULT_TMP_DIR, "workspace")
):
    """Initialize environment"""

    # application data
    if os.path.isdir(app_data_path):
        shutil.rmtree(app_data_path)
    os.makedirs(app_data_path)

    # application workspace
    if os.path.isdir(workspace_path):
        shutil.rmtree(workspace_path)
    os.makedirs(workspace_path)


if __name__ == "__main__":
    initialize_environment()


