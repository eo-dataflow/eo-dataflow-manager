import sys

sys.dont_write_bytecode = True


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "test_name: mark test to run"
    )
