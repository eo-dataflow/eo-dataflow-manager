# install conda-lock

```commandline
conda install -c conda-forge conda-lock
```

# generate lock files

```commandline
conda lock --kind explicit -f run-env.yaml -p linux-64  --filename-template 'conda-run-{platform}.lock'
conda lock --kind explicit -f run-env.yaml -f dev-env.yaml -p linux-64  --filename-template 'conda-dev-{platform}.lock'
```
